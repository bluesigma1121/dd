<?php

App::uses('AppModel', 'Model');

/**
 * Setting Model
 *
 */
class Setting extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
//	public $validate = array(
//		'dollar_rate' => array(
//			'numeric' => array(
//				'rule' => array('numeric'),
//				//'message' => 'Your custom message here',
//				//'allowEmpty' => false,
//				//'required' => false,
//				//'last' => false, // Stop validation after this rule
//				//'on' => 'create', // Limit validation to 'create' or 'update' operations
//			),
//		),
//	);


    public function find_setting($id = null) {
        $setting = array();
        $setting = $this->find('first', array('conditions' => array('Setting.id' => $id), 'recursive' => -1));
        return $setting;
    }

}
