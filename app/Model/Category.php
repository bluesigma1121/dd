<?php

App::uses('AppModel', 'Model');

/**
 * Category Model
 *
 * @property Enquiry $Enquiry
 * @property Specification $Specification
 */
class Category extends AppModel {

    public $actsAs = array(
        'Sitemap.Sitemap' => array(
            'primaryKey' => 'id', //Default primary key field
            'loc' => 'buildUrlCategories', //Default function called that builds a url, passes parameters (Model $Model, $primaryKey)
            'lastmod' => 'modified', //Default last modified field, can be set to FALSE if no field for this
            'changefreq' => 'weekly', //Default change frequency applied to all model items of this type, can be set to FALSE to pass no value
            'priority' => '0.9', //Default priority applied to all model items of this type, can be set to FALSE to pass no value
            'conditions' => array('Category.is_active' => 1), //Conditions to limit or control the returned results for the sitemap
        )
    );

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'category_name' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'level' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * hasMany associations
     *
     * @var array
     */
    public $belongsTo = array(
        'ParentCategory' => array(
            'className' => 'Category',
            'foreignKey' => 'parent_category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $hasMany = array(
        'Enquiry' => array(
            'className' => 'Enquiry',
            'foreignKey' => 'category_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => ''
        ),
    );

    public function fun_add_level_one($data) {
        $data['Category']['category_name'] = ucwords($data['Category']['category_name']);
        $cat_name = $this->find('first', array('conditions' => array('Category.category_name' => $data['Category']['category_name']), 'fields' => array('Category.category_name'), 'recursive' => -1));
        if (empty($cat_name)) {
            $data['Category']['level'] = '1';
            $data['Category']['is_active'] = 0;
            $data['Category']['parent_category_id'] = 0;
            $this->create();
            if ($this->save($data)) {
                return 1;
            } else {
                return 0;
            }
        } else {
            return 2;
        }
    }

    public function fun_add_level_two($data) {
        $f = 0; //return result flag
        $cat['Category']['parent_category_id'] = $data['Category']['parent_category_id1'];
        foreach ($data['option_name'] as $c) {
            $cat['Category']['level'] = '2';
            $cat['Category']['category_name'] = ucwords($c['category_name']);
            $cat['Category']['long_desc'] = $c['long_desc'];
            $cat['Category']['short_desc'] = $c['short_desc'];
            $cat['Category']['is_active'] = 2;
            $this->create();
            if ($this->save($cat)) {
                $f = 1;
            } else {
                $f = 0;
            }
        }
        return $f;
    }

    public function fun_add_level_three($data) {
        $f = 0; //return result flag
        $cat['Category']['parent_category_id'] = $data['Category']['parent_category_id1'];
        foreach ($data['option_name'] as $c) {
            $this->create();
            $cat['Category']['level'] = '3';
            $cat['Category']['category_name'] = ucwords($c['category_name']);
            $cat['Category']['long_desc'] = $c['long_desc'];
            $cat['Category']['short_desc'] = $c['short_desc'];
            $cat['Category']['is_active'] = 0;
            $this->create();
            if ($this->save($cat)) {
                $f = 1;
            } else {
                $f = 0;
            }
        }
        return $f;
    }

    public function fun_add_level_four($data) {
        $f = 0; //return result flag
        $cat['Category']['parent_category_id'] = $data['Category']['parent_category_id1'];
        foreach ($data['option_name'] as $c) {
            $this->create();
            $cat['Category']['level'] = '4';
            $cat['Category']['category_name'] = ucwords($c['category_name']);
            $cat['Category']['long_desc'] = $c['long_desc'];
            $cat['Category']['short_desc'] = $c['short_desc'];
            $cat['Category']['is_active'] = 0;
            $this->create();
            if ($this->save($cat)) {
                $f = 1;
            } else {
                $f = 0;
            }
        }
        return $f;
    }

    public function fun_add_level_five($data) {
        $f = 0; //return result flag
        $cat['Category']['parent_category_id'] = $data['Category']['parent_category_id1'];
        foreach ($data['option_name'] as $c) {
            $this->create();
            $cat['Category']['level'] = '5';
            $cat['Category']['category_name'] = ucwords($c['category_name']);
            $cat['Category']['long_desc'] = $c['long_desc'];
            $cat['Category']['short_desc'] = $c['short_desc'];
            $cat['Category']['is_active'] = 0;
            $this->create();
            if ($this->save($cat)) {
                $f = 1;
            } else {
                $f = 0;
            }
        }
        return $f;
    }

    public function fun_add_level_six($data) {
        $f = 0; //return result flag
        $cat['Category']['parent_category_id'] = $data['Category']['parent_category_id1'];
        foreach ($data['option_name'] as $c) {
            $this->create();
            $cat['Category']['level'] = '6';
            $cat['Category']['category_name'] = ucwords($c['category_name']);
            $cat['Category']['long_desc'] = $c['long_desc'];
            $cat['Category']['short_desc'] = $c['short_desc'];
            $cat['Category']['is_active'] = 0;
            $this->create();
            if ($this->save($cat)) {
                $f = 1;
            } else {
                $f = 0;
            }
        }
        return $f;
    }

    public function fun_edit_level_one($data) {
        $f = 0; //return result flag
        $data['Category']['category_name'] = ucwords($data['Category']['category_name']);

        if ($this->save($data)) {
            $f = 1;
        } else {
            $f = 0;
        }
        return $f;
    }

    public function fun_edit_level_two($data) {
        $f = 0; //return result flag
        $data['Category']['category_name'] = ucwords($data['Category']['category_name']);
        if ($this->save($data)) {
            $f = 1;
        } else {
            $f = 0;
        }
        return $f;
    }

    function limit_text($text, $limit) {
        if (str_word_count($text, 0) > $limit) {
            $words = str_word_count($text, 2);
            $pos = array_keys($words);
            $text = substr($text, 0, $pos[$limit]) . '...';
        }
        return $text;
    }

    public function recent_view_function($cook_data) {
        App::uses('Product', 'Model');
        $this->Product = new Product();
        $make_array = array();
        $result = array_unique($cook_data);
        $result = array_reverse($result);
        $recent_view = $this->Product->find('all', array(
            'conditions' => array('Product.id' => $result),
            'limit' => 5,
            'recursive' => 0,
            'is_active' => 1,
            'fields' => array('Product.id', 'Product.product_name','Product.alias', 'Product.slug', 'Product.category_id', 'Category.category_name', 'Category.slug'),
        ));

//        foreach ($recent_view as $key => $final_view) {
//            $recent_view[$key]['Product']['cat_name'] = $result_ids[$recent_view[$key]['Product']['id']]['cat_id'];
//        }

        return $recent_view;
    }

    public function bread_cum_function($id = null) {
        $level4 = $this->find('first', array(
            'fields' => array(
                'Category.id', 'Category.category_name',
                'Category.level', 'Category.parent_category_id'),
            'conditions' => array('Category.id' => $id), 'recursive' => -1));

        if (!empty($level4)) {

            $bredcum_array[$level4['Category']['level']] = $level4;
            if ($level4['Category']['parent_category_id'] != 0) {
                $level3 = $this->find('first', array(
                    'fields' => array(
                        'Category.id', 'Category.category_name',
                        'Category.level', 'Category.parent_category_id'),
                    'conditions' => array('Category.id' => $level4['Category']['parent_category_id']), 'recursive' => -1));
                $bredcum_array[$level3['Category']['level']] = $level3;
                if ($level3['Category']['parent_category_id'] != 0) {
                    $level2 = $this->find('first', array(
                        'fields' => array(
                            'Category.id', 'Category.category_name',
                            'Category.level', 'Category.parent_category_id'),
                        'conditions' => array('Category.id' => $level3['Category']['parent_category_id']), 'recursive' => -1));
                    $bredcum_array[$level2['Category']['level']] = $level2;
                    if ($level2['Category']['parent_category_id'] != 0) {
                        $level1 = $this->find('first', array(
                            'fields' => array(
                                'Category.id', 'Category.category_name',
                                'Category.level', 'Category.parent_category_id'),
                            'conditions' => array('Category.id' => $level2['Category']['parent_category_id']), 'recursive' => -1));
                        $bredcum_array[$level1['Category']['level']] = $level1;
                        if ($level1['Category']['parent_category_id'] != 0) {
                            $level0 = $this->find('first', array(
                                'fields' => array(
                                    'Category.id', 'Category.category_name',
                                    'Category.level', 'Category.parent_category_id'),
                                'conditions' => array('Category.id' => $level1['Category']['parent_category_id']), 'recursive' => -1));
                            $bredcum_array[$level0['Category']['level']] = $level0;
                            if ($level0['Category']['parent_category_id'] != 0) {
                                $levelm1 = $this->find('first', array(
                                    'fields' => array(
                                        'Category.id', 'Category.category_name',
                                        'Category.level', 'Category.parent_category_id'),
                                    'conditions' => array('Category.id' => $level0['Category']['parent_category_id']), 'recursive' => -1));
                                $bredcum_array[$levelm1['Category']['level']] = $levelm1;
                                if ($levelm1['Category']['parent_category_id'] != 0) {
                                    $levelm2 = $this->find('first', array(
                                        'fields' => array(
                                            'Category.id', 'Category.category_name',
                                            'Category.level', 'Category.parent_category_id'),
                                        'conditions' => array('Category.id' => $levelm1['Category']['parent_category_id']), 'recursive' => -1));
                                    $bredcum_array[$levelm2['Category']['level']] = $levelm2;
                                }
                            }
                        }
                    }
                }
            }

            ksort($bredcum_array);
            return $bredcum_array;
        }
    }

    public function tree_view_cat_function($id = null) {
        $is_active = '';
        if (SessionComponent::check('show_active')) {
            $is_active = SessionComponent::read('show_active');
        }
        if (!empty($id)) {
            if ($is_active) {
                $conditions = array('Category.id' => $id, 'Category.is_active' => 1);
            } else {
                $conditions = array('Category.id' => $id);
            }
            $main_cat = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
        } else {
            if ($is_active) {
                $conditions = array('Category.parent_category_id' => 0, 'Category.is_active' => 1);
            } else {
                $conditions = array('Category.parent_category_id' => 0);
            }
            $main_cat = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
        }

        $cat_data = array();
        foreach ($main_cat as $key1 => $cat) {
            $cat_data['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
            $cat_data['Level1'][$key1]['id'] = $cat['Category']['id'];
            $cat_data['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $cat_data['Level1'][$key1]['is_active'] = $cat['Category']['is_active'];
            $cat_data['Level1'][$key1]['no_of_child'] = $cat['Category']['no_of_child'];

            if ($is_active) {
                $conditions = array('Category.parent_category_id' => $cat['Category']['id'], 'Category.is_active' => 1);
            } else {
                $conditions = array('Category.parent_category_id' => $cat['Category']['id']);
            }

            $level_two = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $cat_data['Level1'][$key1]['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $cat_data['Level1'][$key1]['Level2'][$key2]['id'] = $level2['Category']['id'];
                $cat_data['Level1'][$key1]['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $cat_data['Level1'][$key1]['Level2'][$key2]['is_active'] = $level2['Category']['is_active'];
                $cat_data['Level1'][$key1]['Level2'][$key2]['no_of_child'] = $level2['Category']['no_of_child'];

                if ($is_active) {
                    $conditions = array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1);
                } else {
                    $conditions = array('Category.parent_category_id' => $level2['Category']['id']);
                }

                $level_three = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['is_active'] = $level3['Category']['is_active'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['no_of_child'] = $level3['Category']['no_of_child'];

                    if ($is_active) {
                        $conditions = array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1);
                    } else {
                        $conditions = array('Category.parent_category_id' => $level3['Category']['id']);
                    }

                    $level_four = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['is_active'] = $level4['Category']['is_active'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_child'] = $level4['Category']['no_of_child'];

                        if ($is_active) {
                            $conditions = array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1);
                        } else {
                            $conditions = array('Category.parent_category_id' => $level4['Category']['id']);
                        }

                        $level_five = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['is_active'] = $level5['Category']['is_active'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_child'] = $level5['Category']['no_of_child'];

                            if ($is_active) {
                                $conditions = array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1);
                            } else {
                                $conditions = array('Category.parent_category_id' => $level5['Category']['id']);
                            }

                            $level_six = $this->find('all', array('conditions' => $conditions, 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['is_active'] = $level6['Category']['is_active'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_child'] = $level6['Category']['no_of_child'];
                            }
                        }
                    }
                }
            }
        }

        return $cat_data;
    }

    public function our_client_fun() {
        App::uses('OurClient', 'Model');
        $this->OurClient = new OurClient();
        $ourclients = $this->OurClient->find('all', array(
            'conditions' => array('OurClient.is_active' => 1),
            'limit' => 12,
            'order' => 'rand()',
            'recursive' => -1,
            'fields' => array('OurClient.id', 'OurClient.client_name', 'OurClient.logo', 'OurClient.link'),
        ));
        return $ourclients;
    }

    public function cleanString($string) {
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $string = preg_replace('/-+/', '-', $string);
        return strtolower($string);
    }

    public function find_sub_cat_ids($cat_id) {

        $cat_ids = array();
        array_push($cat_ids, $cat_id);
        $main_cat = $this->find('all', array('conditions' => array('Category.parent_category_id' => $cat_id), 'recursive' => -1));

        if (!empty($main_cat)) {
            foreach ($main_cat as $key1 => $cat) {
                array_push($cat_ids, $cat['Category']['id']);
                $level_two = $this->find('all', array('conditions' => array('Category.parent_category_id' => $cat['Category']['id']), 'recursive' => -1));

                if (!empty($level_two)) {
                    foreach ($level_two as $key2 => $level2) {
                        array_push($cat_ids, $level2['Category']['id']);
                        $level_three = $this->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id']), 'recursive' => -1));

                        if (!empty($level_three)) {
                            foreach ($level_three as $key3 => $level3) {
                                array_push($cat_ids, $level3['Category']['id']);
                                $level_four = $this->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id']), 'recursive' => -1));

                                if (!empty($level_four)) {
                                    foreach ($level_four as $key4 => $level4) {
                                        array_push($cat_ids, $level4['Category']['id']);
                                        $level_five = $this->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id']), 'recursive' => -1));


                                        if (!empty($level_five)) {
                                            foreach ($level_five as $key5 => $level5) {
                                                array_push($cat_ids, $level5['Category']['id']);
                                                $level_six = $this->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id']), 'recursive' => -1));

                                                if (!empty($level_six)) {
                                                    foreach ($level_six as $key6 => $level6) {
                                                        array_push($cat_ids, $level6['Category']['id']);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $cat_ids;
    }

    public function deactive_child($id) {
        if ($this->updateAll(array('Category.is_active' => 0), array('Category.id' => $id))) {
            return 1;
        } else {
            return 0;
        }
    }

    public function active_child($id) {
        if ($this->updateAll(array('Category.is_active' => 1), array('Category.id' => $id))) {
            return 1;
        } else {
            return 0;
        }
    }

    public function update_view_counter($id) {
        $prev_data = CookieComponent::read('cat_view');
        if (!empty($prev_data)) {
            if (!in_array($id, $prev_data)) {
                $recent_view_array = CookieComponent::read('cat_view');
                array_push($prev_data, $id);
                CookieComponent::write('cat_view', $prev_data, false, '+2 weeks');
                $counter_data = $this->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.no_of_view'), 'recursive' => -1));
                $cnt = $counter_data['Category']['no_of_view'] + 1;
                $this->updateAll(array('Category.no_of_view' => $cnt), array('Category.id' => $id));
            }
        } else {
            $recent_view_array = array();
            array_push($recent_view_array, $id);
            CookieComponent::write('cat_view', $recent_view_array, false, '+2 weeks');
            $counter_data = $this->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.no_of_view'), 'recursive' => -1));
            $cnt = $counter_data['Category']['no_of_view'] + 1;
            $this->updateAll(array('Category.no_of_view' => $cnt), array('Category.id' => $id));
        }
        return 1;
    }

    public function main_cat_for_email() {
        $category = $this->find('all', array(
            'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1),
            'recursive' => -1
        ));
        return $category;
    }

    public function update_details($id) {
        $cat = $this->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));
        $update_cat['Category']['cat_breadcrub'] = $cat['Category']['category_name'];
        $update_cat['Category']['id'] = $id;
        if (empty($cat['Category']['page_title'])) {
            if (!empty($cat['Category']['category_name'])) {
                $update_cat['Category']['page_title'] = $cat['Category']['category_name'];
            }
        }

        if (empty($cat['Category']['page_desc'])) {
            if (!empty($cat['Category']['short_desc'])) {
                $temp_str = substr(strip_tags($cat['Category']['short_desc']), 0, 160);
                $last = strrpos($temp_str, '.');
                $update_cat['Category']['page_desc'] = substr($temp_str, 0, $last + 1);
            }
        }
        $update_cat['Category']['cat_slug'] = $this->cleanString($cat['Category']['category_name']);

        $this->save($update_cat);
        return 1;
    }

    public function check_cat_url($id, $slug) {
        $category = $this->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.id', 'Category.category_name'), 'recursive' => 0));
        $cat_slug = $this->cleanString($category['Category']['category_name']);
        if (($id == $category['Category']['id']) && ($slug == $cat_slug)) {
            return 1;
        } else {

            return 0;
        }
    }

    public function upload_category_excel($data) {
        Configure::load('idata');
// $months = Configure::read('idata.months');

        $import_file = $data['Category']['category_upload'];
        $sub = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz23456789', 5)), 0, 5);
        if ($import_file['error'] == UPLOAD_ERR_OK) {
            $destination_file = APP . WEBROOT_DIR . DS . 'files' . DS . 'uploads' . DS . $sub . $import_file['name'];

            if (move_uploaded_file($import_file['tmp_name'], $destination_file)) {
                chmod($destination_file, 0777);
                $reader = new Spreadsheet_Excel_Reader();
                $reader->read($destination_file, true);
                $records_success = array();
                $err_arr = array();
                $err_sts_flash = 0;
                set_time_limit(600);
                $err = array();


                if (($reader->sheets[0]['numRows']) <= 100) {

                    for ($i = 2; $i <= $reader->sheets[0]['numRows']; $i++) {
                        $j = 1;
                        $category_name = @$reader->sheets[0]['cells'][$i][$j++];
                        $records_success[$i]['category_name'] = $category_name;
                        $records_success[$i]['cat_breadcrub'] = strtolower($category_name);
                        $records_success[$i]['slug'] = $this->cleanString($category_name);
                        $records_success[$i]['short_desc'] = @$reader->sheets[0]['cells'][$i][$j++];
                        $records_success[$i]['page_title'] = @$reader->sheets[0]['cells'][$i][$j++];
                        $records_success[$i]['page_desc'] = @$reader->sheets[0]['cells'][$i][$j++];

                        if (empty($records_success[$i]['page_desc'])) {
                            if (!empty($records_success[$i]['short_desc'])) {
                                $temp_str = substr(strip_tags($records_success[$i]['short_desc']), 0, 160);
                                $last = strrpos($temp_str, '.');
                                $records_success[$i]['page_desc'] = substr($temp_str, 0, $last + 1);
                            }
                        }

                        if (empty($records_success[$i]['page_title'])) {
                            if (!empty($records_success[$i]['category_name'])) {
                                $records_success[$i]['page_title'] = $records_success[$i]['category_name'];
                            }
                        }
                        $record_to_be_saved = $records_success[$i];
                        $err_flg = 0;
                        $error_reason = '';
//check all field are not empty

                        if (empty($record_to_be_saved['category_name'])) {
                            $err_flg = 1;
                            $error_reason = 'Category Name Empty';
                        }
                        if ($this->find('first', array('conditions' => array('Category.category_name' => $record_to_be_saved['category_name'])))) {
                            $err_flg = 1;
                            $error_reason .= 'Category Name already exists,';
                        }
                        if ($err_flg == 0) {
                            $this->create();
                            $record_to_be_saved['parent_category_id'] = $data['Category']['category_id'];
                            $record_to_be_saved['level'] = $data['Category']['level'];
                            if ($this->save($record_to_be_saved)) {
                                
                            } else {
                                
                            }
                        } else {
                            $records_failed[$i] = $record_to_be_saved;
                            $records_failed[$i]['error'] = $error_reason;
                        }
                    }

                    if (empty($records_failed)) {
                        return $err_sts_flash;
                    } else {
                        SessionComponent::write('category_fails', $records_failed);
                        return 1;
                    }
                } else {
                    $err_sts_flash = 3;
                    return $err_sts_flash;
                }
            }
        }
    }

    public function set_count($cat_id, $counter) {
        if ($this->updateAll(array('Category.no_of_child' => $counter), array('Category.id' => $cat_id))) {
            return 1;
        } else {
            return 0;
        }
    }

    public function find_sub_count($cat_id) {

        $total_count = $this->find('count', array('conditions' => array('Category.parent_category_id' => $cat_id), 'recursive' => -1));
        return $total_count;
    }

    public function find_count() {
        $level1 = $this->find('list', array('conditions' => array('Category.level' => 1), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));

        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {

                $total = $this->find_sub_count($cat_id);

                $this->set_count($cat_id, $total);
            }
        }
        $level2 = $this->find('list', array('conditions' => array('Category.level' => 2), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level2)) {
            foreach ($level2 as $key => $cat_id) {
                $total = $this->find_sub_count($cat_id);

                $this->set_count($cat_id, $total);
            }
        }
        $level3 = $this->find('list', array('conditions' => array('Category.level' => 3), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level3)) {
            foreach ($level3 as $key => $cat_id) {
                $total = $this->find_sub_count($cat_id);

                $this->set_count($cat_id, $total);
            }
        }
        $level4 = $this->find('list', array('conditions' => array('Category.level' => 4), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level4)) {
            foreach ($level4 as $key => $cat_id) {
                $total = $this->find_sub_count($cat_id);

                $this->set_count($cat_id, $total);
            }
        }
        $level5 = $this->find('list', array('conditions' => array('Category.level' => 5), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level5)) {
            foreach ($level5 as $key => $cat_id) {
                $total = $this->find_sub_count($cat_id);

                $this->set_count($cat_id, $total);
            }
        }
        $level6 = $this->find('list', array('conditions' => array('Category.level' => 6), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level6)) {
            foreach ($level6 as $key => $cat_id) {
                $child_count = $this->find_sub_count($cat_id);
                $total = count($child_count);
                $this->set_count($cat_id, $total);
            }
        }
        //$this->Session->setFlash(__('The child count has been updated.'), 'success');
        //  return $this->redirect($this->referer());
        return 1;
    }

    public function get_front_category_tree($cat_id = null, $parent = null, $level = 0) {
        $cat_tree_root = 1;
        if (empty($cat_id) || empty($level)) {
            return $cat_tree;
        }
        for ($i = 0; $i < $level; $i++) {
            $main_cat = $this->find('first', array('conditions' => array('Category.id' => $parent), 'recursive' => -1));
            if ($main_cat['Category']['level'] == 1) {
                return $main_cat['Category']['id'];
            } else {
                $parent = $main_cat['Category']['parent_category_id'];
            }
        }
        return $cat_tree_root;
    }

}
