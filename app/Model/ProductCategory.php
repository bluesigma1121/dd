<?php

App::uses('AppModel', 'Model');

/**
 * ProductCategory Model
 *
 * @property Category $Category
 * @property Product $Product
 */
class ProductCategory extends AppModel {

    /**
     * Validation rules
     *
     * @var array
     */
    public $validate = array(
        'category_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'product_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
            //'message' => 'Your custom message here',
            //'allowEmpty' => false,
            //'required' => false,
            //'last' => false, // Stop validation after this rule
            //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

    /**
     * belongsTo associations
     *
     * @var array
     */
    public $belongsTo = array(
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Product' => array(
            'className' => 'Product',
            'foreignKey' => 'product_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

    public function auto_update_product_cat($id = null) {
        $level4 = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'recursive' => -1));
        if (!empty($level4)) {
            if ($level4['Category']['parent_category_id'] != 0) {
                if ($level4['Category']['no_of_product'] > 0) {
                    $cnt = $level4['Category']['no_of_product'] - 1;
                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level4['Category']['id']));
                }
                $level3 = $this->Category->find('first', array('conditions' => array('Category.id' => $level4['Category']['parent_category_id']), 'recursive' => -1));
                if (!empty($level3)) {
                    if ($level3['Category']['parent_category_id'] != 0) {
                        if ($level3['Category']['no_of_product'] > 0) {
                            $cnt = $level3['Category']['no_of_product'] - 1;
                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level3['Category']['id']));
                        }
                        $level2 = $this->Category->find('first', array('conditions' => array('Category.id' => $level3['Category']['parent_category_id']), 'recursive' => -1));
                        if (!empty($level2)) {
                            if ($level2['Category']['parent_category_id'] != 0) {
                                if ($level2['Category']['no_of_product'] > 0) {
                                    $cnt = $level2['Category']['no_of_product'] - 1;
                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level2['Category']['id']));
                                }
                                $level1 = $this->Category->find('first', array('conditions' => array('Category.id' => $level2['Category']['parent_category_id']), 'recursive' => -1));
                                if (!empty($level1)) {
                                    if ($level1['Category']['parent_category_id'] != 0) {
                                        if ($level1['Category']['no_of_product'] > 0) {
                                            $cnt = $level1['Category']['no_of_product'] - 1;
                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level1['Category']['id']));
                                        }
                                        $level0 = $this->Category->find('first', array('conditions' => array('Category.id' => $level1['Category']['parent_category_id']), 'recursive' => -1));
                                        if (!empty($level0)) {
                                            if ($level0['Category']['parent_category_id'] != 0) {
                                                if ($level0['Category']['no_of_product'] > 0) {
                                                    $cnt = $level0['Category']['no_of_product'] - 1;
                                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level0['Category']['parent_category_id']));
                                                }
                                            } else {
                                                if ($level0['Category']['no_of_product'] > 0) {
                                                    $cnt = $level0['Category']['no_of_product'] - 1;
                                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level0['Category']['parent_category_id']));
                                                }
                                            }
                                        }
                                    } else {
                                        if ($level1['Category']['no_of_product'] > 0) {
                                            $cnt = $level1['Category']['no_of_product'] - 1;
                                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level1['Category']['id']));
                                        }
                                    }
                                }
                            } else {
                                if ($level2['Category']['no_of_product'] > 0) {
                                    $cnt = $level2['Category']['no_of_product'] - 1;
                                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level2['Category']['id']));
                                }
                            }
                        }
                    } else {
                        if ($level3['Category']['no_of_product'] > 0) {
                            $cnt = $level3['Category']['no_of_product'] - 1;
                            $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level3['Category']['id']));
                        }
                    }
                }
            } else {
                if ($level4['Category']['no_of_product'] > 0) {
                    $cnt = $level4['Category']['no_of_product'] - 1;
                    $this->Category->updateAll(array('Category.no_of_product' => $cnt), array('Category.id' => $level4['Category']['id']));
                }
            }
        }
        return 1;
    }

    public function find_product_ids($cat_id) {
        $dist_product_ids = $this->find('list', array('conditions' => array('ProductCategory.category_id' => $cat_id, 'Product.is_active' => 1), 'fields' => array('ProductCategory.product_id', 'ProductCategory.product_id'), 'group' => 'ProductCategory.product_id', 'recursive' => 1));
        return $dist_product_ids;
    }

    /* public function get_child_count($cat_id) {
      $lev_4 = array();
      $lev_2_mer = array();
      $lev_3 = array();
      $level_4 = array();
      $level_3 = array();
      $dist_product = array();
      $dist_product = $this->find_product_ids($cat_id);
      $level1 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $cat_id), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
      if (!empty($level1)) {
      foreach ($level1 as $key => $catlevel1) {
      if (!empty($catlevel1)) {
      $dist_product1 = array();
      $dist_product1 = $this->find_product_ids($catlevel1['Category']['id']);
      $level2 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel1['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
      if (!empty($level2)) {
      foreach ($level2 as $catlevel2) {
      if (!empty($catlevel2)) {
      $dist_product2 = array();
      $dist_product2 = $this->find_product_ids($catlevel2['Category']['id']);
      $level3 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel2['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
      if (!empty($level3)) {
      foreach ($level3 as $catlevel3) {
      if (!empty($catlevel3)) {
      $dist_product3 = array();
      $dist_product3 = $this->find_product_ids($catlevel3['Category']['id']);

      $count_l4 = count($dist_product3);
      $this->set_count_product($catlevel3['Category']['id'], $count_l4);
      if (!empty($dist_product3)) {
      foreach ($dist_product3 as $pro3) {
      array_push($level_4, $pro3);
      }
      }
      }
      $level_4 = array_unique($level_4);
      }
      }
      $lev_3_mer = array_merge($dist_product2, $level_4);
      $level_3 = array_unique($lev_3_mer);
      $count_l3 = count($level_3);
      if (!empty($level_3)) {
      foreach ($level_3 as $id) {
      array_push($lev_3, $id);
      }
      }
      $this->set_count_product($catlevel2['Category']['id'], $count_l3);
      }
      }
      }
      $lev_2_mer = array_merge($dist_product1, $lev_3);
      $lev_2_mer = array_unique($lev_2_mer);
      $count_l2 = count($lev_2_mer);
      $this->set_count_product($catlevel1['Category']['id'], $count_l2);
      }
      }
      }
      $final_mer = array_merge($lev_2_mer, $dist_product);
      $final_mer = array_unique($final_mer);
      return count($final_mer);
      } */

    public function get_child_count($cat_id) {
        $lev_4 = array();
        $lev_2_mer = array();
        $lev_3_mer = array();
        $lev_4_mer = array();
        $lev_5_mer = array();
        $lev_5 = array();
        $lev_3 = array();
        $lev_4 = array();
        $lev_2 = array();
        $level_4 = array();
        $level_3 = array();
        $level_6 = array();
        $level_5 = array();
        $dist_product = array();
        $dist_product = $this->find_product_ids($cat_id);
        $level1 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $cat_id), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $catlevel1) {
                // debug($catlevel1);
                if (!empty($catlevel1)) {
                    $dist_product1 = array();
                    $dist_product1 = $this->find_product_ids($catlevel1['Category']['id']);
                    //   debug($dist_product1);
                    $level2 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel1['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                    if (!empty($level2)) {

                        foreach ($level2 as $catlevel2) {
                            if (!empty($catlevel2)) {
                                $dist_product2 = array();
                                $dist_product2 = $this->find_product_ids($catlevel2['Category']['id']);
                                $level3 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel2['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));


                                if (!empty($level3)) {
                                    foreach ($level3 as $catlevel3) {
                                        if (!empty($catlevel3)) {
                                            $dist_product3 = array();
                                            $dist_product3 = $this->find_product_ids($catlevel3['Category']['id']);
                                            $level4 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel3['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));


                                            if (!empty($level4)) {
                                                foreach ($level4 as $catlevel4) {
                                                    if (!empty($catlevel4)) {
                                                        $dist_product4 = array();
                                                        $dist_product4 = $this->find_product_ids($catlevel4['Category']['id']);
                                                        $level5 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel4['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));

                                                        if (!empty($level5)) {
                                                            foreach ($level5 as $catlevel5) {
                                                                if (!empty($catlevel5)) {
                                                                    $dist_product5 = array();
                                                                    $dist_product5 = $this->find_product_ids($catlevel5['Category']['id']);
                                                                    //$level6 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel5['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));

                                                                    $count_l6 = count($dist_product5);
                                                                    $this->set_count_product($catlevel5['Category']['id'], $count_l6);
                                                                    if (!empty($dist_product5)) {
                                                                        foreach ($dist_product5 as $pro5) {
                                                                            array_push($level_6, $pro5);
                                                                        }
                                                                    }
                                                                    $level_6 = array_unique($level_6);
                                                                }
                                                            }
                                                        }
                                                        $lev_5_mer = array_merge($dist_product4, $level_6);
                                                        $level_5 = array_unique($lev_5_mer);
                                                        $count_l5 = count($level_5);
                                                        if (!empty($level_5)) {
                                                            foreach ($level_5 as $id) {
                                                                array_push($lev_5, $id);
                                                            }
                                                        }
                                                        $this->set_count_product($catlevel4['Category']['id'], $count_l5);
                                                    }
                                                }
                                            }
                                            $lev_4_mer = array_merge($dist_product3, $level_5);
                                            $level_4 = array_unique($lev_4_mer);
                                            $count_l4 = count($level_4);
                                            if (!empty($level_4)) {
                                                foreach ($level_4 as $id) {
                                                    array_push($lev_4, $id);
                                                }
                                            }
                                            $this->set_count_product($catlevel2['Category']['id'], $count_l4);
                                        }
                                    }
                                }
                                $lev_3_mer = array_merge($dist_product2, $level_4);
                                $level_3 = array_unique($lev_3_mer);
                                $count_l3 = count($level_3);
                                if (!empty($level_3)) {
                                    foreach ($level_3 as $id) {
                                        array_push($lev_3, $id);
                                    }
                                }
                                $this->set_count_product($catlevel2['Category']['id'], $count_l3);
                            }
                        }
                    }
                    $lev_2_mer = array_merge($dist_product1, $lev_3);

                    $lev_2_mer = array_unique($lev_2_mer);
                    // debug($lev_2_mer);
                    $count_l2 = count($lev_2_mer);
                    if (!empty($lev_2_mer)) {
                        foreach ($lev_2_mer as $id) {
                            array_push($lev_2, $id);
                        }
                    }
                    $this->set_count_product($catlevel1['Category']['id'], $count_l2);
                }
            }
        }
        $final_mer = array_merge($lev_2, $dist_product);
        $final_mer = array_unique($final_mer);
        return count($final_mer);
    }

    public function get_child_count_try($cat_id) {
        $lev_4 = array();
        $lev_2_mer = array();
        $lev_3 = array();
        $level_4 = array();
        $level_3 = array();
        $dist_product = array();
        $dist_product = $this->find_product_ids($cat_id);
        $level1 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $cat_id), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
        if (!empty($level1)) {
            foreach ($level1 as $key => $catlevel1) {
                if (!empty($catlevel1)) {
                    $dist_product1 = array();
                    $dist_product1 = $this->find_product_ids($catlevel1['Category']['id']);
                    $level2 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel1['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                    if (!empty($level2)) {
                        foreach ($level2 as $catlevel2) {
                            if (!empty($catlevel2)) {
                                $dist_product2 = array();
                                $dist_product2 = $this->find_product_ids($catlevel2['Category']['id']);
                                $level3 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel2['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                                if (!empty($level3)) {
                                    foreach ($level3 as $catlevel3) {
                                        if (!empty($catlevel3)) {
                                            $dist_product3 = array();
                                            $dist_product3 = $this->find_product_ids($catlevel3['Category']['id']);
                                            $level4 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel3['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                                            // $count_l4 = count($dist_product3);
                                            // $this->set_count_product($catlevel3['Category']['id'], $count_l4);
                                            if (!empty($level4)) {
                                                foreach ($level4 as $catlevel4) {
                                                    if (!empty($catlevel4)) {
                                                        $dist_product4 = array();
                                                        $dist_product4 = $this->find_product_ids($catlevel4['Category']['id']);
                                                        $level5 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel4['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                                                        if (!empty($level5)) {
                                                            foreach ($level5 as $catlevel5) {
                                                                if (!empty($catlevel5)) {
                                                                    $dist_product5 = array();
                                                                    $dist_product5 = $this->find_product_ids($catlevel5['Category']['id']);
                                                                    $level6 = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $catlevel5['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level', 'Category.parent_category_id'), 'recursive' => 0));
                                                                    if (!empty($catlevel5)) {
                                                                        
                                                                    } else {
                                                                        
                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        $level_4 = array_unique($level_4);
                                    }
                                }
                                $lev_3_mer = array_merge($dist_product2, $level_4);
                                $level_3 = array_unique($lev_3_mer);
                                $count_l3 = count($level_3);
                                if (!empty($level_3)) {
                                    foreach ($level_3 as $id) {
                                        array_push($lev_3, $id);
                                    }
                                }
                                $this->set_count_product($catlevel2['Category']['id'], $count_l3);
                            }
                        }
                    }
                    $lev_2_mer = array_merge($dist_product1, $lev_3);
                    $lev_2_mer = array_unique($lev_2_mer);
                    $count_l2 = count($lev_2_mer);
                    $this->set_count_product($catlevel1['Category']['id'], $count_l2);
                }
            }
        }
        $final_mer = array_merge($lev_2_mer, $dist_product);
        $final_mer = array_unique($final_mer);
        return count($final_mer);
    }

    public function set_count_product($cat_id, $counter) {
        if ($this->Category->updateAll(array('Category.no_of_product' => $counter), array('Category.id' => $cat_id))) {
            return 1;
        } else {
            return 0;
        }
    }

    public function clean_extra_relation() {
        $product_list = $this->Product->find('list');
        $this->deleteAll(array('ProductCategory.product_id NOT' => $product_list));
        return 1;
    }

}
