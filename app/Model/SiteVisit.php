<?php
App::uses('AppModel', 'Model');
/**
 * SiteVisit Model
 *
 */
class SiteVisit extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'ip' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
