<style>
    .table-heading{
        box-sizing: border-box;
        color: rgb(104, 112, 116);
        display: block;
        font-family: 'Open Sans', sans-serif;
        font-size: 14px;
        font-weight: normal;
        height: 20px;
        line-height: 20px;
        margin-bottom: 10px;
        margin-top: 5px;
        text-align: center;
        text-shadow: none;
    }
    .fa-dollar{
        color: #3498db;
    }
    .fa-inr{
        color: #3498db;
    }
    .pagination span {
        background: #f7f7f7;
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
        PADDING: 5px 14PX;
        MARGIN: 10px 10px;
        border-radius: 4px;
        border: 1px solid #ddd;
    }
</style>
<?php echo $this->element('listing_search'); ?>
<?php //echo $this->element('front_breadcumb'); ?>
<!--=== Content ===--> 
<div class="content container">
    <div class="row">
        <?php echo $this->element('front_cat_prod_filter'); ?>
        <div class="col-md-12">
            <!--/end pagination-->
            <h1>Search Results</h1>
            <div class="msg1">
                
            </div>
            
            <?php if (!empty($results)) { ?>
                <div class = "filter-results" style = "padding-top:0px;">                   
                        
                        <div class="row">
                            <div class="col-sm-8">
                                <div class="text-center">
                                    <span class="table-heading">  PRODUCT TITLE </span>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <?php
                                echo $this->Html->link(__('<span class="table-heading">PUBLISH DATE</span>'), array('controller' => 'products', 'action' => 'publish_session'), array('escape' => false));
                                ?>
                            </div>
                            <div class="col-sm-2">
                                <?php
                                echo $this->Html->link(__('<span class="table-heading">PRICE </span>'), array('controller' => 'products', 'action' => 'price_session'),array('escape' => false));
                                ?>
                            </div>
                        </div>
                        <?php for ($i=0;$i<sizeof($results);$i++) {
                            ?>
                            <div class="list-product-description product-description-brd margin-bottom-30 m_top">
                                <div class="row ">
                                    <div class="col-sm-2">
                                        <?php echo $results[$i]['Product']['product_image']; /*
                                          <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'id' => $results[$i]['Product']['id'], 'slug' => $this->Link->cleanString($results[$i]['Product']['product_name']))); ?> ">
                                         */ ?>
                                        <a target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($results[$i]['Product']['slug']), 'id' => $results[$i]['Product']['id'], 'slug' => $results[$i]['Product']['slug'])); ?> ">
                                            <?php if (!empty($results[$i]['Product']['product_image'])) { ?>
                                                <?php
                                                echo $this->Html->image('product_images/' . $results[$i]['Product']['product_image'], array('class' => 'img-thumbnail image_size'));
                                            } else {
                                                echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                                             }
                                            ?>
                                        </a>
                                    </div> 


                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div style="padding-right: 10px;">
                                                <ul class="list-inline overflow-h">
                                                    <li>
                                                        <span class="title-price h4_fnt">
                                                            <a target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($results[$i]['Product']['slug']), 'id' => $results[$i]['Product']['id'], 'slug' => $results[$i]['Product']['slug'])); ?> "> <?php echo $results[$i]['Product']['product_name']; ?></a>
                                                        </span>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div style="padding-right: 10px;">
                                                <p>
                                                    <?php
                                                    echo $this->Text->truncate(
                                                    strip_tags($results[$i]['Product']['product_description']), 150, array(
                                                        'ellipsis' => '...',
                                                        'exact' => false
                                                            )
                                                    );
                                                    ?>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 price_div">
                                        <div class="row">
                                            <div class="margin_in min_hei_pub">
                                                <span class="text-center title-price h4_fnt">
                                                    <?php /*
                                                    if ($product['Product']['under_publishing']) {
                                                        echo "Under Publishing";
                                                    } else {
                                                        if (!empty($product['Product']['pub_date'])) {
                                                            echo Date('F Y', //strtotime($product['Product']['pub_date']));
                                                       }
                                                    }
                                                    */?>
                                                    <?php if (!empty($results[$i]['Product']['pub_date']) &&  $results[$i]['Product']['pub_date']!= "0000-00-00" && $results[$i]['Product']['pub_date']!= "1970-01-01"): ?>
                                                        <?php echo Date('F Y', strtotime($results[$i]['Product']['pub_date'])); ?>
                                                    <?php else: ?>
                                                        <?php echo Date('F Y', strtotime("+1 month")); ?>
                                                    <?php endif; ?>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="margin_in">
                                                <?php /*
                                                  $url_gen = Router::url('/', true) . 'enquiries/send_enquiry/' . $product['Product']['id'] . '/' . $selected_cat['Category']['id'] . 'category_listing';

                                                  <span
                                                  id="call_me_<?php echo $results[$i]['Product']['id']; ?>"
                                                  class="btn-u btn-u-green"
                                                  rel="<?php echo $results[$i]['Product']['id']; ?>"
                                                  onclick="getPopupForm(this.id, '<?php echo $url_gen; ?>');" >
                                                  Send Inquiry
                                                  </span>
                                                 */ ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-2 price_div">
                                        <div class="row">
                                            <div class="margin_in min_hei_pub">
                                                <span class="title-price margin-right-10 font_clr">
                                                    <?php $final_price = round(($results[$i]['Product']['price'] * $dollar_rate), 2); ?>
                                                    <?php if ($cur_curency == 0): ?>
                                                        <b><i class="fa fa-inr"></i></b><?php echo $final_price; ?>
                                                    <?php else: ?>
                                                        <b><i class="fa fa-dollar"></i> </b>
                                                        <?php echo number_format($final_price, 2); ?>
                                                    <?php endif; ?>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>               
                
                <div class="row">
                    <div class="col-md-12 text-center pagination">
                    
                        <?php 
                            echo $this->Paginator->prev('« Previous',null,null,array('class' => 'disabled'));
                            echo $this->Paginator->next('Next »',null, null, array('class' => 'disabled')); 
                        ?>
                    </div>
                    <div class="col-md-12 text-center">
                        <?php echo $this->Paginator->counter(array('format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')));?>                 
                    </div>
                </div>
                
            <?php } else {
                ?>
                <div class = "filter-results" style = "padding-top:0px;">
                    <div class = "row">
                        <div class="col-sm-12  contex-bg">
                            <div class="alert alert-warning text-center">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <strong>Products Not Found </strong>
                            </div>
                        </div>
                        <div class="col-sm-12 m_top">
                            <div class="text-center">
                            <h3> Didn't find what you are looking for? </h3> <h4>Send us an email at <a href='mailto:sales@decisiondatabases.com'>sales@decisiondatabases.com</a> and we will soon revert back to you.</h4>

                                <button class="btn-u"onclick="goBack()">Go Back</button>
                            </div>
                        </div>

                    </div>
                </div> 
            <?php }
            ?>

        </div>
    </div><!--/end row-->
</div>
