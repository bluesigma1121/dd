<style>
    .search-block {
        padding: 0;
        text-align: center;
        position: relative;
        margin-bottom: 20px;
        background: url(img/front/bg/7.jpg) 50% 0 repeat fixed;
    }
</style>
<script>
    $(document).ready(function () {
        $('#collapse-One292').collapse('show');
    });
</script>
<!--=== Search Block ===-->
<?php echo $this->element('listing_search'); ?>    
<!--=== End Search Block ===-->
<div class="container content">
    <div class="row">             
        <!-- Begin Content -->
        <div class="col-md-9">
            <!-- Accordion v1 -->    
            <div class="panel-group acc-v1" id="accordion-1">
                <?php foreach ($cat_data['Level1'] as $key1 => $cat): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <?php if ($cat['id'] == $id) { ?>
                                    <a class="accordion-toggle" data-toggle="collapse" aria-expanded="true"  data-parent="#accordion-1" href="#collapse-One<?php echo $cat['id'] ?>">
                                        <?php echo $cat['category_name'] ?>(<?php echo $cat['no_of_product'] ?>)<br> 

                                    </a> 
                                    <?php /*
                                      <p class="mar_span_cat_list">
                                      <?php echo $this->Text->truncate($cat['short_desc'], 350); ?> </p>
                                     * 
                                     */ ?>
                                <?php } else { ?>
                                    <a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion-1" href="#collapse-One<?php echo $cat['id'] ?>">
                                        <?php echo $cat['category_name'] ?> (<?php echo $cat['no_of_product']; ?>)
                                    </a>
                                <?php } ?>
                            </h4>
                        </div>
                        <?php if ($cat['id'] == $id) { ?>
                            <div id="collapse-One<?php echo $cat['id'] ?>" class="panel-collapse collapse in" style="height: 0px;">
                            <?php } else { ?>
                                <div id="collapse-One<?php echo $cat['id'] ?>" class="panel-collapse collapse" style="height: 0px;">
                                <?php } ?>
                                <div class="panel-body">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <ul class="list-unstyled">
                                                <li>
                                                    <i class="fa fa-angle-double-right color-green"></i>
                                                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat['id'], 'slug' => $this->Link->cleanString($cat['category_name']))); ?> "><?php echo $cat['category_name'] ?> (<?php echo $cat['no_of_product']; ?>)</a>
                                                </li> 
                                                <?php if (!empty($cat['Level2'])) { ?>
                                                    <ul>
                                                        <?php foreach ($cat['Level2'] as $key2 => $cat2): ?>
                                                            <li><i class="fa fa-angle-double-right color-green"></i>
                                                                <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat2['id'], 'slug' => $this->Link->cleanString($cat2['category_name']))); ?> "><?php echo $cat2['category_name'] ?> (<?php echo $cat2['no_of_product']; ?>)</a>
                                                                <?php if (!empty($cat2['Level3'])) { ?>
                                                                    <ul>
                                                                        <?php foreach ($cat2['Level3'] as $key3 => $cat3): ?>
                                                                            <li><i class="fa fa-angle-double-right color-green"></i>   <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat3['id'], 'slug' => $this->Link->cleanString($cat3['category_name']))); ?>"><?php echo $cat3['category_name'] ?> (<?php echo $cat3['no_of_product']; ?>)</a>
                                                                                <?php if (!empty($cat3['Level4'])) { ?>
                                                                                    <ul>
                                                                                        <?php foreach ($cat3['Level4'] as $key4 => $cat4): ?>
                                                                                            <li class="list-unstyled">
                                                                                                <i class="fa fa-angle-double-right color-green"></i>
                                                                                                <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat4['id'], 'slug' => $this->Link->cleanString($cat4['category_name']))); ?>"><?php echo $cat4['category_name'] ?>(<?php echo $cat4['no_of_product']; ?>)</a> 
                                                                                                <?php if (!empty($cat4['Level5'])) { ?>
                                                                                                    <ul>
                                                                                                        <?php foreach ($cat4['Level5'] as $key5 => $cat5): ?>
                                                                                                            <li class="list-unstyled">
                                                                                                                <i class="fa fa-angle-double-right color-green"></i>
                                                                                                                <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat5['id'], 'slug' => $this->Link->cleanString($cat5['category_name']))); ?>"><?php echo $cat5['category_name'] ?>(<?php echo $cat5['no_of_product']; ?>)</a> 
                                                                                                                <?php if (!empty($cat5['Level6'])) { ?>
                                                                                                                    <ul>
                                                                                                                        <?php foreach ($cat5['Level6'] as $key6 => $cat6): ?>
                                                                                                                            <li class="list-unstyled">
                                                                                                                                <i class="fa fa-angle-double-right color-green"></i>
                                                                                                                                <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat6['id'], 'slug' => $this->Link->cleanString($cat6['category_name']))); ?>"><?php echo $cat6['category_name'] ?>(<?php echo $cat6['no_of_product']; ?>)</a>
                                                                                                                            </li>
                                                                                                                        <?php endforeach; ?>
                                                                                                                    </ul>
                                                                                                                <?php } ?>
                                                                                                            </li>
                                                                                                        <?php endforeach; ?>
                                                                                                    </ul>
                                                                                                <?php } ?>
                                                                                            </li>
                                                                                        <?php endforeach; ?>
                                                                                    </ul>
                                                                                <?php } ?>
                                                                            </li>
                                                                        <?php endforeach; ?>
                                                                    </ul>
                                                                <?php } ?>
                                                            <?php endforeach; ?>
                                                        </li>
                                                    </ul>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div> 
            <div class="col-md-3 section-block">                    

                <div class="headline"><h4>Recently Viewed</h4></div>
                <ul>
                    <?php foreach ($recent_view as $key => $rec_view) {
                        ?>
                        <li><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($rec_view['Category']['category_name']), 'id' => $rec_view['Product']['id'], 'slug' => $rec_view['Product']['slug'])); ?> "><h5><?php echo $rec_view['Product']['product_name']; ?></h5></a></li>
                    <?php } ?>

                </ul>
                <div class="clearfix"></div>

            </div>
        </div>          
    </div>                     
