<!--=== Search Block ===-->
<?php
$color_div = array( 1 => 'service-block-blue', 2 => 'service-block-red', 3 => 'service-block-dark', 4 => 'service-block-brown', 5 => 'service-block-orange', 6 => 'service-block-purple', );
?>

<div class="search-block parallaxBg">
    <div class="container" style="background-color: rgba(0, 0, 0, 0.43);">
        <div class="col-md-12">
            <!-- <h1>300+ Clients&nbsp;&nbsp;|&nbsp;&nbsp;20,000+ Reports&nbsp;&nbsp;|&nbsp;&nbsp;10+ Industry Verticals</h1> -->
            <h1>500+ Clients&nbsp;&nbsp;|&nbsp;&nbsp;70,000+ Reports&nbsp;&nbsp;|&nbsp;&nbsp;10+ Industry Verticals</h1>
        </div>

        <div class="col-md-12 margin-bottom-20">
            <?php  //echo $this->Form->create('Category', array('controller' => 'categories', 'action' => 'home_search_form', 'id' => 'search_frm', 'role' => 'form')); ?>
            <!-- Old Textbox for Home Search Start VG-21-10-2016 -->
            <?php //echo $this->Form->input('search_txt', array('class' => 'form-control text_box_height text_box_in_height', 'id' => 'search_txt_id', 'label' => false)); ?>
            <!-- Old Textbox for Home Search End VG-21-10-2016 -->

            <!-- New Textbox for Home Search Start VG-21-10-2016 -->
            <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                <?php echo $this->Form->input('q', array('class' => 'form-control text_box_height_list', 'label' =>false,'required','autocomplete'=>'off','id'=>'search_text')); ?>
                <div class="col-md-12 pull-left" id="suggest" style=" margin-top: -9px; clear:both; margin-left: -14px;overflow-x: hidden;position: absolute; z-index: 2;"></div>
                <div class="col-md-1 col-sm-1"  style=" margin-top: -49px;float: right;display: block;">
                    <?php echo $this->Form->submit('Search',array('class' => 'btn btn-default ','style'=>' height: 38px; width: 120%;border-radius: 0px;font-weight: 500;background: #666666; color: #ffffff;')); ?>
                </div>
            <?php  echo $this->Form->end(); ?>
            <!-- New Textbox for Home Seacrch End VG-21-10-2016 -->

        </div>

    </div>
</div>

<!-- New Code For Covid-19 Start VG-21/04/2020 -->
<style>
.dd_covid{background:#135cc9;box-shadow:0 14px 28px rgba(0,0,0,.25),0 10px 10px rgba(0,0,0,.22);overflow:hidden;padding:60px;margin:60px auto;position:relative;width:100%;background-position:bottom center;background-repeat:no-repeat;background-size:cover}.dd_covid .card .title{font-size:40px;font-weight:900;color:#fff}.dd_covid .card .title small{color:#fff;font-size:32px;font-weight:900}.dd_covid .card p{font-size:20px;color:#fff;letter-spacing:1px}.btn-covid{display:inline-block;padding:5px 24px;color:#fff;transition:all .33s ease;text-decoration:none;font-size:16px;border:2px solid #fff;margin-top:14px;letter-spacing:2px;text-decoration:none}.btn-covid:focus,.btn-covid:hover{background:#fff;color:#0074bd;text-decoration:none}
</style>
<section class="dd_covid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <h2 class="title">COVID -19 : <small>Impact Analysis</small></h2>
                    <p>Get access to special report on IMPACT OF COVID 19 across various industries and market.</p>
                    <?php echo $this->Html->link('Access Reports', array('controller' => 'products', 'action' => 'latest_market_reports'), array("class" => "btn-covid", "target" => "_blank",'escape' => false));?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- New Code For Covid-19 End VG-21/04/2020 -->


<!-- New Section For  Our Clients Start -->
<div class="container content-md hidden-xs">
    <?php if (!empty($ourclients)) { ?>
            <div class="row margin-bottom-10">
                <div class="headline"><h2>Some of Our Clients</h2></div>

                    <?php foreach ($ourclients as $key => $clients) { ?>
                        <div class="col-md-2 col-sm-6 pad-05">
                        <div class="thumbnail_home">
                        <div class="service-block">
                            <div class="item">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <!-- <a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">   -->
                                    <?php
                                        echo $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => 'img-responsive'));
                                    } else {
                                        echo $this->Html->image('noimage.png', array('class' => 'img-responsive'));
                                    }
                                    ?>
                                <!-- </a> -->
                            </div>
                        </div>

                        </div>

                    </div> <!-- .col-md-2 -->
                    <?php } ?>
            </div>

    <?php } ?>
</div> <!-- .container -->
<!-- New Section For  Our Clients End -->

<div class="container content-md">
    <div class="headline"><h2>Market Research Reports</h2></div>
    <div class="row margin-bottom-10">
        <?php foreach ($research_cat as $rckey => $res_cat): ?>
            <?php
            if (empty($res_cat['Category']['div_color'])) {
                $res_cat['Category']['div_color'] = "service-block-blue";
            }
            if (empty($res_cat['Category']['icon'])) {
                $res_cat['Category']['icon'] = "fa fa-globe";
            }
            ?>
            <div class="col-md-4 col-sm-6 pad-05">
                <div class="thumbnail_home">
                    <?php if ($res_cat['Category']['no_of_product'] < 1): ?>
                        <div class="service-block  <?php echo $res_cat['Category']['div_color']; ?>">
                            <div class="caption_home">
                                <div class="text-center">
                                    <h2>Launching Soon</h2>
                                </div>
                            </div>
                            <i class="icon-custom icon-color-light rounded-x <?php echo $res_cat['Category']['icon']; ?>"></i>
                            <h2 class="heading-md"><?php echo $res_cat['Category']['category_name']; ?></h2>
                            <p><?php echo $this->Text->truncate($res_cat['Category']['short_desc'], 90); ?></p>
                            <?php /*
                              <ul class="list-inline block-grid-v1-add-info">
                              <li><a href="#"><i class="fa fa-eye"></i>  <?php echo $res_cat['Category']['no_of_view']; ?></a></li>
                              <li><a href="#"><i class="fa fa-retweet"></i> <?php echo $res_cat['Category']['no_of_product'] ?></a></li>
                              </ul>
                             *
                             */ ?>
                        </div>
                    <?php else: ?>
                        <div class="service-block  <?php echo $res_cat['Category']['div_color']; ?> mylink">
                            <i class="icon-custom icon-color-light rounded-x <?php echo $res_cat['Category']['icon']; ?>"></i>
                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $res_cat['Category']['id'], 'slug' => $this->Link->cleanString($res_cat['Category']['category_name']))); ?>">
                                <h2 class="heading-md"><?php echo $res_cat['Category']['category_name']; ?></h2></a>
                            <p><?php echo $this->Text->truncate($res_cat['Category']['short_desc'], 100); ?></p>
                            <?php /*
                              <ul class="list-inline block-grid-v1-add-info">
                              <li><a href="#"><i class="fa fa-eye"></i>  <?php echo $res_cat['Category']['no_of_view']; ?></a></li>
                              <li><a href="#"><i class="fa fa-retweet"></i> <?php echo $res_cat['Category']['no_of_product'] ?></a></li>
                              </ul>
                             *
                             */ ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

 <!-- New Code For Our Clients Start VG-22/10/2016 -->
<!-- <div class="container content-md">
    <div class="headline"><h2>Some of Our Clients</h2></div>
     <?php if (!empty($ourclients)) { ?>
    <div class="row margin-bottom-20">
     <div class="row row-setting">
            <div class="main">

                <ul id="carousel" class="elastislide-list">
                  <?php foreach ($ourclients as $key => $clients) { ?>
                    <li id="style" class="size">
                      <?php if (!empty($clients['OurClient']['logo'])) { ?>
                          <a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">  <?php
                              echo $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => 'img-responsive'));
                          } else {
                              echo $this->Html->image('noimage.png', array('class' => 'img-responsive'));
                          }?>
                      </a>
                    </li>
                  <?php } ?>
                </ul>

            </div>
      </div>
    </div>
    <?php } ?>
</div> -->

<!-- New Code For Our Clients End VG-22/10/2016 -->



<!-- New Code For Testimonial Start VG-15/12/2016 -->
<div class="container content-md"style="display:none;">
  <?php if (!empty($testimonial)) { ?>
    <div class="headline"><h2>Testimonials</h2></div>
    <div class="row margin-bottom-20">
     <div class="row">
        <?php foreach ($testimonial as $key => $testimonials) { ?>
            <div class="col-md-4 col-sm-6 pad-05" >
                <div class="thumbnail_home">
                        <div class="service-block" >
                            <div class="item" style="cursor: default !important;" >
                                    <center>
                                    <strong style=" font-size: 16px; text-transform: capitalize;"><?php echo $testimonials['Testimonial']['testimonial_title'];?></strong >
                                    </center><br>
                                 <h5 style="text-align: justify;line-height: 20px;"> <?php echo " \" "; echo  $testimonials['Testimonial']['testimonial_description'] ;echo" \" "; ?> </h5>
                            </div>
                        </div>
                </div>
            </div>
            <?php } ?>
         </div>
    </div>
    <?php } ?>
</div>
<!-- New Code For Testimonial End VG-15/12/2016-->

<div class="container content margin_home_home" style="margin-top:30px;">
    <!-- Top Categories -->
    <?php /*
      <div class="headline"><h2>Top Categories</h2></div>
     *
     */ ?>
    <div class="headline"><h2>Recently Added Reports</h2></div>
    <div class="row category margin-bottom-20">
        <div class="col-md-8">
            <?php foreach ($home_products as $home_product): ?>
                <div class="inner-results">
                    <h3><a href="<?php
                        echo Router::url(array(
                            'controller' => 'products', 'action' => 'category_details',
                            'main' => $this->Link->cleanString($home_product['Category']['slug']),
                            'id' => $home_product['Product']['id'],
                            'slug' => $home_product['Product']['slug']));
                        ?> ">
                               <?php echo $home_product['Product']['alias'] ?>
                        </a></h3>
                    <ul class="list-inline up-ul">
                        <li><strong>Publisher Name: <?php echo ($home_product['Product']['publisher_name']) ?></strong></li>

                        <li>
                            <strong>Publish Date:
                                <?php if (!empty($home_product['Product']['pub_date']) && $home_product['Product']['pub_date'] != "0000-00-00" && $home_product['Product']['pub_date'] != "1970-01-01"): ?>
                                    <?php echo Date('F Y', strtotime($home_product['Product']['pub_date'])); ?>
                                <?php else: ?>
                                    <?php echo Date('F Y', strtotime("+1 month")); ?>
                                <?php endif; ?>
                            </strong>
                        </li>
                        <li><strong>Price: $<?php echo $home_product['Product']['price'] ?></strong></li>
                    </ul>
                    <p>
                        <?php
                        echo $this->Text->truncate(
                                strip_tags($home_product['Product']['product_description']), 240, array(
                            'ellipsis' => '...',
                            'exact' => false
                                )
                        );
                        ?>
                        <a href="<?php
                        echo Router::url(array(
                            'controller' => 'products', 'action' => 'category_details',
                            'main' => $this->Link->cleanString($home_product['Category']['slug']),
                            'id' => $home_product['Product']['id'],
                            'slug' => $home_product['Product']['slug']));
                        ?> ">
                            Read More..</a>
                    </p>
                </div>
                <hr>
            <?php endforeach; ?>

            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'latest_market_reports')); ?>" class="btn-u btn-u-lg text_box_height search_pading">View More</a>
        </div>

        <?php /*
          <div class="col-md-8 col-sm-6">
          <?php
          $i = 0;
          foreach ($home_cat as $hkey => $cate_home) {
          ++$i;
          ?>
          <div class="col-md-6 col-sm-6">
          <div class="content-boxes-v3 margin-bottom-10 md-margin-bottom-20">
          <i class="icon-custom  rounded-x <?php echo $cate_home['Category']['icon'] ?> <?php echo $color_div[$i]; ?> bg_wht_home icon-line icon-badge"></i>
          <div class="content-boxes-in-v3">
          <h3><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cate_home['Category']['id'], 'slug' => $this->Link->cleanString($cate_home['Category']['category_name']))); ?>"> <?php echo $cate_home['Category']['category_name']; ?> (<?php echo $cate_home['Category']['no_of_product']; ?>)</a></h3>
          <p><?php echo $this->Text->truncate($cate_home['Category']['short_desc'], 100); ?></p>
          </div>
          </div>
          </div>
          <?php
          if ($i == 5) {
          $i = 0;
          }
          }
          ?>
          </div>
         *
         */ ?>
        <!-- Info Blocks -->

        <!-- End Info Blocks -->



        <!-- Begin Section-Block -->
        <div class="col-md-4 col-sm-12">

            <div class="section-block">
                <?php /*
                  <?php if (!empty($recent_view)): ?>
                  <div class="headline"><h4>Recently Viewed Reports</h4></div>
                  <ul class="list-inline">
                  <?php foreach ($recent_view as $key => $rec_view): ?>
                  <li style="padding-bottom:5px;">
                  <i class="fa fa-angle-double-right color-green"></i>
                  <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($rec_view['Category']['slug']), 'id' => $rec_view['Product']['id'], 'slug' => $rec_view['Product']['slug'])); ?> "><?php echo $rec_view['Product']['alias']; ?></a>
                  </li>
                  <?php endforeach; ?>
                  </ul>
                  <br>
                  <?php endif; ?>
                 */ ?>

                <div class="headline"><h4>Recent Press Release</h4></div>
                <ul class="list-inline">
                    <?php foreach ($press_releases as $key => $press_relese): ?>
                        <li style="padding-bottom:5px;">
                            <i class="fa fa-angle-double-right color-green"></i>
                            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_details', 'folder' => 'press-releases', 'article_id' => $press_relese['Article']['id'], 'url_slug' => $this->Link->cleanString($press_relese['Article']['slug']))); ?>">
                                <?php echo $press_relese['Article']['headline'] ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <br>

                <div class="headline"><h4>Recent Articles</h4></div>
                <ul class="list-inline">
                    <?php foreach ($articles as $key => $article): ?>
                        <li style="padding-bottom:5px;">
                            <i class="fa fa-angle-double-right color-green"></i>
                            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_details', 'folder' => 'analysis', 'article_id' => $article['Article']['id'], 'url_slug' => $this->Link->cleanString($article['Article']['slug']))); ?>">
                                <?php echo $article['Article']['headline'] ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <br>

<!-- RSS FEEDS code Start VG-16/12/2016 -->
                <div class="headline"><h4>RSS Feeds</h4></div>
                <ul class="list-inline">
                    <?php foreach ($rss as $key => $rss): ?>
                        <li style="padding-bottom:5px;">
                            <i class="fa fa-angle-double-right color-green"></i>
                                <?php echo $rss['Product']['product_name'] ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                <br>
<!-- RSS FEEDS code End VG-16/12/2016 -->

            </div>
            <!-- End Section-Block -->
        </div>
        <!-- End Top Categories -->
    </div> <!--/container-->

    <!-- <div class="container">
        <?php if (!empty($ourclients)) { ?>
            <div class="row">
                <div class="owl-carousel-style-v2 margin-bottom-50">
                    <div class="headline"><h2>Our Clients</h2></div>
                    <div class="owl-slider-v3">
                        <?php foreach ($ourclients as $key => $clients) { ?>
                            <div class="item">
                                <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                    <a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">  <?php
                                        echo $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => 'img-responsive'));
                                    } else {
                                        echo $this->Html->image('noimage.png', array('class' => 'img-responsive'));
                                    }
                                    ?>
                                </a>
                            </div>
                        <?php }
                        ?>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div> -->
</div>
</div>
<style>
    hr {
        margin: 15px 0;
    }
    .btn-u-lg, a.btn-u-lg {
        font-size: 18px;
        padding: 5px 25px;
    }
    .cust-img{
        height:300px !important;
        width:300px !important;
    }
    .search-block h1 {
      font-size: 28px;
      text-align: center;
      margin-left: 0;
      margin-bottom: 15px;
      text-transform: uppercase;
      margin-top: 15px;
    }
</style>
