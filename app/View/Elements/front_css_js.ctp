<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> 

<?php if (isset($meta_name) && !empty($meta_name)): ?>
    <title><?php echo $meta_name ?></title>
<?php else: ?>
    <title>DecisionDatabases.com:Market Research Reports ,Industry Analysis, Market Insights</title>
<?php endif; ?>
    
<?php if (isset($meta_keyword) && !empty($meta_keyword)): ?>    
    <meta name="keywords" content="<?php echo $meta_keyword ?>">
<?php else: ?>
    <meta name="keywords" content="reports">
<?php endif; ?>
    
<?php if (isset($meta_desc) && !empty($meta_desc)): ?>
    <meta name="description" content="<?php echo $meta_desc ?>">
<?php else: ?>
    <meta name="description" content="decisiondatabases.com">
<?php endif; ?>

<meta name="p:domain_verify" content="50ee012b114822b79f28403c177bad5f"/>

<?php
echo $this->Html->meta('favicon.ico', '/img/favicon.png', array(
    'type' => 'icon'
));
?>
<script>
    var base_url = "<?php echo Router::url('/', true) ?>";
</script>

<?php if ($this->request->is('mobile')) { ?>
    <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" defer="defer">
    
    <?php echo $this->Html->css(array('front/responsive.min.css'), ['defer' => 'defer']); ?>
    <?php echo $this->Html->css(array('front/blue.min.css'), ['defer' => 'defer']); ?>
<?php }else{ ?>

    <!-- Favicon -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!-- <link rel="stylesheet" href="/resources/demos/style.css"> -->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
	

    <?php
	echo $this->Html->css(array('front/app.min.css','front/bootstrap/css/bootstrap.min.css','front/style.min.css','front/line-icons/line-icons.min.css','front/font-awesome/css/font-awesome.min.css','front/owl-carousel/owl-carousel/owl.carousel.min.css','front/sky-forms/version-2.0.1/css/custom-sky-forms.css','front/page_search.min.css','front/shop.style.min.css','front/plugins/scrollbar/src/perfect-scrollbar.min.css','front/plugins/noUiSlider/jquery.nouislider.min.css','front/page_job_inner1.min.css','front/plugins/jquery-steps/css/custom-jquery.steps.min.css','front/bvalidator.min.css','front/plugins/brand-buttons/brand-buttons.min.css','front/plugins/brand-buttons/brand-buttons-inversed.min.css','front/token-input-facebook.min.css','front/token-input-mac.min.css','front/token-input.min.css','front/bootstrap-tokenfield.min.css','front/header-default.min.css','front/f_custom.min.css'));
	
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
    <?php echo $this->Html->css(array('front/blue.css')); ?>
<?php } ?>
<?php /*
  <script id="_webengage_script_tag" type="text/javascript">
  var _weq = _weq || {};
  _weq['webengage.licenseCode'] = '~10a5cbc6b';
  _weq['webengage.widgetVersion'] = "4.0";

  (function(d) {
  var _we = d.createElement('script');
  _we.type = 'text/javascript';
  _we.async = true;
  _we.src = (d.location.protocol == 'https:' ? "https://ssl.widgets.webengage.com" : "http://cdn.widgets.webengage.com") + "/js/widget/webengage-min-v-4.0.js";
  var _sNode = d.getElementById('_webengage_script_tag');
  _sNode.parentNode.insertBefore(_we, _sNode);
  })(document);
  </script>
 * 
 */ ?>