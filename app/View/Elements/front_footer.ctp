<footer class="footer-v1">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-md-4 md-margin-bottom-40">
                    <a href="<?= Router::url('/', true); ?>">
                        <?= $this->Html->image('front/logo.png', array('class' => 'footer-logo', 'id' => 'logo-footer', 'style' => 'height:82px;')) ?>
                    </a>
                    <p>www.decisiondatabases.com is a online aggregator of market and research database which includes databases,
                        reports and profiles. We have started this portal with a aim to become one stop shop provider for marketing
                        and research data requirements for various sector and country databases.</p>    
                </div><!--/col-md-3-->
                <!-- End About -->

                <!-- Latest -->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="posts">
                        <div class="headline"><span>Main Categories</span></div>
                        <ul class="list-unstyled latest-list">
                            <?php
                            foreach ($main_category as $key => $cat):
                                ?>
                                <li>
                                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $key, 'slug' => $this->Link->cleanString($cat))); ?>"><?php echo $cat; ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                </div><!--/col-md-3-->  
                <!-- End Latest --> 

                <!-- Link List -->
                <div class="col-md-2 md-margin-bottom-40">
                    <div class="headline"><span>Useful Links</span></div>
                    <ul class="list-unstyled link-list">
                        <li><a href="<?php echo Router::url('/', true); ?>">Home</a></li> 
                        <li> <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'aboutus')); ?>">About Us</a> </li>
                        <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'contactus')); ?>">Contact Us</a></li>
                        <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'careers')); ?>">Careers</a></li>
                        <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'privacy_policy')); ?>">Privacy Policy</a></li>
                        <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'refundpolicy')); ?>">Refund Policy</a></li>
                        <li><a href="<?= Router::url(array('controller' => 'users', 'action' => 'terms_conditions')); ?>">Terms & Conditions</a></li>
                         <!--Code starts here-VG-19/08/2016-->
                        <li><a href="<?= Router::url(array('controller' => 'sitemap', 'ext'=>'xml')); ?>" target="_blank">sitemap</a></li>
                        <!--<li><a href="<?= Router::url(array('controller' => 'feeds', 'action' => 'feeds','ext'=>'xml')); ?>" target="_blank">Feeds</a></li>-->
                        <!--Code ends here-VG-19/08/2016-->
                    </ul>
                </div><!--/col-md-3-->
                <div class="col-md-3 md-margin-bottom-40">
                    <div class="headline"><span>Insights</span></div>
                    <ul class="list-unstyled link-list">
                        <li> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_listing','section'=>'press-releases')); ?>">Press release</a> </li>
                        <li> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_listing','section'=>'analysis')); ?>">Analysis</a> </li>
                        <li> <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'latest_market_reports')); ?>">Latest Market Reports</a> </li>
                        <li>
                          <span class="creditcards"></span><img src="<?php echo Router::url('/', true) ?>/img/logos/creditcards.png" alt="" class="img-responsive" style="height:48px; margin-top:30px;">                          
                        </li>

                    </ul>
                </div>
                <!-- End Link List -->                    
                <?php /*
                  <!-- Address -->
                  <div class="col-md-3 map-img md-margin-bottom-40">
                  <div class="headline"><h2>Contact Us</h2></div>
                  <address class="md-margin-bottom-40">
                  25, Lorem Lis Street, Orange <br />
                  California, US <br />
                  Phone: 800 123 3456 <br />
                  Fax: 800 123 3456 <br />
                  Email: <a href="mailto:info@anybiz.com" class="">info@anybiz.com</a>
                  </address>
                  </div><!--/col-md-3-->
                  <!-- End Address -->
                 * 
                 */ ?>
            </div>
        </div> 
    </div><!--/footer-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-3">                     
                    <p style="display: inline-block;">
                        <?php echo date("Y") ?> &copy; All Rights Reserved.
                    </p>
                    <?php //echo $this->Html->image('paypal.png', array('style' => array("display: inline-block;width: 70px;float: right;"))); ?>
                </div>
                <!-- Social Links -->
                <div class="col-md-9">
                    <ul class="footer-socials list-inline">
                        <li>
                            <a href="https://www.linkedin.com/company/decisiondatabases-com" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <!--<li>
                            <a href="https://plus.google.com/110413391982956710873" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Google Plus">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>-->
                        <li>
                            <a href="https://www.facebook.com/pages/DecisionDatabasescom/868788886510347" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/DD_Insights" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                        <!-- https://www.pinterest.com/search/boards/?q=decisiondatabases.com -->
                            <a href="https://in.pinterest.com/decisiondatabases/market-research-reports/" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Pinterest">
                                <i class="fa fa-pinterest-square"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Social Links -->
            </div>
        </div> 
    </div><!--/copyright-->
</footer>

<?php
    echo $this->Html->script(array(
        'front/plugins/jquery/jquery.min.js',
        'front/jquery.tokeninput.min.js',
        'front/jquery.bvalidator-yc.js',
        'front/plugins/jquery.parallax.min.js',
        'front/plugins/owl-carousel.min.js',
        //'front/jquery.fancybox.min.js',
        'front/plugins/bootstrap/js/bootstrap.min.js',
        'front/plugins/back-to-top.min.js',
        'front/app.min.js',
        'front/front_custom.min.js'
    ));
    ?>


 <!-- Google Captcha Code Start here VG-15/2/2015  -->
  <script src='https://www.google.com/recaptcha/api.js'></script>
  <script>
  
      $("#contact_us").submit(function(event) {

     var recaptcha = $("#g-recaptcha-response").val();
     if (recaptcha === "") {
        event.preventDefault();
        alert("Please check the recaptcha");
     }
   });
    $("#enquiry_form").submit(function(event) {

          var recaptcha = $("#g-recaptcha-response").val();
          if (recaptcha === "") {
              event.preventDefault();
              alert("Please check the recaptcha");
          }
       });
  </script>
  <!-- Google Captcha Code end here VG-15/2/2015  -->

<?php echo $this->Html->script(array('ga-code')); ?>
<!--Start of Zopim Live Chat Script-->
<script type="text/javascript">
    window.$zopim || (function (d, s) {
        var z = $zopim = function (c) {
            z._.push(c)
        }, $ = z.s =
                d.createElement(s), e = d.getElementsByTagName(s)[0];
        z.set = function (o) {
            z.set.
                    _.push(o)
        };
        z._ = [];
        z.set._ = [];
        $.async = !0;
        $.setAttribute("charset", "utf-8");
        $.src = "//v2.zopim.com/?2vMaeOWSrkq7wNCPHgcNhmuYo3G6tfws";
        z.t = +new Date;
        $.
                type = "text/javascript";
        e.parentNode.insertBefore($, e)
    })(document, "script");
</script>
<!--End of Zopim Live Chat Script-->


<!-- New autocomplete search code Start VG-25/11/2016  -->
<script type="text/javascript">
    $(document).ready(function(){
        var query=null;
        var oldquery=null;
        $('#search_text').on('keyup',function(e){ 
             
              query=$(this).val();
                if(query != ' ' || query.length > 0)
                {
                    if(e.keyCode !=38 && e.keyCode != 40)        
                    {          
                        $.ajax({
                               url: "<?php echo Router::url(['controller' => 'Categories', 'action' => 'home_search_autofill']);?>",
                                method:"POST",
                                async: true,
                                data:{query:query},
                                success:function(data)
                                {  
                                    $('#suggest').fadeIn();
                                    $('#suggest').html(data);
                                }
                                
                        });
                  }
                }
                else{
                     
                        $('#suggest').remove();
                } 
        });
        
                $(document).on('click', '#result', function(){  
                //$('#search_text').val($(this).text());  
                $('#suggest').fadeOut();  
            }); 

           $(document).on('mouseenter','#result',function(){
                  query=$("#search_text").val(); 
                  if(query != ' ' || query.length > 0)
                  {
                       $(this).prev().removeClass('selected');
                        $(this).addClass('selected');
                  }
                  else
                  {
                    $('#suggest').fadeOut(5000);  
                  }
            });
            $(document).on('mouseleave','#result',function(){
                $(this).removeClass('selected');
              
            });
    });
</script>
<!-- New autocomplete search code End VG-25/11/2016  -->
<script type="text/javascript">
    //$('#carousel').elastislide();
</script>

<script>

  function acceptCondition() {
    $('#viewTermsAndCodition').modal('hide');
    $('#enquiry_form').submit();
  }
  function notAcceptCondition() {
    $('#viewTermsAndCodition').modal('hide');
    $('#EnquiryTerm').prop("checked", false);
  }
 
  function openModal(te) {
    if($('#EnquiryTerm').is(':checked')) {
      $.ajax({
          url: $('#termsandcondition').val(),
      }).done(function(output) {      
          $("#viewTermsAndCodition").on("shown.bs.modal", function () {
              $(this).find('.modal-body').html(output)
          }).modal('show'); 
      });
    }
  }

</script>    

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" defer="defer">    