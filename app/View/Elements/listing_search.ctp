<style>
    .search-block {
        padding: 10px 0px;
        text-align: center;
        position: relative;
        margin-bottom: 20px;
        //background: url(../img/front/bg/7.jpg) 50% 0 repeat fixed;
    }
</style>

<div class="search-block">
    <div class="container">
        <div class="col-md-12">
            <?php //echo $this->Form->create('Category',array('controller' => 'categories', 'action' => 'home_search_form', 'id' => 'search_frm', 'role' => 'form')); ?>
                
                <!-- Old Textbox for search Start -->

                <?php //echo $this->Form->input('search_txt', array('class' => 'form-control text_box_height_list', 'id' => 'search_txt_id', 'label' => false)); ?>

                <!-- Old Textbox for search End -->  

                  <!-- New Textbox for Home Search Start VG-21-10-2016 -->
              <?php echo $this->Form->create('Category', array('type' => 'get','action'=>'home_search_form')); ?>
                  <?php  
                  $arr= explode("?",$_SERVER['REQUEST_URI']); //print_r($arr);//echo $this->base.'/search-results';
                  if($arr[0] != $this->base.'/search-results')
                      { 
                          echo $this->Form->input('q', array('class' => 'form-control text_box_height_list', 'id'=>'search_text', 'label' => false,'required','autocomplete'=>'off'));
                      }
                  else
                      {
                           echo $this->Form->input('q', array('class' => 'form-control text_box_height_list', 'id'=>'search_text', 'label' => false,'required','autocomplete'=>'off','value'=>$_GET['q']));     
                      }

                   ?>
                     <div class="col-md-1"  style=" margin-top: -49px;float: right;display: block;">
                            <?php echo $this->Form->submit('Search',array('class' => 'btn btn-default ','style'=>' height: 38px; width: 120%;border-radius: 0px;font-weight: 500;background: #666666; color: #ffffff;')); ?>
                     </div>
                    <div class="col-md-12 pull-left" id="suggest" style=" margin-top: -9px; clear:both; margin-left: -14px;overflow-x: hidden;position: absolute; z-index: 2;"></div>

                      <?php  echo $this->Form->end(); ?>
                
            <!-- New Textbox for Home Seacrch End VG-21-10-2016 -->            
        </div>
    </div>
</div>