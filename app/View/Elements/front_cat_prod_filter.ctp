<script type="text/javascript">
    $(document).ready(function () {
        $('#clearAll').click(function (event) {  //on click 
            $('.check_uncheck').each(function () { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                       
            });
        });

        $('.check_uncheck').click(function () {
            $('#filter_form').submit();
        });
    });
</script>
<style>
    .filter-by-block .panel-group span a {
        color: #FFF;
        display: block;
        font-size: 14px;
        border-bottom: 1px solid #dedede;
        background-color: #005994;
        padding: 5px;
        text-decoration: none;
    }
</style>
<!--/navbar-collapse-->
<div class="col-md-3 filter-by-block md-margin-bottom-80">
    <div>
        <button type="button" class="navbar-toggle cust_filter" title="Hide Filter" data-toggle="collapse" data-target=".navbar-responsive-collapse_hide">
            <span class="fa fa-bars"></span>
        </button>
        <div class="navbar-responsive-collapse_hide">
            <?php echo $this->Form->create('Product', array('class' => 'form-horizontal', 'id' => 'filter_form', 'role' => 'form')); ?>       
            <?php if (isset($cat_data['Level1'][0]['Level2'])) {
                ?>
                <div class="panel-group" id="accordion">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <span class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne_cat">
                                    Categories
                                    <i class="fa fa-angle-down"></i>
                                </a>
                            </span>
                        </div>
                        <div id="collapseOne_cat" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <ul class="list-unstyled checkbox-list">
                                    <div class="accordion-inner">
                                        <?php
                                        echo $cat_data['Level1'][0]['category_name'];
                                        foreach ($cat_data['Level1'] as $key1 => $cat):
                                            ?>
                                            <?php
                                            if (!empty($cat['Level2'])) {
                                                ?>
                                                <ul class="list-unstyled">
                                                    <?php foreach ($cat['Level2'] as $key2 => $cat2): ?>
                                                        <li><i class="fa fa-angle-double-right color-green"></i>
                                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat2['id'], 'slug' => $this->Link->cleanString($cat2['category_name']))); ?> "><?php echo $cat2['category_name'] ?> (<?php echo $cat2['no_of_product'] ?>)</a>
                                                            <?php if (!empty($cat2['Level3'])) { ?>
                                                                <ul>
                                                                    <?php foreach ($cat2['Level3'] as $key3 => $cat3): ?>
                                                                        <li><i class="fa fa-angle-double-right color-green"></i>   <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat3['id'], 'slug' => $this->Link->cleanString($cat3['category_name']))); ?>"><?php echo $cat3['category_name'] ?> (<?php echo $cat3['no_of_product'] ?>)</a>
                                                                            <?php if (!empty($cat3['Level4'])) { ?>
                                                                                <ul>
                                                                                    <?php foreach ($cat3['Level4'] as $key4 => $cat4): ?>
                                                                                        <li class="list-unstyled">
                                                                                            <i class="fa fa-angle-double-right color-green"></i>
                                                                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat4['id'], 'slug' => $this->Link->cleanString($cat4['category_name']))); ?>"><?php echo $cat4['category_name'] ?> (<?php echo $cat4['no_of_product'] ?>)</a> 
                                                                                            <?php if (!empty($cat4['Level5'])) { ?>
                                                                                                <ul>
                                                                                                    <?php foreach ($cat4['Level5'] as $key5 => $cat5): ?>
                                                                                                        <li class="list-unstyled">
                                                                                                            <i class="fa fa-angle-double-right color-green"></i>
                                                                                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat5['id'], 'slug' => $this->Link->cleanString($cat5['category_name']))); ?>"><?php echo $cat5['category_name'] ?> (<?php echo $cat5['no_of_product'] ?>)</a> 
                                                                                                            <?php if (!empty($cat5['Level6'])) { ?>
                                                                                                                <ul>
                                                                                                                    <?php foreach ($cat5['Level6'] as $key6 => $cat6): ?>
                                                                                                                        <li class="list-unstyled">
                                                                                                                            <i class="fa fa-angle-double-right color-green"></i>
                                                                                                                            <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $cat6['id'], 'slug' => $this->Link->cleanString($cat6['category_name']))); ?>"><?php echo $cat6['category_name'] ?> (<?php echo $cat6['no_of_product'] ?>)</a>
                                                                                                                        </li>
                                                                                                                    <?php endforeach; ?>
                                                                                                                </ul>
                                                                                                            <?php } ?>
                                                                                                        </li>
                                                                                                    <?php endforeach; ?>
                                                                                                </ul>
                                                                                            <?php } ?>
                                                                                        </li>
                                                                                    <?php endforeach; ?>
                                                                                </ul>
                                                                            <?php } ?>
                                                                        </li>
                                                                    <?php endforeach; ?>
                                                                </ul>
                                                            <?php } ?>
                                                        <?php endforeach; ?>
                                                    </li>
                                                </ul>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </div>
                                </ul>        
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
            <?php /* if ((isset($regions) && (!empty($regions)))) { ?>
              <div class="panel-group" id="accordion">
              <div class="panel panel-default">
              <div class="panel-heading">
              <h2 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
              Region
              <i class="fa fa-angle-down"></i>
              </a>
              </h2>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
              <div class="panel-body">
              <ul class="list-unstyled checkbox-list">
              <div class="accordion-inner scroll-list_filter">
              <?php foreach ($regions as $regkey => $region) { ?>
              <li>
              <label>
              <?php
              echo $this->Form->checkbox('Region.' . $regkey, array('multiple' => 'checkbox', 'class' => 'check_uncheck', 'hiddenField' => false, 'value' => $regkey));
              ?>
              <?php echo $region; ?>
              </label>
              </li>
              <?php } ?>
              </div>
              </ul>
              </div>
              </div>
              </div>
              </div>
              <?php } ?>
              <!--/end panel group-->
              <?php if ((isset($regions) && (!empty($regions)))) { ?>
              <div class="panel-group" id="accordion-v2">
              <div class="panel panel-default">
              <div class="panel-heading">
              <h2 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion-v2" href="#collapseTwo">
              Countries
              <i class="fa fa-angle-down"></i>
              </a>
              </h2>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse in">
              <div class="panel-body">
              <ul class="list-unstyled checkbox-list">
              <div class="accordion-inner scroll-list_filter">
              <?php foreach ($countries as $coukey => $country) { ?>
              <li>
              <label>
              <?php
              echo $this->Form->checkbox('Country.' . $coukey, array('class' => 'check_uncheck', 'hiddenField' => false, 'value' => $coukey));
              ?>
              <?php echo $country; ?>
              </label>
              </li>
              <?php } ?>
              </div>
              </ul>
              </div>
              </div>
              </div>
              </div>
              <?php } ?>
              <?php foreach ($specification_options as $filter_spkey => $spec_options): ?>
              <div class="panel-group" id="accordion-<?php echo $filter_spkey; ?>">
              <div class="panel panel-default">
              <div class="panel-heading">
              <h2 class="panel-title">
              <a data-toggle="collapse" data-parent="#accordion-<?php echo $filter_spkey; ?>" href="#collapseTwo<?php echo $filter_spkey; ?>">
              <?php echo ucfirst($spec_options['specification_name']); ?>
              <i class="fa fa-angle-down"></i>
              </a>
              </h2>
              </div>
              <div id="collapseTwo<?php echo $filter_spkey; ?>" class="panel-collapse collapse in">
              <div class="panel-body">
              <ul class="list-unstyled checkbox-list">
              <div class="accordion-inner scroll-list_filter">
              <?php foreach ($spec_options['specification_options'] as $opkey => $option) {
              ?>
              <li>
              <label>
              <?php
              echo $this->Form->checkbox('SpecificationOption.' . $opkey, array('hiddenField' => false, 'class' => 'check_uncheck', 'value' => $opkey));
              ?>
              <?php echo $option; ?>
              </label>
              </li>
              <?php } ?>
              </div>
              </ul>
              </div>
              </div>
              </div>
              </div>
              <?php endforeach; ?>
             * 
             */ ?>
            <?php echo $this->Form->end(); ?>
            <!--/end panel group-->
            <?php /* if ((isset($regions) && (!empty($regions)))) { ?>
                <button type="button" id="clearAll" class="btn-u btn-brd btn-brd-hover btn-u-lg btn-u-sea-shop btn-block">Reset Filters</button>
            <?php } */ ?> 
        </div>
    </div>
</div>