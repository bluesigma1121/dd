<!--<script type="text/javascript">
    //var filter = "<? // $filters['stream'];                                                             ?>";
    $(document).ready(function() {
        $('#curr').change(function() {
            $('#change_currency').submit();
        });
    });
</script> -->

<script>
    function goBack() {
        window.history.back()
    }
</script>

<header class="header-v1 header-sticky">
    <!-- Topbar -->
    <div class="topbar-v1">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline top-v1-contacts">
                        <li>
                            <i class="fa fa-envelope"></i> Email: <a href="mailto:sales@decisiondatabases.com">sales@decisiondatabases.com</a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/decisiondatabases-com" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Linkedin">
                                <i class="fa fa-linkedin"></i>
                            </a>
                        </li>
                        <!--<li>
                            <a href="https://plus.google.com/110413391982956710873" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Google Plus">
                                <i class="fa fa-google-plus"></i>
                            </a>
                        </li>-->
                        <li>
                            <a href="https://www.facebook.com/pages/DecisionDatabasescom/868788886510347" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/DD_Insights" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                        <!-- https://www.pinterest.com/search/boards/?q=decisiondatabases.com -->
                            <a href="https://in.pinterest.com/decisiondatabases/market-research-reports/" target="_blank" class="tooltips" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="Pinterest">
                                <i class="fa fa-pinterest-square"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <ul class="list-inline top-v1-data">
                    <!--<li><a href="#"><i class="fa fa-phone"></i> US Toll free : +1-646-5869239</a></li>-->

<!--                        <li><a href="<?php //echo Router::url(array('controller' =>         'cart_items', 'action' => 'add_to_cart')); ?>"><i class="fa fa-shopping-cart">  
</i> Cart (<?php //echo $cart_count; ?>)</a></li> -->
                        <?php if (AuthComponent::user('role') == 11) { ?>
                            <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'my_account', AuthComponent::user('id'))); ?>"><strong> Welcome  </strong><?php echo ucfirst(AuthComponent::user('first_name')); ?></a></li>
                            <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'my_account', AuthComponent::user('id'))); ?>"><strong> My Account </strong></a></li>
                            <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'logout')); ?>">Logout</a></li>
                        <?php } else { ?>
                           <!-- <li><a href="<?php //echo Router::url(array('controller' => 'users', 'action' => 'login')); ?>">Login</a></li>-->
<!--                      <li><a href="<?php // echo Router::url(array('controller' =>  'users',                       'action' => 'registration')); ?>">Register</a></li> 
                        <?php } ?> -->
                    </ul>
                </div>
            </div>        
        </div>
    </div>
    <!-- End Topbar -->
    <!-- Navbar -->
    <div class="navbar navbar-default mega-menu" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="fa fa-bars"></span>
                </button>
                <a class="navbar-brand" href="<?php echo Router::url('/', true); ?>" style="padding-top: 00px;">
                    <?= $this->Html->image('front/logo.png', array('id' => 'logo-header', 'alt' => 'logo', 'style' => 'height:60px;')) ?>
                </a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-responsive-collapse">
                <ul class="nav navbar-nav">
                    <!-- Home -->
                    <li>
                        <a href="<?php echo Router::url('/', true); ?>">Home</a> 
                    </li> 
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'category_list')); ?>">Categories</a> 
                    </li>
                    <li>
                        <a href="<?php echo Router::url('/', true); ?>reports/3-company-profiles">SWOT ANALYSIS</a> 
                    </li>
                    <li>
                        <a href="http://blog.decisiondatabases.com/">Blog</a> 
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'aboutus')); ?> ">About Us</a> 
                    </li>
                    <li>
                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'contactus')); ?> ">Contact Us</a> 
                    </li>

                    <!-- End Home -->
                </ul>
            </div> <!--/navbar-collapse-->
        </div>    
    </div>            
    <!-- End Navbar -->
</header>
