<style type="text/css">
    .desg{
        min-height: 232px;
        padding: 10px;
        /* min-width: 250px; */
        background-color: #f7f7f7;
        width:204px;
    }
    .profile-body{
        padding: 20px;
        background: #f7f7f7;
    }
    .detail_li{
        padding: 2px;
        border-left: 1px solid #72c02c;
    }
    #first{
        border-left:none !important;
    }
    .main-content{

    }
    .search-block {
        padding: 0;
        text-align: center;
        position: relative;
        margin-bottom: 20px;
        background: url(../img/front/bg/7.jpg) 50% 0 repeat fixed;
    }
    .content{
        padding-top: 0px !important;
    }
    .font_clr{
        color: #72c02c;
    }
    .h4_fnt{
        font-size: 16px;
    }
    li a{
        color:#060606;
    }
    .panel-group{
        margin-bottom: 8px;
    }
    .margin_top_search{
        margin-top: 20px;
    }

    .pagination>li>a, .pagination>li>span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #428bca;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
        height: 30px !important;
        width: 40px !important;
    }
    .pagination li a:hover {
        color: #5fb611;
        background: none;
        border-color: none;

    }
    .pagination li a {
        margin-left: -11px;
    } 

    .contex-bg p {
        opacity: 0.8;
        padding: 8px 10px;
    } 
    .m_top{
        margin-top: 10px;
    } 
    .mar_top_bot_pag{
        margin-top: -34px;
        margin-bottom: -30px;
    } 
    .mar_tb1{
        margin-bottom: -14px;
    }
    .img-thumbnail {
        display: inline-block;
        width: 100% \9;
        max-width: 100%;
        height: auto;
        padding: 4px;
        line-height: 1.42857143;
        background-color: #fff;
        border: none !important;
        border-radius: 4px;
        -webkit-transition: all .2s ease-in-out;
        -o-transition: all .2s ease-in-out;
        transition: all .2s ease-in-out;
    }
</style>

<script type="text/javascript">
    //var filter = "<? // $filters['stream'];                                                             ?>";
    $(document).ready(function() {
        $('#limit_search').change(
                function() {
                    $('#search_products').submit();
                });
        $('.send_enquery').fancybox({
            fitToView: false,
            autoSize: true,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            type: 'ajax',
        });
    });
</script>

<?php echo $this->element('listing_search'); ?>

<?php echo $this->element('front_breadcumb'); ?>
<!--=== Content ===-->
<div class="content container">
    <div class="row">
        <?php echo $this->element('front_cat_prod_filter'); ?>
        <div class="col-md-9">
            <!--/end pagination-->
            <h2><?php echo $selected_cat['Category']['category_name']; ?></h2>
            <p><?php
                echo $selected_cat['Category']['short_desc'];
                ?>
            </p>
            <?php if (!empty($products)) { ?>

                <div class = "filter-results" style = "padding-top:0px;">
                    <div class = "row">
                        <div class="col-sm-4 margin_top_search">
                            <?php echo $this->Form->create('Product', array('id' => 'search_products', 'class' => 'form-horizontal'));
                            ?>
                            <div class="col-sm-5">
                                <?php
                                echo $this->Form->input('limit_of_records', array(
                                    'options' => $limits,
                                    'class' => 'form-control search-query',
                                    'id' => 'limit_search', 'label' => false,
                                    'selected' => $limit_per_page,
                                ));
                                ?>
                            </div>
                            Records Per Page
                            <?php echo $this->Form->end(); ?>
                        </div>
                        <?php
                        if (isset($this->params['paging']['ProductCategory']['pageCount'])) {
                            $page_cnt = $this->params['paging']['ProductCategory']['pageCount'];
                        } elseif (isset($this->params['paging']['Product']['pageCount'])) {
                            $page_cnt = $this->params['paging']['Product']['pageCount'];
                        }
                        ?>

                        <div class="col-sm-8">
                            <?php if ($page_cnt > 1) { ?>
                                <div class="text-center mar_tb ">
                                    <ul class="pagination pagination-v2 pagination-sm" style="visibility: visible;"> 
                                        <?php
                                        $this->Paginator->options(array('url' => array('controller' => 'products',
                                                'action' => 'category_listing', 'id' => $selected_cat['Category']['id'],
                                                'slug' => $this->Link->cleanString($selected_cat['Category']['category_name']),
                                                'page' => 1, 'strPage' => 'page'
                                        )));
                                        ?> 

                                        <li><?php echo $this->Paginator->prev(__('<'), array(), null, array('class' => 'prev disabled')); ?></li>
                                        <li><?php echo $this->Paginator->numbers(array('separator' => '')); ?></li>
                                        <li><?php echo $this->Paginator->next(__('>'), array(), null, array('class' => 'next disabled')); ?></li>
                                    </ul>
                                </div>
                            <?php } ?>
                            <div class="text-center mar_tb"> 
                                <?php /*
                                  <p class="pull-right">
                                  <?php
                                  echo $this->Paginator->counter(array(
                                  'format' => __('{:start} to {:end} of  {:count} items ')
                                  ));
                                  ?> </p>
                                 * 
                                 */ ?>
                            </div>
                        </div>

                    </div>
                    <div>
                        <p>
                            <?php
                            echo $this->Paginator->counter(array(
                                'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                            ));
                            ?> 
                        </p>
                        <?php foreach ($products as $key => $product) {
                            ?>
                            <div class="list-product-description product-description-brd margin-bottom-30 m_top">
                                <div class="row ">
                                    <div class="col-sm-2">
                                        <?php /*
                                          <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'id' => $product['Product']['id'], 'slug' => $this->Link->cleanString($product['Product']['product_name']))); ?> ">
                                         */ ?>
                                        <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($product['main_cat']['slug']), 'id' => $product['Product']['id'], 'slug' => $product['Product']['slug'])); ?> ">
                                            <?php if (!empty($product['Product']['product_image'])) { ?>
                                                <?php
                                                echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size'));
                                            } else {
                                                echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                                            }
                                            ?>
                                        </a>
                                    </div>
                                    <div class="col-sm-10">
                                        <div class="overflow-h margin-bottom-5" style="margin-bottom:0px;">
                                            <ul class="list-inline overflow-h">
                                                <li><h4 class="title-price h4_fnt"><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($product['main_cat']['slug']), 'id' => $product['Product']['id'], 'slug' => $product['Product']['slug'])); ?> "> <?php echo $product['Product']['product_name']; ?></a></h4></li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <div class="margin-bottom-10d">
                                                        <span class="title-price margin-right-10 font_clr">
                                                            <?php $final_price = round(($product['Product']['price'] * $dollar_rate), 2); ?>
                                                            <?php if ($cur_curency == 0): ?>
                                                                <b><i class="fa fa-inr"></i> </b><?php echo $final_price; ?>
                                                            <?php else: ?>
                                                                <b><i class="fa fa-dollar"></i> </b>
                                                                <?php echo number_format($final_price, 2); ?>
                                                            <?php endif; ?>
                                                        </span>
                                                    </div> 
                                                </div>
                                                <div class="col-lg-5"> 
                                                    <div class="pull-right">
                                                        <?php
                                                        echo $this->Html->link(__('<i class="fa fa-bell-o"></i> Send Inquiry'), array('controller' => 'enquiries', 'action' => 'send_enquiry', $product['Product']['id'], $selected_cat['Category']['id'], 'category_listing'), array('class' => 'btn-u btn-u-green send_enquery', 'escape' => false, 'title' => 'Send Inquiry'));
                                                        ?>                               
                                                    </div>
                                                    <?php echo $this->Form->create('CartItem', array('controller' => 'cart_items', 'action' => 'cart', 'class' => '', 'id' => 'cart_frm')) ?>               
                                                    <?php echo $this->Form->input('product_id', array('type' => 'hidden', 'value' => $product['Product']['id'])) ?>
                                                    <?php echo $this->Form->button('<i class="fa fa-shopping-cart"></i> Add to Cart', array('class' => 'btn-u btn-u-blue', 'escape' => false)); ?>
                                                    <?php echo $this->Form->end(); ?>
                                                </div>
                                                <div class="col-lg-3"></div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p class="margin-bottom-20d" style="margin-bottom:0px;">
                                                        <?php
                                                        echo $this->Text->truncate(
                                                                strip_tags($product['Product']['product_description']), 150, array(
                                                            'ellipsis' => '...',
                                                            'exact' => false
                                                                )
                                                        );
                                                        ?>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
                                </div>
                            </div>   
                        <?php } ?>
                    </div>
                </div>
                <?php if ($page_cnt > 1) { ?>
                    <div class="text-center mar_top_bot_pag"> 
                        <?php
                        $this->Paginator->options(array('url' => array('controller' => 'products',
                                'action' => 'category_listing', 'id' => $selected_cat['Category']['id'],
                                'slug' => $this->Link->cleanString($selected_cat['Category']['category_name']),
                                'page' => 1, 'strPage' => 'page'
                        )));
                        ?> 
                        <ul class="pagination pagination-v2" style="visibility: visible;">
                            <li><?php echo $this->Paginator->prev(__('<'), array(), null, array('class' => 'prev disabled text-center')); ?></li>
                            <li><?php echo $this->Paginator->numbers(array('separator' => '')); ?></li>
                            <li><?php echo $this->Paginator->next(__('>'), array(), null, array('class' => 'next disabled text-center')); ?></li>
                        </ul> 
                    </div> 
                <?php } ?>
                <div class="text-center m_top"> 
                    <p>
                        <?php
                        echo $this->Paginator->counter(array(
                            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                        ));
                        ?> 
                    </p>
                </div> 
            <?php } else {
                ?>
                <div class = "filter-results" style = "padding-top:0px;">
                    <div class = "row">
                        <div class="col-sm-12  contex-bg">
                            <div class="alert alert-warning text-center">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <strong>Products Not Found </strong>
                            </div>
                        </div>
                        <div class="col-sm-12 m_top">
                            <div class="text-center">
                                <button class="btn-u"onclick="goBack()">Go Back</button>
                            </div>
                        </div>

                    </div>
                </div>
            <?php }
            ?>

        </div>
    </div><!--/end row-->
</div>