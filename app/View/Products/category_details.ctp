<style>
.tab-content div img {
    width: 600px!important;
    height: auto!important;
    margin-left: calc(100% - 652px)!important;
	vertical-align: middle!important;
}


.tab-content div {
    /* font-size: 14px; */
    text-align: left!important;
    /* color: #363636; */
}
.job-description .left-inner img {
	/* float: left; */
    float: none !important;
   
	/* width: 126px; */
	width: 600px!important;
    height: auto!important;
	/* height: auto; */
	padding: 3px;
	margin: 0 10px 10px 0;
    border: 0 !important;
	/* border: 1px solid #eee; */
}
@media (max-width: 450px) and (min-width: 20px)
{
.tab-content div img {
    width: 265px!important;
    height: auto!important;
    margin-left: calc(100% - 262px)!important;
    margin: 0!important;
}
.job-description .left-inner img {
	/* float: left; */
    float: none !important;
    border: 0 !important;
	/* width: 126px; */
	width: 285px!important;
    height: auto!important;
	/* height: auto; */
	padding: 3px;
	margin: 0 10px 10px 0;
	/* border: 1px solid #eee; */
}
}

</style>

<?php echo $this->Html->css(array('front/product_description.css')); ?>
<?php
$my_cat_color = array(
    1 => 'primary',
    2 => 'success',
    3 => 'info',
    4 => 'warning',
        )
?> 
<script type="text/javascript">
       $(document).ready(function () {
        $('#enquiry_form').bValidator();
    });
</script>

<!--=== Search Block ===-->
<?php echo $this->element('listing_search'); ?>    
<!--=== End Search Block ===--> 
<?php echo $this->element('front_breadcumb'); ?>    
<div class="job-description">
    <div class="container content mgr_tp_pd">  
        <div class="row">
            <!-- Left Inner -->
            <div class="col-md-8">
                <div class="left-inner section-block">
                    <div class="row">
                        <div class="col-md-2 margin_bot_home">
                            <?php if (!empty($product['Product']['product_image'])) { ?>
                                <?php
                                echo $this->Html->image('product_images/' . $product['Product']['product_image'], array('class' => 'img-thumbnail image_size_home'));
                            } else {
                                echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size_home'));
                            }
                            ?>
                        </div>
                        <div class="col-md-10 margin_bot_home">
                            <h1 class="product-heading">
                                <strong><?php echo $product['Product']['product_name']; ?></strong>
                            </h1>
                            <ul class="details list-inline">
                                <li id="first">ID:<?php echo $product['Product']['product_no']; ?></li>
                                <?php if (!empty($product['Product']['pub_date']) && $product['Product']['pub_date'] != "0000-00-00" && $product['Product']['pub_date'] != "1970-01-01"): ?>
                                    <li class="spart_pd"><?php echo Date('F Y', strtotime($product['Product']['pub_date'])); ?></li>
                                <?php else: ?>
                                    <li class="spart_pd"><?php echo Date('F Y', strtotime("+1 month")); ?></li>
                                <?php endif; ?>
                                <?php if (!empty($product['Product']['publisher_name'])) { ?>
                                    <li class="spart_pd"><?php echo $product['Product']['publisher_name']; ?></li>
                                <?php } ?>
                            </ul>

                            <?php //echo $this->Form->create('CartItem', array('controller' => 'cart_items', 'action' => 'cart', 'class' => '', 'id' => 'cart_frm')) ?>
                            
                              <div class="row" style="margin-top: 15px;">
                                  <?php /*
                              <div class="col-md-12">
                              <?php 
                              $options = $licence_type;
                              $attributes = array(
                              'legend' => false, 'separator' => '&nbsp;&nbsp;&nbsp;',
                              'data-bvalidator' => 'required', 'value' => 1);
                              ?>
                              <?php echo $this->Form->radio('licence_type', $options, $attributes); ?>
                              </div>*/ ?>
                              </div> 
                            <div class="row">
                                <div class="col-md-12">
                                    <?php foreach($licence_type as $key=>$value): ?>
                                    <label><?php echo $value; ?></label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <?php endforeach; ?>
                                </div>
                            </div> 
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-9"> 
                                    <?php //echo $this->Form->input('product_id', array('type' => 'hidden', 'value' => $product['Product']['id'])) ?>
                                    <?php //echo $this->Form->submit('Buy Now_FOrm', array('class' => 'btn-u btn-u-blue', 'div' => false)); ?>
                                    <?php
                                    echo $this->Html->link('Buy Now', array('controller' => 'enquiries', 'action' => 'get_lead_info_form','id' => $product['Product']['id'], 'ref_page' => 'buy-now'), array("class" => "btn-u btn-u-primary", "target" => "_blank"));
                                    ?>
                                    <?php
                                    echo $this->Html->link('Download Sample', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'id' => $product['Product']['id'], 'ref_page' => 'download-sample'), array("class" => "btn-u btn-u-green", "target" => "_blank")
                                    );
                                    ?>
                                    <?php
                                    echo $this->Html->link('Ask Questions', array('controller' => 'enquiries', 'action' => 'get_lead_info_form', 'id' => $product['Product']['id'], 'ref_page' => 'ask-questions'), array("class" => "btn-u btn-u-purple", "target" => "_blank")
                                    );
                                    ?>
                                </div>
                            </div>
                            <?php //echo $this->Form->end(); ?>
                        </div>

                        <hr style="margin:10px;">
                        <div class="tab-v1">
                            <ul class="nav nav-justified nav-tabs">
                                <li class="active"><a data-toggle="tab" href="#profile">Description</a></li>

                                <li><a data-toggle="tab" href="#passwordTab">Table Of Content</a></li>
                                <?php if (!empty($product['Product']['related_news'])) { ?>
                                    <li><a data-toggle="tab" href="#relatednewsshow">Related News</a></li>
                                <?php } ?>

                                <?php if (!empty($product['Product']['summary'])) { ?>
                                    <li><a data-toggle="tab" href="#prod_summary">Summary</a></li>
                                <?php } ?>

                                <?php if (!empty($product['Product']['glossary'])) { ?>
                                    <li><a data-toggle="tab" href="#glossary">Glossary</a></li>
                                <?php } ?>

                                <?php if (!empty($product['Product']['customization'])) { ?>
                                    <li><a data-toggle="tab" href="#customization">Customizations</a></li>
                                <?php } ?>

                                <?php if (!empty($product['Product']['industry_analysis'])) { ?>
                                    <li><a data-toggle="tab" href="#industry_analysis">Industry Analysis</a></li>
                                <?php } ?>

                                <?php if (!empty($product['Product']['latest_dev'])) { ?>
                                    <li><a data-toggle="tab" href="#latest_dev">Latest Developments</a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">

                               <div id="profile" class="profile-edit tab-pane fade in active margin_p_pd">
                                    <p>

                                    
                                        <?php
                                        $testdespr = str_replace("&lt;img", "<img", $product['Product']['product_description']);
                                        $testdespr = str_replace("/&gt;", "/>", $product['Product']['product_description']);
                                        $testdespr = str_replace('&quot;', '"', $product['Product']['product_description']);
                                        //$test = str_replace('&quot;',"'",$product['Product']['product_description']);
                                        $testdespr = html_entity_decode($testdespr);
                                        echo $testdespr;
                                        //echo $product['Product']['product_description']; ?>
                                        <?php //echo "<b>".$product['Product']['product_description']."</b>"; ?>
                                        <?php
                                        //$new_exploded_arr = explode(".", $product['Product']['product_description']);

                                         //$str=$product['Product']['product_description'];
                                        // $first_line = explode('<br>', $product['Product']['product_description'])[0];
                                        // $new_str = str_replace($first_line,'<b>'.$first_line.'</b>',$product['Product']['product_description']);
                                      
                                        //$first_line = explode(PHP_EOL, $str)[0];
                                       // $new_str = str_replace($first_line,'<b>'.$first_line.'</b>',nl2br($str));
                                       /*$strlen=strlen($str);

            echo "<pre>";
            for($i=0;$i<524;$i++)
            {
                echo $new_str = '<b>'.$str[$i].'</b>';
               
            }
            for($j=524;$j<1257;$j++)
            {
           // echo $new_str1 = '<b>'.$str[$j].'</b>';
            echo $new_str1 = $str[$j];
            }
            for($t=1257;$t<1278;$t++)
            {
            echo $new_str2 = '<b>'.$str[$t].'</b>';
            }

            for($p=1278;$p<1599;$p++)
            {
            echo $new_str3 = $str[$p];
            }
            for($q=1599;$q<1665;$q++)
            {
            echo $new_str4 = '<b>'.$str[$q].'</b>';
            }
            for($r=1665;$r<1721;$r++)
            {
            echo $new_str5 = $str[$r];
            }
            for($s=1721;$s<1788;$s++)
            {
            echo $new_str6 = '<b>'.$str[$s].'</b>';
            }
            for($u=1788;$u<1815;$u++)
            {
            echo $new_str7 = $str[$u];
            }
            for($v=1815;$v<1853;$v++)
            {
            echo $new_str8 =  '<b>'.$str[$v].'</b>';
            }
            for($w=1853;$w<2336;$w++)
            {
            echo $new_str9 =  $str[$w];
            }
            for($x=2336;$x<2491;$x++)
            {
            echo $new_str10 =  '<b>'.$str[$x].'</b>';
            }
            for($y=2491;$y<2764;$y++)
            {
            echo $new_str11 =  $str[$y];
            }
            for($z=2764;$z<2838;$z++)
            {
            echo $new_str12 =  '<b>'.$str[$z].'</b>';
            }
            for($a=2838;$a<3302;$a++)
            {
            echo $new_str13 =  $str[$a];
            }
            for($b=3302;$b<3362;$b++)
            {
            echo $new_str14 =  '<b>'.$str[$b].'</b>';
            }
            for($c=3362;$c<3907;$c++)
            {
            echo $new_str14 =  $str[$c];
            }
            for($d=3907;$d<3976;$d++)
            {
            echo $new_str15 =  '<b>'.$str[$d].'</b>';
            }
            for($e=3976;$e<$strlen;$e++)
            {
            echo $new_str16 =  $str[$e];
            }*/
                                        
                                        //echo "<pre>";
                                        //print_r($new_str);
                                       // exit;
                                        ?>
                                        <?php
                                        // $str=$product['Product']['product_description'];
                                        // $Words = explode(".", $str);
                                        // //$Words = explode(array(".",":"), $str);
                                        // $WordCount = count($Words);
                                        // $NewSentence = '';
                                        // for ($i = 0; $i < $WordCount; ++$i) {
                                        //     if ($i <2) {
                                        //         $NewSentence .= '<strong>' . $Words[$i] . '</strong>';
                                        //     } else {
                                        //         $NewSentence .= $Words[$i] . ' ';
                                        //     }
                                        // }
                                        // echo $NewSentence;
                                      // echo "<pre>";
                                        //print_r($NewSentence);
                                        ?>
                                    </p>
                                </div>
 						   
  

                                <div id="relatednewsshow" class="profile-edit tab-pane fade in  margin_p_pd">
                                    <p>
                                        <?php
                                        if (!empty($product['Product']['related_news'])) {
                                            echo $product['Product']['related_news'];
                                        }
                                        ?>
                                    </p>
                                </div>

                                <div id="prod_summary" class="profile-edit tab-pane fade in  margin_p_pd">
                                    <p>
                                        <?php
                                        if (!empty($product['Product']['summary'])) {
                                            echo $product['Product']['summary'];
                                        }
                                        ?>
                                         
                                    </p>
                                </div>

                                <div id="glossary" class="profile-edit tab-pane fade in  margin_p_pd">
                                    <p>
                                        <?php
                                        if (!empty($product['Product']['glossary'])) {
                                            echo $product['Product']['glossary'];
                                        }
                                        ?>
                                    </p>
                                </div>




                                <div id="customization" class="profile-edit tab-pane fade in  margin_p_pd">
                                    <p>
                                        <?php
                                        if (!empty($product['Product']['customization'])) {
                                            echo $product['Product']['customization'];
                                        }
                                        ?>
                                    </p>
                                </div>
                                <div id="industry_analysis" class="profile-edit tab-pane fade in  margin_p_pd">
                                    <p>
                                        <?php
                                        if (!empty($product['Product']['industry_analysis'])) {
                                            echo $product['Product']['industry_analysis'];
                                        }
                                        ?>
                                    </p>
                                </div>
                                <div id="latest_dev" class="profile-edit tab-pane fade in  margin_p_pd">
                                    <p>
                                        <?php
                                        if (!empty($product['Product']['latest_dev'])) {
                                            echo $product['Product']['latest_dev'];
                                        }
                                        ?>
                                    </p>
                                </div>
                                <div id="passwordTab" class="profile-edit tab-pane fade margin_p_pd">
                                    <?php if ($product['Product']['is_set_toc'] == 0) { ?>
                                        <p>
                                            <?php echo $product['Product']['product_specification']; ?>
                                        </p>
                                    <?php } else { ?>
                                    <br>
                                       <center>       
                                       <h3>To receive Table of Contents, please write to us at <br> <small>  <a href='mailto:sales@decisiondatabases.com'>sales@decisiondatabases.com</a></small></h3>
                                       </center>                                       <!--  <?php echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
                                        <div class="reg-header mar_top_hed">
                                            <span class="section-heading">Request TOC</span>
                                        </div> 
                                        <?php if (!AuthComponent::user('id')): ?>
                                            <div class="row">
                                                <div class="col-sm-2">
                                                    <label>Title<span class="color-red">*</span></label> 
                                                    <?php echo $this->Form->input('title', array('class' => 'form-control', 'type' => 'select', 'options' => $title, 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>First Name<span class="color-red">*</span></label> 
                                                    <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'First Name', 'data-bvalidator' => 'required,alpha','autocomplete'=>'off')); ?>
                                                </div>
                                                <div class="col-sm-4">
                                                    <label>Last Name<span class="color-red">*</span></label>
                                                    <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Last Name', 'data-bvalidator' => 'required,alpha','autocomplete'=>'off')); ?>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-sm-6">
                                                    <label>Organization <span class="color-red">*</span></label> 
                                                    <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Organisation', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                                                </div>

                                                <div class="col-sm-6">
                                                    <label>Designation <span class="color-red">*</span></label> 
                                                    <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Designation', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                                                </div> 
                                                <div class="col-sm-8">
                                                    <label>Email Address <span class="color-red">*</span></label>
                                                    <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Email', 'data-bvalidator' => 'required,email','autocomplete'=>'off')); ?>
                                                </div>

                                            </div>
                                        <?php endif; ?>
                                        <?php /* <div class="row">
                                          <div class="col-sm-6">
                                          <label>Mobile <span class="color-red">*</span></label>
                                          <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => '+_(___)___-__-__', 'id' => 'customer_phone', 'value' => AuthComponent::user('mobile'), 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required')); ?>
                                          </div>
                                          <div class="col-sm-6">
                                          <div class="mar_phone">
                                          <input type="checkbox" id="phone_mask" checked=""> <label id="descr" for="phone_mask"></label>
                                          </div>
                                          </div>
                                          </div> */ ?>
                                        <div class="row">
                                            <div class="col-sm-6">

                                                <label>Mobile <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Mobile', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label>Subject <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('subject', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Subject', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                                            </div>
                                        </div>
                                        <div class="row"> 
                                            <div class="col-sm-12">
                                                <label>Message <span class="color-red">*</span></label> 
                                                <?php echo $this->Form->input('message', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'textarea', 'placeholder' => 'Message', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                                            </div>
                                        </div> 
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label>Captcha   <span class="color-red">*</span></label>
                                                <?php echo $this->Html->image($this->Html->url(array('controller' => 'users', 'action' => 'captcha'), true), array('label' => false, 'width' => '150', 'height' => '40', 'alt' => 'captcha')); ?>
                                            </div>
                                            <div class="col-sm-4">
                                                <label>Enter Code <span class="color-red">*</span></label>
                                                <?php
                                                echo $this->Form->input('captcha', array('data-bvalidator' => 'required', 'class' => 'input-large form-control hastip', 'data-placement' => 'right', 'type' => 'text', 'placeholder' => 'Enter Captcha Code',
                                                    'title' => 'Enter the characters you see in the image.', 'label' => false,'autocomplete'=>'off'));
                                                ?>
                                            </div>
                                            <div class='hidden'>
                                                <?php echo $this->Form->input('ref_page', array('value' => $ref_page[3])) ?>
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <?php echo $this->Form->submit('Send', array('class' => 'btn btn-lg btn-info')) ?>
                                            </div>
                                        </div>
                                        <?php echo $this->Form->end(); ?> -->
                                    <?php } ?>
                                </div>


                            </div>
                        </div>
                        <div class="row row_mar">
                            <div class="col-lg-12">
                                <div class="headline"><span class="headline-title">Related Categories</span></div>
                                <?php
                                $i = 0;
                                foreach ($cat_array as $key => $cat_name) {
                                    $i++;
                                    ?>
                                    <a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $key, 'slug' => $this->Link->cleanString($cat_name))); ?>" class="btn-u btn-u-xs rounded-4x btn-u-dark mar_cat">
                                        <?php echo $cat_name; ?>
                                    </a>
                                    <?php
                                    if ($i == 4) {
                                        $i = 0;
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <?php /*
                          <?php if (!empty($product['Product']['tags'])) { ?>
                          <div class="row row_mar">
                          <div class="col-lg-12">
                          <div class="headline"><h4>Tags</h4></div>
                          <?php
                          $i = 0;
                          $tag_array = explode(',', $product['Product']['tags']);
                          foreach ($tag_array as $key => $tag_name) {
                          $i++;
                          if (!empty($tag_name)) {
                          ?>
                          <a href="#" class="btn-u btn-u-xs rounded-4x btn-u-dark mar_cat">
                          <?php echo $tag_name; ?>
                          </a>
                          <?php }
                          ?>
                          <?php
                          if ($i == 4) {
                          $i = 0;
                          }
                          }
                          ?>
                          </div>
                          </div>
                          <?php } ?>
                         * 
                         */ ?>
                    </div>
                    <div class="row" style="margin-top: 15px;">
                        <div class="share">
                            <span class='st_linkedin_hcount' displayText='LinkedIn'></span>
                            <span class='st_facebook_hcount' displayText='Facebook'></span>
                            <span class='st_twitter_hcount' displayText='Tweet'></span>
                            <span class='st_sharethis_hcount' displayText='ShareThis'></span>
                            <span class='st_pinterest_hcount' displayText='Pinterest'></span>
                            <span class='st_email_hcount' displayText='Email'></span>
                        </div>
                    </div>

                </div> 
                <?php /* commenting related products code adding it in sidebar
                  <?php if (!empty($related_products) && isset($related_products)): ?>
                  <div class="headline"><span class="headline-title">Related Reports</span></div>
                  <?php foreach ($related_products as $key => $prod): ?>
                  <?php if (!empty($prod['Product']['id'])): ?>
                  <div class="col-sm-12">
                  <div class="people-say">
                  <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $prod['main_cat']['slug'], 'id' => $prod['Product']['id'], 'slug' => $prod['Product']['slug'])); ?>">
                  <?php
                  if (!empty($prod['Product']['product_image'])):
                  echo $this->Html->image('product_images/' . $prod['Product']['product_image'], array('class' => 'mar img-thumbnail image_size_home'));
                  else:
                  echo $this->Html->image('noimage.png', array('class' => 'mar img-thumbnail image_size_home'));
                  endif;
                  ?>
                  </a>
                  <div class="inner-results">
                  <span class="secondory-product-heading"><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $prod['main_cat']['slug'], 'id' => $prod['Product']['id'], 'slug' => $prod['Product']['slug'])); ?> "> <?php echo $prod['Product']['meta_name']; ?></a> </span>
                  <ul class="details list-inline">

                  </ul>
                  </div> <hr>
                  </div>
                  </div>
                  <?php endif; ?>
                  <?php endforeach; ?>
                  <?php endif; ?>
                 * 
                 */ ?>
            </div>
            <!-- End Left Inner -->

            <!-- Right Inner -->
            <div class="col-md-4 section-block"> 
                <?php /*
                  <div class="service-block rounded-2x service-block-dark-blue">
                  <?php
                  $url_table = Router::url('/', true) . 'enquiries/send_enquiry_table_of_content/' . $product['Product']['id'] . '/' . $product['ProductCategory'][0]['category_id'] . '/report_details';
                  ?>
                  <div class="row">
                  <div class="col-md-2">
                  <i class="icon-custom icon-color-light rounded-x icon-line fa fa-table"></i>
                  </div>
                  <div class="col-md-10">
                  <span id="call_second_<?php echo $product['Product']['id']; ?>"
                  rel="<?php echo $product['Product']['id']; ?>"
                  onclick="getPopupForm(this.id, '<?php echo $url_table; ?>');" class="tile-title">
                  <?php echo $ref_page[4]; ?>
                  </span>

                  </div>
                  </div>

                  </div>
                 * 
                 */ ?>
                <div class="service-block rounded-2x service-block-green">  
                    <div class="row">
                        <div class="col-md-2">
                            <i class="icon-custom icon-color-light rounded-x icon-line fa fa-file"></i>
                        </div>
                        <div class="col-md-10">
                            <?php
                            echo $this->Html->link('Get Custom Research at price of Syndicate', array(
                                'controller' => 'enquiries',
                                'action' => 'get_lead_info_form',
                                'id' => $product['Product']['id'], 'ref_page' => 'get-custom-research'
                                    ), array(
                                'class' => 'tile-title', "target" => "_blank"
                                    )
                            );
                            ?>
                        </div>
                    </div>
                </div>

                <?php if (!empty($related_products) && isset($related_products)): ?>
                    <div class="headline"><span class="headline-title">Related Reports</span></div>
                    <?php foreach ($related_products as $key => $prod): ?>
                        <?php if (!empty($prod['Product']['id'])): ?>
                            <a class="related_products" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $prod['main_cat']['slug'], 'id' => $prod['Product']['id'], 'slug' => $prod['Product']['slug'])); ?> "> <?php echo $prod['Product']['alias']; ?></a>
                            <p><?php
                                echo $this->Text->truncate(
                                        strip_tags($prod['Product']['product_description']), 160, array(
                                    'ellipsis' => '&nbsp;',
                                    'exact' => false
                                        )
                                );
                                ?><a class="related_products" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $prod['main_cat']['slug'], 'id' => $prod['Product']['id'], 'slug' => $prod['Product']['slug'])); ?> "> read more..</a></p>
                            <hr class="small_space"/>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>



                <?php if (!empty($related_articles) && isset($related_articles)): ?>
                    <div class="headline"><span class="headline-title">Related Articles</span></div>
                    <?php foreach ($related_articles as $key => $article): ?>
                        <?php if (!empty($article['Article']['id'])): ?>
                            <a class="related_products" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_details', 'folder' => $article['Article']['article_type'], 'article_id' => $article['Article']['id'], 'url_slug' => $this->Link->cleanString($article['Article']['slug']))); ?>">
                                <?php echo $article['Article']['headline'] ?>
                            </a>
                            <p><?php
                                echo $this->Text->truncate(
                                        strip_tags($article['Article']['description']), 160, array(
                                    'ellipsis' => '&nbsp;',
                                    'exact' => false
                                        )
                                );
                                ?>
                                <a class="related_products" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_details', 'folder' => $article['Article']['article_type'], 'article_id' => $article['Article']['id'], 'url_slug' => $this->Link->cleanString($article['Article']['slug']))); ?>">
                                    read more...
                                </a></p>
                            <hr class="small_space"/>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>


            </div> 
        </div> 
    </div> 
</div>  <!-- End Right Inner -->

<script type="text/javascript">var switchTo5x = true;</script>
<script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
<script type="text/javascript">stLight.options({publisher: "fdd4a6a8-7a36-4540-a998-72dc1e815377", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>

<style>
    .stMainServices{
        min-height: 22px;
    }
    .stButton_gradient{
        min-height: 22px;
    }
    .small_space{
        margin: 10px
    }
    .related_products{
        font-size: 14px;
    }
</style>
