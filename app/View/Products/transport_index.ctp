<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
    .cat{
        width: 0px;
    }
</style>

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Products</h5> 
            </div>

            <div class="ibox-content">
                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped" id="product_index_tbl">
                        <thead>
                            <tr> 
                                <th><?php echo $this->Paginator->sort('product_name'); ?></th>
                                <th>Categories</th>

                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead> 
                        <?php
                        foreach ($products as $product) {
                            ?>
                            <tbody>

                            <td><?php echo h($product['Product']['product_name']); ?>&nbsp;</td>
                            <td>
                                <select>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                </select> 
                                <select>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                </select> 
                                <select>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                </select> 
                                <select>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                </select> 
                                <select>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                </select> 
                                <select>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                    <option>One</option>
                                </select> 
                                
                            </td>
                            <td class="actions" style="min-width: 250px; max-width: 250px;">
                                <a href="#" class="btn   btn btn-sm btn-primary  tooltip-f" title="">Set Category & Migrate</a>
                                <a href="#" class="btn   btn-outline btn-sm btn-info  tooltip-f" title="">View</a>
                                <a href="#" class="btn   btn-outline btn-sm btn-warning  tooltip-f" title="">Edit</a>
                                <a href="#" class="btn   btn-outline btn-sm btn-danger  tooltip-f" title="">Delete</a>
                                <?php echo $this->Html->link(__('Preview'), array('controller' => 'products', 'action' => 'product_preview', $product['Product']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'Product Preview', 'target' => '_blank')); ?>
                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>