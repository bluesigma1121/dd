<script type="text/javascript">
    $(document).ready(function () {
        $('#add_product').bValidator();
        $('#photo_upload').hide();
        $('#photo_chng').click(function () {
            $('#photo_upload').toggle();
        });
    });
</script>

<style>
    .image_size{
        height: 100px;
        width: 100px;
    }
    .bread{
        margin-bottom: 10px;
    }
    .breadcrumb>li+li:before {
        padding: 0 5px;
        color: #ccc;
        content: ">>\00a0" !important;
    }
</style>

<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>
<div class="ibox float-e-margins"> 
    <div id="form" action="#" class="wizard-big wizard clearfix" role="application" novalidate="novalidate">
        <div class="bread">
            <ol class="breadcrumb">
                <li>
                    <a href="#">Products</a>
                </li>
                <?php if (!empty($this->request->data['Category']['category_name'])) : ?>
                    <li class="active">
                        <strong><?php echo $this->request->data['Category']['category_name']; ?></strong>
                    </li>
                <?php endif; ?>
            </ol>
        </div>
        <div class="steps clearfix"><ul role="tablist">
                <li  class="first current" aria-disabled="false" aria-selected="true"  title="Active">
                    <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit', $pro_id)) ?>">
                        <span class="number">1.</span> Edit Product <span class="badge badge-warning">Active</span></a></li>
                <li  class="first current"  title="Active"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit_category', $pro_id)) ?>" ><span class="number">2.</span> Edit Categories</a></li>
                <li  class="first current"  title="Active" ><a  href="<?php echo Router::url(array('controller' => 'specifications', 'action' => 'add_product_spc', $pro_id)) ?>" ><span class="number">3.</span> Edit Specification</a></li>
                <?php if ($res == 1) { ?>
                    <li  class="first current"  title="Active"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" ><span class="number">4.</span> Edit Countries</a></li>
                    <?php
                } else {
                    ?>
                    <li class = "first current" title = "Active"><a href = "<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" ><span class = "number">4.</span> Add Countries</a></li>
                <?php }
                ?>

            </ul>
        </div>
    </div>


    <!--    <div class="ibox-title">
            <h5> Add Product</h5>
        </div>-->
    <div class="ibox-content">
        <?php echo $this->Form->create('Product', array('id' => 'add_product', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row">
            <div class="form-group"><label class="col-lg-2 control-label">Product Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('product_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Product Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
                <?php
                if (AuthComponent::user('role')!=1) {
                    if($this->request->data['Product']['is_active']==0){
                        echo $this->Html->link(__('Make this product ready to active'), array('controller' => 'products', 'action' => 'ready_to_active', $this->request->data['Product']['id'], 'pending_status'), array('class' => 'btn btn-outline btn-sm btn-success dim', 'title' => 'Active Product'));
                    }
                }else{
                    echo $this->Html->link(__('Activate Product'), array('controller' => 'products', 'action' => 'active', $this->request->data['Product']['id'], 'pending_status'), array('class' => 'btn btn-outline btn-sm btn-success dim', 'title' => 'Active Product'));
                }
                ?>
            </div>

            <div class="form-group"><label class="col-lg-2 control-label">Product Alias</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('alias', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Product Alias', 'label' => false)); ?>
                </div>
            </div>            

            <div class="form-group"><label class="col-lg-2 control-label">Publisher Name</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('publisher_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Publisher Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>            
            <?php /* ALTER TABLE  `products` ADD  `is_set_toc` INT NULL DEFAULT  '0' AFTER  `slug` */ ?>
            <div class="form-group"><label class="col-lg-2 control-label">Request TOC</label>
                <div class="col-lg-6">
                    <?php echo $this->Form->checkbox('is_set_toc', array('value' => '1')); ?>
                </div>
            </div> 

            <div class="form-group"><label class="col-lg-2 control-label">Product TOC</label>
                <div class="col-lg-10">
                    <?php echo $this->Form->input('product_specification', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Product Specification', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div> 
            <div class="form-group">
                <label class="col-lg-2 control-label">Related News</label>
                <div class="col-lg-10">
                    <?php echo $this->Form->input('related_news', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Related News', 'label' => false)); ?>
                </div>
            </div> 
            <div class="form-group"><label class="col-lg-2 control-label">Product Description</label>
                <div class="col-lg-10">
                    <?php echo $this->Form->input('product_description', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Product Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Summary</label>
                <div class="col-lg-10">
                    <?php echo $this->Form->input('summary', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Related News', 'label' => false)); ?>
                </div>
            </div> 

            <div class="form-group">
                <label class="col-lg-2 control-label">Glossary</label>
                <div class="col-lg-10">
                    <?php echo $this->Form->input('glossary', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Related News', 'label' => false)); ?>
                </div>
            </div> 

            <div class="form-group">
                <label class="col-lg-2 control-label">Customizations</label>
                <div class="col-lg-10">
                    <?php echo $this->Form->input('customization', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'label' => false)); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Industry Analysis</label>
                <div class="col-lg-10">
                    <?php echo $this->Form->input('industry_analysis', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'label' => false)); ?>
                </div>
            </div>

            <div class="form-group">
                <label class="col-lg-2 control-label">Latest Developments</label>
                <div class="col-lg-10">
                    <?php echo $this->Form->input('latest_dev', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'label' => false)); ?>
                </div>
            </div>

            <div class="form-group"  id="main_div">
                <label class="col-lg-2 control-label">Select Primary Category</label>
                <div class="col-lg-4">
                    <?php
                    echo $this->Form->input('category_id', array(
                        'options' => $category,
                        'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld1', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                    ?>
                </div>
            </div>
            <div class="form-group">
                <label class="control-label col-sm-3">Profile image</label>
                <div class="controls col-sm-4"> 
                    <?php if (!empty($this->request->data['Product']['product_image'])) { ?>
                        <?php
                        echo $this->Html->image('product_images/' . $this->request->data['Product']['product_image'], array('class' => 'img-thumbnail image_size'));
                    } else {
                        echo $this->Html->image('noimage.png', array('class' => 'img-thumbnail image_size'));
                    }
                    ?>
                </div>
            </div>
            <?php echo $this->Form->input('id'); ?>
            <?php echo $this->Form->input('prev_photo', array('type' => 'hidden', 'value' => $this->request->data['Product']['product_image'])); ?>
            <div class="form-group">
                <label class="control-label col-sm-3"></label>
                <div class="controls col-sm-4">
                    <a id="photo_chng">Change Photo
                    </a> &nbsp;&nbsp;&nbsp;
                    <div hidden id="photo_upload">
                        <?php echo $this->Form->input('product_image', array('type' => 'file', 'class' => 'form-control', 'label' => false)); ?>
                    </div>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Single User License(In Dollar)</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Corporate User License(In Dollar)</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('corporate_price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Multi User License(In Dollar)</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('dollar_price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Discount %</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('tags', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Discount %', 'label' => false, 'data-bvalidator' => 'required,number')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Published</label>
                <div class="col-lg-2">
                    <?php echo $this->Form->input('pub_month', array('class' => 'form-control', 'type' => 'select', 'options' => $months, 'selected' => Date('m', strtotime($this->request->data['Product']['pub_date'])), 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
                <div class="col-lg-2">
                    <?php echo $this->Form->input('pub_year', array('class' => 'form-control', 'type' => 'select', 'options' => array_combine(range(1950, 2050), range(1950, 2050)), 'selected' => Date('Y', strtotime($this->request->data['Product']['pub_date'])), 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div> 
            <?php /*
              <div class="form-group"><label class="col-lg-2 control-label">Tags</label>
              <div class="col-lg-4">
              <?php echo $this->Form->input('tags', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Tags', 'label' => false, 'data-bvalidator' => 'required')); ?>
              </div>
              </div>
             * 
             */ ?>
            <?php /*  <div class="form-group">
              <label class="col-lg-2 control-label">Price</label>
              <div class="col-lg-5">
              <div class="col-lg-6">
              <?php echo $this->Form->input('price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
              </div>
              <div class="col-lg-6">
              <label class="col-lg-2 control-label">Dollar</label>
              <div class="col-lg-9">
              <?php echo $this->Form->input('dollar_price', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Price', 'label' => false, 'data-bvalidator' => 'required')); ?>
              </div>
              </div>
              
              </div>
              </div> */ ?>
            <div class="form-group"><label class="col-lg-2 control-label">Meta Title</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('meta_name', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Meta Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Meta Description</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('meta_desc', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Meta Keywords</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('meta_keywords', array('class' => 'form-control', 'type' => 'textarea', 'placeholder' => 'Meta Keywords ', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-2 control-label">Slug</label>
                <div class="col-lg-4">
                    <?php echo $this->Form->input('slug', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Slug Name', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
        </div>
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?> 
            <?php echo $this->Html->link(__('Activate'), array('action' => 'activate_product', $pro_id), array('class' => 'btn btn-sm btn-warning', 'title' => 'Activate'));
            ?>
        </div>
        <?php echo $this->Form->end(); ?> 
    </div>
</div>