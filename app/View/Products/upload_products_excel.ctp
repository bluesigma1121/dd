<script>
    $(document).ready(function() {
        //$('#upload_pro').bValidator();
    });
</script> 
<style>
    .err_msg{
        color: red;
    }
</style>

<div class="page-content">
    <div class="widget-box top_margin">
        <div class="widget-header widget-header-blue widget-header-flat">
            <h4>Upload Products</h4>           
        </div>
        <div class="widget-body">
            <div class="portlet-body form">
                <?php echo $this->Form->create('Product', array('id' => 'upload_pro', 'class' => 'form-horizontal', 'type' => 'file', 'role' => 'form')); ?>
                  <div class="form-group" style="margin-left:310px;">
                <input type="radio" id="market_reports" name="category_id" value="2">
                <label for="market_reports">Market Reports</label>&nbsp;&nbsp;
                <input type="radio" id="company_profiles" name="category_id" value="3">
                <label for="company_profiles">Company Profiles</label><br>
                </div>

                <div class="form-group">
                    <label for="exampleInputFile" class="col-md-3 control-label">Product Excel:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('product_upload', array('type' => 'file', 'id' => 'exampleInputFile', 'class' => 'form-control', 'data-bvalidator' => 'required', 'data-bvalidator-msg' => 'Please select file of type .xls', 'placeholder' => 'Select File', 'label' => false)); ?>
                    </div>
                </div>
                <div class="form-actions text-center">
                    <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php echo $this->Html->link(__('Back'), array('action' => 'index'), array('class' => 'btn btn-warning btn-sm', 'div' => false)); ?>
                </div>
                <?php echo $this->Form->end(); ?> 
            </div>
        </div>
    </div>
    <hr>

    <?php if(!empty($sts)){ ?>
        <div class="widget-box">
            <div class="widget-header widget-header-blue widget-header-flat">
                <h4>Product Uploaded Status</h4>           
            </div>
            <div class="table-responsive mtp">
                
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Product Name</th>
                            <th>Status</th>
                            <th>Category Status</th>
                        </tr>
                    </thead>
                    <tbody>                        
                        
                        <?php for ($i=1; $i < count($sts) ; $i++) {  ?>
                            <tr>
                                <td><?= $i ?></td>
                                <td><?= $sts[$i][0] ?></td>
                                <td>
                                    <?php   if($sts[$i][1] == 0){ 
                                                echo 'Uploaded!'; 
                                            }elseif($sts[$i][1] == 1){ 
                                                echo 'Error in Upload!'; 
                                            }elseif($sts[$i][1] == 2){ 
                                                echo 'Duplicate Record Found!'; 
                                            }elseif($sts[$i][1] == 4){ 
                                                echo 'Product Name Missing!'; 
                                            }elseif($sts[$i][1] == 5){ 
                                                echo 'Product Price Missing!'; 
                                            }elseif($sts[$i][1] == 6){ 
                                                echo 'Invalid Price Format!'; 
                                            }elseif($sts[$i][1] == 7){ 
                                                echo 'Invalid Publish Date Format!'; 
                                            }  ?></td>
                                <td>
                                    <?php 
                                        if(!empty($sts[$i][2])){
                                            echo $sts[$i][2].' Category Not Found';
                                        }else{
                                            if($sts[$i][1] == 0){
                                                echo 'Uploaded';
                                            }else{
                                                echo '-';
                                            }
                                        }  
                                    ?>
                                </td>
                            </tr>     
                        <?php } ?>
                    </tbody>
                </table>
            </div>        
        </div>
    <?php } ?>
</div>