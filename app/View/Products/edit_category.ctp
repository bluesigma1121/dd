<script>
    $(document).ready(function() {
        var i = 1;
        $("#add_category_pro").bValidator();
        var prod_id = "<?php echo $pro_id; ?>";
        $('#prnt_of_chld1').change(function() {
            $("#div2").hide();
            $("#prnt_of_chld2").prop('disabled', true);
            $("#div3").hide();
            $("#prnt_of_chld3").prop('disabled', true);
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld1').val();
            var lname = '';
            var lname = $("#prnt_of_chld1 option:selected").text();
            $('#parent_cat_id_per').val(id);
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id + "/" + prod_id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length != 0) {
                        $("#div2").show();
                        $("#prnt_of_chld2").prop('disabled', false);
                        $("#prnt_of_chld2").empty();
                        $("#prnt_of_chld2").append(

                                );
                        $.each(data, function(i, val) {
                            $("#prnt_of_chld2").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                    }
                    else {
//                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
//                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
//                        i++;
                        $("#div2").hide();
                        $("#prnt_of_chld2").prop('disabled', true);
                        $("#div3").hide();
                        $("#prnt_of_chld3").prop('disabled', true);
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div2").hide();
                $("#prnt_of_chld2").prop('disabled', true);
                $("#prnt_of_chld2").empty();
                $("#prnt_of_chld2").append(

                        );
            }
        });
        $('#prnt_of_chld2').change(function() {
            $("#div3").hide();
            $("#prnt_of_chld3").prop('disabled', true);
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld2').val();
            var lname = '';
            var lname = $("#prnt_of_chld2 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id + "/" + prod_id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length != 0) {
                        $("#div3").show();
                        $("#prnt_of_chld3").prop('disabled', false);
                        $("#prnt_of_chld3").empty();
                        $("#prnt_of_chld3").append(

                                );
                        $.each(data, function(i, val) {
                            $("#prnt_of_chld3").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                    }
                    else {
//                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
//                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');">&nbsp&nbsp</div>');
//                        i++;
                        $("#div3").hide();
                        $("#prnt_of_chld3").prop('disabled', true);
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div3").hide();
                $("#prnt_of_chld3").prop('disabled', true);
                $("#prnt_of_chld3").empty();
            }
        });
        $('#prnt_of_chld3').change(function() {
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld3').val();
            var lname = '';
            var lname = $("#prnt_of_chld3 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id + "/" + prod_id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length != 0) {
                        $("#div4").show();
                        $("#prnt_of_chld4").prop('disabled', false);
                        $("#prnt_of_chld4").empty();
                        $("#prnt_of_chld4").append(

                                );
                        $.each(data, function(i, val) {
                            $("#prnt_of_chld4").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                    }
                    else {
//                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
//                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
//                        i++;
                        $("#div4").hide();
                        $("#prnt_of_chld4").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div4").hide();
                $("#prnt_of_chld4").prop('disabled', true);
                $("#prnt_of_chld4").empty();
                $("#prnt_of_chld4").append(

                        );
            }
        });
        $('#prnt_of_chld4').change(function() {
            $("#div5").hide();
            $("#prnt_of_chld5").prop('disabled', true);
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld4').val();
            var lname = '';
            var lname = $("#prnt_of_chld4 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id + "/" + prod_id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length != 0) {
                        $("#div5").show();
                        $("#prnt_of_chld5").prop('disabled', false);
                        $("#prnt_of_chld5").empty();
                        $("#prnt_of_chld5").append(

                                );
                        $.each(data, function(i, val) {
                            $("#prnt_of_chld5").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                    }
                    else {
//                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
//                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
//                        i++;
                        $("#div5").hide();
                        $("#prnt_of_chld5").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div5").hide();
                $("#prnt_of_chld5").prop('disabled', true);
                $("#prnt_of_chld5").empty();
                $("#prnt_of_chld5").append(

                        );
            }
        });
        $('#prnt_of_chld5').change(function() {
            $("#div6").hide();
            $("#prnt_of_chld6").prop('disabled', true);
            var id = $('#prnt_of_chld5').val();
            var lname = '';
            var lname = $("#prnt_of_chld5 option:selected").text();
            if (id != '') {
                var base_path = "<?= Router::url('/', true) ?>";
                var urls = base_path + "categories/dropdwn_fill/" + id + "/" + prod_id;
                $.ajax({
                    url: urls,
                    dataType: 'json'
                }).done(function(data) {
                    if (data.length != 0) {
                        $("#div6").show();
                        $("#prnt_of_chld6").prop('disabled', false);
                        $("#prnt_of_chld6").empty();
                        $("#prnt_of_chld6").append(

                                );
                        $.each(data, function(i, val) {
                            $("#prnt_of_chld6").append(
                                    $("<option></option>").text(val).val(i)
                                    );
                        });
                    }
                    else {
//                        $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
//                        $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
//                        i++;
                        $("#div6").hide();
                        $("#prnt_of_chld6").prop('disabled', true);
                    }
                });
            }
            else {
                $("#div6").hide();
                $("#prnt_of_chld6").prop('disabled', true);
                $("#prnt_of_chld6").empty();
                $("#prnt_of_chld6").append(

                        );
            }
        });
//        $('#prnt_of_chld6').change(function() {
//            var id = $('#prnt_of_chld6').val();
//            if (id != '') {
//                var lname = '';
//                var lname = $("#prnt_of_chld6 option:selected").text();
//                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" type="hidden" value="' + id + '" id="cat_hidden' + i + '">');
//                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
//                i++;
//            }
//        });

        $('#add_parent_1').click(function() {
            var id = $('#prnt_of_chld1').val();
            var lname = '';
            var lname = $("#prnt_of_chld1 option:selected").text();
            if (lname != '') {
                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                i++;
            }

        });

        $('#add_parent_2').click(function() {
            var id = $('#prnt_of_chld2').val();
            var lname = '';
            var lname = $("#prnt_of_chld2 option:selected").text();
            if (lname != '') {
                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');">&nbsp&nbsp</div>');
                i++;
            }
        });
        $('#add_parent_3').click(function() {
            var id = $('#prnt_of_chld3').val();
            var lname = '';
            var lname = $("#prnt_of_chld3 option:selected").text();
            if (lname != '') {
                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                i++;
            }
        });

        $('#add_parent_4').click(function() {
            var id = $('#prnt_of_chld4').val();
            var lname = '';
            var lname = $("#prnt_of_chld4 option:selected").text();
            if (lname != '') {
                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                i++;
            }
        });
        $('#add_parent_5').click(function() {
            var id = $('#prnt_of_chld5').val();
            var lname = '';
            var lname = $("#prnt_of_chld5 option:selected").text();
            if (lname != '') {
                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                i++;
            }
        });

        $('#add_parent_6').click(function() {
            var id = $('#prnt_of_chld6').val();
            var lname = '';
            var lname = $("#prnt_of_chld6 option:selected").text();
            if (lname != '') {
                $('#new_id').append('<input name="data[cat_hidden][' + i + ']" class="form-control" value="' + id + '" type="hidden" id="cat_hidden' + i + '">');
                $('#selectedlabel').append('<div class="aaa"><label class = "control-label col-sm-2 bg" id="label' + i + '" > ' + lname + '<input type="button" name="minus' + i + '" id="minus ' + i + '" class="btn btn-danger btn-xs pull-right" value="x" onclick=" del_row(' + i + ');"></div>');
                i++;
            }
        });



        $('#ref').click(function() {
            $("#div2").hide();
            $("#prnt_of_chld2").prop('disabled', true);
            $("#div3").hide();
            $("#prnt_of_chld3").prop('disabled', true);
            $("#div4").hide();
            $("#prnt_of_chld4").prop('disabled', true);
            $("option:selected").removeAttr("selected");
        });
        /*  $('.check_uncheck').click(function(event) {
         var myVar = this.id;
         
         var d_id = $(this).attr('data-role');
         $('.check_uncheck').each(function() { //loop through each checkbox
         this.checked = false;
         });
         $('#' + myVar).prop('checked', true);
         
         if (d_id != '') {
         var base_path = "<?= Router::url('/', true) ?>";
         var urls = base_path + "product_categories/edit_main_cat/";
         $.ajax({
         url: urls,
         type: "POST",
         data: {cid: d_id},
         dataType: 'json'
         }).done(function(data) {
         
         });
         }
         }); */

//        $("#add_category_pro").submit(function() {
//            var a = ($("#prnt_of_chld2").val());
//            var b = ($("#prnt_of_chld3").val());
//            var c = ($("#prnt_of_chld4").val());
//            var d = ($("#prnt_of_chld5").val());
//            var e = ($("#prnt_of_chld6").val());
//
//            var parent = ($("#prnt_of_chld1").val());
//            if (parent == null && ($("#div2").is(':hidden'))) {
//                alert('Please Select parent Category');
//                return false;
//            }
//
//            if (a == null && (($("#div2").is(':visible')) && ($("#div3").is(':hidden')))) {
//                alert('Please Select Category');
//                return false;
//            }
//            if (b == null && ($("#div3").is(':visible') && ($("#div4").is(':hidden')))) {
//                alert('Please Select Category');
//                return false;
//            }
//
//            if (c == null && ($("#div4").is(':visible') && ($("#div5").is(':hidden')))) {
//                alert('Please Select Category');
//                return false;
//            }
//            if (d == null && ($("#div5").is(':visible') && ($("#div6").is(':hidden')))) {
//                alert('Please Select Category');
//                return false;
//            }
//            if (e == null && ($("#div6").is(':visible'))) {
//                alert('Please Select Category');
//                return false;
//            }
//        });
    });
    function del_row(i) {
        $("#label" + i).remove();
        $("#cat_hidden" + i).remove();
    }
</script> 

<style>
    .bg { background: aquamarine ;
          margin-left: 5px;
    }
    .width_cust{
        width: 50px;
        height: 22px;
    }
    .breadcrumb>li+li:before {
        padding: 0 5px;
        color: #ccc;
        content: ">>\00a0" !important;
    }
    .breadcrumb {
        background-color: #F9F9F9 !important;
        padding: 0;
        margin-bottom: 0;
    }
    .li_color{
        color: rgb(92, 207, 98);   
    } 
    .bread{
        margin-bottom: 10px;
    } 
    .next_cat_div{
        margin-top: 10px;
    }
</style>
<div class="ibox float-e-margins">
    <div id="form" action="#" class="wizard-big wizard clearfix" role="application" novalidate="novalidate">
        <div class="bread">
            <ol class="breadcrumb">
                <li>
                    <a href="#">Products</a>
                </li>
                <?php if (!empty($prod_name['Category']['category_name'])) : ?>
                    <li class="active">
                        <strong><?php echo $prod_name['Category']['category_name']; ?></strong>
                    </li>
                <?php endif; ?>
            </ol>
        </div>
        <div class="steps clearfix"><ul role="tablist">
                <li  class="first current" aria-disabled="false" aria-selected="true" title="Active" >
                    <a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit', $pro_id)) ?>">
                        <span class="number">1.</span> Edit Product</a> </li>
                <li  class="first current"  title="Active" ><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'edit_category', $pro_id)) ?>" ><span class="number">2.</span> Edit Categories <span class="badge badge-warning">Active</span> </a></li>
                <li  class="first current"  title="Active" ><a  href="<?php echo Router::url(array('controller' => 'specifications', 'action' => 'add_product_spc', $pro_id)) ?>" ><span class="number">3.</span> Edit Specification</a></li>
                <?php if ($res == 1) { ?>
                    <li  class="first current"  title="Active"><a  href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" ><span class="number">4.</span> Edit Countries</a></li>
                    <?php
                } else {
                    ?>
                    <li class = "first current" title = "Active"><a href = "<?php echo Router::url(array('controller' => 'products', 'action' => 'add_product_countries', $pro_id)) ?>" ><span class = "number">4.</span> Add Countries</a></li>
                <?php }
                ?>
            </ul>
        </div>
    </div>
    <!--    <div class="ibox-title">
            <h5> Add Categories</h5>
        </div>-->
    <div class="ibox-content">
        <div class="row">
            <div class="form-group">
                <div class="col-lg-3" id="main_div">
                    <?php
                    echo $this->Form->input('parent_category_id1', array(
                        'options' => $category,
                        'class' => 'form-control hastip', 'size' => '10', 'id' => 'prnt_of_chld1', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                    ?>
                    <input class="btn btn-info btn-sm tooltip-f" style="margin-top: 20px;" type="Button" id="add_parent_1" value="Add" title="Add Category">
                </div>
                <div class="col-lg-3" id="div2" hidden>
                    <?php
                    echo $this->Form->input('parent_category_id2', array(
                        'options' => '',
                        'empty' => '--Select--', 'class' => 'form-control hastip', 'size' => '10', 'id' => 'prnt_of_chld2', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                    ?>
                    <input class="btn btn-info btn-sm tooltip-f" style="margin-top: 20px;" type="Button" id="add_parent_2" value="Add" title="Add Category">
                </div>
                <div class="col-lg-3" id="div3" hidden>
                    <?php
                    echo $this->Form->input('parent_category_id3', array(
                        'options' => '',
                        'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld3', 'size' => '10', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                    ?>
                    <input class="btn btn-info btn-sm tooltip-f" style="margin-top: 20px;" type="Button" id="add_parent_3" value="Add" title="Add Category">
                </div>
                <div class="col-md-3" id="div4" hidden>
                    <?php
                    echo $this->Form->input('parent_category_id4', array(
                        'options' => '',
                        'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld4', 'size' => '10', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                    ?>
                    <input class="btn btn-info btn-sm tooltip-f" style="margin-top: 20px;" type="Button" id="add_parent_4" value="Add" title="Add Category">
                </div>
                <div class="col-md-3 next_cat_div" id="div5" hidden>
                    <?php
                    echo $this->Form->input('parent_category_id5', array(
                        'options' => '',
                        'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld5', 'size' => '10', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                    ?>
                    <input class="btn btn-info btn-sm tooltip-f" style="margin-top: 20px;" type="Button" id="add_parent_5" value="Add" title="Add Category">
                </div>
                <div class="col-md-3 next_cat_div" id="div6" hidden>
                    <?php
                    echo $this->Form->input('parent_category_id6', array(
                        'options' => '',
                        'empty' => '--Select--', 'class' => 'form-control hastip', 'id' => 'prnt_of_chld6', 'size' => '10', 'data-bvalidator' => 'required', 'label' => false, 'title' => 'Select Category Name'));
                    ?>
                    <input class="btn btn-info btn-sm tooltip-f" style="margin-top: 20px;" type="Button" id="add_parent_6" value="Add" title="Add Category">
                </div>
            </div>
        </div>    
        <div class="row">
            <?php echo $this->Form->create('Product', array('id' => 'add_category_pro')) ?>
            <div class="">
                <div id="new_id" style="margin-top: 20px;">
                    <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
                    <?php
                    if ($prod_name['Product']['is_active'] == 0) {
                        echo $this->Html->link(__('Activate Product'), array('controller' => 'products', 'action' => 'active', $prod_name['Product']['id'], 'pending_status'), array('class' => 'btn btn-sm btn-success dim', 'title' => 'Active Product'));
                    }
                    ?>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
            <!--            <button class="btn btn-info btn-sm" id="ref" > Add More </button>-->
        </div>
    </div>
    <div class="ibox-content">
        <div class="ibox-title">
            <h5> Currently Selected Categories <b><?php echo ucfirst($prod_name['Product']['product_name']); ?></b></h5>
        </div>
        <div class="row" id="selectedlabel">
            <?php ?>
        </div>
    </div> 
    <div class="ibox-content">
        <div class="ibox-title">
            <h5> Selected Categories for <b><?php echo ucfirst($prod_name['Product']['product_name']); ?></b></h5>
        </div>
        <div class="row" id="select_cat">
            <div class="col-lg-2">
            </div>
            <div class="table-responsive col-lg-8">
                <table class="table table-bordered table-striped col-md-8">
                    <thead>
                        <tr>
                            <th>Category Name</th>
<!--                            <th>Is Main Category</th>-->
                            <th class="actions"><?php echo __('Actions'); ?></th>
                        </tr>
                    </thead> 
                    <?php
                    foreach ($select_prod_category as $category) {
                        ?>
                        <tbody>
                        <td>  <ul class="pull-left breadcrumb">
                                <?php
                                if (!empty($category['Bread'])) {
                                    foreach ($category['Bread'] as $key => $bred) {
                                        ?> 
                                        <?php if ($bred['Category']['level'] == 1) { ?>

                                            <?php if ($category['ProductCategory']['category_id'] == $bred['Category']['id']) { ?>
                                                <li class="li_color"> 
                                                    <?php echo $bred['Category']['category_name']; ?></li>
                                            <?php } else { ?>
                                                <li> 
                                                    <?php echo $bred['Category']['category_name']; ?></li>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <?php if ($category['ProductCategory']['category_id'] == $bred['Category']['id']) { ?>
                                                <li class="li_color"> <?php echo $bred['Category']['category_name']; ?></li>
                                            <?php } else { ?>
                                                <li> <?php echo $bred['Category']['category_name']; ?></li>
                                            <?php } ?>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                            </ul>
                        </td>

                        <td class="actions">
                            <?php echo $this->Form->postLink(__('Delete'), array('controller' => 'product_categories', 'action' => 'delete', $category['ProductCategory']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $category['Category']['category_name'])); ?>
                        </td>
                        </tbody>
                    <?php } ?>
                </table>
            </div>
            <div class="col-lg-2">
            </div>
        </div>
    </div>
</div>

