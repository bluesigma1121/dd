<script>
    $(document).ready(function () {
        $('#ArticlesReportAddForm').bValidator();
        
        //$('#ArticlesReportArticleId').multiselect();
    });
</script>
<div class="ibox float-e-margins">
    <div class="ibox-content">
        <?php echo $this->Form->create('ArticlesReport', array('class' => 'form-horizontal')); ?>
        <fieldset>
            <legend><?php echo __('Add Articles Report'); ?></legend>
            <div class="row">
                <div class="form-group">
                    <label class="col-lg-3 control-label">Articles</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('article_id', array('class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-lg-3 control-label">Products</label>
                    <div class="col-lg-8">
                        <?php echo $this->Form->input('product_id', array('class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required')); ?>
                    </div>
                </div>
            </div>
        </fieldset>
        <?php echo $this->Form->end(__('Submit')); ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

        <li><?php echo $this->Html->link(__('List Articles Reports'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('List Articles'), array('controller' => 'articles', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Article'), array('controller' => 'articles', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
    </ul>
</div>
