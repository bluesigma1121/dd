<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style> 

<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Products CRUD</h5>
            </div>
            <div class="ibox-content">
                <?php echo $this->Form->create('Product', array('id' => 'index', 'class' => 'form-horizontal')); ?>
                <div class="row">
	                <div class="col-lg-2 pull-right">
    	                <a class="btn btn-sm btn-warning" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'add')); ?>" title="Add Product"><i class="fa fa-products"></i>Add Product</a>
        	        </div>
                </div>

                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Slug</th>
                                <th>Publisher Name</th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead> 
                        <?php foreach ($products as $product) { ?>
                            <tbody>
                            <td><?php echo h($product['Product']['id']); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['product_name']); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['price']); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['slug']); ?>&nbsp;</td>
                            <td><?php echo h($product['Product']['publisher_name']); ?>&nbsp;</td>                          
                            <td class="actions">
                            	<a href="/mis/product_view/<?=  $product['Product']['id'] ?>"
                            		class="btn btn-outline btn-sm btn-warning dim"
                            		title="View"> 
                        			View 
                    			</a>


                                <?php echo $this->Html->link(__('Edit'), array('controller' => 'products','action' => 'edit', $product['Product']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>       

                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div> 
