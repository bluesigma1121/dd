<div class="row">
	<div class="col-md-12">
		<h1> Leads By Report </h1>
	</div>
	<div class="col-md-12">
		<form class="form-inline">
          <div class="row">
            <div class="col-md-3">
              <label for="">From :</label>
              <input type="date" name="from" class="form-control input-sm" value="<?php echo empty($_GET['from']) ? (!empty($from) ? date('Y-m-d', strtotime($from)) : date('Y-m-d')) : $_GET['from'] ?>">
            </div>
            <div class="col-md-3">
              <label for="">To :</label>
              <input type="date" name="to" class="form-control input-sm" value="<?php echo empty($_GET['to']) ? (!empty($to) ? $to : date('Y-m-d')) : $_GET['to'] ?>">
            </div>
            <div class="col-lg-1 search_mtp">
              <input type="submit" class="btn btn-outline btn-md btn-primary fa fa-search" value="Submit">
			</div>
          </div>
        </form>
		<hr>
	</div>
</div>

<div class="row">

	<div class="col-md-8 col-md-offset-2">
		<table class="table table-bordered table-condensed">
			<thead>
				<tr>
					<th class="text-center"> Reports </th>
					<th> Publisher </th>
					<th> Leads </th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($leads_by_report as $index => $leads_by_report) { ?>
				    <tr> 
						<td class="text-left"> <?= $leads_by_report['Product']
							['publisher_name'] ?> </td> 
				    	<?php if(empty($leads_by_report['Product']['alias'])) { ?>
						    <td class="text-left"> - Not Set - </td>
						<?php } else { ?>
						    <td class="text-left"> <?= $leads_by_report['Product']
						    ['alias'] ?> </td>
						<?php } ?>
					    <td> <?= $leads_by_report[0]['lead_count'] ?> </td>    
				    <tr>
				<?php } ?>
			</tbody>
		</table>
	</div>

</div>
