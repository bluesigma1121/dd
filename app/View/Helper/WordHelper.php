<?php

class WordHelper extends AppHelper {

//    public function set_word($number) {
//
//        $hyphen = '-';
//        $conjunction = ' and ';
//        $separator = ', ';
//        $negative = 'negative ';
//        $decimal = ' point ';
//        $dictionary = array(
//            0 => 'zero',
//            1 => 'one',
//            2 => 'two',
//            3 => 'three',
//            4 => 'four',
//            5 => 'five',
//            6 => 'six',
//            7 => 'seven',
//            8 => 'eight',
//            9 => 'nine',
//            10 => 'ten',
//            11 => 'eleven',
//            12 => 'twelve',
//            13 => 'thirteen',
//            14 => 'fourteen',
//            15 => 'fifteen',
//            16 => 'sixteen',
//            17 => 'seventeen',
//            18 => 'eighteen',
//            19 => 'nineteen',
//            20 => 'twenty',
//            30 => 'thirty',
//            40 => 'fourty',
//            50 => 'fifty',
//            60 => 'sixty',
//            70 => 'seventy',
//            80 => 'eighty',
//            90 => 'ninety',
//            100 => 'hundred',
//            1000 => 'thousand',
//            1000000 => 'lakh',
//            1000000000 => 'crore',
//            1000000000000 => 'arab',
//            1000000000000000 => 'kharab',
//            1000000000000000000 => 'neel'
//        );
//
//        if (!is_numeric($number)) {
//            return false;
//        }
//
//        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
//            // overflow
//            trigger_error(
//                    'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX, E_USER_WARNING
//            );
//            return false;
//        }
//
//        if ($number < 0) {
//            return $negative . $this->set_word(abs($number));
//        }
//
//        $string = $fraction = null;
//
//        if (strpos($number, '.') !== false) {
//            list($number, $fraction) = explode('.', $number);
//        }
//
//        switch (true) {
//            case $number < 21:
//                $string = $dictionary[$number];
//                break;
//            case $number < 100:
//                $tens = ((int) ($number / 10)) * 10;
//                $units = $number % 10;
//                $string = $dictionary[$tens];
//                if ($units) {
//                    $string .= $hyphen . $dictionary[$units];
//                }
//                break;
//            case $number < 1000:
//                $hundreds = $number / 100;
//                $remainder = $number % 100;
//                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
//                if ($remainder) {
//                    $string .= $conjunction . $this->set_word($remainder);
//                }
//                break;
//            default:
//                $baseUnit = pow(1000, floor(log($number, 1000)));
//                $numBaseUnits = (int) ($number / $baseUnit);
//                $remainder = $number % $baseUnit;
//                $string = $this->set_word($numBaseUnits) . ' ' . $dictionary[$baseUnit];
//                if ($remainder) {
//                    $string .= $remainder < 100 ? $conjunction : $separator;
//                    $string .= $this->set_word($remainder);
//                }
//                break;
//        }
//
//        if (null !== $fraction && is_numeric($fraction)) {
//            $string .= $decimal;
//            $words = array();
//            foreach (str_split((string) $fraction) as $number) {
//                $words[] = $dictionary[$number];
//            }
//            $string .= implode(' ', $words);
//        }
//
//        return $string;
//    }

    function set_word($no) {
        $words = array('0' => '', '1' => 'one', '2' => 'two', '3' => 'three', '4' => 'four', '5' => 'five', '6' => 'six', '7' => 'seven', '8' => 'eight', '9' => 'nine', '10' => 'ten', '11' => 'eleven', '12' => 'twelve', '13' => 'thirteen', '14' => 'fouteen', '15' => 'fifteen', '16' => 'sixteen', '17' => 'seventeen', '18' => 'eighteen', '19' => 'nineteen', '20' => 'twenty', '30' => 'thirty', '40' => 'fourty', '50' => 'fifty', '60' => 'sixty', '70' => 'seventy', '80' => 'eighty', '90' => 'ninty', '100' => 'hundred', '1000' => 'thousand', '100000' => 'lakh', '10000000' => 'crore');
        if ($no == 0)
            return '';
        else {
            $novalue = '';
            $highno = $no;
            $remainno = 0;
            $value = 100;
            $value1 = 1000;
            while ($no >= 100) {
                if (($value <= $no) && ($no < $value1)) {
                    $novalue = $words["$value"];
                    $highno = (int) ($no / $value);
                    $remainno = $no % $value;
                    break;
                }
                $value = $value1;
                $value1 = $value * 100;
            }
            if (array_key_exists("$highno", $words))
                return $words["$highno"] . " " . $novalue . " " . $this->set_word($remainno);
            else {
                $unit = $highno % 10;
                $ten = (int) ($highno / 10) * 10;
                return $words["$ten"] . " " . $words["$unit"] . " " . $novalue . " " . $this->set_word($remainno);
            }
        }
    }

    function moneyFormatIndia($num) {
        $explrestunits = "";
        if (strlen($num) > 3) {
            $lastthree = substr($num, strlen($num) - 3, strlen($num));
            $restunits = substr($num, 0, strlen($num) - 3); // extracts the last three digits
            $restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
            $expunit = str_split($restunits, 2);
            for ($i = 0; $i < sizeof($expunit); $i++) {
                // creates each of the 2's group and adds a comma to the end
                if ($i == 0) {
                    $explrestunits .= (int) $expunit[$i] . ","; // if is first value , convert into integer
                } else {
                    $explrestunits .= $expunit[$i] . ",";
                }
            }
            $thecash = $explrestunits . $lastthree;
        } else {
            $thecash = $num;
        }
        return $thecash; // writes the final format where $currency is the currency symbol.
    }

}
