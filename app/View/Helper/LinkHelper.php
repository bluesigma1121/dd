<?php

App::uses('AppHelper', 'View/Helper');

class LinkHelper extends AppHelper {

    public function cleanString($string) {
        $string = str_replace(' ', '-', $string);
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string);
        $string = preg_replace('/-+/', '-', $string);
        return strtolower($string);
    }

}