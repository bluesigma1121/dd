<style>
    .mtp{
        margin-top: 10px;
    }
    .search_mtp{
        margin-top: 3px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Setting List</h5>
            </div>
            <div class="ibox-content"> 
                <div class="table-responsive mtp">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('dollar_rate'); ?></th>
                                <th><?php echo $this->Paginator->sort('mail_to'); ?></th>
                                <th><?php echo $this->Paginator->sort('created'); ?></th>
                                <th><?php echo $this->Paginator->sort('modified'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead>
                        <?php foreach ($settings as $setting): ?>
                            <tbody>
                            <td><?php echo h($setting['Setting']['dollar_rate']); ?>&nbsp;</td>
                            <td><?php echo h($setting['Setting']['mail_to']); ?>&nbsp;</td>
                            <td><?php echo date('d-m-Y h:i a', strtotime($setting['Setting']['created'])); ?>&nbsp;</td>
                            <td><?php echo date('d-m-Y h:i a', strtotime($setting['Setting']['modified'])); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $setting['Setting']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>                               
                            </td>
                            </tbody>
                        <?php endforeach; ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
