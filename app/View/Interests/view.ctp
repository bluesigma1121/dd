<div class="interests view">
<h2><?php echo __('Interest'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($interest['Interest']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($interest['User']['title'], array('controller' => 'users', 'action' => 'view', $interest['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Product'); ?></dt>
		<dd>
			<?php echo $this->Html->link($interest['Product']['id'], array('controller' => 'products', 'action' => 'view', $interest['Product']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($interest['Interest']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($interest['Interest']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Interest'), array('action' => 'edit', $interest['Interest']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Interest'), array('action' => 'delete', $interest['Interest']['id']), null, __('Are you sure you want to delete # %s?', $interest['Interest']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Interests'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Interest'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Products'), array('controller' => 'products', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Product'), array('controller' => 'products', 'action' => 'add')); ?> </li>
	</ul>
</div>
