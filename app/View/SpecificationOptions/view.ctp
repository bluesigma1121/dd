<div class="specificationOptions view">
<h2><?php echo __('Specification Option'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($specificationOption['SpecificationOption']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Option Name'); ?></dt>
		<dd>
			<?php echo h($specificationOption['SpecificationOption']['option_name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Specification'); ?></dt>
		<dd>
			<?php echo $this->Html->link($specificationOption['Specification']['id'], array('controller' => 'specifications', 'action' => 'view', $specificationOption['Specification']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Is Active'); ?></dt>
		<dd>
			<?php echo h($specificationOption['SpecificationOption']['is_active']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($specificationOption['SpecificationOption']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($specificationOption['SpecificationOption']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Specification Option'), array('action' => 'edit', $specificationOption['SpecificationOption']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Specification Option'), array('action' => 'delete', $specificationOption['SpecificationOption']['id']), null, __('Are you sure you want to delete # %s?', $specificationOption['SpecificationOption']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Specification Options'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Specification Option'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Specifications'), array('controller' => 'specifications', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Specification'), array('controller' => 'specifications', 'action' => 'add')); ?> </li>
	</ul>
</div>
