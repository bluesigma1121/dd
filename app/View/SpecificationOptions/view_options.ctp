<script>
    $(document).ready(function() {
        $('.add_child_option').click(function() {
            var id = $(this).attr('specification_id');
            $('#specification_id').val(id);
        });
        $('.edit_child_speci').click(function() {
            var sp_id = $(this).attr('spec_id');
            var sp_name = $(this).attr('spec_name');
            $('#spec_id').val(sp_id);
            $('#spec_name').val(sp_name);
        });
        $('.edit_child_option').click(function() {
            var op_id = $(this).attr('opt_id');
            var op_name = $(this).attr('opt_name');
            $('#opt_id').val(op_id);
            $('#opt_name').val(op_name);
        });
    });
    
</script>
<style>
    .model-display{
        display: none;
    }
</style>
<div class="modal fade model-display" id="add_child_opt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('SpecificationOption', array('controller' => 'specification_options', 'action' => 'add_modal_option', 'id' => 'add_child_opt_form')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Add Options</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->input('specification_id', array('class' => 'form-control', 'label' => false, 'type' => 'hidden', 'value' => $spec['Specification']['id'], 'id' => 'specification_id')); ?>              

                <p><?php echo $this->Form->input('option_name', array('type' => 'text', 'class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required', 'placeholder' => 'Enter Option Name')); ?></p>
                <div>
                    <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-success')); ?>                   
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<!-- edit specification-->
<div class="modal fade model-display" id="edit_child_spec" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('Specification', array('controller' => 'specifications', 'action' => 'edit_using_modal', 'id' => 'edit_child_speci_form')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Specification</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'value' => $spec['Specification']['id'], 'type' => 'hidden', 'id' => 'spec_id')); ?>                          
                <p>
                    <?php echo $this->Form->input('specification_name', array('label' => false, 'class' => 'form-control', 'data-bvalidator' => 'required', 'placeholder' => 'Enter Specification Name', 'id' => 'spec_name'));
                    ?></p>
                <div>
                    <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-success')); ?>                   
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<!--  edit options -->
<div class="modal fade model-display" id="edit_child_opt" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <?php echo $this->Form->create('SpecificationOption', array('controller' => 'specification_options', 'action' => 'edit_op', 'id' => 'edit_opt_speci_form')); ?>
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Edit Options</h4>
            </div>
            <div class="modal-body">
                <?php echo $this->Form->input('id', array('class' => 'form-control', 'label' => false, 'type' => 'hidden', 'id' => 'opt_id')); ?>                          
                <p>
                    <?php echo $this->Form->input('option_name', array('label' => false, 'class' => 'form-control', 'data-bvalidator' => 'required', 'placeholder' => 'Enter Option Name', 'id' => 'opt_name'));
                    ?></p>
                <div>
                    <?php echo $this->Form->submit('Submit', array('class' => 'btn btn-success')); ?>                   
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
</div>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5>Add Options To <?php echo $spec['Specification']['specification_name'] ?></h5>
    </div>
    <div class="ibox-content"> 
        <table class="table table-bordered">
            <tr>
                <th>Specifications</th>
                <th>Options</th>
            </tr>
            <tr>
                <td><?= ucwords($spec['Specification']['specification_name']); ?>
                    <div class="pull-right">
                        <a data-toggle="modal" class = 'btn btn-sm btn-warning fa fa-plus color-black tooltips add_child_option' href="#add_child_opt" specification_id ="<?= $spec['Specification']['id'] ?>" data-toggle = 'tooltip' data-placement = 'top' title = 'Add Options'>                                                                                                                                                                                                 
                        </a> 
                        <a data-toggle="modal" class = 'btn btn-sm btn-info fa fa-pencil color-black tooltips edit_child_speci' href="#edit_child_spec" spec_id ="<?= $spec['Specification']['id']; ?>" spec_name="<?= $spec['Specification']['specification_name'] ?>" data-toggle = 'tooltip' data-placement = 'top' title = 'Edit Specification'>                                                                                                                                                                                                 
                        </a>
                        <?php echo $this->Form->postLink(__(''), array('controller' => 'specifications', 'action' => 'delete', $spec['Specification']['id']), array('class' => 'btn btn-sm btn-danger fa fa-trash-o color-black tooltips', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Delete Specification'), __('Are you sure you want to delete # %s?', $spec['Specification']['specification_name'])); ?>
                    </div>
                </td>
                <td>
                    <ul>
                        <? foreach ($spec['SpecificationOption'] as $op): ?>
                        <li>
                            <?= ucwords($op['option_name']); ?>
                            <div class="pull-right">
                                <a data-toggle="modal" class = 'btn btn-sm btn-info fa fa-pencil color-black tooltips edit_child_option' href="#edit_child_opt" opt_id ="<?= $op['id']; ?>" opt_name="<?= $op['option_name'] ?>" data-toggle = 'tooltip' data-placement = 'top' title = 'Edit Options'>                                                                                                                                                                                                 
                                </a>
                                <?php echo $this->Form->postLink(__(''), array('controller' => 'specification_options', 'action' => 'delete', $op['id']), array('class' => 'btn btn-sm btn-danger fa fa-trash-o color-black tooltips', 'data-toggle' => 'tooltip', 'data-placement' => 'top', 'title' => 'Delete Option'), __('Are you sure you want to delete # %s?', $op['option_name'])); ?>
                            </div>
                        </li>
                        <br>
                        <? endforeach; ?>
                    </ul>
                </td>
            </tr>
        </table>
        <div class="row">
            <div class="text-center">
                <a href="<?php echo Router::url(array('controller' => 'specifications', 'action' => 'index')) ?>" class="btn btn-sm btn-success">Back</a>      
            </div>
        </div>
    </div>
</div>



