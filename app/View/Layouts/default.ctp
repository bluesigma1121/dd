<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->  
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->  
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->  
    <head>
        <?= $this->element('front_css_js'); ?>
    </head>
    <body>
        <div class="wrapper">
            <?php echo $this->element('front_header');?>
            <?php echo $this->Session->flash(); ?>
            <?php echo $this->fetch('content'); ?>
            <?php echo $this->element('front_footer');?>
        </div>


        
        <?php
            echo $this->Html->script(array(
                // 'front/plugins/jquery/jquery.min.js',
                'front/jquery.tokeninput.js',
                'front/jquery.bvalidator-yc.js',
                'front/plugins/jquery.parallax.js',
                // 'front/plugins/owl-carousel.js',
                //'front/jquery.fancybox.min.js',
                'front/plugins/bootstrap/js/bootstrap.min.js',
                // 'front/plugins/back-to-top.js',
                // 'front/app.js',
                'front/front_custom.js'
            ), ['async'] );
        ?>

        <!-- Google Captcha Code Start here VG-15/2/2015  -->
        <?php if(($this->request->params['controller'] == "enquiries" && $this->request->params['action'] == "get_lead_info_form") || ($this->request->params['controller'] == "users" && $this->request->params['action'] == "contactus")){ ?>

            <script src='https://www.google.com/recaptcha/api.js'></script>
            <script>
            
                $("#contact_us").submit(function(event) {

                var recaptcha = $("#g-recaptcha-response").val();
                if (recaptcha === "") {
                    event.preventDefault();
                    alert("Please check the recaptcha");
                }
            });
                $("#enquiry_form").submit(function(event) {

                    var recaptcha = $("#g-recaptcha-response").val();
                    if (recaptcha === "") {
                        event.preventDefault();
                        alert("Please check the recaptcha");
                    }
                });
            </script>
        <?php } ?>
        <!-- Google Captcha Code end here VG-15/2/2015  -->

        <?php echo $this->Html->script(array('ga-code')); ?>
        <!--Start of Zopim Live Chat Script-->
        <script type="text/javascript">
            window.$zopim || (function (d, s) {
                var z = $zopim = function (c) {
                    z._.push(c)
                }, $ = z.s =
                        d.createElement(s), e = d.getElementsByTagName(s)[0];
                z.set = function (o) {
                    z.set.
                            _.push(o)
                };
                z._ = [];
                z.set._ = [];
                $.async = !0;
                $.setAttribute("charset", "utf-8");
                $.src = "//v2.zopim.com/?2vMaeOWSrkq7wNCPHgcNhmuYo3G6tfws";
                z.t = +new Date;
                $.
                        type = "text/javascript";
                e.parentNode.insertBefore($, e)
            })(document, "script");
        </script>
        <!--End of Zopim Live Chat Script-->


        <!-- New autocomplete search code Start VG-25/11/2016  -->
        <script type="text/javascript">
            $(document).ready(function(){
                var query=null;
                var oldquery=null;
                $('#search_text').on('keyup',function(e){ 
                    
                    query=$(this).val();
                        if(query != ' ' || query.length > 0)
                        {
                            if(e.keyCode !=38 && e.keyCode != 40)        
                            {          
                                $.ajax({
                                    url: "<?php echo Router::url(['controller' => 'Categories', 'action' => 'home_search_autofill']);?>",
                                        method:"POST",
                                        async: true,
                                        data:{query:query},
                                        success:function(data)
                                        {  
                                            $('#suggest').fadeIn();
                                            $('#suggest').html(data);
                                        }
                                        
                                });
                        }
                        }
                        else{
                            
                                $('#suggest').remove();
                        } 
                });
                
                $(document).on('click', '#result', function(){  
                        //$('#search_text').val($(this).text());  
                    $('#suggest').fadeOut();  
                }); 

                $(document).on('mouseenter','#result',function(){
                    query=$("#search_text").val(); 
                    if(query != ' ' || query.length > 0)
                    {
                        $(this).prev().removeClass('selected');
                            $(this).addClass('selected');
                    }
                    else
                    {
                        $('#suggest').fadeOut(5000);  
                    }
                });
                $(document).on('mouseleave','#result',function(){
                    $(this).removeClass('selected');
                
                });
            });
        </script>
        

        <script>

            function acceptCondition() {
                $('#viewTermsAndCodition').modal('hide');
                $('#enquiry_form').submit();
            }
            function notAcceptCondition() {
                $('#viewTermsAndCodition').modal('hide');
                $('#EnquiryTerm').prop("checked", false);
            }
            
            function openModal(te) {
                if($('#EnquiryTerm').is(':checked')) {
                $.ajax({
                    url: $('#termsandcondition').val(),
                }).done(function(output) {      
                    $("#viewTermsAndCodition").on("shown.bs.modal", function () {
                        $(this).find('.modal-body').html(output)
                    }).modal('show'); 
                });
                }
            }

        </script>

        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" defer="defer">    
    </body>
</html>
