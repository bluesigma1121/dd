<!DOCTYPE html>

<html>

    <head> 
        <style>
            .min_height_content{
                min-height:1200px !important;
            }
        </style>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>decisiondatabases.com | Dashboard</title>
        <?php echo $this->element('admin_css_js'); ?>
    </head>
    <body>
        <div id="wrapper">
            <?php echo $this->element('admin_sidebar'); ?>
            <div id="page-wrapper" class="gray-bg dashbard-1">
                <?php echo $this->element('admin_header'); ?>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="wrapper wrapper-content min_height_content">
                            <?php echo $this->Session->flash(); ?>
                            <?php echo $this->fetch('content'); ?>
                        </div>
                        <?php echo $this->element('admin_footer'); ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
