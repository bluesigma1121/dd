<?php //debug($order_details); debug($order_pro); ?>
<table  border="0" cellpadding="0" cellspacing="0" align="left"> 
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            New order received for product <?= $order_pro['OrderProduct']['product_name'] ?><br/><br/>           
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Customer Name : <?php echo $order_details['Order']['ship_user_name']; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Email : <?php echo $order_details['Order']['ship_email']; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Mobile : <?php echo $order_details['Order']['mobile']; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Product Name : <?php echo $order_pro['OrderProduct']['product_name']; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Order ID : <?php echo $order_pro['OrderProduct']['order_id']; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Token : <?php echo $order_details['Order']['token']; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Payer Code : <?php echo $order_details['Order']['payer_code']; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Transaction No. : <?php echo $order_details['Order']['transaction_no']; ?> 
        </td>
    </tr>
    
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            License Type : <?php echo $license_type[$order_pro['OrderProduct']['licence_type']]; ?> 
        </td>
    </tr>
    
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Price : <?php echo $order_details['Order']['total_cost']; ?> 
        </td>
    </tr>
</table>