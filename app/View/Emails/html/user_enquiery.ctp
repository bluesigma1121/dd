<table  border="0" cellpadding="0" cellspacing="0" align="left"> 
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Dear <?php echo ucfirst($data['User']['first_name']); ?>,<br/><br/>        
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Thank you for connecting with <strong>Decision Databases.</strong> <br/><br/> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
          We received your inquiry for  <span style="font-style:italic"><?= $pro_name['Product']['product_name'] ?></span><br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        One of our representative will soon assist you through the purchase of this report.  In the mean time, if you have any specific research requirement, please write back to us so that we can collectively work on it and provide you with the most relevant report.<br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        We wish you good health.<br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        Thanks <br/>
        Warm Regards,<br/>
        Diksha Hada | Business Development<br/>
        50,000+ Industry & Country Research Reports<br/>
        Email ID: <a href="mailto:sales@decisiondatabases.com">sales@decisiondatabases.com</a><br/>
        Website: <a href="https://www.decisiondatabases.com/" target="_blank">www.decisiondatabases.com</a> <br/><br/>
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 12px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        Disclaimer:
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 12px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
        This email and any files transmitted with it are confidential and contain privileged or copyright information. You must not present this message to another party without gaining permission from the sender. If you are not the intended recipient you must not copy, print, distribute, publish or use this email or the information contained in it for any purpose other than to notify the sender. If you have received this message in error, please notify the sender immediately, and delete this email from your system.<br/><br/>
        </td>
    </tr>
    <!-- <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
           We will share the sample report shortly.
        </td>
    </tr> -->
</table>