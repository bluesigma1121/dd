<table  border="0" cellpadding="0" cellspacing="0" align="left"> 
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            New Lead Received for   <?= $product_name ?><br/><br/>           
        </td>
    </tr>
     <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Lead ID : <?php echo $enq_id; ?> 
        </td>
    </tr>
    <tr>
        <td  class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Name : <?php echo ucfirst($data['User']['first_name']); ?>   <?php echo ucfirst($data['User']['last_name']); ?>   
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Email : <?= $data['User']['email'] ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Number : <?= $data['User']['mobile'] ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Organisation : <?= $data['User']['organisation']; ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Job Title : <?= $data['User']['job_title']; ?>
        </td>
    </tr>
    
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Country : <?= $data['User']['country']; ?>
        </td>
    </tr>
    
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Subject : <?= $enq_data['Enquiry']['subject'] ?>
        </td>
    </tr>
    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Message : <?= $enq_data['Enquiry']['message'] ?>
        </td>
    </tr>

    <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Visited Ip : <?= $enq_data['Enquiry']['visited_ip'] ?>
        </td>
    </tr>
      <tr>
        <td   class="center" style="font-size: 16px; color: #687074; font-weight: normal; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
            Ref Page : <?=  $enq_data['Enquiry']['ref_page'] ?>
        </td>
    </tr>
</table>

