<div class="userCategories view">
<h2><?php echo __('User Category'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($userCategory['UserCategory']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('User'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userCategory['User']['title'], array('controller' => 'users', 'action' => 'view', $userCategory['User']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Category'); ?></dt>
		<dd>
			<?php echo $this->Html->link($userCategory['Category']['id'], array('controller' => 'categories', 'action' => 'view', $userCategory['Category']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($userCategory['UserCategory']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($userCategory['UserCategory']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit User Category'), array('action' => 'edit', $userCategory['UserCategory']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete User Category'), array('action' => 'delete', $userCategory['UserCategory']['id']), null, __('Are you sure you want to delete # %s?', $userCategory['UserCategory']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List User Categories'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User Category'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New User'), array('controller' => 'users', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
	</ul>
</div>
