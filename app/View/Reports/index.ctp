<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
          <h5>Log Report</h5> 
      </div>
      <div class="ibox-content">
        <form class="form-inline" >
          <div class="row">
            
            <div class="col-md-3">
              <label for="">From :</label>
              <input type="date" name="from" class="form-control input-sm" value="<?php echo empty($_GET['from']) ? date('Y-m-d') : $_GET['from'] ?>">
            </div>
            <div class="col-md-3">
              <label for="">To :</label>
              <input type="date" name="to" class="form-control input-sm" value="<?php echo empty($_GET['to']) ? date('Y-m-d') : $_GET['to'] ?>"><br>
            </div>
            
            <div class="col-lg-1 search_mtp" style="margin-top:10px">
              <input type="submit" class="btn btn-outline btn-md btn-primary fa fa-search" value="Filter">
            </div>
            <?php if(!empty($result)) { ?>
              <div class="col-lg-1 search_mtp" style="margin-top:10px">
                <a href="<?php echo Router::url(array('controller' => 'reports', 'action' => 'exporttoexcel',$_GET['from'],$_GET['to'])); ?>"  class="btn btn-outline btn-md btn-primary fa fa-download" target="_blank">&nbsp;&nbsp; Export Excel</a>
                
              </div>
            <?php } ?>
          </div>
        </form>
        <hr>
        <div class="row">
          <?php if(!empty($result)) { ?>
            <div class="table-responsive mtp">
              <table id="enq_index_tbl" class="table table-bordered table-striped">
                <thead>
                    <tr>
                      <th>#</th>
                      <th>Product Name</th>
                      <th>Publisher</th>
                      <th>Modified On</th>
                    </tr>
                </thead>
                <tbody>
                  <?php foreach ($result as $key => $value) {  ?>
                    <tr>
                      <td><?php echo $key + 1 ?></td>
                      <td><?php echo $value['Product']['product_name']?></td>
                      <td><?php echo $value['Product']['publisher_name']?></td>
                      <td><?php echo $value['Product']['modified']?></td>
                    </tr>
                  <?php } ?>
                </tbody>
              </table>
            </div>
          <?php } ?>
        </div>
      </div>
    </div>
  </div>
</div>