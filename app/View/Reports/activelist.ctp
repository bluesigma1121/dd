<div class="row">
  <div class="col-lg-12">
    <div class="ibox float-e-margins">
      <div class="ibox-title">
          <h5>Log Report</h5> 
      </div>
      <div class="ibox-content">
        <form class="form-inline"  action="<?php echo Router::url(array('controller' => 'reports', 'action' => 'activelistexport')); ?>" target="_blank">
          <div class="row">
            <div class="col-md-5">
              <label for="">Publisher :</label>
              <select name="publisher" class="form-control">
                <option value="">Select Publisher</option>
                <?php foreach ($publishers as $key => $value) { ?>
                  <option value="<?php echo $value['Product']['publisher_name']?>" <?php echo !empty($_GET['publisher']) && $_GET['publisher'] == $value['Product']['publisher_name'] ? 'selected' : '' ?> ><?php echo $value['Product']['publisher_name']?></option>
                <?php } ?>
              </select>
              <!-- <input type="text" name="publisher" class="form-control input-sm" value="<?php echo empty($_GET['publisher']) ? '' : $_GET['publisher'] ?>"> -->
            </div>
            
            <div class="col-lg-1 search_mtp" style="margin-top:10px">
              <input type="submit" class="btn btn-outline btn-md btn-primary fa fa-search" value="Export To Excel">
            </div>
          </div>
        </form>
        
      </div>
    </div>
  </div>
</div>