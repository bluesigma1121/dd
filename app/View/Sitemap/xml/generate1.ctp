<?php App::uses('CakeTime', 'Utility'); ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <!--   Product   -->
    <?php if(!empty($products)){ ?>
        <?php foreach ($products as $product): ?>
        <url>
            <loc><?php echo (Router::url('/', true) . "ip/" . $product['Product']['id'] . "-" . $product['Product']['slug']); ?></loc>      
            <lastmod><?php echo $this->Time->toAtom($product['Product']['modified']); ?></lastmod>
            <priority>0.9</priority>
            <changefreq>daily</changefreq>
        </url>
        <?php endforeach; ?>
    <?php }?>
<!--   Page   -->
</urlset>