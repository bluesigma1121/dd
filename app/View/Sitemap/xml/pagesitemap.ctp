<?php App::uses('CakeTime', 'Utility'); ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

    <?php foreach ($categories as $category): ?>
    <url>
        <loc><?php echo (Router::url('/', true) . "reports/" . $category['Category']['id'] . "-" . $category['Category']['cat_slug']); ?></loc>      
        <lastmod><?php echo $this->Time->toAtom($category['Category']['modified']); ?></lastmod>
        <priority>0.9</priority>
        <changefreq>weekly</changefreq>
    </url>
    <?php endforeach; ?>
</urlset>