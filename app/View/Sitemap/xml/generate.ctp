<?php App::uses('CakeTime', 'Utility'); ?>
<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">
    <!--   Domain   -->    
    <url>
        <loc><?php echo $this->Html->url('/',true); ?></loc>
        <changefreq>daily</changefreq>
        <priority>1.0</priority>
    </url>
    <?php if(!empty($products)){ ?>
        <url>
            <loc><?php echo (Router::url('/', true) . "sitemap1.xml") ?></loc>
            <priority>0.9</priority>
            <changefreq>daily</changefreq>
        </url>
    <?php } ?>

    <?php if(!empty($products1)){ ?>
        <url>
            <loc><?php echo (Router::url('/', true) . "sitemap2.xml") ?></loc>
            <priority>0.9</priority>
            <changefreq>daily</changefreq>
        </url>
    <?php } ?>

    <?php if(!empty($products2)){ ?>
        <url>
            <loc><?php echo (Router::url('/', true) . "sitemap3.xml") ?></loc>
            <priority>0.9</priority>
            <changefreq>daily</changefreq>
        </url>
    <?php } ?>
    <!--   Category   -->

    <?php if(!empty($categories)){ ?>
        <url>
            <loc><?php echo (Router::url('/', true) . "cat-sitemap.xml") ?></loc>
            <priority>0.9</priority>
            <changefreq>daily</changefreq>
        </url>
    <?php } ?>
    
<!--   Page   -->
</urlset>