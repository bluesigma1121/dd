<div class="breadcrumbs">
    <div class="container">
        <h1 class="pull-left"><?php echo $heading ?></h1>
    </div>
</div>

<div class="container content">		
    <div class="row">
        <div class="col-md-12">
            <!-- News Block-->
            <?php foreach ($articles as $article): ?>
                <div class="row clients-page">
                    <div class="col-md-12">
                        <h3>
                            <a href="<?php echo Router::url(array('controller'=>'articles','action'=>'article_details','folder'=>$type,'article_id'=>$article['Article']['id'],'url_slug'=>$this->Link->cleanString($article['Article']['slug']))); ?>">
                                <?php echo $article['Article']['headline'] ?>
                            </a>
                        </h3>
                        <ul class="list-inline">
                            <li>
                                <strong>
                                    <i class="fa fa-map-marker color-green"></i> 
                                    Published On: <?php echo date('d-F-Y', strtotime($article['Article']['created'])) ?>
                                </strong>
                            </li>
                        </ul>
                        <p>
                            <?php
                            echo $this->Text->truncate(
                                    strip_tags($article['Article']['description']), 400, array(
                                'ellipsis' => '...',
                                'exact' => false
                                    )
                            );
                            ?>

                        </p>
                    </div>
                </div>
                <hr/>
            <?php endforeach; ?>
            <!-- End News Block-->
            <?php /*
            <p>
                <?php
                echo $this->Paginator->counter(array(
                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                ));
                ?>	</p>
            <div class="paging">
                <?php
                echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
                echo $this->Paginator->numbers(array('separator' => ''));
                echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
                ?>
            </div>
             * 
             */?>
        </div><!--/col-md-12-->
    </div><!--/row-->        
</div>