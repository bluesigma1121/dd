<div class="articles index">
    <div class="row">
        <div class="col-md-4">
            <h2><?php echo __('Articles'); ?></h2>
        </div>
        <div class="col-md-6">
            <?php echo $this->Form->create('Article', array('id' => 'index', 'class' => 'form-horizontal')); ?>
                <div class="row">
                    <div class="col-md-8">
                        <?php echo $this->Form->input('search_text', array('type' => 'text', 'class' => 'form-control', 'placeholder' => 'Enter Text For Search', 'label' => false)); ?>
                    </div>
                    <div class="col-md-4">
                        <?php echo $this->Form->button(' Search', array('type' => 'submit', 'class' => 'btn btn-outline btn-lg btn-primary fa fa-search')); ?>
                    </div>
                </div>
            <?php echo $this->form->end(); ?>
        </div>
        <div class="col-md-2">
            <?php echo $this->Html->link('Add Article',array('action'=>'add'),array('class'=>'btn btn-primary')) ?>
        </div>
    </div>

    <table class="table table-bordered">
        <tr>
            <th><?php echo $this->Paginator->sort('id'); ?></th>
            <th><?php echo $this->Paginator->sort('headline'); ?></th>
            <th><?php echo $this->Paginator->sort('article_type'); ?></th>
            <th><?php echo $this->Paginator->sort('category_id'); ?></th>            
            <th><?php echo $this->Paginator->sort('meta_title'); ?></th>
            <th><?php echo $this->Paginator->sort('meta_desc'); ?></th>
            <th><?php echo $this->Paginator->sort('meta_keywords'); ?></th>
            <th class="actions"><?php echo __('Actions'); ?></th>
        </tr>
        <?php foreach ($articles as $article): ?>
            <tr>
                <td><?php echo h($article['Article']['id']); ?>&nbsp;</td>
                <td><?php echo h($article['Article']['headline']); ?>&nbsp;</td>
                <td><?php echo h($article['Article']['article_type']); ?>&nbsp;</td>
                <td>
                    <?php echo $this->Html->link($article['Category']['category_name'], array('controller' => 'categories', 'action' => 'view', $article['Category']['id'])); ?>
                </td>
                <td><?php echo h($article['Article']['meta_title']); ?>&nbsp;</td>
                <td><?php echo h($article['Article']['meta_desc']); ?>&nbsp;</td>
                <td><?php echo h($article['Article']['meta_keywords']); ?>&nbsp;</td>
                <td class="actions">
                    <?php echo $this->Html->link(__('View'), array('action' => 'view', $article['Article']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim tooltip-f')); ?>
                    <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $article['Article']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim tooltip-f')); ?>
                    <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $article['Article']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim tooltip-f'), __('Are you sure you want to delete # %s?', $article['Article']['id'])); ?>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <p>
        <?php
        echo $this->Paginator->counter(array(
            'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
        ));
        ?>	</p>
    <div class="paging">
        <?php
        echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
        echo $this->Paginator->numbers(array('separator' => ''));
        echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
        ?>
    </div>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('New Article'), array('action' => 'add')); ?></li>
        <li><?php echo $this->Html->link(__('List Categories'), array('controller' => 'categories', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('New Category'), array('controller' => 'categories', 'action' => 'add')); ?> </li>
    </ul>
</div>
