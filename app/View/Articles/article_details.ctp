<style>
.section-block h1 {
    color: #333;
    font-size: 28px;
    font-weight: 200;
    margin: 10px 0 15px;
}
</style>
<div class="container content">		
    <div class="container mar_top_bot">
        <div class="row">
            <ul class="pull-left breadcrumb">
                <li>
                    <a href="<?php echo Router::url('/', true) ?>"> Home </a>               
                </li>

                <li> 
                    <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'article_listing', 'section' =>$folder)); ?>"><?php echo $heading ?></a>
                </li>
                <li><a href="<?php Router::url(array('controller' => 'articles', 'action' => 'article_details', 'folder' => $folder, 'article_id' => $article['Article']['id'], 'url_slug' => $this->Link->cleanString($article['Article']['slug']))); ?>"><?php echo $article['Article']['headline'] ?></a></li>
            </ul>
        </div>
    </div>

    <div class="row blog-page blog-item left-inner section-block">
        <!-- Left Sidebar -->
        <div class="col-md-12 md-margin-bottom-60">
            <!--Blog Post-->        
            <div class="blog margin-bottom-40">
                <h1><?php echo $article['Article']['headline']; ?></h1>
                <ul class="details list-inline">
                    <li id="first"><?php echo $heading ?></li>
                    <li class="spart_pd"><?php echo date('d-F-Y', strtotime($article['Article']['created'])) ?></li>
                </ul>
                <br/>
                <p><?php echo $article['Article']['description']; ?></p>
            </div>
            <!--End Blog Post-->
            <hr>
        </div>
    </div><!--/row-->        
</div>