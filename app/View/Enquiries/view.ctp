<div class="row">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="m-b-md text-center">
                                <h2 class="btn btn-primary btn-rounded btn-block">Inquiry Details</h2>
                            </div>
                        </div>
                        <div class="col-lg-4"> </div>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-lg-2">
                        <strong> Name :</strong>
                    </div>
                    <div class="col-lg-2"> 
                        <?php echo h(ucfirst($enquiry['User']['name'])); ?>
                    </div>
                    <div class="col-lg-2">
                        <strong>Category Name :</strong>
                    </div>
                    <div class="col-lg-2"> 
                        <?php echo h($enquiry['Category']['category_name']); ?>
                    </div>
                    <div class="col-lg-2">
                        <strong>  Date :</strong>
                    </div>
                    <div class="col-lg-2"> 
                        <?php echo Date('d-M-Y h:i a',strtotime($enquiry['Enquiry']['created'])); ?>
                    </div>
                </div>


                <div class="row"> 
                    <div class="col-lg-2">
                        <strong>  Subject :</strong>
                    </div>
                    <div class="col-lg-10"> 
                        <?php echo h($enquiry['Enquiry']['subject']); ?>
                    </div>
                </div> 

                <div class="row">
                    <div class="col-lg-2">
                        <strong>  Message :</strong> 
                    </div>
                    <div class="col-lg-10"> 
                        <?php echo h($enquiry['Enquiry']['message']); ?>
                    </div>
                </div> 

                <div class="row">
                    <div class="text-center">
                        <button onclick="goBack();" class="btn btn-info btn-rounded btn-outline">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>