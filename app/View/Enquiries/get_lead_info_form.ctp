<style type="text/css">
    .mrtp_enq{
        padding: 5px;
    }
    .margin-bottom-40{
        margin-top: 15px;
    }
    .reg-header {
        border-bottom: none;
    }
    .mar_top_hed{
        margin-top: -40px;
        color: #555;
        text-align: center;
        border-bottom: solid 1px #eee;
    }
    .paypal-logo{
      margin-right: -60px;
      z-index: 99;
    }
    .logo-margin{
      margin-right: -40px;
    }
    .payment-logo{
      margin-top: -10px;
    }
</style>
<script type="text/javascript">

 /* Ciode Start VG-6-1-2017*/
    var options = {
        validateOn: 'keydown',
       position:{x:'center', y:'top'},
    };
/* Code END VG-6-1-2017*/

    $(document).ready(function () {

        $('#enquiry_form').bValidator(options);

        /*Copy and Paste Prevention Code Start VG-3-1-2017*/
          $('#EnquiryEmail').bind('copy paste cut',function(e) {
         e.preventDefault();
         });

        $('#EnquiryConfirmEmail').bind('copy paste cut',function(e) {
         e.preventDefault();
         });
         /*Copy and Paste Prevention Code End VG-3-1-2017*/

    });
</script>
<div class="container content">
    <div class="row margin-bottom-40">
        <!--register-->
        <div class="container content">
            <div class="row">
                <div class="col-md-10 col-md-offset-1 col-sm-12">
                 <!-- code starts here-VG-06/08/2016 -->

                <?php //echo "<pre>"; print_r($related_category_name['Category']['id']);exit; //echo $this->Form->hidden('cat_main', ['value'=>$cat_main]);?>

                <?php if(!empty($related_category_name[0]['Category']['id'])) { ?>
                    <?php  echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
                    <div class="reg-header"><h2><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($cat_main), 'id' => $pro_name['Product']['id'], 'slug' => $pro_name['Product']['slug'])); ?> " target="_blank"> <?php echo $pro_name['Product']['product_name'] ?></a></h2></div> <br/>
                    <div class="reg-header mar_top_hed"><h2><?php echo $ref_page ?>&nbsp; &nbsp;<a  class="btn-u btn-u-green btn-xs" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $related_category_name[0]['Category']['id'], 'slug' => $this->Link->cleanString($related_category_name[0]['Category']['cat_slug']))); ?>">View Related Reports
                        </a> </h2>
                <?php } else {?>
                    <?php  echo $this->Form->create('Enquiry', array('id' => 'enquiry_form', 'class' => 'reg-page', 'type' => 'file')); ?>
                    <div class="reg-header"><h2><a href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_details', 'main' => $this->Link->cleanString($cat_main), 'id' => $pro_name['Product']['id'], 'slug' => $pro_name['Product']['slug'])); ?> " target="_blank"><?php echo $pro_name['Product']['product_name'] ?></a></h2></div> <br/>
                    <div class="reg-header mar_top_hed"><h2><?php echo $ref_page ?>&nbsp; &nbsp;<?php if(!empty($related_category_name['Category']['id'])){?><a class="btn-u btn-u-green btn-xs" target="_blank" href="<?php echo Router::url(array('controller' => 'products', 'action' => 'category_listing', 'id' => $related_category_name['Category']['id'], 'slug' => $this->Link->cleanString($related_category_name['Category']['cat_slug']))); ?>">View Related Reports
                        </a> <?php } ?></h2>
                <?php } ?>
                <!-- code ends here-VG-06/08/2016 -->
                    </div>

                    <div class="row">
                        <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'): ?>
                        <input type="hidden" name="pro_name" value="<?php echo $pro_name['Product']['product_name'];?>">
                            <div class="col-md-3">
                                <label>Select License Type : <span class="color-red">*</span></label>
                            </div>
                            <div class="col-md-9">
                                <?php
                                $options = $licence_type;
                                $attributes = array(
                                    'legend' => false, 'separator' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                                    'data-bvalidator' => 'required', 'value' => 1);
                                ?>
                                <?php echo $this->Form->radio('licence_type', $options, $attributes); ?>
                            </div>
                            <br/><br/>
                        <?php endif; ?>
                        <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now') { ?>
                        <div class="col-sm-2">
                            <label>Title<span class="color-red">*</span></label>
                            <?php echo $this->Form->input('title', array('class' => 'form-control', 'type' => 'select',
                                'options' => $title, 'placeholder' => 'Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                        </div>
                        <?php } ?>
                        <div class="col-sm-5">
                            <label>First Name<span class="color-red">*</span></label>
                            <?php echo $this->Form->input('first_name', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                'type' => 'text', 'value' => AuthComponent::user('first_name'), 'placeholder' => 'First Name',
                                'data-bvalidator' => 'required,alphadot','autocomplete'=>'off')); ?>
                        </div>
                        <div class="col-sm-5">
                            <label>Last Name<span class="color-red">*</span></label>
                            <?php echo $this->Form->input('last_name', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                'type' => 'text', 'value' => AuthComponent::user('last_name'), 'placeholder' => 'Last Name',
                                'data-bvalidator' => 'required,alphadot','autocomplete'=>'off')); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>Organisation <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('organisation', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                'type' => 'text', 'value' => AuthComponent::user('organisation'), 'placeholder' => 'Organisation',
                                'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                        </div>

                        <div class="col-sm-6">
                            <label>Designation <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('job_title', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                'type' => 'text', 'value' => AuthComponent::user('designation'), 'placeholder' => 'Designation',
                                'data-bvalidator' => 'required,alphadot','autocomplete'=>'off')); ?>
                        </div>

                        <div class="col-sm-12">
                            <label>Mobile <span class="color-red">*</span></label>
                            <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                'type' => 'text', 'placeholder' => 'Mobile', 'id' => 'customer_phone_test', 'value' => AuthComponent::user('mobile'),
                                'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required,alphanb','autocomplete'=>'off')); ?>
                        </div>
                        <?php if (!AuthComponent::user()): ?>
                            <div class="col-sm-12"></div>
                            <div class="col-sm-6">
                                <label>Corporate Email <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('email', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                    'type' => 'text', 'placeholder' => 'Corporate Email', 'value' => AuthComponent::user('email'),'data-bvalidator' => 'required,email','autocomplete'=>'off')); ?>
                            </div>
                           <div class="col-sm-6">
                                  <label>Confirm Email <span class="color-red">*</span></label>
                                  <?php echo $this->Form->input('Confirm_email', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                      'type' => 'text', 'placeholder' => 'Confirm Email', 'value' => AuthComponent::user('Confirm_email'),'data-bvalidator' => 'required,email,equalto[EnquiryEmail]','autocomplete'=>'off')); ?>
                            </div>

                            <div class="col-sm-12">
                            <?php else: ?>
                                <div class="col-sm-6">
                            <?php endif; ?>
                          <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){?>
                                <label>Address <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('address', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                    'type' => 'text', 'value' => AuthComponent::user('address'), 'placeholder' => 'Address',
                                    'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                            <?php } ?>
                            </div>

                        </div>


                        <div class="row">
                         <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){?>
                            <div class="col-sm-6">
                                <label>City <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('city', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                    'type' => 'text', 'value' => AuthComponent::user('city'), 'placeholder' => 'City', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                            </div>
                            <?php } ?>
                              <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){?>
                            <div class="col-sm-6">
                                <label>State <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('state', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                    'type' => 'text', 'value' => AuthComponent::user('state'), 'placeholder' => 'State', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                            </div>
                            <?php } ?>
                            <div class="col-sm-12">
                                <label>Country <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('country', array('class' => 'form-control margin-bottom-20', 'label' => false,
                                    'type' => 'text', 'value' => AuthComponent::user('country'), 'placeholder' => 'Country',
                                    'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                            </div>
                        <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){?>
                            <div class="col-sm-6">
                                <label>Zip code <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('pin_code', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'zipcode', 'value' => AuthComponent::user('pin_code'), 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                            </div>
                          <?php } ?>
                        </div>

                        <!--<div class="row">
                        <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){?>
                            <div class="col-sm-12">
                                <label>Subject <span class="color-red">*</span></label>
                                <?php echo $this->Form->input('subject', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => 'Subject', 'data-bvalidator' => 'required','autocomplete'=>'off')); ?>
                            </div>
                         <?php } ?>
                        </div>-->
                        <div class="row">
                            <?php if (!isset($ex_ref_page)){?>
                                <div class="col-sm-12">
                                    <label>Specific Requirement from this report <span class="color-red">*</span></label>
                                    <?php echo $this->Form->input('message', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'textarea', 'placeholder' => 'Specific Requirement from this report. ', 'data-bvalidator' => 'required,alphanum','autocomplete'=>'off')); ?>
                                    <label class="text-danger">URL not allowed in requirement.</label>
                                </div>
                            <?php } ?>
                        </div>
                         <!-- commented old Captcha VG-26/2/2018  -->

                        <!-- <div class="row">
                            <div class="col-sm-3 col-md-offset-2">
                                <label>Captcha<span class="color-red">*</span></label>
                                <?php echo $this->Html->image($this->Html->url(array('controller' => 'users', 'action' => 'captcha'), true), array('label' => false, 'width' => '150', 'height' => '40', 'alt' => 'captcha')); ?>
                            </div>
                            <div class="col-sm-3">
                                <label>Enter Captcha Code <span class="color-red">*</span></label>
                                <?php
                                echo $this->Form->input('captcha', array('data-bvalidator' => 'required', 'class' => 'input-large form-control hastip', 'data-placement' => 'right', 'type' => 'text', 'placeholder' => 'Enter Captcha Code',
                                    'title' => 'Enter the characters you see in the image.', 'label' => false,'autocomplete'=>'off'));
                                ?>
                            </div>
                        </div> -->
                <!-- commented old Captcha VG-26/2/2018  -->

                        <!--Change Captcha VG-26/2/2018  -->
                                    <div class="row margin-bottom-20">
                                      <div class="col-md-7 col-md-offset-0">
                                        <div class="g-recaptcha" data-sitekey="6LdfhkYUAAAAANU1u6w1Xd3r_0x6yz4h9ZmOjXlA"></div>
                                        <!-- <div class="g-recaptcha" data-sitekey="6LekeEYUAAAAAHPe-rEZtnpqMLcQ_TtFdWSsYsP3"  data-bvalidator='required'></div> -->
                                      </div>
                                    </div>
                        <!--Change Captcha VG-26/2/2018  -->
                        <br/><br/>
                        <!-- Selecting payment gateway -->
                        <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'){ ?>
                          <div class="row">
                              <div class="col-sm-1 paypal-logo">
                                <label>
                                    <input type="radio" name="payment_option" value="paypal">
                                </label>
                              </div>
                              <div class="col-sm-2 ">
                                <?php echo $this->Html->image('logos/paypal.png', array('class' => 'img-responsive','alt' => 'Paypal','width' => "100")); ?>
                              </div>
                              <div class="col-sm-1 logo-margin">
                                <label>
                                    <input type="radio" name="payment_option" value="ccavenue" checked>
                                </label>
                              </div>
                              <div class="col-ms-2 payment-logo">
                                <?php echo $this->Html->image('logos/visa_mastercart_amex.png', array('class' => 'img-responsive','alt' => 'Paypal','width' => "150")); ?>
                              </div>
                          </div>
                          <br/><br/>
                        <?php } ?>
                        <!-- *************************** -->
                        <div class="row">
                            <?php if (isset($ex_ref_page) && $ex_ref_page == 'buy-now'): ?>
                                <div class="col-lg-6 checkbox">
                                    <label>
                                        <?php echo $this->Form->checkbox('term', array('label' => false, 'data-bvalidator' => 'required', 'onclick' => 'openModal()')); ?>
                                        <input type="hidden" id="termsandcondition" value="<?php echo Router::url(array('controller' => 'users', 'action' => 'accept_terms_conditions')); ?>">

                                        I have read  <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'terms_conditions')); ?>"  target="_blank" class="color-green">Terms and Conditions</a> and I accept it.
                                    </label><br/>
                                    <label style="padding-left: 0px;">* Due to the nature of goods, orders once placed can't be cancelled.</label>
                                </div>
                            <?php endif; ?>

                        </div>
                          <div class="row">

                            <div class="col-lg-12 text-right">
                                <?php echo $this->Form->submit('Submit', array('class' => 'btn-u')) ?>
                            </div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>



<div class="modal  fade" id="viewTermsAndCodition" role="dialog" data-keyboard="false" data-backdrop="static">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Terms And Conditions</h4>
      </div>
      <div class="modal-body">
      </div>
    </div>
    
  </div>
</div>