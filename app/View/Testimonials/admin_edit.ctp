<script type="text/javascript">
    $(document).ready(function() {
        $('#edit_testimonial').bValidator();
    });
</script>

<style>
    .image_size{
        height: 100px;
        width: 100px;
    }
</style>
<?php echo $this->Html->script('back/ckeditor/ckeditor.js'); ?>
<div class="ibox float-e-margins">
    <div class="ibox-title">
        <h5> Add Product</h5>
    </div>
    <div class="ibox-content">
        <?php echo $this->Form->create('Testimonial', array('id' => 'edit_testimonial', 'class' => 'form-horizontal', 'type' => 'file')); ?>
        <div class="row col-lg-offset-1">
            <div class="form-group"><label class="col-lg-3 control-label">Title</label>
                <div class="col-lg-4">
                   <?php echo $this->Form->input('testimonial_title', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'Testimonial Title', 'label' => false, 'data-bvalidator' => 'required')); ?>
                                    <?php echo $this->Form->input('id'); ?>
                </div>
            </div>
            <div class="form-group"><label class="col-lg-3 control-label">Description</label>
                <div class="col-lg-6">
                     <?php echo $this->Form->input('testimonial_description', array('class' => 'ckeditor form-control', 'type' => 'textarea', 'placeholder' => 'Testimonial Description', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            
            
        <div class="text-center">
            <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-success btn-sm', 'div' => false)); ?>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
</div>