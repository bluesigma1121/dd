<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <div class="pull-right">
                    <?php echo $this->Html->link(__('Add Specification'), array('action' => 'add'), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'Add Specification')); ?>
                </div>
                <h5>Specifications</h5> 
            </div>
            <div class="ibox-content">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th><?php echo $this->Paginator->sort('specification_name'); ?></th>
                                <th><?php echo $this->Paginator->sort('is_searchable'); ?></th>
                                <th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                        </thead> 
                        <?php foreach ($specifications as $specification) { ?>
                            <tbody>
                            <td><?php echo h($specification['Specification']['specification_name']); ?>&nbsp;</td>
                            <td><?php echo h($yesno[$specification['Specification']['is_searchable']]); ?>&nbsp;</td>
                            <td class="actions">
                                <?php echo $this->Html->link(__('view'), array('action' => 'view', $specification['Specification']['id']), array('class' => 'btn btn-outline btn-sm btn-warning dim', 'title' => 'View')); ?>
                                <?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $specification['Specification']['id']), array('class' => 'btn btn-outline btn-sm btn-info dim', 'title' => 'Edit')); ?>              
                                <?php echo $this->Html->link(__('Add Options'), array('controller' => 'specification_options', 'action' => 'add', $specification['Specification']['id']), array('class' => 'btn btn-outline btn-sm btn-primary dim', 'title' => 'Add Options')); ?>
                                <?php echo $this->Html->link(__('View Options'), array('controller' => 'specification_options', 'action' => 'view_options', $specification['Specification']['id']), array('class' => 'btn btn-outline btn-sm btn-success dim', 'title' => 'View Options')); ?>              
                                <?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $specification['Specification']['id']), array('class' => 'btn btn-outline btn-sm btn-danger dim', 'title' => 'Delete'), __('Are you sure you want to delete # %s?', $specification['Specification']['specification_name'])); ?>
                            </td>
                            </tbody>
                        <?php } ?>
                    </table>
                    <div class="row col-md-12">
                        <div class="dataTables_paginate paging_bootstrap">
                            <p>
                                <?php
                                echo $this->Paginator->counter(array(
                                    'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
                                ));
                                ?> </p>
                            <ul class="pagination" style="visibility: visible;">
                                <li><?= $this->Paginator->prev(__('First'), array(), null, array('class' => 'prev disabled')); ?></li>
                                <li><?= $this->Paginator->numbers(array('separator' => '')); ?></li>
                                <li><?= $this->Paginator->next(__('Last'), array(), null, array('class' => 'next disabled')); ?></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>