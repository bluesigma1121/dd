<div class="row">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="m-b-md text-center">
                                <h2 class="btn btn-primary btn-rounded btn-block">Specification Details</h2>
                            </div>
                        </div>
                        <div class="col-lg-4"> </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <dl class="dl-horizontal">
                            <dt>Specification Name:</dt> <dd> <?php echo h($specification['Specification']['specification_name']); ?></dd>

                        </dl>
                    </div>
                    <div class="col-lg-4">
                        <dl class="dl-horizontal">
                            <dt>Is Searchable On:</dt> <dd> <?php echo h($yesno[$specification['Specification']['is_searchable']]); ?></dd>
                        </dl>
                    </div>
                    <div class="col-lg-4">
                        <dl class="dl-horizontal">
                            <dt>Created On:</dt> <dd> <?php echo h($specification['Specification']['created']); ?>                            </dd>
                        </dl>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center">
                        <button onclick="goBack();" class="btn btn-info btn-rounded btn-outline">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>