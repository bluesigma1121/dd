<div class="container content">		
    <div class="row margin-bottom-40">
        <div class="col-md-12 md-margin-bottom-40">
            <div class="headline"><h2>Refund Policy</h2></div>
            <p>
            As per industry standards and the nature of our product, we do not reimburse/ refund or cancel orders post payment receipt. 
Buyers are instructed to read and validate the product details and clarify all their queries over mail through sales@decisiondatabases.com before proceeding for the payment. The company therefore does not offer a refund on the sold products but hereby agrees to resolve actively all the post purchase concerns and queries at the earliest.</p>
<p> 
Decision Databases solely keeps the right to determine the validity of a refund request and is not legally liable both civilly and criminally for any chargebacks and fees involved in the transaction. 
Under no circumstances we reimburse the payment, instead provide the customer with an equivalent credit amount which can be used within a year.
            <!-- The nature of the services that are provided by Decision Databases are digital commodity. These services are considered to be consumed as soon as it is delivered to the buyer. The company therefore does not offer a refund on the sold products. -->
            
            </p>
            <!-- <p>The company solely keeps the right to determine the validity of a refund request.</p> -->
        </div>
    </div>
</div>