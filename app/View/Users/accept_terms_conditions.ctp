<p>
    DecisionDatabases.com provides its services to you subject to the following conditions. 
    If you visit or shop at DecisionDatabases.com, you accept these conditions. Please read them 
    carefully. We reserve the right from time to time to make changes to these Terms and Conditions 
    at our absolute discretion. The Terms and Conditions applying to each transaction with us are 
    those which are available on the website at the time that you place your order and it is your 
    responsibility to check the Terms and Conditions before placing an order.
</p>
<strong>Privacy</strong>
<p>
    Our Privacy Notice is at the foot of these Terms and Conditions and governs your visit to 
    DecisionDatabases.com. Please read it carefully so that you understand our practices.
</p>

<strong>Electronic Communications</strong>
<p>
    When you visit DecisionDatabases.com or send e-mails to us, you are communicating with us 
    electronically. You consent to receive communications from us electronically. We will 
    communicate with you by e-mail or by posting notices on this site. You agree that all 
    agreements, notices, disclosures and other communications that we provide to you electronically 
    satisfy any legal requirement that such communications be in writing.
</p>

<strong>Copyright</strong>
<p>
    All content included on this site, such as text, graphics, logos, button icons, images, and 
    audio clips, digital downloads, data compilations, and software, is the property of 
    DecisionDatabases.com or its content suppliers and protected by international copyright laws. 
    The compilation of all content on this site is the exclusive property of DecisionDatabases.com 
    and protected by international copyright laws. All software used on this site is the property 
    of DecisionDatabases.com or its software suppliers and protected by international copyright laws. 
    You agree not to copy, reproduce, duplicate, sell, resell, or exploit for any commercial purposes, 
    any portion of the Site, use of the Site, or access to the Site. You may not re-use and/or extract 
    part of the content of this website without DecisionDatabases.com’ express consent in writing. 
    In particular, you may not utilize any data mining, robots or similar search and extraction tools 
    for re-use of any substantial part of this website without DecisionDatabases.com’ express consent 
    in writing. You may not use substantial parts of this website (for example product listing and 
    prices) on your own website without our express consent in writing.
</p>

<strong>Our Contract</strong>
<p>
    Once you place an order for a product listed on our website, payment for that product becomes due 
    and owing immediately. Notwithstanding the fact that the product will not be dispatched until 
    monies are received in full, you are liable to pay to DecisionDatabases.com all monies due on foot 
    of the order. Once we receive your order we have to purchase the product from the publisher, 
    process the payment and arrange the shipping. Because of this element of work involved, we regret 
    that it is not possible for an order to be cancelled once it is submitted. DecisionDatabases.com 
    will not be responsible for any delay or failure to comply with our obligations under our contract 
    with you where such delay or failure arises from any cause which is beyond our reasonable control. Billing and Invoicing will be done in the name of BLUESIGMA BUSINESS CONSULTING SOLUTIONS LLP. 
</p>
<strong>License and Site Access</strong>
<p>
    DecisionDatabases.com grants you a limited revocable license to create a link to the home page 
    of DecisionDatabases.com provided that the link does not portray DecisionDatabases.com, its 
    associated companies, or its products, in a negative, defamatory, derogatory or misleading way. 
    You may not use any DecisionDatabases.com logos, graphics or trademarks as part of the link without 
    our express consent in writing. Access and make personal use of this site but not to download 
    (other than page caching) or modify it, or any portion of it, except with the express written 
    consent of DecisionDatabases.com. This license does not include any resale or commercial use of 
    this site or its contents; any collection and use of any product listings, descriptions, or prices; 
    any derivative use of this site or its contents; any downloading or copying of account information 
    for the benefit of another merchant; or any use of data mining, robots, or similar data gathering 
    and extraction tools. This site or any portion of this site may not be reproduced, duplicated, copied, 
    sold, resold, visited, or otherwise exploited for any commercial purpose without express written 
    consent of DecisionDatabases.com. You may not frame or utilize framing techniques to enclose any 
    trademark, logo, or other proprietary information (including images, text, page layout, or form) of 
    DecisionDatabases.com and our affiliates without express written consent. You may not use any Meta 
    tags or any other "hidden text" utilizing DecisionDatabases.com’ name or trademarks without the 
    express written consent of DecisionDatabases.com. Any unauthorized use terminates the permission or 
    license granted by DecisionDatabases.com.
</p>
<strong>Product Descriptions</strong>
<p>
    DecisionDatabases.com, and its affiliates attempt to be as accurate as possible. However, 
    DecisionDatabases.com does not warrant that product descriptions or other content of this site 
    is accurate, complete, reliable, current, or error-free.
</p>

<strong>Product Pricing</strong>
<p>
    We offer you consistently low prices on all market research, newsletters, directories, training 
    materials, CD-ROMs, and related products. All prices are listed in DOLLARS. Despite our best 
    efforts, a small number of the items in our catalogue may be mispriced. Rest assured, however, 
    that we verify prices of products sold and shipped by DecisionDatabases.com as part of our shipping 
    procedures.
    <br/>
    If an item's correct price is lower than our stated price, we charge the lower amount and ship 
    you the item. If an item's correct price is higher than our stated price, we will, at our discretion, 
    either contact you for instructions before shipping or cancel your order and notify you of such 
    cancellation.
</p>

<strong>Shipping</strong>
<p>
    DecisionDatabases.com can ship to virtually any address in the world. The majority of our products 
    are dispatched electronically. Hardcopy orders, such as books, binders, newsletters, software or 
    CD-ROMS may be dispatched by mail or courier. Hardcopy orders incur an additional shipping charge.
</p>

<strong>Delivery Time</strong>
<p>
    When you place an order with DecisionDatabases.com, we endeavor to deliver the product as soon as 
    possible. Electronic delivery is always the fastest option; the majority of electronic information 
    products are dispatched within 24 hours via Email or Download. However, the exact delivery schedule 
    may vary for each product and is based on whether the product is under study, published or needing 
    revamp. Exact delivery schedule will be communicated to you during the purchase process or completion 
    of the same. Hardcopy products are generally dispatched from the EU or the US, and delivery to some 
    markets may be delayed through local carriers, and local customs. Wherever possible we encourage 
    clients to order products electronically.
</p>

<strong>Bulk Discounts</strong>
<p>
    We may be able to offer additional discounts on large orders of a single title or on large orders 
    of many individual titles.
</p>

<strong>Orders/Return Policy</strong>
<p>
    <!-- The nature of the information being sold means that DecisionDatabases.com cannot accept returns 
    of products once an order has been placed. -->

            As per industry standards and the nature of our product, we do not reimburse/ refund or cancel orders post payment receipt. 
Buyers are instructed to read and validate the product details and clarify all their queries over mail through sales@decisiondatabases.com before proceeding for the payment. The company therefore does not offer a refund on the sold products but hereby agrees to resolve actively all the post purchase concerns and queries at the earliest.</p>
<p> 
Decision Databases solely keeps the right to determine the validity of a refund request and is not legally liable both civilly and criminally for any chargebacks and fees involved in the transaction. 
Under no circumstances we reimburse the payment, instead provide the customer with an equivalent credit amount which can be used within a year.
 
</p>

<strong>Links</strong>
<p>
    This site may contain links to third party Web sites and material from third-party information and 
    content providers. As DecisionDatabases.com has no control over such sites and resources, you 
    acknowledge and agree that DecisionDatabases.com is not responsible for the availability or 
    otherwise of such external sites or resources, and does not endorse and is not responsible or 
    liable for any content, advertising, products, or other materials on or available from such sites 
    or resources. You further acknowledge and agree that DecisionDatabases.com shall not be responsible 
    or liable, directly or indirectly, for any damage or loss caused or alleged to be caused by or in 
    connection with use of or reliance on any such content, goods, or services available on or through 
    any such site or resource.
</p>

<strong>Third Party Content</strong>
<p>
    As a distributor of content published and supplied by third parties, DecisionDatabases.com has no 
    editorial control over said content. Such content expresses the opinions, the advice, the services, 
    the offers, the statements, or other information of the authors, and not of DecisionDatabases.com
</p>

<strong>Indemnity</strong>
<p>
    You agree to hold DecisionDatabases.com, and its subsidiaries, affiliates, officers, agents, co 
    branders or other partners, and employees, harmless from any claim or demand, including reasonable 
    legal fees, made by any third party due to or arising out of your use of the Site, your connection 
    to the Site, your violation of the TOS, or your violation of any rights of another.
</p>

<strong>Applicable Law</strong>
<p>
    By visiting DecisionDatabases.com, you agree that the laws of the India, without regard to 
    principles of conflict of laws, will govern these Conditions of Use and any dispute of any sort that 
    might arise between you and DecisionDatabases.com or its affiliates.
</p>

<strong>Disclaimer of Warranties and Limitation of Liability</strong>
<p>
    THIS SITE IS PROVIDED BY DECISIONDATABASES.COM ON AN "AS IS" AND "AS AVAILABLE" BASIS. 
    DECISIONDATABASES.COM MAKES NO REPRESENTATIONS OR WARRANTIES OF ANY KIND, EXPRESS OR IMPLIED, AS TO 
    THE OPERATION OF THIS SITE OR THE INFORMATION, CONTENT, MATERIALS, OR PRODUCTS INCLUDED ON THIS SITE. 
    YOU EXPRESSLY AGREE THAT YOUR USE OF THIS SITE IS AT YOUR SOLE RISK.
    TO THE FULL EXTENT PERMISSIBLE BY APPLICABLE LAW, DECISIONDATABASES.COM DISCLAIMS ALL WARRANTIES, 
    EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS 
    FOR A PARTICULAR PURPOSE. DECISIONDATABASES.COM DOES NOT WARRANT THAT THIS SITE, ITS SERVERS, OR E-MAIL 
    SENT FROM DECISIONDATABASES.COM ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. DECISIONDATABASES.COM 
    WILL NOT BE LIABLE FOR ANY DAMAGES OF ANY KIND ARISING FROM THE USE OF THIS SITE, INCLUDING, 
    BUT NOT LIMITED TO DIRECT, INDIRECT, INCIDENTAL, PUNITIVE, AND CONSEQUENTIAL DAMAGES.
    DecisionDatabases.com knows that you care how information about you is used and shared, and we 
    appreciate your trust that we will do so carefully and sensibly. This notice describes our privacy policy. 
    By visiting DecisionDatabases.com, you are accepting the practices described in this Privacy Notice.
</p>
<strong>What personal information about customers does DecisionDatabases.com gather?</strong>
<p>
    The information we learn from customers helps us personalize and continually improve your 
    delivery of business information.
</p>

<strong>Here are the types of information we gather:</strong>
<p>
    <em>Information You Give Us:</em> We receive and store any information you enter on our Web site or give us in 
    any other way. You can choose not to provide certain information, but then you might not be able to 
    take advantage of many of our features. We use the information that you provide for such purposes as 
    responding to your requests, customizing future shopping for you, improving our products, and 
    communicating with you.
    <br/>
    <br/>
    <em>Automatic Information:</em> We receive and store certain types of information whenever you interact 
    with us. For example, like many Web sites, we use "cookies", and we obtain certain types of information 
    when your Web browser accesses DecisionDatabases.com.
    <br/>
    <strong><em>Note:</em></strong> Your browsing and purchasing patterns for market research information 
    may be commercially sensitive. We recognize this. A number of companies offer utilities designed to 
    help you visit Web sites anonymously. Although we will not be able to provide you with a personalized 
    experience at DecisionDatabases.com if we cannot recognize you, we want you to be aware that these 
    tools exist.
    <br/>
    <em>E-mail Communications:</em> To help us make e-mails more useful and interesting, we often receive 
    a confirmation when you open e-mail from DecisionDatabases.com if your computer supports such 
    capabilities. We also compare our customer list to lists received from other companies, in an effort 
    to avoid sending unnecessary messages to our customers. If you do not want to receive e-mail or other 
    mail from us, please adjust your Customer Communication Preferences.
    <br/>
    <em>Information from Other Sources:</em>
    We might receive information about you or your organization from other sources and add it to our 
    account information.
</p>

<strong>What about Cookies?</strong>
<p>
    Cookies are alphanumeric identifiers that we transfer to your computer's hard drive through your 
    Web browser to enable our systems to recognize your browser and to provide additional features 
    such personalized information, and storage of items in your Shopping Cart between visits.
    The Help portion of the toolbar on most browsers will tell you how to prevent your browser 
    from accepting new cookies, how to have the browser notify you when you receive a new cookie, 
    or how to disable cookies altogether. However, cookies allow you to take full advantage of some 
    of DecisionDatabases.com’ coolest features, and we recommend that you leave them turned on.
</p>

<strong>How secure is information about me?</strong>
<p>
    We protect the security of your information during transmission by using Secure Sockets Layer 
    (SSL) software, which encrypts information you input. It is important for you to protect against 
    unauthorized access to your password and to your computer. Be sure to sign off when finished using 
    a shared computer.
</p>

<strong>Opt-in/Opt-out</strong>
<p> 
    If you do not want to receive e-mail or other mail from us, please visit: <br/>
    <strong><a href="#">http://www.DecisionDatabases.com/unsubscribe</a></strong><br/>
    If you wish to receive e-mail or other mail from us, please visit:<br/> 
    <strong><a href="<?php echo Router::url('/', true) . 'users' . '/registration'; ?>">http://www.DecisionDatabases.com/register</a></strong>
</p>

<div class="row">
    <div class="col-md-12 text-right">
        <button type="button" class="btn btn-primary" onclick="notAcceptCondition()">Not Accept</button>
        <button type="button" class="btn btn-primary" onclick="acceptCondition()">Accept</button>
    </div>
</div>