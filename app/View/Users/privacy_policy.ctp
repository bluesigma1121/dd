<div class="container content">		
    <div class="row margin-bottom-40">
        <div class="col-md-12 md-margin-bottom-40">
            <div class="headline"><h2>Privacy Policy</h2></div>
            <p>
                DecisionDatabases.com knows that you care how information about you is used and shared, 
                and we appreciate your trust that we will do so carefully and sensibly. This notice describes our 
                privacy policy. By visiting DecisionDatabases.com, you are accepting the practices described in 
                this Privacy Notice.
            </p>
            <strong>What personal information about customers does DecisionDatabases.com gather?</strong>
            <p>The information we learn from customers helps us personalize and continually improve your delivery of business information.</p>

            <strong>Here are the types of information we gather:</strong>
            <p>
                <em>Information You Give Us:</em> We receive and store any information you enter on our Web site or 
                give us in any other way. You can choose not to provide certain information, but then you might not be able 
                to take advantage of many of our features. We use the information that you provide for such purposes 
                as responding to your requests, customizing future shopping for you, improving our products, and 
                communicating with you.
            </p>
            <p>
                <em>Automatic Information:</em> We receive and store certain types of information whenever you interact 
                with us. For example, like many Web sites, we use "cookies", and we obtain certain types of information 
                when your Web browser accesses DecisionDatabases.com.
                <strong>Note:</strong> Your browsing and purchasing patterns for market research information may be 
                commercially sensitive. We recognize this. A number of companies offer utilities designed to help you 
                visit Web sites anonymously. Although we will not be able to provide you with a personalized experience at 
                DecisionDatabases.com if we cannot recognize you, we want you to be aware that these tools exist.
            </p>
            <p>
                <em>E-mail Communications:</em> To help us make e-mails more useful and interesting, we often receive 
                a confirmation when you open e-mail from DecisionDatabases.com if your computer supports such 
                capabilities. We also compare our customer list to lists received from other companies, in an effort 
                to avoid sending unnecessary messages to our customers. If you do not want to receive e-mail or other 
                mail from us, please adjust your Customer Communication Preferences.
            </p>

            <p>
                <em>Information from Other Sources:</em> We might receive information about you or your organization from 
                other sources and add it to our account information.
            </p>

            <strong>What about Cookies?</strong>
            <p>
                Cookies are alphanumeric identifiers that we transfer to your computer's hard drive through your 
                Web browser to enable our systems to recognize your browser and to provide additional features such 
                personalized information, and storage of items in your Shopping Cart between visits.
                The Help portion of the toolbar on most browsers will tell you how to prevent your browser from 
                accepting new cookies, how to have the browser notify you when you receive a new cookie, or how 
                to disable cookies altogether. However, cookies allow you to take full advantage of some of 
                DecisionDatabases.com’ coolest features, and we recommend that you leave them turned on.
            </p>
            <strong>How secure is information about me?</strong>
            <p>
                We protect the security of your information during transmission by using Secure Sockets Layer (SSL) 
                software, which encrypts information you input. It is important for you to protect against unauthorized 
                access to your password and to your computer. Be sure to sign off when finished using a shared computer.
                <br>
                <br></p>
            <strong>Opt-in/Opt-out</strong><br/>
            <p>If you do not want to receive e-mail or other mail from us, please visit: <br/>
                <strong><a href="#">http://www.DecisionDatabases.com/unsubscribe</a></strong><br/>
                <br>
                If you wish to receive e-mail or other mail from us, please visit: <br/>
                <strong><a href="<?php echo Router::url('/', true) . 'users' . '/registration'; ?>">http://www.DecisionDatabases.com/register</a></strong>
            </p>
        </div>
    </div>
</div>