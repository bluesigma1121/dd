<div class="container">

    <!--Reg Block-->
    <div class="reg-block">
        <?php echo $this->Form->create('User', array('id' => 'login', 'type' => 'file')); ?>
        <div class="reg-block-header">
            <?php echo $this->Session->flash(); ?>
            <h2>Sign In</h2>
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
            <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'type' => 'email', 'placeholder' => 'Email')); ?>
        </div>
        <div class="input-group margin-bottom-20">
            <span class="input-group-addon"><i class="fa fa-lock"></i></span>
            <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'type' => 'password', 'placeholder' => 'Password')) ?>
        </div>
        <div class="margin-bottom-20">
            <div class="text-center">
                <a href="<?php echo Router::url(array('action' => 'forgot_password')) ?>">Forgot password?
                </a>
            </div>         
        </div>
        <hr>
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <?php echo $this->Form->submit('Login', array('class' => 'btn-u btn-block')) ?>
            </div>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    <!--End Reg Block-->
</div><!--/container-->
