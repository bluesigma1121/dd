<style>
    /*404 Error Page v1 
    ------------------------------------*/
    .error-v1 {
        padding-bottom: 30px;
        text-align: center;	
    }

    .error-v1 p {
        color: #555;
        font-size: 16px;
    }

    .error-v1 span {
        color: #555;
        display: block;
        font-size: 35px;
        font-weight: 200;
    }

    .error-v1 span.error-v1-title {
        color: #777;	
        font-size: 180px;
        line-height: 200px;
        padding-bottom: 20px;
    }

    /*For Mobile Devices*/
    @media (max-width: 500px) { 
        .error-v1 p {
            font-size: 12px;
        }	

        .error-v1 span {
            font-size: 25px;
        }

        .error-v1 span.error-v1-title {
            font-size: 140px;
        }
    }
</style>

<div class="container content">		
    <!--Error Block-->
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="error-v1">
                <span>Congratulations ! Your payment is successful.</span>
                <p class="text-center">Thanks for placing an order with DecisionDatabases.com .Our team will get 
                    in touch with you shortly and take this forward. Alternatively, you could mail us at 
                    sales@decisiondatabases.com 
                </p>
                <?php /*
                  <a class="btn-u btn-bordered" href="<?php echo Router::url(array('controller' => 'orders', 'action' => 'user_order_index')) ?>">My Orders</a>
                 */ ?>

            </div>
        </div>
    </div>
    <!--End Error Block-->
</div>