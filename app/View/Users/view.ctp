<script type="text/javascript">
    $(document).ready(function() {
        $('.add_offer').fancybox({
            maxWidth: 1200,
            maxHeight: 1200,
            fitToView: true,
            width: '100%',
            height: '100%',
            autoSize: true,
            closeClick: false,
            openEffect: 'none',
            closeEffect: 'none',
            type: 'ajax',
        });
    });

</script>
<div class="row">
    <div class="wrapper wrapper-content animated fadeInUp">
        <div class="ibox">
            <div class="ibox-content">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <div class="m-b-md text-center">
                                <h2 class="btn btn-primary btn-rounded btn-block">User Details</h2>
                            </div>
                        </div>
                        <div class="col-lg-4"> </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4">
                        <dl class="dl-horizontal">
                            <dt>Name:</dt> <dd> <?php echo h(ucfirst($user['User']['name'])); ?></dd>
                            <dt>Organisation:</dt> <dd> <?php echo h($user['User']['organisation']); ?></dd>
                            <dt>City:</dt> <dd> <?php echo h($user['User']['city']); ?></dd>
                            <dt>Country:</dt> <dd> <?php echo h($user['User']['country']); ?></dd>
                        </dl>
                    </div>
                    <div class="col-lg-4">
                        <dl class="dl-horizontal">
                            <dt>Email:</dt> <dd> <?php echo h($user['User']['email']); ?></dd>
                            <dt>Job Title:</dt> <dd> <?php echo h($user['User']['job_title']); ?></dd>
                            <dt>Pin code:</dt> <dd> <?php echo h($user['User']['pin_code']); ?></dd>
                            <?php if (!empty($user['User']['last_login'])) { ?>
                                <dt>Last Login:</dt> <dd> <?php echo date('d-M-y h:i a', strtotime($user['User']['last_login'])); ?></dd>
                            <?php } ?>
                        </dl>
                    </div>
                    <div class="col-lg-4">
                        <dl class="dl-horizontal">
                            <dt>Role:</dt> <dd> <?php echo h($roles[$user['User']['role']]); ?></dd>
                            <dt>Address:</dt> <dd> <?php echo h($user['User']['address']); ?></dd>
                            <dt>State:</dt> <dd> <?php echo h($user['User']['state']); ?></dd>
                            <?php if (!empty($user['User']['visited_ip'])) { ?>
                                <dt>Registration IP:</dt> <dd> <?php echo $user['User']['visited_ip']; ?></dd>
                            <?php } ?>
                        </dl>
                    </div>
                </div>
                <?php /*  <div class="row">
                  <table class="table">
                  <tr>
                  <td>Name  : </td>
                  <td><?php echo h($user['User']['name']); ?> </td>
                  <td>Email  : </td>
                  <td><?php echo h($user['User']['email']); ?> </td>
                  <td>Role  : </td>
                  <td><?php echo h($roles[$user['User']['role']]); ?></td>
                  <td>Organisation  : </td>
                  <td><?php echo h($user['User']['organisation']); ?> </td>
                  </tr>
                  </table>
                  </div>
                 */ ?> 

                <div class="row"> 
                    <div class="text-center">
                        <Strong class="text-center"><h2>Inquiries</h2></strong>
                    </div>
                    <table class="table table-bordered  col-lg-8">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Date </th>
                                <th>Subject</th>
                                <th>Category Name</th>
                                <th>Product Name</th>
                                <th>Ref Page</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php
                            $i = 0;
                            foreach ($enqueries as $ekey => $enquiry) {
                                ?>
                                <tr>
                                    <td><?php echo++$i ?></td>
                                    <td><?php echo $enquiry['Enquiry']['created']; ?></td>
                                    <td><?php echo $enquiry['Enquiry']['subject']; ?></td>
                                    <td><?php echo $enquiry['Category']['category_name']; ?></td>
                                    <td><?php echo $enquiry['Product']['product_name']; ?></td>
                                    <td><?php echo $enquiry['Enquiry']['ref_page']; ?></td>
                                </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>


                <div class="row"> 
                    <div class="text-center">
                        <Strong class="text-center"><h2>Cart Items</h2></strong>
                    </div>
                    <table class="table table-bordered  col-lg-8">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name </th>
                                <th>Product Name</th>
                                <th>Price</th>
                                <th>Offer Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody> 
                            <?php
                            $i = 0;

                            foreach ($cartItems as $cartItem) {
                                ?>

                                <tr>
                                    <td><?php echo++$i ?></td>
                                    <td><?php echo h($user['User']['name']); ?>&nbsp;</td>
                                    <td><?php echo h($cartItem['Product']['product_name']); ?>&nbsp;</td>
                                    <td><?php echo h($cartItem['CartItem']['current_price']); ?>&nbsp;</td>
                                    <td><?php echo h($cartItem['CartItem']['offer_price']); ?>&nbsp;</td>


                                    <td class="actions" style="min-width: 250px; max-width: 250px;">
                                        <?php // echo $this->Html->link(__('view'), array('action' => 'view', $cartItem['CartItem']['id']), array('class' => 'btn   btn-outline btn-sm btn-warning ', 'title' => 'View')); ?>
                                        <?php echo $this->Html->link(__(''), array('action' => 'offer_price', $cartItem['CartItem']['id'], $user['User']['id']), array('class' => 'btn btn-outline btn-sm btn-warning fa fa-plus add_offer ', 'title' => 'Add offer Price')); ?>
                                    </td>
                                </tr>

                            <?php } ?>

                        </tbody>
                    </table>
                </div>

                <div class="row">
                    <div class="text-center">
                        <button onclick="goBack();" class="btn btn-info btn-rounded btn-outline">Back</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>