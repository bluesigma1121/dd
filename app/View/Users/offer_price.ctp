<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>decisiondatabases.com</title>
        <!-- Bootstrap core CSS -->
        <?php echo $this->Html->css(array('back/bootstrap.min.css', 'back/bvalidator.css')); ?>
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="js/html5shiv.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
        <style type="text/css">
            .mar_top_hed{
                margin-top: -40px;
                color: #555;
                text-align: center;
                margin-bottom: 5px;
                border-bottom: solid 1px #eee;

            } 
            .inver{
                color: red !important
            }
            .ver{
                color: green !important
            }
        </style> 


        <?= $this->Html->script(array('back/plugins/jquery-ui/jquery-ui.min.js', 'back/bootstrap.min.js', 'back/jquery.bvalidator-yc.js')); ?>
    </head>
    <body> 

        <div class="ibox-content">
            <?php echo $this->Form->create('CartItem', array('id' => 'Cart_Item', 'class' => 'form-horizontal', 'type' => 'file')); ?>
            <div class="row col-lg-offset-2">
                <div class="form-group">
                    <label class="col-md-3 control-label">Current Price:</label>
                    <div class="col-md-4">
                        <?php echo $curr_price['CartItem']['current_price'] ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Offer Price:</label>
                    <div class="col-md-4">
                        <?php echo $this->Form->input('offer_price', array('type' => 'text', 'data-bvalidator' => '', 'class' => 'form-control', 'label' => false)); ?>
                    </div>
                </div>

            </div>
            <div class="text-center">
                <?php echo $this->Form->submit(__('Save'), array('class' => 'btn btn-primary btn-sm', 'div' => false)); ?>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>



    </body>
</html>