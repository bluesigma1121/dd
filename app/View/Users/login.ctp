<div class="row margin-bottom-40">
    <div class="container content">
        <div class="col-md-4 md-margin-bottom-40 div_login"> 
            <?php echo $this->Form->create('User', array('id' => 'registration', 'class' => 'reg-page', 'type' => 'file')); ?>
            <div class="reg-header">
                <h2>Login</h2>
            </div>
            <div class="input-group margin-bottom-20">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'type' => 'email', 'placeholder' => 'Email')); ?>
            </div>
            <div class="input-group margin-bottom-20">
                <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                <?php echo $this->Form->input('password', array('class' => 'form-control', 'label' => false, 'type' => 'password', 'placeholder' => 'Password')) ?>
            </div>
            <div class="margin-bottom-20">
                <div class="text-center">
                    <a href="<?php echo Router::url(array('action' => 'forgot_password')) ?>">Forgot password?
                    </a>
                </div>         
            </div>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <?php echo $this->Form->submit('Login', array('class' => 'btn-u btn-block')) ?>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>

        <div class="col-md-6 md-margin-bottom-40 div_login-2"> 
            <h3>Don't have account? Get registered in simple one step.</h3>
            <a class="btn btn-block btn-windows-inversed rounded" href="<?php echo Router::url(array('action' => 'registration')) ?>"> <i class="fa fa-chevron-right"></i> Register Here
            </a>
        </div>
    </div> 
</div><!--/row-->