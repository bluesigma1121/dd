<div class="row margin-bottom-40">
    <div class="container content">
        <div class="col-md-4 md-margin-bottom-40"> 
            <?php echo $this->Form->create('User', array('id' => 'registration', 'class' => 'reg-page', 'type' => 'file')); ?>
            <div class="reg-header">
                <h1>Reset your Password</h1>
                <h5 class="tcg">Changing your password is simple.</h5>
            </div>
            <div class="input-group margin-bottom-20">
                <span class="input-group-addon"><i class="fa fa-envelope"></i></span>
                <?php echo $this->Form->input('email', array('class' => 'form-control', 'label' => false, 'type' => 'email', 'data-bvalidator' => 'email,required', 'placeholder' => 'Enter Registered Email.')); ?>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <?php echo $this->Form->submit('Reset', array('class' => 'btn-u btn-block')) ?>
                </div>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
        <div class="col-md-6 md-margin-bottom-40 div_login-2"> 
            <h3>Don't have account? Get registered in simple one step.</h3>
            <a class="btn btn-block btn-windows-inversed rounded" href="<?php echo Router::url(array('action' => 'registration')) ?>"> <i class="fa fa-chevron-right"></i> Register Here
            </a>
        </div>
        <div class="col-md-6 md-margin-bottom-40 div_login-2"> 
            <h3>Already have an account?</h3>
            <a class="btn btn-block btn-bitbucket-inversed rounded" href="<?php echo Router::url(array('action' => 'login')) ?>">  <i class="fa fa-lock"></i> Get Back To Login
            </a>
        </div>
    </div>
</div>
