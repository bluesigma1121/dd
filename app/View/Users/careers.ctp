<div class="container content">		
    <div class="row margin-bottom-30">
        <div class="col-md-12 mb-margin-bottom-30">
            <div class="headline"><h2>We are Hiring !</h2></div>
        </div>

        <div class="col-md-6">
            <div class="headline"><h3>Market Research Analysts</h3></div>
            <ul>
                <li> 2+yrs of Market Research Experience </li>
                <li> Should be an excellent performer and go-getter</li>
                <li> Should be result oriented, self motivated person </li>
                <li> Excellent Analytical and Communication Skills</li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="headline"><h3> Content Writers</h3></div>
            <ul>
                <li> 1+yrs of Content Writing</li>
                <li> Should be an excellent performer and go-getter</li>
                <li> Should be result oriented, self motivated person </li>
                <li> Excellent Analytical and Communication Skills</li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="headline"><h3> PHP Developers</h3></div>
            <ul>
                <li> 2+yrs of Coding experience in a web based project</li>
                <li> Should be an excellent performer and go-getter</li>
                <li> Should be result oriented, self motivated person </li>
                <li> Excellent Analytical and Communication Skills</li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="headline"><h3> Internation Sales Managers</h3></div>
            <ul>
                <li> 1+yrs of International Sales - Outbound</li>
                <li> Should be an excellent performer and go-getter</li>
                <li> Should be result oriented, self motivated person </li>
                <li> Excellent Analytical and Communication Skills</li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="headline"><h3> Data Management Executive</h3></div>
            <ul>
                <li> Freshers - Graduate ( B.Sc/B.Com)</li>
                <li> Excellent communication and logical skills</li>
                <li> Data entry on the website on a daily basis</li>
                <li> Consolidate, streamline and organize data </li>
                <li> Make presentations and document creation</li>
            </ul>
        </div>
        <div class="col-md-6">
            <div class="headline"><h3> Market Research Executive</h3></div>
            <ul>
                <li> Freshers - Graduate, B.E, B.Tech + MBA preferred</li>
                <li> Excellent communication and logical skills</li>
                <li> Carrying out primary and secondary research</li>
                <li> Data compiling, data creation</li>
                <li> Report drafting, editing, formatting</li>
                <li> Content Writing for various sectors</li>
            </ul>
        </div>
        
        
        














        
        
        
        
    </div>
    <div class="col-md-12">
        <p><strong>Send your updated CV to <a href="mailto:hr@decisiondatabases.com">hr@decisiondatabases.com</a></strong></p>
    </div>
</div>