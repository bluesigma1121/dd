<style>
.flash h2 {
    color: #fff;
    font-weight: 500;
    display: inline-block;
    margin: 15px;
}

.headline h1 {
    margin: 0 0 -2px;
    padding-bottom: 5px;
    display: inline-block;
    border-bottom: 2px solid #3498db;
    font-size: 22px;
}

</style>
<div class="container content">		
    <div class="row margin-bottom-40">
        <div class="col-md-12 md-margin-bottom-40">
            <div class="headline"><h1>About Us</h1></div>
            <p>
                DecisionDatabases.com is a global business research reports provider, enriching decision makers and strategists with qualitative statistics. 
		DecisionDatabases.com is proficient in providing syndicated research report, customized research reports, company profiles and industry databases 
		across multiple domains. Our expert research analysts have been trained to map client’s research requirements to the correct research resource 
		leading to a distinctive edge over its competitors. We provide intellectual, precise and meaningful data at a lightning speed. 
            </p>

            <p>Why buy research through us : </p>

            <ul class="list-unstyled">
                <li><i class="fa fa-check color-green"></i>&nbsp;We deliver research from expert analysts spread across the globe</li>
                <li><i class="fa fa-check color-green"></i>&nbsp;We offer flexibility to buy only certain sections and tables without buying the entire report</li>
                <li><i class="fa fa-check color-green"></i>&nbsp;Flexibility to increase or decrease the scope of report </li>
                <li><i class="fa fa-check color-green"></i>&nbsp;We offer custom research if the proposed TOC does not meet your business requirement </li>
                <li><i class="fa fa-check color-green"></i>&nbsp;24x7 email support </li>
                <li><i class="fa fa-check color-green"></i>&nbsp;We offer broad as well as niche industry reports</li>
            </ul>
            <br/>
            <br/>
            <p>
                For more information, write to us at <a href="mailto:sales@decisiondatabases.com" target="_blank">sales@decisiondatabases.com</a>
            </p>
        </div>
    </div><!--/row-->
</div><!--/container-->

<div class="col-md-12 flash text-center">
   <h2 class="text-center"><i class="fa fa-users"></i><br>300+ Clients&nbsp;&nbsp;&nbsp;&nbsp;|</h2>
    <h2 class="text-center"><i class="fa fa-file-text"></i><br>20,000+ Reports&nbsp;&nbsp;&nbsp;&nbsp;|</h2>
    <h2 class="text-center"><i class="fa fa-certificate"></i><br>10+ Industry Verticals</h2>
</div>

<!-- New Section For  Our Clients Start -->
<div class="container content-md">
        <?php if (!empty($ourclients)) { ?>
              <div class="row margin-bottom-10">
                    <div class="headline"><h2>Our Top Clients</h2></div>

                        <?php foreach ($ourclients as $key => $clients) { ?>
                         <div class="col-md-2 col-sm-6 pad-05">
                          <div class="thumbnail_home">
                            <div class="service-block">
                              <div class="item">
                                  <?php if (!empty($clients['OurClient']['logo'])) { ?>
                                      <!-- <a href="http://<?php echo $clients['OurClient']['link']; ?>" target="blank">  -->
                                      <?php
                                          echo $this->Html->image('client_images/' . $clients['OurClient']['logo'], array('class' => 'img-responsive'));
                                      } else {
                                          echo $this->Html->image('noimage.png', array('class' => 'img-responsive'));
                                      }
                                      ?>
                                  <!-- </a> -->
                              </div>
                            </div>

                          </div>

                        </div> <!-- .col-md-2 -->
                        <?php } ?>
                </div>

        <?php } ?>
    </div> <!-- .container -->
<!-- New Section For  Our Clients End -->

<style>
    p, li, li a {
        color: #888;
        font-size: 13px;
    }

    .flash
    {
      background-color: #145DC9;
      margin-bottom: 50px;
      height: 120px;
      padding: 3px 0px;
      margin-top: -25px;
    }
    .flash h1{
      color: #fff;
      font-weight: 500;
      display: inline-block;
      margin: 15px;
    }
    .flash i{
      font-size: 48px;
    }
</style>
