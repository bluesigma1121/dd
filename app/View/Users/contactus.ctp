<script type="text/javascript">
    $(document).ready(function () {
        $('#contact_us').bValidator();
    });
</script>
<div class="container content">
    <div class="row margin-bottom-30">
        <div class="col-md-9 mb-margin-bottom-30">
            <div class="headline"><h2>Contact Us</h2></div>
            <?php echo $this->Form->create('User', array('class' => '', 'id' => 'contact_us')); ?>
            <label>Name <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('name', array('class' => 'form-control', 'label' => false, 'data-bvalidator' => 'required,alpha')); ?>
                </div>
            </div>

            <label>Email <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('email', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required,email')); ?>
                </div>
            </div>

            <label>Organization <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('organization', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>

            <label>Designation <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('job_title', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <?php /* <div class="row">
              <div class="col-sm-5">
              <label>Contact Number <span class="color-red">*</span></label>
              <?php echo $this->Form->input('mobile', array('class' => 'form-control margin-bottom-20', 'label' => false, 'type' => 'text', 'placeholder' => '+_(___)___-__-__', 'id' => 'customer_phone', 'size' => '20', 'maxlength' => '25', 'data-bvalidator' => 'required')); ?>
              </div>
              <div class="col-sm-4">
              <div class="mar_phone">
              <input type="checkbox" id="phone_mask" checked=""> <label id="descr" for="phone_mask"></label>
              </div>
              </div>
              </div>
             */ ?>

            <label>Mobile <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('mobile', array('class' => 'form-control', 'type' => 'text', 'label' => false, 'data-bvalidator' => 'required')); ?>
                </div>
            </div>
            <label>Message <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Form->input('message', array('class' => 'form-control', 'type' => 'textarea', 'label' => false, 'data-bvalidator' => 'required,alphanum')); ?>
                    <label class="text-danger">URL not allowed in requirement.</label>
                </div>
            </div>
            <!--<label>Captcha  <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php echo $this->Html->image($this->Html->url(array('controller' => 'users', 'action' => 'captcha'), true), array('label' => false, 'width' => '150', 'height' => '40', 'alt' => 'captcha')); ?>
                </div>
            </div>
            <label>Enter Above Code <span class="color-red">*</span></label>
            <div class="row margin-bottom-20">
                <div class="col-md-7 col-md-offset-0">
                    <?php
                    echo $this->Form->input('captcha', array('data-bvalidator' => 'required', 'class' => 'input-large form-control hastip', 'data-placement' => 'right', 'type' => 'text', 'placeholder' => 'Enter Captcha Code',
                        'title' => 'Enter the characters you see in the image.', 'label' => false));
                    ?>
                </div>
            </div> -->

            <!--Change Captcha VG-15/2/2018  -->
            <div class="row margin-bottom-20">
              <div class="col-md-7 col-md-offset-0">
                <div class="g-recaptcha" data-sitekey="6LdfhkYUAAAAANU1u6w1Xd3r_0x6yz4h9ZmOjXlA"></div>
              </div>
            </div>
<!--Change Captcha VG-15/2/2018  -->

            <p>
                <?php echo $this->Form->button('Submit', array('class' => 'btn-u btn-u-blue')); ?>
            </p>
            <?php $this->Form->end(); ?>
        </div>
        <div class="col-md-3">
            <!-- Contacts -->
            <div class="headline"><h2>Contacts</h2></div>
            <ul class="list-unstyled who margin-bottom-30">
                <li><a href="#"><i class="fa fa-home"></i>3rd Floor,Fountain chambers,<br/>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nanabhai Lane,Fort, Mumbai - 1</a></li>
                <li><a href="#"><i class="fa fa-envelope"></i>sales@decisiondatabases.com</a></li>
                <li><a href="#"><i class="fa fa-phone"></i>+91-93077-28237</a></li>
                <!--<li><a href="#"><i class="fa fa-phone"></i> US Toll free : +1-646-5869239</a></li>-->

            </ul>
        </div>
    </div><!--/row-->
</div><!--/container-->
