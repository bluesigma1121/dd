<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class MisController extends AppController {

    public $uses = array();

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'Captcha');

    /**
     * index method
     *
     * @return void
     */
    public function mis_index() {
        $this->uses = array('Category', 'Product');
        $this->layout = 'admin_layout';
        // $this->loadModel('Category');
        $categories = $this->Category->find('list', array(
            'fields' => array('id', 'category_name'),
            'conditions' => array('Category.parent_category_id' => 0))
        );
        $data['hello'] = 'Gracias';
        $data['categories'] = $categories;
        $this->set($data);
        $this-> render();
    }


    public function reports_by_publisher() {
        $this->uses = array('Product');
        $this->layout = 'admin_layout';
        // $this->loadModel('Category');
        $fields = array('Product.publisher_name','COUNT(`Product`.`publisher_name`) AS publisher_count');
        $reports_by_publisher = $this->Product->find('all', array(
            'fields' => $fields,
            'recursive' => -1,
            'group'=> 'Product.publisher_name',
            'order' => 'publisher_count DESC'
        ));
        // debug($reports_by_publisher);
        $data['reports_by_publisher'] = $reports_by_publisher;
        $this->set($data);
        $this-> render();
    }

    public function vmr_reports_by_category() {
        $this->uses = array('Product');
        $this->layout = 'admin_layout';
        // $this->loadModel('Category');
        $fields = array('Product.category_id','COUNT(`Product`.`category_id`) AS category_count',
            'Category.category_name');
        $vmr_reports_by_category = $this->Product->find('all', array(
            'fields' => $fields,
            'conditions'=>array('Product.publisher_name' => 'Value Market Research'),
            'recursive' => 0,
            'group'=> 'Product.category_id',
            'order' => 'category_count DESC'
        ));        
        $data['vmr_reports_by_category'] = $vmr_reports_by_category;
        $this->set($data);
        $this-> render();
    }

    public function leads_by_report() {
        
        $this->uses = array('Enquiry');
        $this->layout = 'admin_layout';
        
        $until = new DateTime();
        $interval = new DateInterval('P3M');
        
        if(!empty($_GET['from'])) {
            $from = $_GET['from'];
        } else {
            $from = $until->sub($interval);
            $from = $from->format('Y-m-d');
        }
        if(!empty($_GET['to'])) {
            $to = $_GET['to'];
        } else {
            $to = new DateTime();
            $to = $to->format('Y-m-d');
        }
        
        $fields = array('Enquiry.id','Product.alias','COUNT(`Enquiry`.`id`) AS lead_count','Product.publisher_name');
        $leads_by_report =  $this->Enquiry->find('all', array(
            'fields'    =>  $fields,
            'recursive' =>  0,
            'conditions' => array('date(Enquiry.created) >=' =>$from, 'date(Enquiry.created) <=' => $to),
            'group'     =>  'Product.id',
            'order'     =>  'publisher_name ASC, lead_count DESC'
        ));        
        
        $data['leads_by_report'] = $leads_by_report;

        $data['from'] = $from;
        $data['to'] = $to;

        $this->set($data);
        $this-> render();
    }

    public function products_crud() {
        $this->uses = array('Product');
        $this->layout = 'admin_layout';
        // $this->loadModel('Category');
        $products = $this->Product->find('all', array(
            'fields' => array('Product.id', 'Product.product_name', 'Product.price',
                'Product.slug', 'Product.publisher_name'),
            'conditions'=>array('Product.is_active' => 1),
            'recursive' => -1,
            'order' => 'Product.created DESC',
            'limit' => 10
        ));        
        $data['products'] = $products;
        $this->set($data);
        $this-> render();
    }

    public function product_view($id) {
        $this->uses = array('Product');
        $this->layout = 'admin_layout';
        $product = $this->Product->findById($id);
        $data['product'] = $product;
        debug($product);
        $this->set($data);
        $this-> render();
    }

    public function user_create() {
        $this->uses = array('User', 'Country');
        $this->layout = 'admin_layout';

        // $countries = $this->Country->find('all');
        $countries = $this->Country->find('list', array(
            'fields' => array('id', 'country_name')
        ));

        if ($this->request->is('get')) {
            debug('display form');
        }

        if ($this->request->is('post')) {
            $this->User->create();
            $user = $this->request->data;
            $response = $this->User->create_user($user);

            if($response['status']) {
                debug('Saved');
                return $this->redirect(array('action' => 'products_crud'));
            }
            else {
                debug('Not Saved: ' . $response['message'] );
            }
        }

        $roles = array(
            1 => 'Admin',
            2 => 'Analyst',
            3 => 'SEO',
        );

        $data['roles'] = $roles;
        $data['countries'] = $countries;
        $this->set($data);
        return $this->render();

    }

    public function user_edit($id) {
        $this->uses = array('User', 'Country');
        $this->layout = 'admin_layout';

        $user = $this->User->findById($id);

        debug($user);

        // $countries = $this->Country->find('all');
        $countries = $this->Country->find('list', array(
            'fields' => array('id', 'country_name')
        ));

        if ($this->request->is('get')) {
            debug('display form');
        }

        if ($this->request->is('post')) {
            $this->User->create();
            $user = $this->request->data;
            $user['User']['role'] = (int) $user['User']['role'] ;
            debug($user); 
            $return_code = $this->User->save($user);

            if($return_code) {
                debug('Saved');
            }
            else {
                debug('Not Saved');
                debug($this->User->validationErrors);
            }
        }

        $roles = array(
            1 => 'Admin',
            2 => 'Analyst',
            3 => 'SEO',
        );

        $data['countries'] = $countries;
        $data['user'] = $user;
        $data['roles'] = $roles;
        $this->set($data);
        return $this-> render('user_create');

    }



    // public function beforeFilter() {
    //     $this->Auth->allow(array('mis_index'));
    // }


}
    