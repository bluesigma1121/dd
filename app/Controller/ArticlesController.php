<?php

App::uses('AppController', 'Controller');

/**
 * Articles Controller
 *
 * @property Article $Article
 * @property PaginatorComponent $Paginator
 */
class ArticlesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';

        if ($this->request->is('post')) {
            $this->Session->write('article_filter', $this->request->data);
        } elseif ($this->Session->check('article_filter')) {
            $this->request->data = $this->Session->read('article_filter');
        } else {
            $this->request->data['Article']['search_text'] = '';
        }
        $conditions_name = array();
        if ($this->Session->check('article_filter')) {
            $data = $this->Session->read('article_filter');

            if (isset($data['Article']['search_text']) && !empty($data['Article']['search_text'])) {
                $conditions_name["Article.headline  LIKE"] = '%' . $data['Article']['search_text'] . '%';
            }
        }

        $this->Article->recursive = 0;
        $this->paginate = array(
            'conditions' => array($conditions_name),
            'order' => 'Article.id DESC',
            'limit' => 20
        );
        $this->set('articles', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Article->exists($id)) {
            throw new NotFoundException(__('Invalid article'));
        }
        $options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
        $article = $this->Article->find('first', $options);
        Configure::load('idata');
        $ar_type = Configure::read('idata.article_type');
        $heading = $ar_type[$article['Article']['article_type']];
        $this->set(compact('article','heading'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = 'admin_layout';
        Configure::load('idata');
        $article_type = Configure::read('idata.article_type');        
        $this->set(compact('article_type'));
        if ($this->request->is('post')) {
            $this->Article->create();
            if ($this->Article->save($this->request->data)) {
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        }
        $categories = $this->Article->Category->find('list', array(
            'fields' => array('id', 'category_name'),
            'conditions' => array('Category.parent_category_id' => 0)));
        $this->set(compact('categories'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = 'admin_layout';
        Configure::load('idata');
        $article_type = Configure::read('idata.article_type');        
        $this->set(compact('article_type'));
        if (!$this->Article->exists($id)) {
            throw new NotFoundException(__('Invalid article'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Article->id = $id;
            $this->request->data['Article']['id'] = $id;
            if ($this->Article->save($this->request->data)) {
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Article.' . $this->Article->primaryKey => $id));
            $this->request->data = $this->Article->find('first', $options);
        }
        $categories = $this->Article->Category->find('list', array(
            'fields' => array('id', 'category_name'),
            'conditions' => array('Category.parent_category_id' => 0)));
        $this->set(compact('categories'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Article->id = $id;
        if (!$this->Article->exists()) {
            throw new NotFoundException(__('Invalid article'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Article->delete()) {
            $this->Session->setFlash(__('The article has been deleted.'));
        } else {
            $this->Session->setFlash(__('The article could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function article_listing($type = "press-release") {
        $this->Article->recursive = 0;
        $this->paginate = array(
            'conditions' => array('Article.article_type' => $type),
            'order' => 'Article.id DESC',
            'limit' => 1000
        );
        Configure::load('idata');
        $ar_type = Configure::read('idata.article_type');
        $heading = $ar_type[$type];
        /* code Start VG-31/01/2017*/
         if($heading == 'Analysis')
        {
            $meta_name = $heading;
            $meta_keyword = "Free Industry Analysis, Current Market Analysis, Industry Analysis and Expert's review.";
             // $meta_desc ="Read our free analysis of trending markets and industries.";
             $meta_desc ="Read Exclusive market analysis by DecisionDatabases.com along with the latest market trends by our industry experts to know the growth prospects of various industries and markets.";
        }
        else
        {
            $meta_name = $heading;
            $meta_keyword = $heading;
            $meta_desc = "DecisionDatabases Press Room brings press releases on the bestselling Market Research Report. Identify the growth trends and Market value in terms of revenue and volume.";
        }
        /* code End VG-31/01/2017*/
        /*$meta_name = $heading;
        $meta_keyword = $heading;
        $meta_desc = $heading;*/
        $articles = $this->Paginator->paginate();
        $this->set(compact('articles', 'heading', 'meta_name', 'meta_keyword', 'meta_desc', 'type'));
    }

    public function article_details($folder = null, $article_id = null, $url_slug = null) {
        $article = $this->Article->find('first', array('conditions' => array('Article.id' => $article_id)));
        Configure::load('idata');
        $ar_type = Configure::read('idata.article_type');
        $heading = $ar_type[$folder];
        $meta_name = $article['Article']['meta_title'];
        $meta_keyword = $article['Article']['meta_keywords'];
        $meta_desc = $article['Article']['meta_desc'];
        $this->set(compact('article', 'heading', 'meta_name', 'meta_keyword', 'meta_desc', 'folder'));
    }

    public function beforeFilter() {
        $this->Auth->allow(array('article_listing', 'article_details'));
    }

}
