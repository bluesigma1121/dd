<?php

App::uses('AppController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class SitemapController extends AppController {

    public $uses = array();

    /**
     * Components
     *
     * @var array
     */
    public $components = array('RequestHandler');

    /**
     * index method
     *
     * @return void
     */
    public function generate() {
        $this->uses = array('Category','Product');

        $categories = $this->Category->find('all', array(
            'fields'        => array('id','cat_slug', 'modified'),
            'conditions'    => array('Category.is_active' => 1),           
            'recursive'     => -1 
           )
        );

        // $products = $this->Product->find('all', array(
        //     'fields'        => array('id','slug', 'modified'),
        //     'conditions'    => array('Product.is_active' => 1),           
        //     'recursive'     => -1 
        //    )
        // );
     
        $data['categories'] = $categories;        
        // $data['products'] = $products;
     
        $data['products'] = $this->Product->query("SELECT id,slug,modified FROM products as Product WHERE is_active=1 ORDER BY id ASC LIMIT 0,20000");
        $data['products1'] = $this->Product->query("SELECT id,slug,modified FROM products as Product WHERE is_active=1 ORDER BY id ASC LIMIT 20000,20000");
        $data['products2'] = $this->Product->query("SELECT id,slug,modified FROM products as Product WHERE is_active=1 ORDER BY id ASC LIMIT 40000,20000");
        $this->set($data);
        $this-> render();
    }
    public function pagesitemap() {
        $this->uses = array('Category');

        $categories = $this->Category->find('all', array(
            'fields'        => array('id','cat_slug', 'modified'),
            'conditions'    => array('Category.is_active' => 1),           
            'recursive'     => -1 
           )
        );
        $data['categories'] = $categories;
        $this->set($data);
        $this-> render();
    }
    public function generate1() {
        $this->uses = array('Product');
        $data['products'] = $this->Product->query("SELECT id,slug,modified FROM products as Product WHERE is_active=1 ORDER BY id ASC LIMIT 0,20000");
        $this->set($data);
        $this-> render();
    }
    public function generate2() {
        $this->uses = array('Product');

        $data['products'] = $this->Product->query("SELECT id,slug,modified FROM products as Product WHERE is_active=1 ORDER BY id ASC LIMIT 20000,20000");
             
        $this->set($data);
        $this-> render();
    }
    public function generate3() {
        $this->uses = array('Product');

        $data['products'] = $this->Product->query("SELECT id,slug,modified FROM products as Product WHERE is_active=1 ORDER BY id ASC LIMIT 40000,20000");
        $this->set($data);
        $this-> render();
    }

    public function beforeFilter() {
        $this->Auth->allow(array('generate','generate1','generate2','generate3', 'pagesitemap'));
    }
}    