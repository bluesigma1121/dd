<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('Paypal', 'Paypal.Lib');

/**
 * CartItems Controller
 *
 * @property CartItem $CartItem
 * @property PaginatorComponent $Paginator
 */
class CartItemsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        $this->CartItem->recursive = 0;
        $this->set('cartItems', $this->Paginator->paginate());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->CartItem->exists($id)) {
            throw new NotFoundException(__('Invalid cart item'));
        }
        $options = array('conditions' => array('CartItem.' . $this->CartItem->primaryKey => $id));
        $this->set('cartItem', $this->CartItem->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->CartItem->create();
            if ($this->CartItem->save($this->request->data)) {
                $this->Session->setFlash(__('The cart item has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The cart item could not be saved. Please, try again.'));
            }
        }
        $products = $this->CartItem->Product->find('list');
        $users = $this->CartItem->User->find('list');
        $orders = $this->CartItem->Order->find('list');
        $this->set(compact('products', 'users', 'orders'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->CartItem->exists($id)) {
            throw new NotFoundException(__('Invalid cart item'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->CartItem->save($this->request->data)) {
                $this->Session->setFlash(__('The cart item has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The cart item could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('CartItem.' . $this->CartItem->primaryKey => $id));
            $this->request->data = $this->CartItem->find('first', $options);
        }
        $products = $this->CartItem->Product->find('list');
        $users = $this->CartItem->User->find('list');
        $orders = $this->CartItem->Order->find('list');
        $this->set(compact('products', 'users', 'orders'));
    }

    public function cart() {
        if($this->request->is('post')){
            $data = $this->request->data;
            $this->Session->write('cart_item',$data);
            $this->redirect(array('controller'=>'orders','action'=>'process_order'));
        }
    }

    public function add_to_cart() {
        $result = array();
        $total = 0;
        /* if (AuthComponent::user('id')) {
          $cart_item_data = $this->CartItem->find('all', array('conditions' => array('CartItem.user_id' => AuthComponent::user('id')), 'recursive' => -1));

          if (!empty($cart_item_data)) {
          $i = 0;
          $total = 0;
          foreach ($cart_item_data as $key => $data) {
          //    debug($data);
          $result[$i] = $this->CartItem->Product->find('first', array('conditions' => array('Product.id' => $data['CartItem']['product_id']), 'recursive' => -1));
          $result[$i]['Product']['licence_type'] = $data['CartItem']['licence_type'];
          $result[$i]['Product']['req_qty'] = 1;
          $result[$i]['Product']['offer_price'] = $data['CartItem']['offer_price'];
          $result[$i]['Product']['is_offer_set'] = $data['CartItem']['is_offer_set'];
          if ($data['CartItem']['is_offer_set'] == 1) {
          $result[$i]['Product']['subtotal'] = $data['CartItem']['offer_price'];
          } else {
          $result[$i]['Product']['subtotal'] = $data['CartItem']['current_price'];
          }
          $total = $total + $result[$i]['Product']['subtotal'];
          $i++;
          }
          }
          } else { */
        if ($this->Session->check('cart_product')) {
            $session_data = $this->Session->read('cart_product');
            if (!empty($session_data)) {
                $i = 0;
                $total = 0;
                foreach ($session_data as $key => $data) {
                    foreach ($data as $lkey => $record) {
                        $result[$i] = $this->CartItem->Product->find('first', array('conditions' => array('Product.id' => $key), 'recursive' => -1));
                        $result[$i]['Product']['licence_type'] = $lkey;
                        $result[$i]['Product']['req_qty'] = 1;
                        $result[$i]['Product']['is_offer_set'] = 0;
                        $result[$i]['Product']['offer_price'] = 0;
                        $result[$i]['Product']['subtotal'] = $record['price'];
                        $total = $total + $result[$i]['Product']['subtotal'];
                        $i++;
                    }
                }
            }
        }
        //}
        Configure::load('idata');
        $licence_type = Configure::read('idata.licence_type');
        $this->set(compact('result', 'total', 'licence_type'));
    }

    public function remove_cart_product($pro_id = NULL, $licence_type = NULL) {

        /* if (AuthComponent::user('id')) {
          $this->CartItem->deleteAll(array('CartItem.product_id' => $pro_id, 'CartItem.licence_type' => $licence_type, 'CartItem.user_id' => AuthComponent::user('id')));
          $this->Session->setFlash(__('This product has been deleted from your shopping cart'), 'success');
          return $this->redirect(array('controller' => 'cart_items', 'action' => 'add_to_cart'));
          } else { */
        if ($this->Session->check('cart_product')) {
            $session_data = $this->Session->read('cart_product');
            if (array_key_exists($pro_id, $session_data)) {
                if (array_key_exists($licence_type, $session_data[$pro_id])) {
                    unset($session_data[$pro_id][$licence_type]);
                }
                if (empty($session_data[$pro_id])) {
                    unset($session_data[$pro_id]);
                }
            }
            $this->Session->write('cart_product', $session_data);
            $this->Session->setFlash(__('This product has been deleted from your shopping cart'), 'success');
            return $this->redirect(array('controller' => 'cart_items', 'action' => 'add_to_cart'));
        }
        //}
    }

    public function payment() {
        
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->CartItem->id = $id;
        if (!$this->CartItem->exists()) {
            throw new NotFoundException(__('Invalid cart item'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->CartItem->delete()) {
            $this->Session->setFlash(__('The cart item has been deleted.'));
        } else {
            $this->Session->setFlash(__('The cart item could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function billing_info() {
        if (!$this->Session->check('cart_product')) {
            $this->Session->setFlash('Add Products to cart first.');
            $this->redirect($this->referer());
        }

        if ($this->request->is('post', 'put')) {
            $this->Paypal = new Paypal(array(
                'sandboxMode' => true,
                'nvpUsername' => 'mytest.b_api1.gmail.com',
                'nvpPassword' => 'QA72NS8GJMVKK5FB',
                'nvpSignature' => 'AWCJppxiinMrB040MY3Rvn2AXOqXAnrt-5SDVjcgdy9UqAR22ivEXV.O'
            ));

            /* $this->Paypal = new Paypal(array(
              'sandboxMode' => false,
              'nvpUsername' => 'admin_api1.decisiondatabases.com',
              'nvpPassword' => '95NQFYL48UGNYUJV',
              'nvpSignature' => 'Ajwath3vWTxZhV-dji.0VNOXCfYMAPmQ-pzAeAM72MLSRQaFx4.3-jHm'
              )); */

            $data = $this->request->data;
            $is_exist = array();
            if (!AuthComponent::user()) {
                $this->loadModel('User');
                $is_exist = $this->User->find('first', array(
                    'conditions' => array('User.email' => $data['User']['email']),
                    'recursive' => -1
                ));
            }
            if (empty($is_exist) && !AuthComponent::user()) {
                $data['User']['password'] = substr(str_shuffle("23456789abcdefghjkmnopqrstuvwxyz"), 0, 6);
                $data['User']['role'] = 11;
                $pwd = $data['User']['password'];
                $this->User->create();
                if ($this->User->save($data)) {
                    $user_id = $this->User->id;
                    $data['User']['id'] = $user_id;
                    $this->Session->write('user_bill_detail', $data);
                    $data['User']['id'] = $user_id;
                    //send mail to user
                    $email = new CakeEmail();
                    $email->config('user_registration');
                    $email->to($this->request->data['User']['email']);
                    $email->viewVars(compact('data', 'pwd', 'category'));
                    $email->subject('Online Registration on Decision Databases');
                    $email->send();

                    //Send mail to DD admin not build yet

                    if ($this->Auth->login($data['User'])) {
                        $last_login = Date('Y-m-d H:i:s');
                        $this->User->updateAll(
                                array('User.last_login' => "'$last_login'"), array('User.id' => $user_id)
                        );

                        $order = $this->CartItem->inser_cart_items($user_id);
                        if ($order) {
                            $order_session = $this->Session->read('order_session');
                            $res = $this->Paypal->setExpressCheckout($order_session);
                            $this->redirect($res);
                        } else {
                            $this->Session->setFlash("Something went wrong. Please try again after some time.", 'error');
                            $this->redirect('/');
                        }
                    }
                }
            } else {
                if (AuthComponent::user()) {
                    $user_id = AuthComponent::user('id');
                } else {
                    $this->Auth->login($is_exist['User']);
                    $user_id = $is_exist['User']['id'];
                }

                $user_bill_details['User']['first_name'] = $this->Auth->user('first_name');
                $user_bill_details['User']['last_name'] = $this->Auth->user('last_name');
                $user_bill_details['User']['address'] = $this->request->data['User']['address'];
                $user_bill_details['User']['city'] = $this->request->data['User']['city'];
                $user_bill_details['User']['state'] = $this->request->data['User']['pin_code'];
                $user_bill_details['User']['country'] = $this->request->data['User']['pin_code'];
                $user_bill_details['User']['email'] = $this->Auth->user('email');
                $user_bill_details['User']['pin_code'] = $this->request->data['User']['pin_code'];
                $user_bill_details['User']['mobile'] = $this->Auth->user('mobile');


                $this->Session->write('user_bill_detail', $user_bill_details);
                $order = $this->CartItem->inser_cart_items($this->Auth->user('id'));
                if ($order) {
                    $order_session = $this->Session->read('order_session');
                    $res = $this->Paypal->setExpressCheckout($order_session);
                    $this->redirect($res);
                } else {
                    $this->Session->setFlash("Something went wrong. Please try again after some time.", 'error');
                    $this->redirect('/');
                }
            }
        }
    }

    public function beforeFilter() {
        $this->Auth->allow(array('cart', 'add_to_cart','payment', 'remove_cart_product'));
    }

}
