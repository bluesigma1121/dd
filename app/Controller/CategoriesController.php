<?php

App::uses('AppController', 'Controller');
App::uses('Spreadsheet_Excel_Reader', 'Vendor');
App::import('Controller', 'Products'); //For home_rss() method  VG-16/12/2016


/**
 * Categories Controller
 *
 * @property Category $Category
 * @property PaginatorComponent $Paginator
 */
class CategoriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Category->recursive = 0;
        $this->set('categories', $this->Paginator->paginate());
    }

    public function category_details_list($id = null, $slug = null) {
        $cat_data = array();
        if (isset($id)) {
            $this->loadModel('Category');
            $prev_data = $this->Cookie->read('cat_view');
            if (!empty($prev_data)) {
                if (!in_array($id, $prev_data)) {
                    $recent_view_array = $this->Cookie->read('cat_view');
                    array_push($prev_data, $id);
                    $this->Cookie->write('cat_view', $prev_data, false, '+2 weeks');
                    $counter_data = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.no_of_view'), 'recursive' => -1));
                    $cnt = $counter_data['Category']['no_of_view'] + 1;
                    $this->Category->updateAll(array('Category.no_of_view' => $cnt), array('Category.id' => $id));
                }
            } else {
                $recent_view_array = array();
                array_push($recent_view_array, $id);
                $this->Cookie->write('cat_view', $recent_view_array, false, '+2 weeks');
                $counter_data = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.no_of_view'), 'recursive' => -1));
                $cnt = $counter_data['Category']['no_of_view'] + 1;
                $this->Category->updateAll(array('Category.no_of_view' => $cnt), array('Category.id' => $id));
            }
        }

        if (!empty($id)) {
            $main_cat = $this->Category->find('all', array('conditions' => array('Category.id' => $id), 'recursive' => -1));
            $cat_data = array();
            foreach ($main_cat as $key1 => $cat) {
                $cat_data['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
                $cat_data['Level1'][$key1]['id'] = $cat['Category']['id'];
                $cat_data['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
                $cat_data['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
                $level_two = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $cat['Category']['id']), 'recursive' => -1));
                foreach ($level_two as $key2 => $level2) {
                    $cat_data['Level1'][$key1]['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['id'] = $level2['Category']['id'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                    $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id']), 'recursive' => -1));
                    foreach ($level_three as $key3 => $level3) {
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                        $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id']), 'recursive' => -1));
                        foreach ($level_four as $key4 => $level4) {
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        }
                    }
                }
            }
        }

        /* $this->loadModel('Product');

          $relatet_top_product = $this->Product->find('all', array(
          'conditions' => array('Product.category_id' => $id),
          'limit' => 6,
          'order' => 'rand()',
          'recursive' => -1,
          'fields' => array('Product.id', 'Product.product_image', 'Product.product_name', 'Product.slug'),
          )); */



        $cook_data = $this->Cookie->read('recent_cook');
        $recent_view = array();
        if (!empty($cook_data)) {
            $recent_view = $this->Category->recent_view_function($cook_data);
        }

        $bredcum_array = array();
        $bredcum_array = $this->Category->bread_cum_function($id);
        $this->set(compact('cat_data', 'bredcum_array', 'id', 'main_cat', 'recent_view'));
    }

    public function category_list($id = null, $slug = null) {
        $cat_data = array();
        if (isset($id)) {
            $counter_data = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.no_of_view'), 'recursive' => -1));
            $cnt = $counter_data['Category']['no_of_view'] + 1;
            $this->Category->updateAll(array('Category.no_of_view' => $cnt), array('Category.id' => $id));
        }

        $main_cat = $this->Category->find('all', array('conditions' => array('Category.is_active' => 1, 'Category.display_home' => 1), 'recursive' => -1));
        $cat_data = array();
        foreach ($main_cat as $key1 => $cat) {
            $cat_data['Level1'][$key1]['category_name'] = $cat['Category']['category_name'];
            $cat_data['Level1'][$key1]['id'] = $cat['Category']['id'];
            $cat_data['Level1'][$key1]['short_desc'] = $cat['Category']['short_desc'];
            $cat_data['Level1'][$key1]['no_of_product'] = $cat['Category']['no_of_product'];
            $level_two = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $cat['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
            foreach ($level_two as $key2 => $level2) {
                $cat_data['Level1'][$key1]['Level2'][$key2]['category_name'] = $level2['Category']['category_name'];
                $cat_data['Level1'][$key1]['Level2'][$key2]['id'] = $level2['Category']['id'];
                $cat_data['Level1'][$key1]['Level2'][$key2]['short_desc'] = $level2['Category']['short_desc'];
                $cat_data['Level1'][$key1]['Level2'][$key2]['no_of_product'] = $level2['Category']['no_of_product'];
                $level_three = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level2['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                foreach ($level_three as $key3 => $level3) {
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['category_name'] = $level3['Category']['category_name'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['id'] = $level3['Category']['id'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['short_desc'] = $level3['Category']['short_desc'];
                    $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['no_of_product'] = $level3['Category']['no_of_product'];
                    $level_four = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level3['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                    foreach ($level_four as $key4 => $level4) {
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['category_name'] = $level4['Category']['category_name'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['id'] = $level4['Category']['id'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['short_desc'] = $level4['Category']['short_desc'];
                        $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['no_of_product'] = $level4['Category']['no_of_product'];
                        $level_five = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level4['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                        foreach ($level_five as $key5 => $level5) {
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['category_name'] = $level5['Category']['category_name'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['id'] = $level5['Category']['id'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['short_desc'] = $level5['Category']['short_desc'];
                            $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['no_of_product'] = $level5['Category']['no_of_product'];
                            $level_six = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $level5['Category']['id'], 'Category.is_active' => 1), 'recursive' => -1));
                            foreach ($level_six as $key6 => $level6) {
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['category_name'] = $level6['Category']['category_name'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['id'] = $level6['Category']['id'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['short_desc'] = $level6['Category']['short_desc'];
                                $cat_data['Level1'][$key1]['Level2'][$key2]['Level3'][$key3]['Level4'][$key4]['Level5'][$key5]['Level6'][$key6]['no_of_product'] = $level6['Category']['no_of_product'];
                            }
                        }
                    }
                }
            }
        }

        $cook_data = $this->Cookie->read('recent_cook');
        $recent_view = array();
        if (!empty($cook_data)) {
            $recent_view = $this->Category->recent_view_function($cook_data);
        }
         $meta_name="Explore All Category Wise Industrial Market Research Reports";
         $meta_desc="Explore more than 15000+ global and region specific reports across our major categories and domains.";
        $meta_keyword="Industry Analysis Reports, Market Research Reports, DecisionDatabases";
        $this->set(compact('cat_data', 'id', 'main_cat', 'recent_view','meta_name','meta_desc','meta_keyword'));


    }

//level_one  function  for indexing one level Category
    public function level_one() {
        $this->layout = 'admin_layout';

        if ($this->Session->check('parent_cat_filter')) {
            $cat_filter = $this->Session->read('parent_cat_filter');
        }
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['Category']['status_by'])) {
                if (empty($data['Category']['search_text'])) {
                    $this->Session->setFlash(__('Please enter text for search'), 'error');
                    return $this->redirect(array('action' => 'level_one'));
                }
            }
            $this->Session->write('parent_cat_filter', $data['Category']);
            return $this->redirect(array('action' => 'level_one'));
        }
        if ($this->Session->check('parent_cat_filter')) {
            $cat_filter = $this->Session->read('parent_cat_filter');
        }
        $conditions = array();
        if (isset($cat_filter['search_by'])) {   //search_by 0=>category name, 1=> category type
            if ($cat_filter['search_by'] == 'category_name') {
                $conditions["Category." . $cat_filter['search_by'] . " LIKE"] = '%' . $cat_filter['search_text'] . '%';
            }
        }
        $this->paginate = array(
            'conditions' => array($conditions, 'Category.parent_category_id ' => 0),
            'limit' => 30,
            'order' => array('Category.seq_no' => 'ASC')
        );
        $this->Category->recursive = 0;
        $categories1 = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('categories1', 'yes_no', 'per_type', 'cat_type'));
    }

    public function level_two() {
        $this->layout = 'admin_layout';
        if ($this->Session->check('lvl_2_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_2_cat_filter');
        }

        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['Category']['status_by'])) {
                if (empty($data['Category']['search_text'])) {
                    $this->Session->setFlash(__('Please enter text for search'), 'error');
                    return $this->redirect(array('action' => 'level_two'));
                }
            }
            $this->Session->write('lvl_2_cat_filter', $data['Category']);
// return $this->redirect(array('action' => 'level_two'));
        }

        if ($this->Session->check('lvl_2_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_2_cat_filter');
        }
        $conditions = array();
        if (isset($cat_filter1['search_by'])) {   //search_by 0=>category name, 1=> category type
            if ($cat_filter1['search_by'] == 'category_name') {
                $conditions["Category." . $cat_filter1['search_by'] . " LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
            if ($cat_filter1['search_by'] == 'parent_category_name') {
                $conditions["ParentCategory.category_name LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
        }
        $this->paginate = array(
            'conditions' => array($conditions, 'Category.level' => 2),
            'limit' => 30,
            'order' => array('Category.category_name' => 'ASC')
        );
        $this->Category->recursive = 0;
        $categories = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $yes_no1 = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'yes_no1', 'categories'));
    }

    public function level_three() {
        $this->layout = 'admin_layout';
        if ($this->Session->check('lvl_3_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_3_cat_filter');
        }
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['Category']['status_by'])) {
                if (empty($data['Category']['search_text'])) {
                    $this->Session->setFlash(__('Please enter text for search'), 'error');
                    return $this->redirect(array('action' => 'level_three'));
                }
            }
            $this->Session->write('lvl_3_cat_filter', $data['Category']);
// return $this->redirect(array('action' => 'level_two'));
        }
        if ($this->Session->check('lvl_3_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_3_cat_filter');
        }
        $conditions = array();
        if (isset($cat_filter1['search_by'])) {   //search_by 0=>category name, 1=> category type
            if ($cat_filter1['search_by'] == 'category_name') {
                $conditions["Category." . $cat_filter1['search_by'] . " LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
            if ($cat_filter1['search_by'] == 'parent_category_name') {
                $conditions["ParentCategory.category_name LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
        }
        $this->paginate = array(
            'conditions' => array($conditions, 'Category.level' => 3),
            'limit' => 30,
            'order' => array('Category.category_name' => 'ASC')
        );
        $this->Category->recursive = 0;
        $categories = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'categories'));
    }

    public function level_four() {
        $this->layout = 'admin_layout';
        if ($this->Session->check('lvl_4_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_4_cat_filter');
        }
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['Category']['status_by'])) {
                if (empty($data['Category']['search_text'])) {
                    $this->Session->setFlash(__('Please enter text for search'), 'error');
                    return $this->redirect(array('action' => 'level_four'));
                }
            }
            $this->Session->write('lvl_4_cat_filter', $data['Category']);
// return $this->redirect(array('action' => 'level_two'));
        }
        if ($this->Session->check('lvl_4_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_4_cat_filter');
        }
        $conditions = array();
        if (isset($cat_filter1['search_by'])) {   //search_by 0=>category name, 1=> category type
            if ($cat_filter1['search_by'] == 'category_name') {
                $conditions["Category." . $cat_filter1['search_by'] . " LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
            if ($cat_filter1['search_by'] == 'parent_category_name') {
                $conditions["ParentCategory.category_name LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
        }
        $this->paginate = array(
            'conditions' => array($conditions, 'Category.level' => 4),
            'limit' => 30,
            'order' => array('Category.category_name' => 'ASC')
        );
        $this->Category->recursive = 0;
        $categories = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'categories'));
    }

    public function level_five() {
        $this->layout = 'admin_layout';
        if ($this->Session->check('lvl_5_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_5_cat_filter');
        }
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['Category']['status_by'])) {
                if (empty($data['Category']['search_text'])) {
                    $this->Session->setFlash(__('Please enter text for search'), 'error');
                    return $this->redirect(array('action' => 'level_five'));
                }
            }
            $this->Session->write('lvl_5_cat_filter', $data['Category']);
// return $this->redirect(array('action' => 'level_two'));
        }
        if ($this->Session->check('lvl_5_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_5_cat_filter');
        }
        $conditions = array();
        if (isset($cat_filter1['search_by'])) {   //search_by 0=>category name, 1=> category type
            if ($cat_filter1['search_by'] == 'category_name') {
                $conditions["Category." . $cat_filter1['search_by'] . " LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
            if ($cat_filter1['search_by'] == 'parent_category_name') {
                $conditions["ParentCategory.category_name LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
        }
        $this->paginate = array(
            'conditions' => array($conditions, 'Category.level' => 5),
            'limit' => 30,
            'order' => array('Category.category_name' => 'ASC')
        );
        $this->Category->recursive = 0;
        $categories = $this->Paginator->paginate();

        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'categories'));
    }

    public function level_six() {
        $this->layout = 'admin_layout';
        if ($this->Session->check('lvl_6_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_6_cat_filter');
        }
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['Category']['status_by'])) {
                if (empty($data['Category']['search_text'])) {
                    $this->Session->setFlash(__('Please enter text for search'), 'error');
                    return $this->redirect(array('action' => 'level_six'));
                }
            }
            $this->Session->write('lvl_6_cat_filter', $data['Category']);
// return $this->redirect(array('action' => 'level_two'));
        }
        if ($this->Session->check('lvl_6_cat_filter')) {
            $cat_filter1 = $this->Session->read('lvl_6_cat_filter');
        }
        $conditions = array();
        if (isset($cat_filter1['search_by'])) {   //search_by 0=>category name, 1=> category type
            if ($cat_filter1['search_by'] == 'category_name') {
                $conditions["Category." . $cat_filter1['search_by'] . " LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
            if ($cat_filter1['search_by'] == 'parent_category_name') {
                $conditions["ParentCategory.category_name LIKE"] = '%' . $cat_filter1['search_text'] . '%';
            }
        }
        $this->paginate = array(
            'conditions' => array($conditions, 'Category.level' => 6),
            'limit' => 30,
            'order' => array('Category.category_name' => 'ASC')
        );
        $this->Category->recursive = 0;
        $categories = $this->Paginator->paginate();
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'categories'));
    }

    public function session_del($i = null) {
        $this->layout = null;
        if ($i == 1) {
            $this->Session->delete('parent_cat_filter');
            return $this->redirect(array('action' => 'level_one'));
        } elseif ($i == 2) {
            $this->Session->delete('lvl_2_cat_filter');
            return $this->redirect(array('action' => 'level_two'));
        } elseif ($i == 3) {
            $this->Session->delete('lvl_3_cat_filter');
            return $this->redirect(array('action' => 'level_three'));
        } elseif ($i == 4) {
            $this->Session->delete('lvl_4_cat_filter');
            return $this->redirect(array('action' => 'level_four'));
        } elseif ($i == 5) {
            $this->Session->delete('lvl_5_cat_filter');
            return $this->redirect(array('action' => 'level_five'));
        } else {
            $this->Session->delete('lvl_6_cat_filter');
            return $this->redirect(array('action' => 'level_six'));
        }
    }

//Add Level one category
    public function add_level_one() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $res = $this->Category->fun_add_level_one($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash('New Level-One category has been saved.', 'success');
                return $this->redirect(array('action' => 'level_one'));
            } elseif ($res == 0) {
                $this->Session->setFlash('The Level-One category could not be saved. Please, try again.', 'error');
            } else {
                $this->Session->setFlash('The submitted category name already exists..', 'error');
                $this->redirect($this->referer());
            }
        }
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $display_home = Configure::read('idata.display_home');

        $this->set(compact('category_name', 'yes_no', 'display_home'));
    }

    public function add_level_two() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {

            if (!empty($this->request->data['option_name'])) {
                $res = $this->Category->fun_add_level_two($this->request->data);
                if ($res == 1) {
                    $this->Session->setFlash('New Level-Two category has been saved.', 'success');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_two'));
                } else {
                    $this->Session->setFlash('The Level-Two category could not be saved. Please, try again.', 'error');
                }
            } else {
                $this->Session->setFlash('Please add the level-2 categories', 'error');
            }
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories'));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'ans'));
    }

    public function add_level_three() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            if (!empty($this->request->data['option_name'])) {
                $res = $this->Category->fun_add_level_three($this->request->data);
                if ($res == 1) {
                    $this->Session->setFlash('New Level-Three category has been saved.', 'success');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_three'));
                } else {
                    $this->Session->setFlash('The Level-Three category could not be saved. Please, try again.', 'error');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_three'));
                }
            } else {
                $this->Session->setFlash('Please add the level-3 categories', 'error');
            }
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 2, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories'));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'ans'));
    }

    public function add_level_four() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            if (!empty($this->request->data['option_name'])) {
                $res = $this->Category->fun_add_level_four($this->request->data);
                if ($res == 1) {
                    $this->Session->setFlash('New Level-Four category has been saved.', 'success');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_four'));
                } else {
                    $this->Session->setFlash('The Level-Four category could not be saved. Please, try again.', 'error');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_four'));
                }
            } else {
                $this->Session->setFlash('Please add the level-4 categories', 'error');
                return $this->redirect(array('controller' => 'categories', 'action' => 'add_level_four'));
            }
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 3, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories'));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'ans'));
    }

    public function add_level_five() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            if (!empty($this->request->data['option_name'])) {
                $res = $this->Category->fun_add_level_five($this->request->data);
                if ($res == 1) {
                    $this->Session->setFlash('New Level-Five category has been saved.', 'success');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_five'));
                } else {
                    $this->Session->setFlash('The Level-Five category could not be saved. Please, try again.', 'error');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_five'));
                }
            } else {
                $this->Session->setFlash('Please add the level-5 categories', 'error');
                return $this->redirect(array('controller' => 'categories', 'action' => 'add_level_five'));
            }
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 4, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories'));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'ans'));
    }

    public function add_level_six() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            if (!empty($this->request->data['option_name'])) {
                $res = $this->Category->fun_add_level_six($this->request->data);
                if ($res == 1) {
                    $this->Session->setFlash('New Level-Six category has been saved.', 'success');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_six'));
                } else {
                    $this->Session->setFlash('The Level-Five category could not be saved. Please, try again.', 'error');
                    return $this->redirect(array('controller' => 'categories', 'action' => 'level_six'));
                }
            } else {
                $this->Session->setFlash('Please add the level-5 categories', 'error');
                return $this->redirect(array('controller' => 'categories', 'action' => 'add_level_six'));
            }
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 5, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories'));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'ans'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        Configure::load('idata');
        $yes_no = Configure::read('shop.yes_no');

        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
        $category = $this->Category->find('first', $options);
        $sub_cate = $this->Category->find('all', array('conditions' => array('Category.parent_category_id' => $category['Category']['id']), 'fields' => array('Category.id', 'Category.category_name', 'Category.level'), 'recursive' => -1));
        $this->set(compact('category', 'yes_no', 'sub_cate'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Category->create();
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        }
    }

    public function dropdwn_fill($id = NULL, $pro_id = NULL) {
        $this->layout = null;
        $this->loadModel('Product');
        $select_prod_cat = $this->Product->find('list', array('conditions' => array('Product.category_id' => $pro_id), 'fields' => array('Product.category_id', 'Product.category_id')));
        $result = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => $id, 'Category.id NOT' => $select_prod_cat), 'fields' => array('Category.id', 'Category.category_name'), 'recursive' => -1));
        $this->set(compact('result'));
        $this->render('ajax_result');
    }

    public function dropdwn_fill_add($id = NULL, $pro_id = NULL) {
        $this->layout = null;
        $this->loadModel('ProductCategory');
        $result = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => $id), 'fields' => array('Category.id', 'Category.category_name'), 'recursive' => -1));
        $this->set(compact('result'));
        $this->render('ajax_result');
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The category has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The category could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);
        }
    }

    public function edit_level_one($id = null) {
        $this->layout = 'admin_layout';
        $this->loadModel('Category');
        $category_name = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.category_name'), 'recursive' => -1));
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Category']['id'] = $id;
            $res = $this->Category->fun_edit_level_one($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash('The edited level-one category updated successfully.', 'success');
                return $this->redirect(array('action' => 'level_one'));
            } else {
                $this->Session->setFlash('The edited level-one category could not be updated. Please, try again.', 'error');
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);
        }
        $parentCategories = $this->Category->ParentCategory->find('list');
        $this->set(compact('parentCategories'));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $display_home = Configure::read('idata.display_home');
        $this->set(compact('category_name', 'yes_no', 'display_home'));
    }

    public function edit_level_two($id = null) {
        $this->layout = 'admin_layout';
        $this->loadModel('Category');
        $category_name = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.category_name'), 'recursive' => -1));
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Category']['id'] = $id;
            $res = $this->Category->fun_edit_level_two($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash('The edited child category updated successfully.', 'success');
                return $this->redirect(array('action' => 'level_two'));
            } else {
                $this->Session->setFlash('The edited child category could not be updated. Please, try again.', 'error');
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));

            $this->request->data = $this->Category->find('first', $options);
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 1, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories', $categories));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'category_name'));
    }

    public function edit_level_three($id = null) {
        $this->layout = 'admin_layout';
        $this->loadModel('Category');
        $category_name = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.category_name'), 'recursive' => -1));
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Category->id = $id;
            $data = $this->request->data;
            $data['Category']['category_name'] = ucwords($data['Category']['category_name']);
            $parent_id = $data['Category']['parent_category_id'];
            if ($this->Category->save($data)) {
                $this->Session->setFlash('The edited child category updated successfully.', 'success');
                return $this->redirect(array('action' => 'level_three'));
            } else {
                $this->Session->setFlash('The edited child category could not be updated. Please, try again.', 'error');
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));

            $this->request->data = $this->Category->find('first', $options);
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 2, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories', $categories));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'category_name'));
    }

    public function edit_level_four($id = null) {
        $this->layout = 'admin_layout';
        $this->loadModel('Category');
        $category_name = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.category_name'), 'recursive' => -1));
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Category->id = $id;
            $data = $this->request->data;
            $data['Category']['category_name'] = ucwords($data['Category']['category_name']);
            if ($this->Category->save($data)) {
                $this->Session->setFlash('The edited level-three category updated successfully.', 'success');
                return $this->redirect(array('action' => 'level_four'));
            } else {
                $this->Session->setFlash('The edited level-three category could not be updated. Please, try again.', 'error');
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
            $this->request->data = $this->Category->find('first', $options);
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 3, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories'));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'category_name'));
    }

    public function edit_level_five($id = null) {
        $this->layout = 'admin_layout';
        $this->loadModel('Category');
        $category_name = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.category_name'), 'recursive' => -1));
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Category->id = $id;
            $data = $this->request->data;
            $data['Category']['category_name'] = ucwords($data['Category']['category_name']);
            $parent_id = $data['Category']['parent_category_id'];
            if ($this->Category->save($data)) {
                $this->Session->setFlash('The edited child category updated successfully.', 'success');
                return $this->redirect(array('action' => 'level_five'));
            } else {
                $this->Session->setFlash('The edited child category could not be updated. Please, try again.', 'error');
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));

            $this->request->data = $this->Category->find('first', $options);
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 4, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories', $categories));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'category_name'));
    }

    public function edit_level_six($id = null) {
        $this->layout = 'admin_layout';
        $this->loadModel('Category');
        $category_name = $this->Category->find('first', array('conditions' => array('Category.id' => $id), 'fields' => array('Category.category_name'), 'recursive' => -1));
        if (!$this->Category->exists($id)) {
            throw new NotFoundException(__('Invalid category'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->Category->id = $id;
            $data = $this->request->data;
            $data['Category']['category_name'] = ucwords($data['Category']['category_name']);
            $parent_id = $data['Category']['parent_category_id'];
            if ($this->Category->save($data)) {
                $this->Session->setFlash('The edited child category updated successfully.', 'success');
                return $this->redirect(array('action' => 'level_six'));
            } else {
                $this->Session->setFlash('The edited child category could not be updated. Please, try again.', 'error');
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));

            $this->request->data = $this->Category->find('first', $options);
        }
        $this->Category->recursive = 0;
        $categories = $this->Category->find('list', array('conditions' => array('Category.level' => 5, 'Category.is_active' => 1), 'fields' => array('id', 'category_name')));
        $this->set(compact('categories', $categories));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no', 'category_name'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */ public function delete($id = null) {
        $this->Category->id = $id;
        if (!$this->Category->exists()) {
            throw new NotFoundException(__('Invalid category'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Category->delete()) {
            $this->Session->setFlash(__('The category has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The category could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect($this->referer());
    }

    public function tree_view_categories($id = null) {

        $this->layout = 'admin_layout';
        Configure::load('idata');
        $switch_roles = Configure::read('idata.switch_roles');

        $cat_data = $this->Category->tree_view_cat_function();

        //$this->Category->find_count();
        $this->set(compact('cat_data', 'switch_roles', 'id'));
    }

    public function tree_view_sub_categories($id = null, $sel_id = null) {

        $this->layout = 'admin_layout';
        Configure::load('idata');
        $switch_roles = Configure::read('idata.switch_roles');

        $cat_data = $this->Category->tree_view_cat_function($id);
        if (empty($sel_id)) {
            $sel_id = $id;
        }
        //$this->Category->find_count();
        $this->set(compact('cat_data', 'switch_roles', 'id', 'sel_id'));
    }

    /*  public function add_tree_category() {
      $this->layout = null;
      if ($this->request->is('post')) {
      $cat_dtl = $this->Category->find('first', array('conditions' => array('Category.id' => $this->request->data['parent_id']), 'fields' => array('Category.level'), 'recursive' => -1));

      $new_cat = $this->Category->find('first', array('conditions' => array('Category.category_name' => $this->request->data['cat_name']), 'recursive' => -1));
      if (empty($new_cat)) {
      $level = 1;
      if ($cat_dtl['Category']['level'] == 1) {
      $level = 2;
      } elseif ($cat_dtl['Category']['level'] == 2) {
      $level = 3;
      } elseif ($cat_dtl['Category']['level'] == 3) {
      $level = 4;
      } else {
      $level = 5;
      }
      if ($level != 5) {
      $data['Category']['category_name'] = $this->request->data['cat_name'];
      $data['Category']['parent_category_id'] = $this->request->data['parent_id'];
      $data['Category']['level'] = $level;
      $this->Category->create();
      if ($this->Category->save($data)) {
      $result = 1;
      } else {
      $result = 0;
      }
      } else {
      $result = 2;
      }
      } else {
      $result = 3;
      }

      $this->set(compact('result'));
      $this->render('blanck_result');
      }
      } */

//    public function edit_tree_category1() {
//        $this->layout = null;
//        if ($this->request->is('post')) {
//            $cat_name = $this->request->data['cat_name'];
//            if (!empty($cat_name)) {
//                if ($this->Category->updateAll(array('Category.category_name' => "'$cat_name'"), array('Category.id' => $this->request->data['parent_id']))) {
//                    $result = 1;
//                } else {
//                    $result = 0;
//                }
//            } else {
//                $result = 0;
//            }
//
//            $this->set(compact('result'));
//            $this->render('blanck_result');
//        }
//    }

    public function add_tree_category() {
        $this->layout = null;
        if ($this->request->is('post')) {

            $this->request->data['Category']['cat_breadcrub'] = $this->request->data['Category']['category_name'];

            if (empty($this->request->data['Category']['page_title'])) {
                if (!empty($this->request->data['Category']['category_name'])) {
                    $this->request->data['Category']['page_title'] = $this->request->data['Category']['category_name'];
                }
            }

            if (empty($this->request->data['Category']['page_desc'])) {
                if (!empty($this->request->data['Category']['short_desc'])) {
                    $temp_str = substr(strip_tags($this->request->data['Category']['short_desc']), 0, 160);
                    $last = strrpos($temp_str, '.');
                    $this->request->data['Category']['page_desc'] = substr($temp_str, 0, $last + 1);
                }
            }

            $this->request->data['Category']['cat_slug'] = $this->Category->cleanString($this->request->data['Category']['category_name']);

            $cat_dtl = $this->Category->find('first', array('conditions' => array('Category.id' => $this->request->data['Category']['parent_category_id']), 'fields' => array('Category.level'), 'recursive' => -1));
            $new_cat = $this->Category->find('first', array('conditions' => array('Category.category_name' => $this->request->data['Category']['category_name']), 'recursive' => -1));

            if (empty($new_cat)) {
//                $level = 1;
//                if ($cat_dtl['Category']['level'] == 1) {
//                    $level = 2;
//                } elseif ($cat_dtl['Category']['level'] == 2) {
//                    $level = 3;
//                } elseif ($cat_dtl['Category']['level'] == 3) {
//                    $level = 4;
//                } else {
//                    $level = 5;
//                }
                $new_id = '';
                if ($cat_dtl['Category']['level'] <= 5) {
                    $this->request->data['Category']['level'] = $cat_dtl['Category']['level'] + 1;
                    $this->Category->create();

                    if ($this->Category->save($this->request->data)) {
                        $new_id = $this->Category->id;
                        $this->Session->setFlash(__('The Category Added'), 'success');
                    } else {
                        $this->Session->setFlash(__('The Category Could Not be Updated'), 'error');
                    }
                } else {
                    $this->Session->setFlash(__('The Category Could Not be Updated Because Level Restricted'), 'error');
                }
            } else {
                $this->Session->setFlash(__('The Category Name Already Exists'), 'error');
            }
            $target = $this->request->data['Category']['target_id'];
            $this->Category->find_count();
            if (isset($this->request->data['Category']['refpage'])) {
                return $this->redirect(array('action' => 'tree_view_sub_categories', $this->request->data['Category']['main_cat'], $new_id, '#' => $target));
            }
            return $this->redirect(array('action' => 'tree_view_categories', $new_id, '#' => $target));
        }
    }

    public function edit_tree_category() {
        $this->layout = null;
        if ($this->request->is('post')) {

            if (empty($this->request->data['Category']['cat_breadcrub'])) {
                $this->request->data['Category']['cat_breadcrub'] = $this->request->data['Category']['category_name'];
            }

            if ($this->Category->save($this->request->data)) {
                $this->Session->setFlash(__('The Category Updated'), 'success');
            } else {
                $this->Session->setFlash(__('The Category Could Not be Updated. Please, try again.'), 'error');
            }
            $target = $this->request->data['Category']['target_id'];

            if (isset($this->request->data['Category']['refpage'])) {
                return $this->redirect(array('action' => 'tree_view_sub_categories', $this->request->data['Category']['main_cat'], $this->request->data['Category']['id'], '#' => $target));
            }
            return $this->redirect(array('action' => 'tree_view_categories', $this->request->data['Category']['id'], '#' => $target));
        }
    }

    public function remove_tree_category() {
        $this->layout = null;
        if ($this->request->is('post')) {
            $this->loadModel('ProductCategory');
            $parent_cat = $this->Category->find('first', array('conditions' => array('Category.id' => $this->request->data['cat_id']), 'fields' => array('Category.parent_category_id', 'Category.category_name'), 'recursive' => -1));
            $cat_id = $parent_cat['Category']['parent_category_id'];
            $prod_cat_dtl = $this->ProductCategory->find('first', array('conditions' => array('ProductCategory.category_id' => $this->request->data['cat_id']), 'recursive' => -1));
            if (empty($prod_cat_dtl)) {
                if ($this->Category->delete($this->request->data['cat_id'])) {
                    $result = $cat_id;
                    // $this->Category->find_count();
                } else {
                    $result = 0;
                }
            } else {
                $result = 0;
            }

            $this->set(compact('result'));
            $this->render('blanck_result');
        }
    }

    public function active_tree_category() {
        $this->layout = null;
        if ($this->request->is('post')) {
            if ($this->Category->updateAll(array('Category.is_active' => 1), array('Category.id' => $this->request->data['cat_id']))) {
                $result = 1;
            } else {
                $result = 0;
            }
            $this->set(compact('result'));
            $this->render('blanck_result');
        }
    }

    public function deactive_tree_category() {
        $this->layout = null;
        if ($this->request->is('post')) {
            $ids = $this->Category->find_sub_cat_ids($this->request->data['cat_id']);
            if ($this->Category->deactive_child($ids)) {
                $result = 1;
            } else {
                $result = 0;
            }
            $this->set(compact('result'));
            $this->render('blanck_result');
        }
    }

    public function get_child($id = null) {
        $this->layout = null;
        $childs = $this->Category->find('list', array('conditions' => array('Category.parent_category_id' => $id),
            'fields' => array('Category.id',
                'Category.category_name')
        ));
        $this->set(compact('childs'));
    }

    public function change_currency() {
        if ($this->request->is(array('post', 'put'))) {
            $this->Session->write('currency', $this->request->data['Category']['currency']);
            return $this->redirect($this->referer());
        }
        return $this->redirect($this->referer());
    }

    public function home() {

        $this->loadModel('Testimonial');
        $this->loadModel('Product');
        $research_cat = $this->Category->find('all', array(
            'conditions' => array(
                'Category.is_active' => 1, 'Category.display_home' => 1
            ),
            'recursive' => -1,
            'order' => array( 'Category.seq_no' => 'ASC' ),
            'cache' => 'categoryList', 
            'cacheConfig' => 'short'
        ));

        $ourclients = $this->Category->our_client_fun();

        // $main_category_list = $this->Category->find('list', array(
        //     'conditions' => array('Category.id' => array(1, 2, 3)),
        //     'fields' => array('Category.category_name')));

        //top 15 market research reports to replace home products
        $products = array();

        $all_cate = $this->Category->find_sub_cat_ids(2);

        $this->loadModel('ProductCategory');
        $product_ids = $this->ProductCategory->find('list', array(
            'conditions' => array('ProductCategory.category_id' => $all_cate),
            'fields' => array('ProductCategory.product_id','ProductCategory.product_id'),
            //'limit' => 10,
            'order' => array('ProductCategory.product_id DESC'),
            'cache' => 'productIdsList', 
            'cacheConfig' => 'short'
        ));

        $this->loadModel('Product');
        
         $products = $this->Product->find('all',array(
            'conditions'=>array('Product.id'=>$product_ids,'Product.is_active'=>1),
            'fields'=>array('Product.id','Product.product_name','Product.alias','Product.slug',
                'Product.category_id','Product.product_description','Product.price','Product.publisher_name','Product.pub_date',
                'Category.slug'),
        // Added publish date in fields section ends VG 11/2/2017
            'order'=>array('Product.modified DESC'),
            'recursive' => 0,
            'limit'=>15,
            'cache' => 'productList', 
            'cacheConfig' => 'short'
        ));
        //debug($products);
        foreach ($products as $pr_key => $pro) {
            $main_cat = $this->Product->find_main_cat($pro['Product']['category_id']);
            $products[$pr_key]['main_cat'] = $main_cat['Category'];
        }

        $home_products = $products;

        //Recent analysis
        $this->loadModel('Article');
        $articles = $this->Article->find('all',array(
            'conditions' => array('Article.article_type' => 'analysis'),
            'fields'=>array('headline','id','slug'),
            'recursive'=>-1,
            'order'=>array('Article.id DESC'),
            'limit'=>10,
            'cache' => 'articlesList', 
            'cacheConfig' => 'short'
        ));

        //Recent Press release
        $press_releases = $this->Article->find('all',array(
            'conditions' => array('Article.article_type' => 'press-releases'),
            'fields'=>array('headline','id','slug'),
            'recursive'=>-1,
            'order'=>array('Article.id DESC'),
            'limit'=>10,
            'cache' => 'articlesList', 
            'cacheConfig' => 'short'
        ));
        /*Testimonial code Start VG-15/12/2016*/
        $testimonial=$this->Testimonial->find('all',array(
                                            'conditions'=>array('Testimonial.is_verified' => 1,'Testimonial.is_active' => 1,'Testimonial.is_deleted' => 0),
                                            'fields' => array('id','testimonial_title','testimonial_description','testimonial_image'),
                                            'recursive' => -1,
                                            'order' => array('Testimonial.id DESC'),
                                            'limit' => 3,
                                            'cache' => 'testimonialList', 
                                            'cacheConfig' => 'short'
                                        ));
        /*Testimonial code End VG-15/12/2016        */

        /*RSS FEEDS code Start VG-16/12/2016*/
        $Products = new ProductsController;
        // Call a method from
        $rss = $Products->home_rss();
        //   debug( $rss);die;
        /*RSS FEEDS code End VG-16/12/2016        */
        $meta_name = "DecisionDatabases.com: Market Research Reports ,Industry Analysis, Market Insights";
        $meta_keyword="Market Research Reports, Industry Outlook, Industry Analysis, Market Insights, Industry Database, Market Information";
        $meta_desc = "DecisionDatabases.com is one stop shop for all types of Market Research Resources. Visit us to buy Market research reports, Company Profiles and Databases.";
        $this->set(compact('market_cat', 'ourclients', 'research_cat', 'main_category_list', 'recent_view',
                'meta_desc','meta_keyword','meta_name','home_cat', 'testimonial', 'home_products', 'cat','press_releases','articles','rss'));
    }

    public function remove($id = null, $l = null) {
        if ($this->Category->updateAll(array('Category.cat_disp_home_page' => 0), array('Category.id' => $id))) {
            $this->Session->setFlash(__('The Category Remove From Homepage.'), 'success');
            if ($l == 1) {
                return $this->redirect(array('action' => 'level_one'));
            } elseif ($l == 2) {
                return $this->redirect(array('action' => 'level_two'));
            } elseif ($l == 3) {
                return $this->redirect(array('action' => 'level_three'));
            } elseif ($l == 4) {
                return $this->redirect(array('action' => 'level_four'));
            } elseif ($l == 5) {
                return $this->redirect(array('action' => 'level_five'));
            } else {
                return $this->redirect(array('action' => 'level_six'));
            }
        } else {
            $this->Session->setFlash(__('The Category Could Not be Remove From Homepage. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function display($id = null, $l = null) {
        if ($this->Category->updateAll(array('Category.cat_disp_home_page' => 1), array('Category.id' => $id))) {
            $this->Session->setFlash(__('The Category Display On Homepage.'), 'success');
            if ($l == 1) {
                return $this->redirect(array('action' => 'level_one'));
            } elseif ($l == 2) {
                return $this->redirect(array('action' => 'level_two'));
            } elseif ($l == 3) {
                return $this->redirect(array('action' => 'level_three'));
            } elseif ($l == 4) {
                return $this->redirect(array('action' => 'level_four'));
            } elseif ($l == 5) {
                return $this->redirect(array('action' => 'level_five'));
            } else {
                return $this->redirect(array('action' => 'level_six'));
            }
        } else {
            $this->Session->setFlash(__('The Category Could Not be Display From Homepage. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function font_awsome_icon($id = null, $l = null) {
        $this->layout = 'admin_layout';
        if ($this->request->is(array('post', 'put'))) {
            $data['Category']['id'] = $id;
            $data['Category']['icon'] = $this->request->data['Category']['icon'];

            if ($this->Category->save($data)) {
                $this->Session->setFlash(__('The Icon has been saved.'), 'success');
                if ($l == 1) {
                    return $this->redirect(array('action' => 'level_one'));
                } elseif ($l == 2) {
                    return $this->redirect(array('action' => 'level_two'));
                } elseif ($l == 3) {
                    return $this->redirect(array('action' => 'level_three'));
                } elseif ($l == 4) {
                    return $this->redirect(array('action' => 'level_four'));
                } elseif ($l == 5) {
                    return $this->redirect(array('action' => 'level_five'));
                } else {
                    return $this->redirect(array('action' => 'level_six'));
                }
            } else {
                $this->Session->setFlash(__('The Icon Could not saved.'), 'error');
                return $this->redirect($this->referer());
            }
        } else {
            $options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));

            $this->request->data = $this->Category->find('first', $options);
        }
    }

   /* public function home_search_autofill($data = null) {
        $this->layout = false;
        $this->loadModel('Product');

        if ($this->request->is('post')) {
            $data = $this->request->data;

            $cat_cond = array();
            if (!empty($data['cat_id'])) {
                $cat_cond["Product.category_id"] = $data['cat_id'];
            }
            $conditions = array();
            //$conditions['OR']["Product.product_name LIKE"] = $data ['id'] . '%';
            $conditions['OR']["Product.product_name LIKE "] = '%'. $data['id']. '%';
            //$conditions['OR']["Product.product_description LIKE"] = '%' . $data['id'] . '%';

            $result_data = $this->Product->find('list', array(
                'conditions' => array($conditions, $cat_cond, 'Product.is_active' => 1, 'Product.is_deleted' => 0),
                'fields' => array('Product.id', 'Product.product_name'),
                'order' => 'Product.product_name ASC',
                'recursive' => -1,
                'limit' => 10
            ));

            $final = array();
            $i = 0;
            foreach ($result_data as $key => $val) {
                $final[$i]['id'] = $key;
                if ($data['size'] > 1200) {
                    $final [$i]['name'] = substr($val, 0, 70);
                } elseif (($data ['size'] < 400)) {
                    $final[$i]['name'] = substr($val, 0, 25) . "..";
                } else {
                    $final[$i]['name'] = substr($val, 0, 35);
                }

                $i++;
            }

            $this->set(compact('final'));
        }
    }*/


    //New autofill method Start VG-25/11/2016

    public function home_search_autofill($data = null) {
         $this->layout = false;

         $this->loadModel('Product');
         $search_results=null;
        if( $this->request->is('ajax') ) {
            // echo $_POST['value_to_send'];
            $raw_data = $this->request->data('query');
            $data=str_replace(array("'","\"",)," ",$raw_data);
                 /*if(!empty($data))
                 {
                        $filt="Product.product_name LIKE '%".$data."%'";
                        $search_results = $this->Product->find('all', array('conditions' => array($filt,'Product.is_active' => 1, 'Product.is_deleted' => 0), 'recursive' => 0,'order' => 'Product.product_name ASC','limit'=>10));
                        $this->set(compact('search_results'));
                  }*/
            if (!empty($data)) {
                 $keyword=$data;
                 $words=explode(" ", $keyword);
                 $filter_str=null;
                 $idx=0; $is_data=false;
                 for ($i=0; $i < sizeof($words); $i++) {
                    if (!empty($words[$i])) {

                      if($idx==0)
                      {
                         $filt="Product.product_name LIKE '%".($words[$i])."%'";
                         $ord = " case when Product.product_name like '".($words[$i])."'  then 1
                                       when Product.product_name like '".($words[$i])."%'  then 2
                                       when Product.product_name like '%".($words[$i])."%' then 3
                                       when Product.product_name like '%".($words[$i])."'  then 4 ";

                         $idx++;
                         $is_data=true;
                      }
                      else
                      {
                          $filt=$filt." AND Product.product_name LIKE '%".($words[$i])."%'";
                          $ord .= " when Product.product_name like '".($words[$i])."'  then 1
                                    when Product.product_name like '".($words[$i])."%'  then 2
                                    when Product.product_name like '%".($words[$i])."%' then 3
                                    when Product.product_name like '%".($words[$i])."'  then 4 ";
                      }
                    }
                  // echo $filt;
                 }

                 // for sentances search use following code
                 // $filt="Product.product_name LIKE '%".$data."%'";
                 // $ord = " case when Product.product_name like '".$data."%'  then 1
                 //               when Product.product_name like '%".$data."%' then 2
                 //               when Product.product_name like '%".$data."'  then 3 end ";
                 // ******************************************

                 if ($is_data==true) {
                    //  $search_results = $this->Product->find('all', array('conditions' => array($filt,'Product.is_active' => 1, 'Product.is_deleted' => 0), 'recursive' => 0,'order' => 'Product.product_name ASC','limit'=>10));
                     $search_results = $this->Product->find('all', array('conditions' => array($filt,'Product.is_active' => 1, 'Product.is_deleted' => 0), 'recursive' => 0,'order' => $ord.' end','limit'=>10));

                     $this->set(compact('search_results'));
                 }

            }
        //    debug($search_results);exit;
        $this->set(compact('search_results'));

        }
    }

    //New autofill method end VG-25/11/2016


   /* ==== Old Code of Searching Start ==== */

    /*public function home_search_form() {
        $this->loadModel('Product');
        if ($this->request->is('post')) {
            $data = $this->request->data; //debug($data);die;
            if (!empty($data['Category']['search_txt'])) {
                $slug_name = $this->Product->find('first', array('conditions' => array('Product.id' => $data['Category']['search_txt']),
                    'fields' => array('Product.slug', 'Product.category_id', 'Category.category_name', 'Category.slug'), 'recursive' => 0));
               // debug($slug_name);die;
                $this->redirect(array('controller' => 'products', 'action' => 'category_details', 'main' => $slug_name['Category']['slug'],
                    'id' => $data['Category']['search_txt'], 'slug' => $slug_name['Product']['slug']));
            } else {
                $this->Session->setFlash('Please enter a text to be search', 'error');

                return $this->redirect($this->referer());
            }
        }
    }*/

    /* ==== Old Code of Searching End ==== */

 /* ==== New Code of Searching Start VG-21-10-2016 ==== */
   public function home_search_form($data=null) {
       
        $this->loadModel('Product');
    //    $this->set(compact('paginator_url'));
       if ($this->request->is('get')) {
            $raw_data = $this->request->query('q');
            $data=str_replace(array("'","\"",)," ",$raw_data);
             //   debug($data);exit;
            if (!empty($data)) {
                 $keyword=$data;
                 $words=explode(" ", $keyword);
                 $filter_str=null;
                 $idx=0; $is_data=false;
                 for ($i=0; $i < sizeof($words); $i++) {
                    if (!empty($words[$i])) {
                        if($idx==0)
                        {
                           $filt="Product.product_name LIKE '%".$words[$i]."%'";
                           $ord = " case when Product.product_name like '".($words[$i])."%'  then 1
                                         when Product.product_name like '%".($words[$i])."%' then 2
                                         when Product.product_name like '%".($words[$i])."'  then 3 ";

                           $idx++;
                           $is_data=true;
                        }
                        else
                        {
                            $filt=$filt." AND Product.product_name LIKE '%".$words[$i]."%'";
                            $ord .= " when Product.product_name like '".($words[$i])."%'  then 1
                                      when Product.product_name like '%".($words[$i])."%' then 2
                                      when Product.product_name like '%".($words[$i])."'  then 3 ";

                        }
                    }


                 }
                 if ($is_data==true) {
                     // $search_results = $this->Product->find('all', array('conditions' => array($filt,'Product.is_active' => 1, 'Product.is_deleted' => 0), 'recursive' => 0,'order' => 'Product.product_name ASC','limit'=>50));
                    // $search_results = $this->Product->find('all', array('conditions' => array($filt,'Product.is_active' => 1, 'Product.is_deleted' => 0), 'recursive' => 0,'order' => $ord.' end','limit'=>50));
                    $this->Paginator->settings = array('conditions' => array($filt,'Product.is_active' => 1, 'Product.is_deleted' => 0), 'recursive' => 0,'order' => $ord.' end','limit'=>25);
                    $search_results = $this->Paginator->paginate('Product');

                    $this->set('results',$search_results);
                    $this->render('search_results');
                 }
                 else
                  {
                        $this->Session->setFlash('Please enter a text to be search', 'error');

                        return $this->redirect($this->referer());
                 }

            } else {
                $this->Session->setFlash('Please enter a text to be search', 'error');

                return $this->redirect($this->referer());
            }
        }
    }
/* ==== New Code of Searching End VG-21-10-2016 ==== */

    public function active($id = NULL) {
        if ($this->Category->updateAll(array('Category.is_active' => 1), array('Category.id' => $id))) {
            $this->Session->setFlash(__('The Category has been Activated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Category could not be Activated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deactive($id = NULL) {
        $ids = $this->Category->find_sub_cat_ids($id);
        if ($this->Category->deactive_child($ids)) {
            $this->Session->setFlash(__('The Category has been DeActivated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Category could not be DeActivated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function change_status_selected() {
        if ($this->request->is('post')) {
            $f = 0;
            if (isset($this->request->data['activate']) && !empty($this->request->data['Category']['selected_products'])) {
                $id = explode(',', $this->request->data['Category']['selected_products']);
                if ($this->Category->updateAll(array('Category.is_active' => 1), array('Category.id' => $id))) {
                    $this->Session->setFlash(__('The Categories has been Activated.'), 'success');
                    return $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('The Categories could not be Activated. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            } elseif (isset($this->request->data['deactivate']) && !empty($this->request->data['Category']['selected_products'])) {
                $ids = explode(',', $this->request->data['Category']['selected_products']);
                if (!empty($ids)) {
                    foreach ($ids as $ckey => $id) {
                        $all_ids = $this->Category->find_sub_cat_ids($id);
                        if ($this->Category->deactive_child($all_ids)) {
                            $f = 1;
                        }
                    }
                    if ($f == 1) {
                        $this->Session->setFlash(__('The Category has been DeActivated.'), 'success');
                        return $this->redirect($this->referer());
                    } else {
                        $this->Session->setFlash(__('The Category could not be DeActivated. Please, try again.'), 'error');
                        return $this->redirect($this->referer());
                    }
                }

                if ($this->Category->updateAll(array('Category.is_active' => 0), array('Category.id' => $id))) {
                    $this->Session->setFlash(__('The Categories has been DeActivated.'), 'success');
                    return $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('The Categories could not be DeActivated. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            } elseif (isset($this->request->data['delete']) && !empty($this->request->data['Category']['selected_products'])) {
                $id = explode(',', $this->request->data['Category']['selected_products']);
                if ($this->Category->deleteAll(array('Category.id' => $id))) {
                    $this->Session->setFlash(__('The Categories has been Deleted.'), 'success');
                    return $this->redirect($this->referer());
                } else {
                    $this->Session->setFlash(__('The Categories could not be Deleted. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            }
        }
    }

    public function dropdwn_contents() {
        $this->layout = null;
        $result = $this->Category->find('list', array('conditions' => array('Category.parent_id' => $coll_id), 'fields' => array('Category.id', 'Category.department_name'), 'recursive' => -1));
        $this->set(compact('result'));
        $this->render('filter');
    }

    public function find_edit_tree_category() {
        $this->layout = null;
        if ($this->request->is('post')) {
            $id = $this->request->data['parent_id'];
            if (!empty($id)) {
                $cat = $this->Category->find('first', array('conditions' => array('Category.id' => $id),
                    'fields' => array('Category.id', 'Category.category_name', 'Category.short_desc', 'Category.long_desc','Category.meta_keywords',
                        'Category.cat_url', 'Category.page_title', 'Category.page_desc', 'Category.cat_breadcrub', 'Category.cat_slug',
                        'Category.is_active'), 'recursive' => -1));
                $result['id'] = $cat['Category']['id'];
                $result['category_name'] = $cat['Category']['category_name'];
                $result['short_desc'] = $cat['Category']['short_desc'];
                $result['long_desc'] = $cat['Category']['long_desc'];

                if (empty($cat['Category']['cat_slug'])) {
                    $cat['Category']['cat_slug'] = $this->Category->cleanString($cat['Category']['category_name']);
                }
                $result['cat_slug'] = $cat['Category']['cat_slug'];
                $result['cat_url'] = Router::url('/', true) . 'reports' . '/' . $cat['Category']['id'] . '-' . $cat['Category']['cat_slug'];
                $result['page_title'] = $cat['Category']['page_title'];
                $result['page_desc'] = $cat['Category']['page_desc'];
                $result['is_active'] = $cat['Category']['is_active'];
                $result['meta_keywords'] = $cat['Category']['meta_keywords'];

                // $result['cat_breadcrub'] = $cat['Category']['cat_breadcrub'];
                $bread = $this->Category->bread_cum_function($id);

                $string = 'Home';
                if (!empty($bread)) {
                    foreach ($bread as $key => $bred) {
                        if (empty($bred['Category']['cat_breadcrub'])) {
                            $string.= ' >> ' . $bred['Category']['category_name'];
                        } else {
                            $string.= ' >> ' . $bred['Category']['cat_breadcrub'];
                        }
                    }
                }
                $result['cat_breadcrub'] = $string;
            }
            $this->set(compact('result'));
            $this->render('filter');
        }
    }

    public function category_publish($id = NULL, $target_id = null) {

        if ($this->Category->updateAll(array('Category.is_active' => 1), array('Category.id' => $id))) {
            $this->Session->setFlash(__('The Category has been Activated.'), 'success');
        } else {
            $this->Session->setFlash(__('The Category could not be Activated. Please, try again.'), 'error');
        }
        $target = $target_id;
        return $this->redirect(array('action' => 'tree_view_categories', $id, '#' => $target));
    }

    public function category_unpublish($id = NULL, $target_id = null) {
        $ids = $this->Category->find_sub_cat_ids($id);
        if ($this->Category->deactive_child($ids)) {
            $this->Session->setFlash(__('The Category has been DeActivated.'), 'success');
        } else {
            $this->Session->setFlash(__('The Category could not be DeActivated. Please, try again.'), 'error');
        }
        $target = $target_id;
        return $this->redirect(array('action' => 'tree_view_categories', $id, '#' => $target));
    }

    public function category_publish_sub($main_id = null, $id = NULL, $target_id = null, $ref = null) {

        if ($this->Category->updateAll(array('Category.is_active' => 1), array('Category.id' => $id))) {
            $this->Session->setFlash(__('The Category has been Activated.'), 'success');
        } else {
            $this->Session->setFlash(__('The Category could not be Activated. Please, try again.'), 'error');
        }
        $target = $target_id;

        if (!empty($ref)) {
            return $this->redirect(array('action' => 'tree_view_sub_categories', $main_id, $id, '#' => $target));
        }
        return $this->redirect(array('action' => 'tree_view_categories', $id, '#' => $target));
    }

    public function category_unpublish_sub($main_id = null, $sel_id = NULL, $target_id = null, $ref = null) {

        $ids = $this->Category->find_sub_cat_ids($sel_id);
        if ($this->Category->deactive_child($sel_id)) {
            $this->Session->setFlash(__('The Category has been DeActivated.'), 'success');
        } else {
            $this->Session->setFlash(__('The Category could not be DeActivated. Please, try again.'), 'error');
        }
        $target = $target_id;

        if (!empty($ref)) {
            return $this->redirect(array('action' => 'tree_view_sub_categories', $main_id, $sel_id, '#' => $target));
        }
        return $this->redirect(array('action' => 'tree_view_categories', $id, '#' => $target));
    }

    public function update_category_dtl() {
        $all = $this->Category->find('list');
        foreach ($all as $key => $id) {
            $this->Category->update_details($id);
        }
        die;
    }

    public function upload_category_excel() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($this->request->data['Category']['category_id'])) {
                $this->Session->setFlash(__('Please Select Category id '), 'error');
                return $this->redirect(array('action' => 'upload_category_excel'));
            }
            if (empty($this->request->data['Category']['category_upload']['name'])) {
                $this->Session->setFlash(__('Please Select Excel File '), 'error');
                return $this->redirect(array('action' => 'upload_category_excel'));
            }
            $cat_details = $this->Category->find('first', array('conditions' => array('Category.id' => $this->request->data['Category']['category_id']), 'recursive' => -1, 'fields' => array('Category.level')));
            if ($cat_details['Category']['level'] >= 6) {
                $this->Session->setFlash(__('Category Level Restricted For  Select Category id '), 'error');
                return $this->redirect(array('action' => 'upload_category_excel'));
            }
            $data['Category']['level'] = $cat_details['Category']['level'] + 1;
            $sts = $this->Category->upload_category_excel($data);

            if ($this->Session->check('category_fails')) {
                $category_fails = $this->Session->read('category_fails');

                $this->Session->delete('category_fails');
                $this->set(compact('category_fails'));
                $this->render('category_upload_fails');
            }

            if ($sts == 1) {
                $this->Session->setFlash(__('Category Added Successfully '), 'success');
                return $this->redirect(array('action' => 'tree_view_categories'));
            } elseif ($sts == 3) {
                $this->Session->setFlash(__('upload Category less than 100'), 'error');
                return $this->redirect(array('action' => 'tree_view_categories'));
            }
        }
        $all_categories = $this->Category->find('list', array('fields' => array('Category.id', 'Category.category_name')));
        $this->set(compact('all_categories'));
    }

    public function download_sample_file() {
        $this->viewClass = 'Media';
        $params = array(
            'id' => 'category_sample.xls',
            'name' => 'category_sample',
            'download' => true,
            'extension' => 'xls',
            'path' => APP . WEBROOT_DIR . DS . 'files' . DS
        );
        $this->set($params);
    }

    public function count_child() {
        // debug("hii");die;
        $level1 = $this->Category->find('list', array('conditions' => array('Category.level' => 1), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));

        if (!empty($level1)) {
            foreach ($level1 as $key => $cat_id) {

                $total = $this->Category->find_sub_count($cat_id);

                $this->Category->set_count($cat_id, $total);
            }
        }
        $level2 = $this->Category->find('list', array('conditions' => array('Category.level' => 2), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level2)) {
            foreach ($level2 as $key => $cat_id) {
                $total = $this->Category->find_sub_count($cat_id);

                $this->Category->set_count($cat_id, $total);
            }
        }
        $level3 = $this->Category->find('list', array('conditions' => array('Category.level' => 3), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level3)) {
            foreach ($level3 as $key => $cat_id) {
                $total = $this->Category->find_sub_count($cat_id);

                $this->Category->set_count($cat_id, $total);
            }
        }
        $level4 = $this->Category->find('list', array('conditions' => array('Category.level' => 4), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level4)) {
            foreach ($level4 as $key => $cat_id) {
                $total = $this->Category->find_sub_count($cat_id);

                $this->Category->set_count($cat_id, $total);
            }
        }
        $level5 = $this->Category->find('list', array('conditions' => array('Category.level' => 5), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level5)) {
            foreach ($level5 as $key => $cat_id) {
                $total = $this->Category->find_sub_count($cat_id);

                $this->Category->set_count($cat_id, $total);
            }
        }
        $level6 = $this->Category->find('list', array('conditions' => array('Category.level' => 6), 'fields' => array('Category.id', 'Category.id'), 'recursive' => 0));
        if (!empty($level6)) {
            foreach ($level6 as $key => $cat_id) {
                $child_count = $this->Category->find_sub_count($cat_id);
                $total = count($child_count);
                $this->Category->set_count($cat_id, $total);
            }
        }
        $this->Session->setFlash(__('The child count has been updated.'), 'success');
        return $this->redirect($this->referer());
    }

    public function display_active($is_active = NULL){
        if(empty($is_active)){
            $this->Session->delete('show_active');
        }
        else{
            $this->Session->write('show_active',1);
        }
        return $this->redirect($this->referer());
    }

    /*public function test_cat(){
        $this->loadModel("ProductCategory");
        $cat_list = $this->ProductCategory->find('all',array(
            'fields'=>array('ProductCategory.product_id','Product.product_name','Category.category_name')));
        $arr = array();
        foreach($cat_list as $key=>$val){
            //$arr[$val['Product']['product_name']][$key]['product_name'] = $val['Product']['product_name'];
            $arr[$val['Product']['product_name']][$key] = $val['Category']['category_name'];
        }

        $arr2 = array();
        foreach($arr as $key2=>$val2){
            if(count($val2)>1){
                $arr2[$key2] = $val2;
            }
        }
        debug($arr2);
        die;
    }*/

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('count_child', 'session_del','category_publish_sub', 'category_upload_fails', 'update_category_dtl', 'deactive_tree_category', 'active_tree_category', 'find_edit_tree_category', 'change_currency', 'home_search_form', 'font_awsome_icon', 'home_search_autofill', 'dropdwn_fill_add', 'home', 'blanck_result', 'remove_tree_category','category_unpublish_sub', 'ajax_result', 'add_tree_category', 'edit_tree_category', 'dropdwn_fill', 'get_child', 'category_details_list', 'category_list');
    }

}
