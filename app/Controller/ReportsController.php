<?php

App::uses('AppController', 'Controller');

/**
 * Reports Controller
 *
 * @property Reports $Reports
 * @property PaginatorComponent $Paginator
 */
class ReportsController extends AppController {

   
    public function index() {
        $this->layout = 'admin_layout';
        $this->loadModel('Product');
        if($_GET){
          $conditions = array();
          
          $result = $this->Product->find('all', array('conditions' => array( 'date(Product.modified) >=' => $_GET['from'], 'date(Product.modified) <=' => $_GET['to']),
                                                      'fields' => array('product_name','publisher_name','modified','price'), 'order' => array('Product.modified'), 'recursive' => -1));

          $this->set(compact('result'));
        }
        
    }

    public function Exporttoexcel($from, $to){
      
      $this->layout = false;
      
      $this->loadModel('Product');

      $result = $this->Product->find('all', array('conditions' => array('date(Product.modified) >=' => $from, 'date(Product.modified) <=' => $to ),
        'fields' => array('product_name','publisher_name','modified'), 'order' => array('Product.modified'), 'recursive' => -1));
      
      if(!empty($result)) {
        App::import('Vendor', 'PHPExcel/Classes/PHPExcel');

        if (!class_exists('PHPExcel')) {
            throw new CakeException('Vendor class PHPExcel not found!');
        }

        $objPHPExcel = new PHPExcel(); 
        $objPHPExcel->setActiveSheetIndex(0); 
        $rowCount = 1; 
        
        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'Product Name'); 
        // $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'Price'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, 'Publisher'); 
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, 'Modified On'); 
        $rowCount++; 

        foreach ($result as $key => $value) { 
          $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$value['Product']['product_name']); 
          // $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$value['Product']['price']); 
          $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount, $value['Product']['publisher_name']); 
          $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value['Product']['modified']); 
          $rowCount++; 
        }

        $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
        $log_report = "title-update-report-".date('dmY').".xlsx";
        header('Content-Disposition: attachment;filename="' . $log_report . '"');
        $objWriter->save("php://output"); 
      }
    }

    public function activelist(){
      $this->layout = 'admin_layout';
      $this->loadModel('Product');
      $publishers = $this->Product->find('all', array('conditions' => array('publisher_name !=' => ''),'fields' => 'DISTINCT publisher_name', 'order' => 'Product.publisher_name ASC' , 'recursive' => -1));  
      
      $this->set(compact('publishers'));
    }

    public function activelistexport(){
      $this->layout = false;
      
      $this->loadModel('Product');
      if(!empty($_GET['publisher'])){
        $result = $this->Product->find('all', array('conditions' => array('is_active' => 1, 'publisher_name LIKE' => "%".$_GET['publisher']."%"),
                                                    'fields' => array('product_name','publisher_name','modified','price'), 'order' => array('Product.modified'), 'recursive' => -1));
        
        
        
          App::import('Vendor', 'PHPExcel/Classes/PHPExcel');
          if (!class_exists('PHPExcel')) {
              throw new CakeException('Vendor class PHPExcel not found!');
          }
          
          $objPHPExcel = new PHPExcel(); 
          $objPHPExcel->setActiveSheetIndex(0); 
          $rowCount = 1; 
          
          $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,'Product Name'); 
          $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,'Price'); 
          $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, 'Publisher'); 
          // $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, 'Modified On'); 
          $rowCount++; 
          if(!empty($result)) {
            foreach ($result as $key => $value) { 
              $objPHPExcel->getActiveSheet()->SetCellValue('A'.$rowCount,$value['Product']['product_name']); 
              $objPHPExcel->getActiveSheet()->SetCellValue('B'.$rowCount,$value['Product']['price']); 
              $objPHPExcel->getActiveSheet()->SetCellValue('C'.$rowCount, $value['Product']['publisher_name']); 
              // $objPHPExcel->getActiveSheet()->SetCellValue('D'.$rowCount, $value['Product']['modified']); 
              $rowCount++; 
            }
          }
          
          $objWriter = new PHPExcel_Writer_Excel2007($objPHPExcel); 
          
          $log_report = "title-active-list-Report-".date('dmY').".xlsx";
          header('Content-Disposition: attachment;filename="' . $log_report . '"');
          $objWriter->save("php://output"); 
          die;
        
    
     
      }
    }
    
    
}
