<?php

App::uses('AppController', 'Controller');

/**
 * OurClients Controller
 *
 * @property OurClient $OurClient
 * @property PaginatorComponent $Paginator
 */
class OurClientsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';

        if ($this->request->is('post')) {
            $data = $this->request->data;
            if (empty($data['OurClient']['search_text'])) {
                $this->Session->setFlash(__('Please Fill Search Box.'), 'error');
                return $this->redirect(array('action' => 'index'));
            }
            $this->Session->write('user_filter', $data['OurClient']);
        }

        if ($this->Session->check('user_filter')) {
            $user_filt = $this->Session->read('user_filter');
        }
        $conditions = array();
        if (isset($user_filt['search_by'])) {
            if ($user_filt['search_by'] == 'client_name') {
                $conditions["OurClient." . $user_filt['search_by'] . " LIKE"] = '%' . $user_filt['search_text'] . '%';
            }
            if ($user_filt['search_by'] == 'link') {
                $conditions["OurClient." . $user_filt['search_by'] . " LIKE"] = '%' . $user_filt['search_text'] . '%';
            }
        }
        $this->Session->delete('user_filter');
        $this->paginate = array(
            'conditions' => array($conditions),
            'order' => 'OurClient.id DESC',
            'limit' => 50
        );
        $this->OurClient->recursive = 0;
        $this->set('ourClients', $this->Paginator->paginate());
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $client_search = Configure::read('idata.client_search');
        $this->set(compact('yes_no', 'client_search'));
    }

    public function active_logo($id = NULL) {
        if ($this->OurClient->updateAll(array('OurClient.is_active' => 1), array('OurClient.id' => $id))) {
            $this->Session->setFlash(__('The Client Logo has been Activated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Client Logo could not be Activated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deactive_logo($id = NULL) {
        if ($this->OurClient->updateAll(array('OurClient.is_active' => 0), array('OurClient.id' => $id))) {
            $this->Session->setFlash(__('The Client Logo has been DeActivated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Client Logo could not be DeActivated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->OurClient->exists($id)) {
            throw new NotFoundException(__('Invalid our client'));
        }
        $options = array('conditions' => array('OurClient.' . $this->OurClient->primaryKey => $id));
        $this->set('ourClient', $this->OurClient->find('first', $options));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $this->OurClient->create();
            $res = $this->OurClient->save_client_fun($this->request->data);
            if ($res == 1) {
                $this->Session->setFlash(__('The our client has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The our client could not be saved. Please, try again.'), 'error');
            }
        }
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->OurClient->exists($id)) {
            throw new NotFoundException(__('Invalid our client'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->OurClient->edit_client_info($this->request->data)) {
                $this->Session->setFlash(__('The our client has been saved.'), 'success');
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The our client could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('OurClient.' . $this->OurClient->primaryKey => $id));
            $this->request->data = $this->OurClient->find('first', $options);
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->OurClient->id = $id;
        if (!$this->OurClient->exists()) {
            throw new NotFoundException(__('Invalid our client'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->OurClient->delete()) {
            $this->Session->setFlash(__('The our client has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The our client could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'index'));
    }

}
