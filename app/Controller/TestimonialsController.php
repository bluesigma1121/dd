<?php

App::uses('AppController', 'Controller');

/**
 * Testimonials Controller
 *
 * @property Testimonial $Testimonial
 * @property PaginatorComponent $Paginator
 */
class TestimonialsController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() { 
        $this->layout = 'admin_layout';
        $this->Testimonial->recursive = 0;
        $this->set('testimonials', $this->Paginator->paginate());
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no'));
    }

    public function user_testi_index($id = null) {
        if (AuthComponent::user('id') != $id) {
            $this->Session->setFlash(__('The user has Not Found .'), 'error');
            return $this->redirect(array('action' => 'login'));
        }
        $this->Testimonial->recursive = 0;
        $this->paginate = array(
            'conditions' => array('Testimonial.user_id' => $id),
            'order' => 'Testimonial.created DESC',
            'limit' => 50
        );
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set('user_testies', $this->Paginator->paginate());
        $this->set(compact('yes_no'));
    }

    public function user_view($id = null) {
        $this->layout = null;
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $testi = $this->Testimonial->find('first', array('conditions' => array('Testimonial.id' => $id)));
        $this->set(compact('yes_no', 'testi'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid testimonial'));
        }
        $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
        $this->set('testimonial', $this->Testimonial->find('first', $options));
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $this->set(compact('yes_no'));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        $this->layout = null;
        if ($this->request->is('post')) {
            $this->request->data['Testimonial']['user_id'] = $this->Auth->user('id');
            $this->Testimonial->create();
            if ($this->Testimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The testimonial has been saved.'), 'success');
                return $this->redirect(array('action' => 'user_testi_index', $this->Auth->user('id')));
            } else {
                $this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'), 'error');
            }
        }
        $users = $this->Testimonial->User->find('list');
        $this->set(compact('users'));
    }
    /*New add method start for super_admin VG-20/12/2016*/
    public function admin_add(){
        $this->layout = 'admin_layout';

        if ($this->request->is('post')) {
            $data= $this->request->data;
            $data['Testimonial']['user_id'] = $this->Auth->user('id');
            
            $this->Testimonial->create();
            if ($this->Testimonial->save($data)) {
                $this->Session->setFlash(__('The testimonial has been saved.'), 'success');
                return $this->redirect(array('action' => 'index', $this->Auth->user('id')));
            } else {
                $this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'), 'error');
            }
        }
        $users = $this->Testimonial->User->find('list');
        $this->set(compact('users'));
    }
/*New add method End for super_admin VG-20/12/2016*/
  
    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        $this->layout = null;
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid testimonial'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $this->request->data['Testimonial']['is_active'] = 0;
            $this->request->data['Testimonial']['is_verified'] = 0;

            if ($this->Testimonial->save($this->request->data)) {
                $this->Session->setFlash(__('The testimonial has been saved.'), 'success');
                return $this->redirect(array('action' => 'user_testi_index', $this->Auth->user('id')));
            } else {
                $this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
            $this->request->data = $this->Testimonial->find('first', $options);
        }
        $users = $this->Testimonial->User->find('list');
        $this->set(compact('users'));
    }

    /*New edit method start for super_admin VG-20/12/2016*/
    public function admin_edit($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Testimonial->exists($id)) {
            throw new NotFoundException(__('Invalid testimonial'));
        }
        if ($this->request->is(array('post', 'put'))) {
            $data= $this->request->data;
            $data['Testimonial']['is_active'] = 0;
            $data['Testimonial']['is_verified'] = 0;
              
            if ($this->Testimonial->save($data)) {
                $this->Session->setFlash(__('The testimonial has been saved.'), 'success');
                return $this->redirect(array('action' => 'index', $this->Auth->user('id')));
            } else {
                $this->Session->setFlash(__('The testimonial could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Testimonial.' . $this->Testimonial->primaryKey => $id));
            $this->request->data = $this->Testimonial->find('first', $options);
        }
        $users = $this->Testimonial->User->find('list');
        $this->set(compact('users'));
    }
/*New edit method End for super_admin VG-20/12/2016*/
    public function active($id = NULL) {
        if ($this->Testimonial->updateAll(array('Testimonial.is_active' => 1, 'Testimonial.is_verified' => 1), array('Testimonial.id' => $id))) {
            $this->Session->setFlash(__('The Testimonial has been Activated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Testimonial could not be Activated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deactive($id = NULL) {
        if ($this->Testimonial->updateAll(array('Testimonial.is_active' => 0), array('Testimonial.id' => $id))) {
            $this->Session->setFlash(__('The Testimonial has been DeActivated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Testimonial could not be DeActivated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function verified($id = NULL) {
        if ($this->Testimonial->updateAll(array('Testimonial.is_verified' => 1), array('Testimonial.id' => $id))) {
            $this->Session->setFlash(__('The Testimonial has been verified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Testimonial could not be verified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deverified($id = NULL) {
        if ($this->Testimonial->updateAll(array('Testimonial.is_verified' => 0), array('Testimonial.id' => $id))) {
            $this->Session->setFlash(__('The Testimonial has been Deverified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Testimonial could not be Deverified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Testimonial->id = $id;
        if (!$this->Testimonial->exists()) {
            throw new NotFoundException(__('Invalid testimonial'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Testimonial->delete()) {
            $this->Session->setFlash(__('The testimonial has been deleted.'),'success');
        } else {
            $this->Session->setFlash(__('The testimonial could not be deleted. Please, try again.'),'error');
        }
        return $this->redirect(array('action' => 'index'));
    }



}
