<?php
App::uses('AppController', 'Controller');
/**
 * RecentViews Controller
 *
 * @property RecentView $RecentView
 * @property PaginatorComponent $Paginator
 */
class RecentViewsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->RecentView->recursive = 0;
		$this->set('recentViews', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->RecentView->exists($id)) {
			throw new NotFoundException(__('Invalid recent view'));
		}
		$options = array('conditions' => array('RecentView.' . $this->RecentView->primaryKey => $id));
		$this->set('recentView', $this->RecentView->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->RecentView->create();
			if ($this->RecentView->save($this->request->data)) {
				$this->Session->setFlash(__('The recent view has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recent view could not be saved. Please, try again.'));
			}
		}
		$users = $this->RecentView->User->find('list');
		$products = $this->RecentView->Product->find('list');
		$this->set(compact('users', 'products'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->RecentView->exists($id)) {
			throw new NotFoundException(__('Invalid recent view'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->RecentView->save($this->request->data)) {
				$this->Session->setFlash(__('The recent view has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The recent view could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('RecentView.' . $this->RecentView->primaryKey => $id));
			$this->request->data = $this->RecentView->find('first', $options);
		}
		$users = $this->RecentView->User->find('list');
		$products = $this->RecentView->Product->find('list');
		$this->set(compact('users', 'products'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->RecentView->id = $id;
		if (!$this->RecentView->exists()) {
			throw new NotFoundException(__('Invalid recent view'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->RecentView->delete()) {
			$this->Session->setFlash(__('The recent view has been deleted.'));
		} else {
			$this->Session->setFlash(__('The recent view could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
