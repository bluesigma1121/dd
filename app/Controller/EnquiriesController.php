<?php

App::uses('AppController', 'Controller');
App::uses('CakeEmail', 'Network/Email');
App::uses('Payu', 'Vendor');
App::import('Controller','Products');
App::uses('Paypal', 'Paypal.Lib');


/**
 * Enquiries Controller
 *
 * @property Enquiry $Enquiry
 * @property PaginatorComponent $Paginator
 */
class EnquiriesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator');

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->layout = 'admin_layout';
        if ($this->request->is('post')) {
            $data = $this->request->data;
            $this->Session->write('enquiry_filter', $data['Enquiry']);
        }
        if ($this->Session->check('enquiry_filter')) {
            $enquiry_filt = $this->Session->read('enquiry_filter');
        }
        $conditions = array();
        if(isset($enquiry_filt['status']) && !empty($enquiry_filt['status'])){
            $conditions["Enquiry.status"] = $enquiry_filt['status'];
        }
        if(isset($enquiry_filt['rating']) && !empty($enquiry_filt['rating'])){
            $conditions["Enquiry.rating"] = $enquiry_filt['rating'];
        }

        // condition for sales team only start VG-29-04-2017
        if (AuthComponent::user('role') == 12) {
            $conditions['Enquiry.created >='] = '2017-05-13 00:00:00';
            $conditions['Enquiry.status !=']  = 10;
            $conditions['Enquiry.rating !=']  = 3;
        }
        // condition for sales team only end VG-29-04-2017
        //Filter by product name start VG-10/05/2017
        if (isset($enquiry_filt['search_text']) && !empty($enquiry_filt['search_text'])) {
            $conditions["Product.product_name  LIKE"] = '%' . $enquiry_filt['search_text'] . '%';
        }
        //Filter by product name end VG-10/05/2017

       // if (isset($enquiry_filt['created']) && !empty($enquiry_filt['created'])) {
       //     $conditions["date(Enquiry.created) ="] = implode('-',$enquiry_filt['created']) ;
      //  }

        // Ignore Created Date Filter on search start VG-12/02/2018
        if (isset($_POST['isdatefilter']) && !empty($_POST['isdatefilter'])) {
          if (isset($enquiry_filt['created']) && !empty($enquiry_filt['created'])) {
              $conditions["date(Enquiry.created) ="] = implode('-',$enquiry_filt['created']) ;
          }
        }
        // Ignore Created Date Filter on search end VG-12/02/2018

        // Do not show buy now on search start VG-12/02/2018
        $conditions['Enquiry.subject !='] = 'Transaction Attempted';
        // Do not show buy now on search end VG-12/02/2018


        $this->Session->delete('enquiry_filter');
        $this->paginate = array(
            'conditions' => array($conditions),
            'fields' => array('User.id', 'User.first_name', 'User.last_name', 'User.email', 'User.organisation', 'User.job_title', 'Product.product_name','User.country',
                'Enquiry.id', 'Enquiry.ref_page', 'Enquiry.status', 'Enquiry.rating', 'Enquiry.remark', 'Enquiry.message','Enquiry.created','Product.publisher_name'),
            'order' => 'Enquiry.id DESC',
            'limit' => 50
        );//message filed added VG-28-04-2017

        $this->Enquiry->recursive = 0;
        $enquiries = $this->Paginator->paginate();
        //debug($enquiries);
        $this->set(compact('enquiries'));
        Configure::load('idata');
        $enquiry_search = Configure::read('idata.enquiry_search');
        $product = Configure::read('idata.product_search'); //for search with product name VG-10-05-2017
        $rating = Configure::read('idata.lead_rating');
        $status = Configure::read('idata.lead_status');
        $isdtfilter = Configure::read('idata.isdtfilter'); // added 12-2-2018
        // Code for enquiry status summary start VG-20-05-2017
        $sample = $this->Enquiry->find('count', ['conditions' => ['Enquiry.status' => '3'] ]  );
        $mailrequest = $this->Enquiry->find('count', ['conditions' => ['Enquiry.status' => '9'] ]  );
        $nostatus = $this->Enquiry->find('count', ['conditions' => ['Enquiry.status' => '0'] ]  );
        // Code for enquiry status summary end VG-20-05-2017

         if (isset($_POST['isdatefilter']) && !empty($_POST['isdatefilter'])) {
          $isdtfilter = true;
        }

        $this->set(compact('enquiry_search', 'rating', 'status','products','sample','mailrequest','nostatus','isdtfilter'));
    }

    public function user_enquiry_index($id = null) {
        if (AuthComponent::user('id') != $id) {
            $this->Session->setFlash(__('The user has Not Found .'), 'error');
            return $this->redirect(array('action' => 'login'));
        }
        $this->Enquiry->recursive = 0;
        $this->paginate = array(
            'conditions' => array('Enquiry.user_id' => $id),
            'order' => 'Enquiry.created DESC',
            'limit' => 50
        );
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');

        $this->set('user_enquirys', $this->Paginator->paginate());
        $this->set(compact('yes_no'));
    }

    public function user_enquiry_view($id = null) {
        $this->layout = null;
        Configure::load('idata');
        $yes_no = Configure::read('idata.yes_no');
        $enquery = $this->Enquiry->find('first', array('conditions' => array('Enquiry.id' => $id)));
        $this->set(compact('yes_no', 'enquery'));
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Enquiry->exists($id)) {
            throw new NotFoundException(__('Invalid enquiry'));
        }
        $options = array('conditions' => array('Enquiry.' . $this->Enquiry->primaryKey => $id));
        $this->set('enquiry', $this->Enquiry->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Enquiry->create();
            if ($this->Enquiry->save($this->request->data)) {
                $this->Session->setFlash(__('The enquiry has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The enquiry could not be saved. Please, try again.'));
            }
        }
        $users = $this->Enquiry->User->find('list');
        $categories = $this->Enquiry->Category->find('list');
        $this->set(compact('users', 'categories'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Enquiry->exists($id)) {
            throw new NotFoundException(__('Invalid enquiry'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Enquiry->save($this->request->data)) {
                $this->Session->setFlash(__('The enquiry has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The enquiry could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Enquiry.' . $this->Enquiry->primaryKey => $id));
            $this->request->data = $this->Enquiry->find('first', $options);
        }
        $users = $this->Enquiry->User->find('list');
        $categories = $this->Enquiry->Category->find('list');
        $this->set(compact('users', 'categories'));
    }

    public function edit_lead($id = null) {
        $this->layout = 'admin_layout';
        if (!$this->Enquiry->exists($id)) {
            throw new NotFoundException(__('Invalid enquiry'));
        }
        if ($this->request->is(array('post', 'put'))) {

            $prev_data = $this->Enquiry->find('first', array(
                'conditions' => array('Enquiry.id' => $id),
                'fields' => array('Enquiry.remark'), 'recursive' => 1));

            $data['Enquiry']['id'] = $this->request->data['Enquiry']['id'];
            $data['Enquiry']['rating'] = $this->request->data['Enquiry']['rating'];
            $data['Enquiry']['status'] = $this->request->data['Enquiry']['status'];
            $cnt = 1;
            $temp_array = array();
            if (!empty($prev_data['Enquiry']['remark'])) {
                $temp_array = json_decode($prev_data['Enquiry']['remark'], true);
                $cnt = count($temp_array);
                $cnt++;
                $date = Date('d-m-Y H:i:s');
                $new_array['date'] = $date;
                $new_array['message'] = $this->request->data['Enquiry']['new_remark'];
                $new_array['status'] = $this->request->data['Enquiry']['status'];
                $new_array['rating'] = $this->request->data['Enquiry']['rating'];
                array_push($temp_array, $new_array);
                $data['Enquiry']['remark'] = json_encode($temp_array);
            } else {
                $date = Date('d-m-Y H:i:s');
                $new_array['date'] = $date;
                $new_array['message'] = $this->request->data['Enquiry']['new_remark'];
                $new_array['status'] = $this->request->data['Enquiry']['status'];
                $new_array['rating'] = $this->request->data['Enquiry']['rating'];
                array_push($temp_array, $new_array);
                $data['Enquiry']['remark'] = json_encode($temp_array);
                //  debug(Date('D-M-Y h:i a', strtotime($date)));
            }
            $data['Enquiry']['next_followup_date'] = $this->request->data['Enquiry']['next_followup_date'];

            if ($this->Enquiry->save($data)) {
                $this->Session->setFlash(__('The enquiry has been saved.'), 'success');
                return $this->redirect(array('action' => 'edit_lead', $id));
            } else {
                $this->Session->setFlash(__('The enquiry could not be saved. Please, try again.'), 'error');
            }
        } else {
            $options = array('conditions' => array('Enquiry.' . $this->Enquiry->primaryKey => $id));
            $this->request->data = $this->Enquiry->find('first', $options);
            $prev_rem = array();
            $prev_rem = json_decode($this->request->data['Enquiry']['remark'], true);
        }
        Configure::load('idata');
        $rating = Configure::read('idata.lead_rating');
        $status = Configure::read('idata.lead_status');
        $users = $this->Enquiry->User->find('list');
        $categories = $this->Enquiry->Category->find('list');
        $this->set(compact('users', 'categories', 'status', 'rating', 'prev_rem'));
    }

    //same as send_enquiry function renamed for popup and getting lead from multiple resources
    public function get_lead_info_form($id = null, $ref_page = null, $main_cat=null) {
        $ex_ref_page = $ref_page;
        Configure::load('idata');
        $title = Configure::read('idata.title');
        $ref_page_arr = Configure::read('idata.ref_page');

        $this->loadModel('Product');
        $pro_name = $this->Product->find('first', array(
            'conditions' => array('Product.id' => $id),
            'fields' => array('Product.id', 'Product.product_name', 'tags', 'price', 'corporate_price','dollar_price','slug','publisher_name'), 'recursive' => -1
                )
        );


        if ($ref_page == 'buy-now') {
        	$licence_type = Configure::read('idata.licence_type');
        	
	        if((empty($pro_name['Product']['tags']) || $pro_name['Product']['tags']<=0)){
				$licence_type[1] = $licence_type[1] . ': $ ' . $pro_name['Product']['price'];
				$licence_type[2] = $licence_type[2] . ': $ ' . $pro_name['Product']['corporate_price'];
				if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
					$licence_type[3] = $licence_type[3] . ': $ ' . $pro_name['Product']['corporate_price'];
				}else{
					$licence_type[3] = $licence_type[3] . ': $ ' . $pro_name['Product']['dollar_price'];
				}
			}else{
				$licence_type[1] = $licence_type[1] . ': $ <strike>' . $pro_name['Product']['price'].'</strike> $ '.number_format($pro_name['Product']['price'] - ($pro_name['Product']['price'] *$pro_name['Product']['tags'] / 100),2,".","");
				$licence_type[2] = $licence_type[2] . ': $ <strike>' . $pro_name['Product']['corporate_price'] .'</strike> $ '.number_format($pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] *$pro_name['Product']['tags'] / 100),2,".","");
				if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
					$licence_type[3] = $licence_type[3] . ': $ <strike>' . $pro_name['Product']['corporate_price'].'</strike> $ '.number_format($pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] *$pro_name['Product']['tags'] / 100),2,".","");
				}else{
					$licence_type[3] = $licence_type[3] . ': $ <strike>' . $pro_name['Product']['dollar_price'].'</strike> $ '.number_format($pro_name['Product']['dollar_price'] - ($pro_name['Product']['dollar_price'] *$pro_name['Product']['tags'] / 100),2,".","");
				}
			}

            $this->set(compact('licence_type', 'ex_ref_page'));
        }

        $ref_page = $ref_page_arr[$ref_page];
        $title = Configure::read('idata.title');
        $this->set(compact('pro_name', 'ref_page', 'title'));

        // code starts here-VG-06/08/2016
         $this->loadModel('Category');
         $this->loadModel('Product');
         $this->loadModel('ProductCategory');

         $mapped_category_id=$this->ProductCategory->find('all',array('conditions'=>array('ProductCategory.product_id'=>$id))); // fetched category_id from ProductCategory model from product_id
         if(count($mapped_category_id)==1 && $mapped_category_id[0]['Category']['parent_category_id']==2){
            $related_category_name = $mapped_category_id; // Here condition checked that if fetched result count is one and parent_category_id is equals 2 then required data is copied in $related_category_name
         }
         else{
            if(!empty($mapped_category_id)){
              $related_category_name=$this->Category->find('first',array('conditions'=>array('Category.id'=>$mapped_category_id[0]['Category']['id']))); // Here required data is fetched from Cateogry model with category id.              
            }
         }
        //Code starts here to find category slug. VG-06/10/2016
         $cat_id=$this->Product->find('all',array('conditions'=>array('Product.id'=>$id)));
         $cid=$cat_id[0]['Product']['category_id'];
         $cat=$this->Category->find('first',array('conditions'=>array('Category.id'=>$cid),'fields'=>array('slug')));
         $cat_main = $cat['Category']['slug'];//Code added here-VG-11/08/2016 for getting main category.
        //Code starts here to find category slug. VG-06/10/2016

         //$cat_main = $this->Session->read('main-category'); //Code added here-VG-06/09/2016 for getting main category through session
         $this->set(compact('related_category_name','cat_main'));//$cat_main added  here-VG-11/08/2016





        /* $_POST = array(
          'mihpayid' => '403993715514035241',
          'mode' => 'CC',
          'status' => 'failure',
          'unmappedstatus' => 'captured',
          'key' => 'UCcgQy',
          'txnid' => 'XxksjwyEVta',
          'amount' => '3100.00',
          'cardCategory' => 'domestic',
          'discount' => '0.00',
          'net_amount_debit' => '3100',
          'addedon' => '2016-01-29 03:37:38',
          'productinfo' => 'Global Biopesticides Market Research Report - Industry Analysis  Size  Share  Growth  Trends and For',
          'firstname' => 'Super',
          'lastname' => 'Admin',
          'address1' => '',
          'address2' => '',
          'city' => 'Nashik',
          'state' => 'Maharashtra',
          'country' => 'India',
          'zipcode' => '441904',
          'email' => 'mmd@yopmail.com',
          'phone' => '09680529244',
          'udf1' => '1',
          'udf2' => 'Aquil',
          'udf3' => 'TL',
          'udf4' => '251',
          'udf5' => '103',
          'udf6' => '',
          'udf7' => '',
          'udf8' => '',
          'udf9' => '',
          'udf10' => '',
          'hash' => '689922da60aa39c8713c9c9012777dc74d548d6ff2b1bcb6aab22dcc6aa7f2296f022536c7eeaffe661300ab7834d4a031167bb6d516b93e7edc96156b7a8152',
          'field1' => '602957761063',
          'field2' => '999999',
          'field3' => '6709136380360291',
          'field4' => '-1',
          'field5' => '',
          'field6' => '',
          'field7' => '',
          'field8' => '',
          'field9' => 'failure',
          'payment_source' => 'payu',
          'PG_TYPE' => 'HDFCPG',
          'bank_ref_num' => '6709136380360291',
          'bankcode' => 'CC',
          'error' => 'E000',
          'error_Message' => 'No Error',
          'name_on_card' => 'test',
          'cardnum' => '512345XXXXXX2346',
          'cardhash' => 'This field is no longer supported in postback params.'
          ); */
        if ($this->request->is('post')) {

            if (isset($_POST['field9']) && $_POST['field9'] == "SUCCESS") {
                $transactions['Transaction']['user_id'] = $_POST['udf4'];
                $transactions['Transaction']['first_name'] = $_POST['firstname'];
                $transactions['Transaction']['last_name'] = $_POST['lastname'];
                $transactions['Transaction']['txnid'] = $_POST['txnid'];
                $transactions['Transaction']['address'] = $_POST['address1'];
                $transactions['Transaction']['city'] = $_POST['city'];
                $transactions['Transaction']['state'] = $_POST['state'];
                $transactions['Transaction']['country'] = $_POST['country'];
                $transactions['Transaction']['pincode'] = $_POST['zipcode'];
                $transactions['Transaction']['email'] = $_POST['email'];
                $transactions['Transaction']['productinfo'] = $_POST['productinfo'];
                $transactions['Transaction']['product_id'] = $_POST['udf4'];
                $transactions['Transaction']['organisation'] = $_POST['udf2'];
                $transactions['Transaction']['job_title'] = $_POST['udf3'];
                $transactions['Transaction']['licence_type'] = $_POST['udf1'];
                $transactions['Transaction']['amount'] = $_POST['amount'];

                $this->loadModel('Transaction');

                // ******************************
                if ($this->Transaction->save($transactions)) {
// ******************
                    $email = new CakeEmail();
                    $email->config('order_placed_hdfc');
                    $email->to($transactions['Transaction']['email']);
                    $email->viewVars(compact('transactions'));
                    $email->subject('Order Update Decision Databases');
                    //$email->attachments(array($form_name => $form_path));
                    $email->send();
//************************
                }
// ***************************

                $enq_data['Enquiry']['product_id'] = $_POST['udf4'];
                $enq_data['Enquiry']['user_id'] = $_POST['udf5'];
                $enq_data['Enquiry']['subject'] = "Transaction Successful";
                $enq_data['Enquiry']['message'] = "REASON FOR TRANSACTION SUCCESSFUL: " . $_POST['field9'] . " PRODUCT NAME: " . $_POST['productinfo'];
                $enq_data['Enquiry']['mobile'] = $_POST['phone'];
                $enq_data['Enquiry']['city'] = $_POST['city'];
                $enq_data['Enquiry']['state'] = $_POST['state'];
                $enq_data['Enquiry']['country'] = $_POST['country'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = "Buy Now Transaction Successful";
                $product_name = $_POST['productinfo'];

                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);

                    //Copy variable
                    $data['User'] = $transactions['Transaction'];
                    $data['User']['mobile'] = $_POST['phone'];
                    if (!empty($mail_to['Setting']['mail_to'])) {
// *******************
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_txn_success');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->viewVars(compact('enq_data', 'data', 'product_name', 'enq_id'));
                        $email_admin->subject('New Lead Recieved : Transaction Success');
                        $email_admin->send();
//***********************
                    }
                }
                $this->redirect(array('controller' => 'users', 'action' => 'payment_success'));
            } else if (isset($_POST['status']) && $_POST['status'] == 'failure') { // IF Transaction fail
                $data['User']['first_name'] = $_POST['firstname'];
                $data['User']['last_name'] = $_POST['lastname'];
                $data['User']['email'] = $_POST['email'];
                $data['User']['organisation'] = $_POST['udf2'];
                $data['User']['job_title'] = $_POST['udf3'];
                $data['User']['city'] = $_POST['city'];
                $data['User']['pin_code'] = $_POST['zipcode'];
                $data['User']['state'] = $_POST['state'];
                $data['User']['country'] = $_POST['country'];
                $data['User']['mobile'] = $_POST['phone'];
                $data['User']['address'] = $_POST['address1'];

                //Failed mail and capture
                $enq_data['Enquiry']['product_id'] = $_POST['udf4'];
                $enq_data['Enquiry']['user_id'] = $_POST['udf5'];
                $enq_data['Enquiry']['subject'] = "Transaction failed";
                $enq_data['Enquiry']['message'] = "REASON FOR TRANSACTION FAIL: " . $_POST['field9'] . "<br/>Product name: " . $_POST['productinfo'];
                $enq_data['Enquiry']['mobile'] = $_POST['phone'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = "Buy Now Transaction Fail";
                $product_name = $_POST['productinfo'];

                $this->loadModel('Enquiry');
                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);

                    if (!empty($mail_to['Setting']['mail_to'])) {
// ****************************
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_txn_fail');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->viewVars(compact('enq_data', 'data', 'product_name', 'enq_id'));
                        $email_admin->subject('New Lead Recieved : Transaction failed');
                        $email_admin->send();
// ****************************
                    }
                }
                $this->redirect(array('controller' => 'users', 'action' => 'payment_failed'));
            } else {
                //Actual post functionality
                if ($ref_page == 'Buy Now') {
                    if ($this->request->data['Enquiry']['term'] == 0) {
                        $this->Session->setFlash(__('Please read & accept terms and conditions.'), 'error');
                        return false;
                    }
                }

              // New Google Captcha code VG-26-2-2018
                // $secretKey = "6LekeEYUAAAAAD7XUUSIByNUfH2dGBJZMjfWBJnn";
                $secretKey = "6LdfhkYUAAAAAMHvLB1siLfv7aHCFmQJoVnwCzSp";

                $responseKey = $_POST['g-recaptcha-response'];
                $userIP = $_SERVER['REMOTE_ADDR'];

                $url = "https://www.google.com/recaptcha/api/siteverify?secret=$secretKey&response=$responseKey&remoteip=$userIP";
                $response = file_get_contents($url);
                $response = json_decode($response);

                if (!$response->success){
                    $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                    return $this->redirect($this->referer());
                }
                // New Google Captcha code VG 26-2-2018

              /*  if ($this->Session->read('captcha_code') != $this->request->data['Enquiry']['captcha']) {
                    $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                    return $this->redirect($this->referer());
                } */
                $validate = true;
                if (!$this->Auth->user('id')) {
                    if (empty($this->request->data['Enquiry']['mobile']) || empty($this->request->data['Enquiry']['email']) ||
                            empty($this->request->data['Enquiry']['first_name']) || empty($this->request->data['Enquiry']['last_name'])) {
                        $this->Session->setFlash(__('Please Fill Form '), 'error');
                        $validate = false;
                        // $this->redirect($this->referer());
                    }
                } else {
                    if (empty($this->request->data['Enquiry']['mobile']) ||
                            empty($this->request->data['Enquiry']['first_name']) || empty($this->request->data['Enquiry']['last_name'])) {
                        $this->Session->setFlash(__('Please Fill Form '), 'error');
                        $validate = false;
                        // $this->redirect($this->referer());
                    }
                }

                // New Code To Filter the Textarea from badwords VG 11-05-2020
                if(!empty($this->request->data['Enquiry']['message'])) {
                    $str = $this->request->data['Enquiry']['message'];
                    $bad_words = array('adult', 'porn', 'ass', 'sex');
                    $reg = '~\b' . implode('\b|\b', $bad_words) . '\b~';
    
                    preg_match_all($reg, preg_replace('~[.,?!]~', '', $str), $matches);
    
                    if(count($matches[0]) > 0) {
                        $this->Session->setFlash(__('Bad Words in Message '), 'error');
                        $validate = false;
                        // $this->redirect($this->referer());
                    }
                    $string = $this->request->data['Enquiry']['message'];
                    if(preg_match('/(http|ftp|mailto|https)/', $string, $matches)) {
                        $this->Session->setFlash(__('URL not allowed in Message '), 'error');
                        $validate = false;
                        // $this->redirect($this->referer());
                    }
                }

                // New Code To Filter the Textarea from badwords VG 11-05-2020

                // New Code to Keep Textbox values VG 21/5/2020
                if($validate == true) {

                    //PAYU URL Selection
                    if ($_SERVER['HTTP_HOST'] == "www.decisiondatabases.com") {
                        $data['User']['key'] = 'lFmqOb';
                        $data['User']['salt'] = 'UKG7x5qd';
                    } else {
                        $data['User']['key'] = 'UCcgQy';
                        $data['User']['salt'] = 'QAX6IZ0Q';
                    }

                    $base_url = Router::url('/', true);
                    $fail_url = $base_url . 'cart_items/add_to_cart';
                    $success_url = $base_url . 'users/payment_success';

                    if (AuthComponent::user()) {
                        $this->request->data['Enquiry']['email'] = AuthComponent::user('email');
                    }
                    //Check if user exist
                    $user_details = $this->Enquiry->User->find('first', array('conditions' => array('User.email' => $this->request->data['Enquiry']['email']), 'recursive' => -1));

                    //Whatever is the user status create backup data for future reference ALL FEILDS REQUIRED FOR USER TABLE


                    if($ref_page == 'Buy Now') //If page is buy-now then show below fields on page    VG-14/2/2017
                    {
                        $data['User']['title'] = $this->request->data['Enquiry']['title'];
                        $data['User']['address'] = $this->request->data['Enquiry']['address'];
                        $data['User']['city'] = $this->request->data['Enquiry']['city'];
                        $data['User']['state'] = $this->request->data['Enquiry']['state'];
                        $data['User']['pin_code'] = $this->request->data['Enquiry']['pin_code'];
                    }
                    // $data['User']['title'] = $this->request->data['Enquiry']['title'];
                    $data['User']['role'] = 11;
                    $data['User']['first_name'] = $this->request->data['Enquiry']['first_name'];
                    $data['User']['last_name'] = $this->request->data['Enquiry']['last_name'];
                    $data['User']['email'] = $this->request->data['Enquiry']['email'];
                    $data['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $data['User']['job_title'] = $this->request->data['Enquiry']['job_title'];
                    $data['User']['organisation'] = $this->request->data['Enquiry']['organisation'];
                    // $data['User']['address'] = $this->request->data['Enquiry']['address'];
                    // $data['User']['city'] = $this->request->data['Enquiry']['city'];
                    // $data['User']['state'] = $this->request->data['Enquiry']['state'];
                    $data['User']['country'] = $this->request->data['Enquiry']['country'];
                    //  $data['User']['pin_code'] = $this->request->data['Enquiry']['pin_code'];
                    $data['User']['visited_ip'] = $this->request->clientIp();
                    // ----------start validation http.com 11/6/2021----
                    //   echo "<h1>Hello</h1>";
                    //   exit;
                    $stringf =  $this->request->data['Enquiry']['first_name'];
                    if(preg_match('/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', $stringf, $matches)) { 
                    return false;}
                    $stringl =  $this->request->data['Enquiry']['last_name'];
                    if(preg_match('/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', $stringl, $matches)) { 
                    return false;}
                    $stringm =  $this->request->data['Enquiry']['mobile'];
                    if(preg_match('/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', $stringm, $matches)) { 
                    return false;}
                    $stringd =  $this->request->data['Enquiry']['job_title'];
                    if(preg_match('/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?/', $stringd, $matches)) { 
                    return false;}
                    //-----------end validation http.com 11/6/2021------

                    //If User Does Not Exist
                    if (empty($user_details)) {
                        $v_code = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789', 5)), 0, 5);
                        $data['User']['password'] = $v_code;

                        if ($this->Enquiry->User->save($data)) {
                            /* $email = new CakeEmail();
                            $email->config('user_registration_enquiery');
                            $email->to($data['User']['email']);
                            $email->viewVars(compact('data', 'v_code'));
                            $email->subject('Online Registration on Decision Databases');
                            $email->send(); */
                            $data['User']['id'] = $this->Enquiry->User->id;
                        }

                        //If user is not registered and page is not buy now

                        if ($ref_page != 'Buy Now') {
                            $enq_data['Enquiry']['product_id'] = $id;
                            $enq_data['Enquiry']['user_id'] = $data['User']['id'];
                            if ($ref_page == 'Buy Now') // if page is buy-now then only show below feild on page   VG-14/2/2017
                            {
                            // $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                        // Default value set to subject and message
                            $enq_data['Enquiry']['subject'] = "NA";
                            $enq_data['Enquiry']['message'] = "NA";
                            }
                            else
                            {
                            $enq_data['Enquiry']['subject'] = "Enquiry from ". $ref_page;
                            $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                            }
                        //  $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                            // $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                            $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                            $enq_data['Enquiry']['organisation'] = $this->request->data['Enquiry']['organisation'];
                            $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                            $enq_data['Enquiry']['ref_page'] = $ref_page;

                            $this->Enquiry->create();
                            if ($this->Enquiry->save($enq_data)) {
                                $enq_id = $this->Enquiry->id;
                                //send enquiry acknowledgement to customer
                                // ***********************
                                $email = new CakeEmail();
                                $email->config('user_enquiery');
                                $email->to($data['User']['email']);
                                $email->viewVars(compact('data', 'pro_name'));
                                $email->subject('Regarding your inquiry for '.$pro_name['Product']['product_name']);
                                $email->send();
                                // ****************************
                            /*IF USER IS NOT REGISTERED SEND SAMPLE ENQUIRY MAIL*/
                                /*Code for new sample enquiry mail Start VG-31/12/2016*/
                                if($ref_page=='Download Sample')
                                {
                                    // *****************************
                                $email = new CakeEmail();
                                    $email->config('sample_request');
                                    $email->to('sample@decisiondatabases.com'); //sample@decisiondatabases.com
                                    $email->viewVars(compact('pro_name','data'));
                                    $email->subject('New Sample Request:'. $pro_name['Product']['product_name']);
                                    $email->send();
                                    // ********************************
                                }
                            /*Code for new sample enquiry mail End VG-31/12/2016*/


                                $this->loadModel('Setting');
                                $mail_to = $this->Setting->find_setting(1);
                                if (!empty($mail_to['Setting']['mail_to'])) {
                                    // *******************************
                                    $email_admin = new CakeEmail();
                                    $email_admin->config('user_enquiery_received');
                                    $email_admin->to($mail_to['Setting']['mail_to']);
                                    $email_admin->viewVars(compact('data', 'pro_name', 'category', 'enq_id', 'enq_data'));
                                    //$email_admin->subject($ref_page." : ".$pro_name['Product']['product_name']);
                                    $email_admin->subject($ref_page." : ".$pro_name['Product']['product_name']);
                                    $email_admin->send();
                                    // ******************************
                                }
                                // **************************
                                if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                                    $cc_ary = explode(',', $mail_to['Setting']['emails']);
                                    $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                                    $email_cc = new CakeEmail();
                                    foreach ($cc_ary as $ekey => $email_id):
                                        $email_cc->config('user_enquiery_received_cc');
                                        $email_cc->to($email_id);
                                        $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                                        $email_cc->subject($ref_page." : ".$pro_name['Product']['product_name']);
                                        $email_cc->send();
                                    endforeach;
                                }
                                    // ******************************
                                $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                                $this->Session->write('thanks', 1);
                                return $this->redirect(array('controller' => 'users', 'action' => 'thanks'));
                            }
                        } else {

                            //If buy now
                            $data['User']['txnid'] = substr(str_shuffle('abcefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'), 5, 11);
                            $data['User']['firstname'] = $data['User']['first_name'] . " " . $data['User']['last_name'];
                            $data['User']['surl'] = $success_url;
                            $data['User']['furl'] = $fail_url;
                            $data['User']['productinfo'] = $pro_name['Product']['product_name'];



                            if((empty($pro_name['Product']['tags']) || $pro_name['Product']['tags']<=0)){
                                if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                    $data['User']['amount'] = $pro_name['Product']['price'];
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                    $data['User']['amount'] = $pro_name['Product']['corporate_price'];
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                    if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                        $data['User']['amount'] = $pro_name['Product']['corporate_price'];
                                    }else{
                                        $data['User']['amount'] = $pro_name['Product']['dollar_price'];
                                    }
                                }
                            }else{
                                if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                    $data['User']['amount'] = $pro_name['Product']['price'] - ($pro_name['Product']['price'] * $pro_name['Product']['tags'] / 100);
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                    $data['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                    if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                        $data['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                    }else{
                                        $data['User']['amount'] = $pro_name['Product']['dollar_price'] - ($pro_name['Product']['dollar_price'] * $pro_name['Product']['tags'] / 100);
                                    }
                                }
                            }

                            $data_required['key'] = $data['User']['key'];
                            $data_required['txnid'] = $data['User']['txnid'];
                            $data_required['amount'] = $data['User']['amount'];
                            $data_required['firstname'] = $data['User']['first_name'];
                            $data_required['lastname'] = $data['User']['last_name'];

                            $data_required['email'] = $data['User']['email'];
                            $data_required['phone'] = $data['User']['mobile'];
                            $data_required['productinfo'] = $data['User']['productinfo'];
                            $data_required['surl'] = $data['User']['surl'];
                            $data_required['furl'] = $data['User']['furl'];

                            $data_required['udf1'] = $this->request->data['Enquiry']['licence_type'];
                            $data_required['udf2'] = $data['User']['organisation'];
                            $data_required['udf3'] = $data['User']['job_title'];
                            $data_required['udf4'] = $pro_name['Product']['id'];
                            $data_required['udf5'] = $data['User']['id'];

                            $data_required['city'] = $data['User']['city'];
                            $data_required['state'] = $data['User']['state'];
                            $data_required['zipcode'] = $data['User']['pin_code'];
                            $data_required['country'] = $data['User']['country'];

                            //Attempt to email notification
                            $enq_data['Enquiry']['product_id'] = $pro_name['Product']['id'];
                            $enq_data['Enquiry']['user_id'] = $data['User']['id'];
                            $enq_data['Enquiry']['subject'] = "Transaction Attempted";
                            $enq_data['Enquiry']['message'] = "This User tried to buy " . $pro_name['Product']['product_name'] . " this product.";
                            $enq_data['Enquiry']['mobile'] = $data_required['phone'];
                            $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                            $enq_data['Enquiry']['ref_page'] = "Buy Now Attempt Has Been Done.";
                            $product_name = $pro_name['Product']['product_name'];

                            $this->Enquiry->create();
                            if ($this->Enquiry->save($enq_data)) {
                                $enq_id = $this->Enquiry->id;
                                $this->loadModel('Setting');
                                $mail_to = $this->Setting->find_setting(1);

                                if (!empty($mail_to['Setting']['mail_to'])) {
                                    // *****************************
                                    $email_admin = new CakeEmail();
                                    $email_admin->config('user_enquiery_txn_success');
                                    $email_admin->to($mail_to['Setting']['mail_to']);
                                    $email_admin->viewVars(compact('enq_data', 'data', 'product_name', 'enq_id'));
                                    $email_admin->subject('New Lead Recieved : Transaction Attempted');
                                    $email_admin->send();
                                    // *****************************
                                }
                            }
                            //END ENQUIRY DATA
                            //debug($data_required);
                        // $this->Payu = new Payu;
                        // $this->Payu->pay_page($data_required, $data['User']['salt']);
                        // die;

                            /* Payment options code start here VG-31/03/2017 */
                        if (!empty($this->request->data['payment_option']) && $this->request->data['payment_option'] == 'paypal')
                        {
                                if((empty($pro_name['Product']['tags']) || $pro_name['Product']['tags']<=0)){
                                    if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                        $amt['User']['amount'] = $pro_name['Product']['price'];
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                        $amt['User']['amount'] = $pro_name['Product']['corporate_price'];
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                        if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                            $amt['User']['amount'] = $pro_name['Product']['corporate_price'];
                                        }else{
                                            $amt['User']['amount'] = $pro_name['Product']['dollar_price'];
                                        }
                                    }
                                }else{
                                    if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                        $amt['User']['amount'] = $pro_name['Product']['price'] - ($pro_name['Product']['price'] * $pro_name['Product']['tags'] / 100);
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                        $amt['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                        if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                            $amt['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                        }else{
                                            $amt['User']['amount'] = $pro_name['Product']['dollar_price'] - ($pro_name['Product']['dollar_price'] * $pro_name['Product']['tags'] / 100);
                                        }
                                    }
                                }

                            $user=$this->request->data['Enquiry'];

                            if($_SERVER['HTTP_HOST'] == "www.decisiondatabases.com")
                            {
                            $this->Paypal = new Paypal(array(
                                'sandboxMode' => false,
                                'nvpUsername' => 'admin_api1.decisiondatabases.com',
                                'nvpPassword' => '95NQFYL48UGNYUJV',
                                'nvpSignature' => 'Ajwath3vWTxZhV-dji.0VNOXCfYMAPmQ-pzAeAM72MLSRQaFx4.3-jHm',
                                ));
                            }else{
                            $this->Paypal = new Paypal(array(
                                'sandboxMode' => true,
                                'nvpUsername' => 'skshrikant510-business-us_api1.gmail.com',
                                'nvpPassword' => '3PLDLETB2E7WJUW7',
                                'nvpSignature' => 'AQJCtaN7Xv31h0261ixZd2P3S1EWAk42FICNBZwgEPYXFRvwY9gl0c3G',
                                ));
                            }
                            $base_url = Router::url('/', true);

                            $cancel_url = $base_url.'payresponse';
                            $return_url = $base_url.'payresponse';


                            $order['description'] = 'Your purchase with Decision Databases';
                            $order['currency'] = 'USD';
                            $order['return'] = $return_url;
                            $order['cancel'] = $cancel_url;
                            $order['custom'] = 'test';
                            $order['shipping'] = 0.00;
                            $order['first_name'] = $user['first_name'];
                            $order['last_name'] = $user['last_name'];
                            $order['address1'] = $user['address'];
                            $order['address2'] = $user['address'];
                            $order['city'] = $user['city'];
                            $order['state'] = $user['state'];
                            $order['zip'] = $user['pin_code'];
                            $order['email'] = $user['email'];
                            $order['mobile'] = $user['mobile'];
                            $order['items'][0]['name'] = $_POST['pro_name'];
                            $order['items'][0]['description'] = $_POST['pro_name'];
                            $order['items'][0]['tax'] = 0.00;
                            $order['items'][0]['subtotal'] = round($amt['User']['amount'], 2);
                            $order['items'][0]['qty'] = 1;
                            $order['items'][0]['product_id'] = 1;
                            $order['items'][0]['licence_type'] = $user['licence_type'];

                            // $this->Session->write('order_session', $order);
                            $res = $this->Paypal->setExpressCheckout($order);
                            //debug($order);
                            // debug($res);
                            //die;
                            $this->redirect($res);

                            // $this->Payu = new Payu;
                            // $this->Payu->pay_page($data_required, $data['User']['salt']);
                            die;
                        }

                        if (!empty($this->request->data['payment_option']) && $this->request->data['payment_option'] == 'ccavenue')
                        {
                            //$this->layout = false;
                            if((empty($pro_name['Product']['tags']) || $pro_name['Product']['tags']<=0)){
                                    if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                        $amt['User']['amount'] = $pro_name['Product']['price'];
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                        $amt['User']['amount'] = $pro_name['Product']['corporate_price'];
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                        if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                            $amt['User']['amount'] = $pro_name['Product']['corporate_price'];
                                        }else{
                                            $amt['User']['amount'] = $pro_name['Product']['dollar_price'];
                                        }
                                    }
                                }else{	
                                    if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                        $amt['User']['amount'] = $pro_name['Product']['price'] - ($pro_name['Product']['price'] * $pro_name['Product']['tags'] / 100);
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                        $amt['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                        if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                            $amt['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                        }else{
                                            $amt['User']['amount'] = $pro_name['Product']['dollar_price'] - ($pro_name['Product']['dollar_price'] * $pro_name['Product']['tags'] / 100);
                                        }
                                    }
                                }

                                $base_url = Router::url('/', true);
                                // $url =  Router::url(array('controller' => 'enquiries', 'action' => 'ccavresponsehandler'));
                                if($_SERVER['HTTP_HOST'] == "www.decisiondatabases.com")
                                    {
                                    $url = 'http://www.decisiondatabases.com/ccavresponse';
                                    }
                                    else {
                                    $url = 'http://test.decisiondatabases.com/ccavresponse';
                                    }
                                $tid=substr(str_shuffle('0123456789'), 1, 5).date('his');
                                $oid=substr(str_shuffle('123456789'), 1, 3).date('his');
                                $user=$this->request->data['Enquiry'];
                                $name = $user['first_name']." ".$user['last_name'];
                                $data = [
                                            'merchant_id'=>'79131',
                                            'order_id'=>$oid,
                                            'tid'=>$tid,
                                            'amount'=>$amt['User']['amount'],
                                            'currency'=>'USD',
                                            'redirect_url'=>$url,
                                            'cancel_url'=>$url,
                                            'language'=>'EN',
                                            'billing_name'=>$name,
                                            'billing_address'=>$user['address'],
                                            'billing_city'=>$user['city'],
                                            'billing_state'=>$user['state'],
                                            'billing_zip'=>$user['pin_code'],
                                            'billing_country'=>$user['country'],
                                            'billing_tel'=>$user['mobile'],
                                            'billing_email'=>$user['email'],
                                            'delivery_name'=>$name,
                                            'delivery_address'=>$user['address'],
                                            'delivery_city'=>$user['city'],
                                            'delivery_state'=>$user['state'],
                                            'delivery_zip'=>$user['pin_code'],
                                            'delivery_country'=>$user['country'],
                                            'delivery_tel'=>$user['mobile'],

                                        ];
                                $this->ccavrequesthandler($data);

                        }
                        /* Payment options code end here VG-31/03/2017 */

                        }
                    } else {

                        $enq_data['Enquiry']['product_id'] = $id;
                        $enq_data['Enquiry']['user_id'] = $user_details['User']['id'];
                        //$enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];

                        if ($ref_page == 'Buy Now') // if page is buy-now then only show below feild on page   VG-03/04/2017
                        {
                            // $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                            $enq_data['Enquiry']['subject'] = "NA";
                            $enq_data['Enquiry']['message'] = "NA";
                        }
                        else
                        {
                            $enq_data['Enquiry']['subject'] = "Enquiry from ". $ref_page;
                            $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                        }
                        // $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                        $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                        $enq_data['Enquiry']['organisation'] = $this->request->data['Enquiry']['organisation'];
                        $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                        $enq_data['Enquiry']['ref_page'] = $ref_page;
                        $data['User']['first_name'] = $user_details['User']['first_name'];
                        $data['User']['last_name'] = $user_details['User']['last_name'];
                        $data['User']['id'] = $user_details['User']['id'];
                        $data['User']['email'] = $user_details['User']['email'];
                        $data['User']['job_title'] = $user_details['User']['job_title'];

                        $this->Enquiry->create();
                        if ($this->Enquiry->save($enq_data)) {
                            $enq_id = $this->Enquiry->id;
                            //debug($enq_id);die;
                            if ($ref_page != 'Buy Now') {
                                //send enquiry acknowledgement to customer
                                    // ***********************
                                $email = new CakeEmail();
                                $email->config('user_enquiery');
                                $email->to($data['User']['email']);
                                $email->viewVars(compact('data', 'pro_name', 'category'));
                                $email->subject('Regarding your inquiry for '.$pro_name['Product']['product_name']);
                                $email->send();
                                //************************
                            }
                            /*IF USER IS REGISTERED SEND SAMPLE ENQUIRY MAIL*/
                                /*Code for new sample enquiry mail Start VG-31/12/2016*/
                                if($ref_page=='Download Sample')
                                {
                                    // *****************************
                                    $email = new CakeEmail();
                                    $email->config('sample_request');
                                    $email->to('sample@decisiondatabases.com'); //sample@decisiondatabases.com
                                    $email->viewVars(compact('pro_name','data'));
                                    $email->subject('New Sample Request:'. $pro_name['Product']['product_name']);
                                    $email->send();
                                    // ***************************
                                }
                            /*Code for new sample enquiry mail End VG-31/12/2016*/

                            $this->loadModel('Setting');
                            $mail_to = $this->Setting->find_setting(1);

                            if (!empty($mail_to['Setting']['mail_to'])) {
                                    // **************************
                                $email_admin = new CakeEmail();
                                $email_admin->config('user_enquiery_received');
                                $email_admin->to($mail_to['Setting']['mail_to']);
                                $email_admin->viewVars(compact('data', 'pro_name', 'category', 'enq_id', 'enq_data'));
                                $email_admin->subject($ref_page." : ".$pro_name['Product']['product_name']);
                                $email_admin->send();
                                    // *************************
                            }
                                    // *************************
                            if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                                $cc_ary = explode(',', $mail_to['Setting']['emails']);
                                $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                                $email_cc = new CakeEmail();
                                foreach ($cc_ary as $ekey => $email_id):
                                    $email_cc->config('user_enquiery_received_cc');
                                    $email_cc->to($email_id);
                                    $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                                    $email_cc->subject($ref_page." : ".$pro_name['Product']['product_name']);
                                    $email_cc->send();
                                endforeach;
                            }
                            //**************************
                            if ($ref_page != 'Buy Now') {
                                $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                                //return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                                $this->Session->write('thanks', 1);

                                //code starts here-VG-23/08/2016.
                                if(!empty($related_category_name[0]['Category']['id'])){

                                    $cat_id=$related_category_name[0]['Category']['id'];
                                    $cat_slug=$related_category_name[0]['Category']['cat_slug'];
                                //Passing parameters of related reports id and slug.
                                }
                                else{

                                $cat_id=$related_category_name['Category']['id'];
                                $cat_slug=$related_category_name['Category']['cat_slug'];
                                //Passing parameters of related reports id and slug.
                                }
                                return $this->redirect(array('controller' => 'users', 'action' => 'thanks','?'=>array('id'=>$cat_id,'slug'=>$cat_slug))); //query string passed here for userscontroller thanks method

                                //code ends here-VG-23/08/2016.
                            } else {
                                $data['User']['txnid'] = substr(str_shuffle('abcefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ23456789'), 5, 11);
                                $data['User']['firstname'] = $data['User']['first_name'] . " " . $data['User']['last_name'];
                                $data['User']['surl'] = $success_url;
                                $data['User']['furl'] = $fail_url;
                                $data['User']['productinfo'] = $pro_name['Product']['product_name'];
                                /*echo "<pre>";
                                print_r($data);*/

                                if((empty($pro_name['Product']['tags']) || $pro_name['Product']['tags']<=0)){
                                    if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                        $data['User']['amount'] = $pro_name['Product']['price'];
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                        $data['User']['amount'] = $pro_name['Product']['corporate_price'];
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                        if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                            $data['User']['amount'] = $pro_name['Product']['corporate_price'];
                                        }else{
                                            $data['User']['amount'] = $pro_name['Product']['dollar_price'];
                                        }
                                    }
                                }else{
                                    if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                        $data['User']['amount'] = $pro_name['Product']['price'] - ($pro_name['Product']['price'] * $pro_name['Product']['tags'] / 100);
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                        $data['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                    } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                        if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                            $data['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                        }else{
                                            $data['User']['amount'] = $pro_name['Product']['dollar_price'] - ($pro_name['Product']['dollar_price'] * $pro_name['Product']['tags'] / 100);
                                        }
                                    }
                                }

                                $data_required['key'] = $data['User']['key'];
                                $data_required['txnid'] = $data['User']['txnid'];
                                $data_required['amount'] = $data['User']['amount'];
                                $data_required['firstname'] = $data['User']['first_name'];
                                $data_required['lastname'] = $data['User']['last_name'];

                                $data_required['email'] = $data['User']['email'];
                                $data_required['phone'] = $data['User']['mobile'];
                                $data_required['productinfo'] = $data['User']['productinfo'];
                                $data_required['surl'] = $data['User']['surl'];
                                $data_required['furl'] = $data['User']['furl'];

                                $data_required['udf1'] = $this->request->data['Enquiry']['licence_type'];

                                $data_required['udf2'] = $data['User']['organisation'];
                            // debug($data_required['udf2']);die();
                                $data_required['udf3'] = $data['User']['job_title'];
                                $data_required['udf4'] = $pro_name['Product']['id'];
                                $data_required['udf5'] = $data['User']['id'];

                                $data_required['city'] = $data['User']['city'];
                                $data_required['state'] = $data['User']['state'];
                                $data_required['zipcode'] = $data['User']['pin_code'];
                                $data_required['country'] = $data['User']['country'];


                                /*echo "<pre>";
                                print_r($data_required);*/

                                //Attempt to email notification
                                $enq_data['Enquiry']['product_id'] = $pro_name['Product']['id'];
                                $enq_data['Enquiry']['user_id'] = $data['User']['id'];
                                $enq_data['Enquiry']['subject'] = "Transaction Attempted";
                                $enq_data['Enquiry']['message'] = "This User tried to buy " . $pro_name['Product']['product_name'] . " this product.";
                                $enq_data['Enquiry']['mobile'] = $data_required['phone'];
                                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                                $enq_data['Enquiry']['ref_page'] = "Buy Now Attempt Has Been Done.";
                                $product_name = $pro_name['Product']['product_name'];

                                $this->Enquiry->create();

                                if ($this->Enquiry->save($enq_data)) {
                                    $enq_id = $this->Enquiry->id;
                                    $this->loadModel('Setting');
                                    $mail_to = $this->Setting->find_setting(1);
                                    // ********************************
                                    if (!empty($mail_to['Setting']['mail_to'])) {
                                        $email_admin = new CakeEmail();
                                        $email_admin->config('user_enquiery_txn_success');
                                        $email_admin->to($mail_to['Setting']['mail_to']);
                                        $email_admin->viewVars(compact('enq_data', 'data', 'product_name', 'enq_id'));
                                        $email_admin->subject('New Lead Recieved : Transaction Attempted');
                                        $email_admin->send();
                                    }
                                    // *********************************


                                }
                                //END ENQUIRY DATA
                                //debug($data_required);

                            // $this->Payu = new Payu;
                            // $this->Payu->pay_page($data_required, $data['User']['salt']);
                            // die;
                        /* Payment options code start here VG-31/03/2017 */
                        if (!empty($this->request->data['payment_option']) && $this->request->data['payment_option'] == 'paypal')
                        {
                            if((empty($pro_name['Product']['tags']) || $pro_name['Product']['tags']<=0)){
                                if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                    $amt['User']['amount'] = $pro_name['Product']['price'];
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                    $amt['User']['amount'] = $pro_name['Product']['corporate_price'];
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                    if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                        $amt['User']['amount'] = $pro_name['Product']['corporate_price'];
                                    }else{
                                        $amt['User']['amount'] = $pro_name['Product']['dollar_price'];
                                    }
                                }
                            }else{
                                if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                    $amt['User']['amount'] = $pro_name['Product']['price'] - ($pro_name['Product']['price'] * $pro_name['Product']['tags'] / 100);
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                    $amt['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                    if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                        $amt['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                    }else{
                                        $amt['User']['amount'] = $pro_name['Product']['dollar_price'] - ($pro_name['Product']['dollar_price'] * $pro_name['Product']['tags'] / 100);
                                    }
                                }
                            }	
                            $user=$this->request->data['Enquiry'];

                            if($_SERVER['HTTP_HOST'] == "www.decisiondatabases.com")
                            {
                            $this->Paypal = new Paypal(array(
                                'sandboxMode' => false,
                                'nvpUsername' => 'admin_api1.decisiondatabases.com',
                                'nvpPassword' => '95NQFYL48UGNYUJV',
                                'nvpSignature' => 'Ajwath3vWTxZhV-dji.0VNOXCfYMAPmQ-pzAeAM72MLSRQaFx4.3-jHm',
                            ));
                            }else{
                            $this->Paypal = new Paypal(array(
                                'sandboxMode' => true,
                                'nvpUsername' => 'skshrikant510-business-us_api1.gmail.com',
                                'nvpPassword' => '3PLDLETB2E7WJUW7',
                                'nvpSignature' => 'AQJCtaN7Xv31h0261ixZd2P3S1EWAk42FICNBZwgEPYXFRvwY9gl0c3G',
                            ));
                            }

                            $base_url = Router::url('/', true);
                            $cancel_url = $base_url.'payresponse';
                            $return_url = $base_url.'payresponse';

                            $order['description'] = 'Your purchase with Decision Databases';
                            $order['currency'] = 'USD';
                            $order['return'] = $return_url;
                            $order['cancel'] = $cancel_url;
                            $order['custom'] = 'test';
                            $order['shipping'] = 0.00;
                            $order['first_name'] = $user['first_name'];
                            $order['last_name'] = $user['last_name'];
                            $order['address1'] = $user['address'];
                            $order['address2'] = $user['address'];
                            $order['city'] = $user['city'];
                            $order['state'] = $user['state'];
                            $order['zip'] = $user['pin_code'];
                            $order['email'] = $user['email'];
                            $order['mobile'] = $user['mobile'];
                            $order['items'][0]['name'] = $_POST['pro_name'];
                            $order['items'][0]['description'] = $_POST['pro_name'];
                            $order['items'][0]['tax'] = 0.00;
                            $order['items'][0]['subtotal'] = round($amt['User']['amount'], 2);
                            $order['items'][0]['qty'] = 1;
                            $order['items'][0]['product_id'] = 1;
                            $order['items'][0]['licence_type'] = $user['licence_type'];
                            // $this->Session->write('order_session', $order);
                            $res = $this->Paypal->setExpressCheckout($order);
                            //debug($order);
                            // debug($res);
                            //die;
                            $this->redirect($res);

                            // $this->Payu = new Payu;
                            // $this->Payu->pay_page($data_required, $data['User']['salt']);
                            die;
                            // $this->Payu = new Payu;
                            // $this->Payu->pay_page($data_required, $data['User']['salt']);
                            die;
                        }

                        if (!empty($this->request->data['payment_option']) && $this->request->data['payment_option'] == 'ccavenue')
                        {
                            //$this->layout = false;

                            if((empty($pro_name['Product']['tags']) || $pro_name['Product']['tags']<=0)){
                                if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                    $amt['User']['amount'] = $pro_name['Product']['price'];
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                    $amt['User']['amount'] = $pro_name['Product']['corporate_price'];
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                    if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                        $amt['User']['amount'] = $pro_name['Product']['corporate_price'];
                                    }else{
                                        $amt['User']['amount'] = $pro_name['Product']['dollar_price'];
                                    }
                                }
                            }else{
                                if ($this->request->data['Enquiry']['licence_type'] == 1) {
                                    $amt['User']['amount'] = $pro_name['Product']['price'] - ($pro_name['Product']['price'] * $pro_name['Product']['tags'] / 100);
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 2) {
                                    $amt['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                } elseif ($this->request->data['Enquiry']['licence_type'] == 3) {
                                    if(empty($pro_name['Product']['dollar_price']) || $pro_name['Product']['dollar_price'] == 0){
                                        $amt['User']['amount'] = $pro_name['Product']['corporate_price'] - ($pro_name['Product']['corporate_price'] * $pro_name['Product']['tags'] / 100);
                                    }else{
                                        $amt['User']['amount'] = $pro_name['Product']['dollar_price'] - ($pro_name['Product']['dollar_price'] * $pro_name['Product']['tags'] / 100);
                                    }
                                }
                            }

                                $base_url = Router::url('/', true);
                                // $url =  Router::url(array('controller' => 'enquiries', 'action' => 'ccavresponsehandler'));
                                if($_SERVER['HTTP_HOST'] == "www.decisiondatabases.com")
                                {
                                $url = 'http://www.decisiondatabases.com/ccavresponse';
                                }
                                else {
                                $url = 'http://test.decisiondatabases.com/ccavresponse';
                                }
                                $tid=substr(str_shuffle('0123456789'), 1, 5).date('his');
                                $oid=substr(str_shuffle('123456789'), 1, 3).date('his');
                                $user=$this->request->data['Enquiry'];
                                $name = $user['first_name']." ".$user['last_name'];
                                $data = [ 'merchant_id'=>'79131',
                                            'order_id'=>$oid,
                                            'tid'=>$tid,
                                            'amount'=>$amt['User']['amount'],
                                            'currency'=>'USD',
                                            'redirect_url'=>$url,
                                            'cancel_url'=>$url,
                                            'language'=>'EN',
                                            'billing_name'=>$name,
                                            'billing_address'=>$user['address'],
                                            'billing_city'=>$user['city'],
                                            'billing_state'=>$user['state'],
                                            'billing_zip'=>$user['pin_code'],
                                            'billing_country'=>$user['country'],
                                            'billing_tel'=>$user['mobile'],
                                            'billing_email'=>$user['email'],
                                            'delivery_name'=>$name,
                                            'delivery_address'=>$user['address'],
                                            'delivery_city'=>$user['city'],
                                            'delivery_state'=>$user['state'],
                                            'delivery_zip'=>$user['pin_code'],
                                            'delivery_country'=>$user['country'],
                                            'delivery_tel'=>$user['mobile'],

                                        ];
                                    $this->ccavrequesthandler($data);

                                }
                                /* Payment options code end here VG-31/03/2017 */

                            }
                        }
                    }

                }
                // End New Code to Keep Textbox values VG 21/5/2020
            }
        }
    }

    public function send_enquiry_speak_to_analyst($id = null, $cat_id = null, $ref_page = null) {
        $this->layout = null;
        $this->loadModel('Product');
        Configure::load('idata');
        $ref = Configure::read('idata.ref_page');
        $ref_page = $ref[2];
        if (isset($this->request->data['Enquiry']['id'])) {
            $id = $this->request->data['Enquiry']['id'];
            $cat_id = $this->request->data['Enquiry']['cat_id'];
            // $ref_page = $this->request->data['Enquiry']['ref_page'];
        }

        $pro_name = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name'), 'recursive' => -1));
        if ($this->request->is('post')) {

            if ($this->Session->read('captcha_code') != $this->request->data['Enquiry']['captcha']) {
                $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                return $this->redirect($this->referer());
            }
            if (!$this->Auth->user('id')) {
                if (empty($this->request->data['Enquiry']['mobile']) || empty($this->request->data['Enquiry']['email']) || empty($this->request->data['Enquiry']['first_name']) || empty($this->request->data['Enquiry']['last_name'])) {
                    $this->redirect($this->referer());
                    $this->Session->setFlash(__('Please Fill Form '), 'error');
                }
                $user_details = $this->Enquiry->User->find('first', array('conditions' => array('User.email' => $this->request->data['Enquiry']['email']), 'recursive' => -1));
                //debug($user_details);
                if (!empty($user_details)) {

                    $enq_data['Enquiry']['product_id'] = $id;
                    $enq_data['Enquiry']['category_id'] = $cat_id;
                    $enq_data['Enquiry']['user_id'] = $user_details['User']['id'];
                    $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                    $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                    $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                    $enq_data['Enquiry']['ref_page'] = $ref_page;
                    $data['User']['first_name'] = $user_details['User']['first_name'];
                    $data['User']['last_name'] = $user_details['User']['last_name'];
                    $data['User']['id'] = $user_details['User']['id'];
                    $data['User']['email'] = $user_details['User']['email'];
                    $data['User']['job_title'] = $user_details['User']['job_title'];

                    $this->loadModel('Category');
                    $category = $this->Category->find('all', array(
                        'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                    ));
                    $this->set(compact('category'));
                    $this->Enquiry->create();
                    if ($this->Enquiry->save($enq_data)) {
                        $enq_id = $this->Enquiry->id;
                        //debug($enq_id);die;
                        $email = new CakeEmail();
                        $email->config('user_enquiery');
                        $email->to($data['User']['email']);
                        $email->viewVars(compact('data', 'pro_name', 'category'));
                        $email->subject('Regarding your inquiry for '.$pro_name['Product']['product_name']);
                        $email->send();

                        $this->loadModel('Setting');
                        $mail_to = $this->Setting->find_setting(1);
                        if (!empty($mail_to['Setting']['mail_to'])) {
                            $email_admin = new CakeEmail();
                            $email_admin->config('user_enquiery_received');
                            $email_admin->to($mail_to['Setting']['mail_to']);
                            $email_admin->viewVars(compact('data', 'pro_name', 'category', 'enq_id', 'enq_data'));
                            $email_admin->subject('New Lead Recieved');
                            $email_admin->send();
                        }
                        if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                            $cc_ary = explode(',', $mail_to['Setting']['emails']);
                            $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                            $email_cc = new CakeEmail();
                            foreach ($cc_ary as $ekey => $email_id):
                                $email_cc->config('user_enquiery_received_cc');
                                $email_cc->to($email_id);
                                $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                                $email_cc->subject('New Lead Recieved');
                                $email_cc->send();
                            endforeach;
                        }

                        $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                        return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                    }
                } else {
                    $v_code = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789', 5)), 0, 5);
                    $data['User']['role'] = 11;
                    $data['User']['first_name'] = $this->request->data['Enquiry']['first_name'];
                    $data['User']['last_name'] = $this->request->data['Enquiry']['last_name'];
                    $data['User']['email'] = $this->request->data['Enquiry']['email'];
                    $data['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $data['User']['job_title'] = $this->request->data['Enquiry']['job_title'];
                    $data['User']['organisation'] = $this->request->data['Enquiry']['organisation'];
                    $data['User']['visited_ip'] = $this->request->clientIp();

                    //   $data['User']['veri_code'] = $v_code;
                    $data['User']['password'] = $v_code;
                    $data['User']['role'] = 11;
                    if ($this->Enquiry->User->save($data)) {
                        $data['User']['id'] = $this->Enquiry->User->id;
                        $enq_data['Enquiry']['product_id'] = $id;
                        $enq_data['Enquiry']['category_id'] = $cat_id;
                        $enq_data['Enquiry']['user_id'] = $data['User']['id'];
                        $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                        $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                        $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                        $enq_data['Enquiry']['ref_page'] = $ref_page;
                        $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                        $this->Enquiry->create();
                        $this->Enquiry->save($enq_data);
                        $enq_id = $this->Enquiry->id;
                        //debug($enq_id);die;
                        $this->loadModel('Category');
                        $category = $this->Category->find('all', array(
                            'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                        ));
                        $this->set(compact('category'));

                        /* $email = new CakeEmail();
                          $email->config('user_registration_enquiery');
                          $email->to($data['User']['email']);
                          $email->viewVars(compact('data', 'pro_name', 'v_code', 'category'));
                          $email->subject('Online Registration on Decision Databases');
                          $email->send(); */

                        $this->loadModel('Setting');
                        $mail_to = $this->Setting->find_setting(1);
                        if (!empty($mail_to['Setting']['mail_to'])) {
                            $email_admin = new CakeEmail();
                            $email_admin->config('user_enquiery_received');
                            $email_admin->to($mail_to['Setting']['mail_to']);
                            $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                            $email_admin->subject('New Lead Recieved');
                            $email_admin->send();
                        }
                        if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                            $cc_ary = explode(',', $mail_to['Setting']['emails']);
                            $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                            $email_cc = new CakeEmail();
                            foreach ($cc_ary as $ekey => $email_id):
                                $email_cc->config('user_enquiery_received_cc');
                                $email_cc->to($email_id);
                                $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                                $email_cc->subject('New Lead Recieved');
                                $email_cc->send();
                            endforeach;
                        }

                        if ($this->Auth->login($data['User'])) {
                            if (AuthComponent::user('role') == 11) {
                                $this->Session->setFlash(__('A mail has been sent to your mailbox . Please use Username And Password For Further Login.'), 'success');
                                return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                            } else {
                                $this->Session->setFlash(__('Your account has been successfully verified.'), 'success');
                                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                            }
                        } else {
                            $this->Session->setFlash(__('Unauthorised user details, verification failed'), 'error');
                            return $this->redirect(array('action' => 'login'));
                        }
                    } else {
                        $this->Session->setFlash(__('Invalid information please try again'), 'success');
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                }
            } else {
                $enq_data['Enquiry']['product_id'] = $id;
                $enq_data['Enquiry']['category_id'] = $cat_id;
                $enq_data['Enquiry']['user_id'] = $this->Auth->user('id');
                $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = $ref_page;
                if ($this->Auth->user('mobile') != $this->request->data['Enquiry']['mobile']) {
                    $user_temp['User']['id'] = $this->Auth->user('id');
                    $user_temp['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $this->Enquiry->User->save($user_temp);
                }
                $data['User']['first_name'] = $this->Auth->user('first_name');
                $data['User']['last_name'] = $this->Auth->user('last_name');
                $data['User']['id'] = $this->Auth->user('id');
                $data['User']['email'] = $this->Auth->user('email');
                $data['User']['job_title'] = $this->Auth->user('job_title');
                $data['User']['visited_ip'] = $this->request->clientIp();
                $this->loadModel('Category');
                $category = $this->Category->find('all', array(
                    'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                ));
                $this->set(compact('category'));



                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);

                    $email = new CakeEmail();
                    $email->config('user_enquiery');
                    $email->to($data['User']['email']);
                    $email->viewVars(compact('data', 'pro_name', 'category'));
                    $email->subject('Regarding your inquiry for '.$pro_name['Product']['product_name']);
                    $email->send();

                    if (!empty($mail_to['Setting']['mail_to'])) {
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_received');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                        $email_admin->subject('New Lead Recieved');
                        $email_admin->send();
                    }

                    if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                        $cc_ary = explode(',', $mail_to['Setting']['emails']);
                        $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                        $email_cc = new CakeEmail();
                        foreach ($cc_ary as $ekey => $email_id):
                            $email_cc->config('user_enquiery_received_cc');
                            $email_cc->to($email_id);
                            $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                            $email_cc->subject('New Lead Recieved');
                            $email_cc->send();
                        endforeach;
                    }


                    $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                    return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                } else {
                    $this->Session->setFlash(__('The enquiry could not be saved. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            }
        }
        // Configure::load('idata');
        $title = Configure::read('idata.title');
        $this->set(compact('title', 'ref_page'));
    }

    public function send_enquiry_table_of_content($id = null, $cat_id = null, $ref_page = null) {
        $this->layout = null;
        $this->loadModel('Product');
        Configure::load('idata');
        $ref = Configure::read('idata.ref_page');
        $ref_page = $ref[4];
        if (isset($this->request->data['Enquiry']['id'])) {
            $id = $this->request->data['Enquiry']['id'];
            $cat_id = $this->request->data['Enquiry']['cat_id'];
            // $ref_page = $this->request->data['Enquiry']['ref_page'];
        }

        $pro_name = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name'), 'recursive' => -1));
        if ($this->request->is('post')) {

            if ($this->Session->read('captcha_code') != $this->request->data['Enquiry']['captcha']) {
                $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                return $this->redirect($this->referer());
            }
            if (!$this->Auth->user('id')) {
                if (empty($this->request->data['Enquiry']['mobile']) || empty($this->request->data['Enquiry']['email']) || empty($this->request->data['Enquiry']['first_name']) || empty($this->request->data['Enquiry']['last_name'])) {
                    $this->redirect($this->referer());
                    $this->Session->setFlash(__('Please Fill Form '), 'error');
                }
                $user_details = $this->Enquiry->User->find('first', array('conditions' => array('User.email' => $this->request->data['Enquiry']['email']), 'recursive' => -1));
                //debug($user_details);
                if (!empty($user_details)) {

                    $enq_data['Enquiry']['product_id'] = $id;
                    $enq_data['Enquiry']['category_id'] = $cat_id;
                    $enq_data['Enquiry']['user_id'] = $user_details['User']['id'];
                    $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                    $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                    $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                    $enq_data['Enquiry']['ref_page'] = $ref_page;
                    $data['User']['first_name'] = $user_details['User']['first_name'];
                    $data['User']['last_name'] = $user_details['User']['last_name'];
                    $data['User']['id'] = $user_details['User']['id'];
                    $data['User']['email'] = $user_details['User']['email'];
                    $data['User']['job_title'] = $user_details['User']['job_title'];

                    $this->loadModel('Category');
                    $category = $this->Category->find('all', array(
                        'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                    ));
                    $this->set(compact('category'));
                    $this->Enquiry->create();
                    if ($this->Enquiry->save($enq_data)) {
                        $enq_id = $this->Enquiry->id;
                        //debug($enq_id);die;
                        $email = new CakeEmail();
                        $email->config('user_enquiery');
                        $email->to($data['User']['email']);
                        $email->viewVars(compact('data', 'pro_name', 'category'));
                        $email->subject('Regarding your inquiry for '.$pro_name['Product']['product_name']);
                        $email->send();

                        $this->loadModel('Setting');
                        $mail_to = $this->Setting->find_setting(1);
                        if (!empty($mail_to['Setting']['mail_to'])) {
                            $email_admin = new CakeEmail();
                            $email_admin->config('user_enquiery_received');
                            $email_admin->to($mail_to['Setting']['mail_to']);
                            $email_admin->viewVars(compact('data', 'pro_name', 'category', 'enq_id', 'enq_data'));
                            $email_admin->subject('New Lead Recieved');
                            $email_admin->send();
                        }
                        if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                            $cc_ary = explode(',', $mail_to['Setting']['emails']);
                            $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                            $email_cc = new CakeEmail();
                            foreach ($cc_ary as $ekey => $email_id):
                                $email_cc->config('user_enquiery_received_cc');
                                $email_cc->to($email_id);
                                $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                                $email_cc->subject('New Lead Recieved');
                                $email_cc->send();
                            endforeach;
                        }

                        $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                        return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                    }
                } else {
                    $v_code = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789', 5)), 0, 5);
                    $data['User']['role'] = 11;
                    $data['User']['first_name'] = $this->request->data['Enquiry']['first_name'];
                    $data['User']['last_name'] = $this->request->data['Enquiry']['last_name'];
                    $data['User']['email'] = $this->request->data['Enquiry']['email'];
                    $data['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $data['User']['job_title'] = $this->request->data['Enquiry']['job_title'];
                    $data['User']['organisation'] = $this->request->data['Enquiry']['organisation'];
                    $data['User']['visited_ip'] = $this->request->clientIp();

                    //   $data['User']['veri_code'] = $v_code;
                    $data['User']['password'] = $v_code;
                    $data['User']['role'] = 11;
                    if ($this->Enquiry->User->save($data)) {
                        $data['User']['id'] = $this->Enquiry->User->id;
                        $enq_data['Enquiry']['product_id'] = $id;
                        $enq_data['Enquiry']['category_id'] = $cat_id;
                        $enq_data['Enquiry']['user_id'] = $data['User']['id'];
                        $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                        $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                        $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                        $enq_data['Enquiry']['ref_page'] = $ref_page;
                        $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                        $this->Enquiry->create();
                        $this->Enquiry->save($enq_data);
                        $enq_id = $this->Enquiry->id;
                        //debug($enq_id);die;
                        $this->loadModel('Category');
                        $category = $this->Category->find('all', array(
                            'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                        ));
                        $this->set(compact('category'));

                        /* $email = new CakeEmail();
                          $email->config('user_registration_enquiery');
                          $email->to($data['User']['email']);
                          $email->viewVars(compact('data', 'pro_name', 'v_code', 'category'));
                          $email->subject('Online Registration on Decision Databases');
                          $email->send(); */

                        $this->loadModel('Setting');
                        $mail_to = $this->Setting->find_setting(1);
                        if (!empty($mail_to['Setting']['mail_to'])) {
                            $email_admin = new CakeEmail();
                            $email_admin->config('user_enquiery_received');
                            $email_admin->to($mail_to['Setting']['mail_to']);
                            $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                            $email_admin->subject('New Lead Recieved');
                            $email_admin->send();
                        }
                        if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                            $cc_ary = explode(',', $mail_to['Setting']['emails']);
                            $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                            $email_cc = new CakeEmail();
                            foreach ($cc_ary as $ekey => $email_id):
                                $email_cc->config('user_enquiery_received_cc');
                                $email_cc->to($email_id);
                                $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                                $email_cc->subject('New Lead Recieved');
                                $email_cc->send();
                            endforeach;
                        }

                        if ($this->Auth->login($data['User'])) {
                            if (AuthComponent::user('role') == 11) {
                                $this->Session->setFlash(__('A mail has been sent to your mailbox . Please use Username And Password For Further Login.'), 'success');
                                return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                            } else {
                                $this->Session->setFlash(__('Your account has been successfully verified.'), 'success');
                                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                            }
                        } else {
                            $this->Session->setFlash(__('Unauthorised user details, verification failed'), 'error');
                            return $this->redirect(array('action' => 'login'));
                        }
                    } else {
                        $this->Session->setFlash(__('Invalid information please try again'), 'success');
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                }
            } else {
                $enq_data['Enquiry']['product_id'] = $id;
                $enq_data['Enquiry']['category_id'] = $cat_id;
                $enq_data['Enquiry']['user_id'] = $this->Auth->user('id');
                $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = $ref_page;
                if ($this->Auth->user('mobile') != $this->request->data['Enquiry']['mobile']) {
                    $user_temp['User']['id'] = $this->Auth->user('id');
                    $user_temp['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $this->Enquiry->User->save($user_temp);
                }
                $data['User']['first_name'] = $this->Auth->user('first_name');
                $data['User']['last_name'] = $this->Auth->user('last_name');
                $data['User']['id'] = $this->Auth->user('id');
                $data['User']['email'] = $this->Auth->user('email');
                $data['User']['job_title'] = $this->Auth->user('job_title');
                $data['User']['visited_ip'] = $this->request->clientIp();
                $this->loadModel('Category');
                $category = $this->Category->find('all', array(
                    'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                ));
                $this->set(compact('category'));



                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);

                    $email = new CakeEmail();
                    $email->config('user_enquiery');
                    $email->to($data['User']['email']);
                    $email->viewVars(compact('data', 'pro_name', 'category'));
                    $email->subject('Regarding your inquiry for '.$pro_name['Product']['product_name']);
                    $email->send();

                    if (!empty($mail_to['Setting']['mail_to'])) {
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_received');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                        $email_admin->subject('New Lead Recieved');
                        $email_admin->send();
                    }

                    if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                        $cc_ary = explode(',', $mail_to['Setting']['emails']);
                        $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                        $email_cc = new CakeEmail();
                        foreach ($cc_ary as $ekey => $email_id):
                            $email_cc->config('user_enquiery_received_cc');
                            $email_cc->to($email_id);
                            $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                            $email_cc->subject('New Lead Recieved');
                            $email_cc->send();
                        endforeach;
                    }


                    $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                    return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                } else {
                    $this->Session->setFlash(__('The enquiry could not be saved. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            }
        }
        // Configure::load('idata');
        $title = Configure::read('idata.title');
        $this->set(compact('title', 'ref_page'));
    }

    public function send_enquiry_speak_to_custom($id = null, $cat_id = null, $ref_page = null) {
        $this->layout = null;
        $this->loadModel('Product');
        Configure::load('idata');
        $ref = Configure::read('idata.ref_page');
        $ref_page = $ref[5];
        if (isset($this->request->data['Enquiry']['id'])) {
            $id = $this->request->data['Enquiry']['id'];
            $cat_id = $this->request->data['Enquiry']['cat_id'];
            // $ref_page = $this->request->data['Enquiry']['ref_page'];
        }

        $pro_name = $this->Product->find('first', array('conditions' => array('Product.id' => $id), 'fields' => array('Product.product_name'), 'recursive' => -1));
        if ($this->request->is('post')) {

            if ($this->Session->read('captcha_code') != $this->request->data['Enquiry']['captcha']) {
                $this->Session->setFlash(__('Please enter correct captcha code.'), 'error');
                return $this->redirect($this->referer());
            }
            if (!$this->Auth->user('id')) {
                if (empty($this->request->data['Enquiry']['mobile']) || empty($this->request->data['Enquiry']['email']) || empty($this->request->data['Enquiry']['first_name']) || empty($this->request->data['Enquiry']['last_name'])) {
                    $this->redirect($this->referer());
                    $this->Session->setFlash(__('Please Fill Form '), 'error');
                }
                $user_details = $this->Enquiry->User->find('first', array('conditions' => array('User.email' => $this->request->data['Enquiry']['email']), 'recursive' => -1));
                //debug($user_details);
                if (!empty($user_details)) {

                    $enq_data['Enquiry']['product_id'] = $id;
                    $enq_data['Enquiry']['category_id'] = $cat_id;
                    $enq_data['Enquiry']['user_id'] = $user_details['User']['id'];
                    $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                    $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                    $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                    $enq_data['Enquiry']['ref_page'] = $ref_page;
                    $data['User']['first_name'] = $user_details['User']['first_name'];
                    $data['User']['last_name'] = $user_details['User']['last_name'];
                    $data['User']['id'] = $user_details['User']['id'];
                    $data['User']['email'] = $user_details['User']['email'];
                    $data['User']['job_title'] = $user_details['User']['job_title'];

                    $this->loadModel('Category');
                    $category = $this->Category->find('all', array(
                        'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                    ));
                    $this->set(compact('category'));
                    $this->Enquiry->create();
                    if ($this->Enquiry->save($enq_data)) {
                        $enq_id = $this->Enquiry->id;
                        //debug($enq_id);die;
                        $email = new CakeEmail();
                        $email->config('user_enquiery');
                        $email->to($data['User']['email']);
                        $email->viewVars(compact('data', 'pro_name', 'category'));
                        $email->subject('Regarding your inquiry for '.$pro_name['Product']['product_name']);
                        $email->send();

                        $this->loadModel('Setting');
                        $mail_to = $this->Setting->find_setting(1);
                        if (!empty($mail_to['Setting']['mail_to'])) {
                            $email_admin = new CakeEmail();
                            $email_admin->config('user_enquiery_received');
                            $email_admin->to($mail_to['Setting']['mail_to']);
                            $email_admin->viewVars(compact('data', 'pro_name', 'category', 'enq_id', 'enq_data'));
                            $email_admin->subject('New Lead Recieved');
                            $email_admin->send();
                        }
                        if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                            $cc_ary = explode(',', $mail_to['Setting']['emails']);
                            $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                            $email_cc = new CakeEmail();
                            foreach ($cc_ary as $ekey => $email_id):
                                $email_cc->config('user_enquiery_received_cc');
                                $email_cc->to($email_id);
                                $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                                $email_cc->subject('New Lead Recieved');
                                $email_cc->send();
                            endforeach;
                        }

                        $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                        return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                    }
                } else {
                    $v_code = substr(str_shuffle(str_repeat('ABCDEFGHIJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz123456789', 5)), 0, 5);
                    $data['User']['role'] = 11;
                    $data['User']['first_name'] = $this->request->data['Enquiry']['first_name'];
                    $data['User']['last_name'] = $this->request->data['Enquiry']['last_name'];
                    $data['User']['email'] = $this->request->data['Enquiry']['email'];
                    $data['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $data['User']['job_title'] = $this->request->data['Enquiry']['job_title'];
                    $data['User']['organisation'] = $this->request->data['Enquiry']['organisation'];
                    $data['User']['visited_ip'] = $this->request->clientIp();

                    //   $data['User']['veri_code'] = $v_code;
                    $data['User']['password'] = $v_code;
                    $data['User']['role'] = 11;
                    if ($this->Enquiry->User->save($data)) {
                        $data['User']['id'] = $this->Enquiry->User->id;
                        $enq_data['Enquiry']['product_id'] = $id;
                        $enq_data['Enquiry']['category_id'] = $cat_id;
                        $enq_data['Enquiry']['user_id'] = $data['User']['id'];
                        $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                        $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                        $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                        $enq_data['Enquiry']['ref_page'] = $ref_page;
                        $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                        $this->Enquiry->create();
                        $this->Enquiry->save($enq_data);
                        $enq_id = $this->Enquiry->id;
                        //debug($enq_id);die;
                        $this->loadModel('Category');
                        $category = $this->Category->find('all', array(
                            'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                        ));
                        $this->set(compact('category'));

                        /* $email = new CakeEmail();
                          $email->config('user_registration_enquiery');
                          $email->to($data['User']['email']);
                          $email->viewVars(compact('data', 'pro_name', 'v_code', 'category'));
                          $email->subject('Online Registration on Decision Databases');
                          $email->send(); */

                        $this->loadModel('Setting');
                        $mail_to = $this->Setting->find_setting(1);
                        if (!empty($mail_to['Setting']['mail_to'])) {
                            $email_admin = new CakeEmail();
                            $email_admin->config('user_enquiery_received');
                            $email_admin->to($mail_to['Setting']['mail_to']);
                            $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                            $email_admin->subject('New Lead Recieved');
                            $email_admin->send();
                        }
                        if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                            $cc_ary = explode(',', $mail_to['Setting']['emails']);
                            $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                            $email_cc = new CakeEmail();
                            foreach ($cc_ary as $ekey => $email_id):
                                $email_cc->config('user_enquiery_received_cc');
                                $email_cc->to($email_id);
                                $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                                $email_cc->subject('New Lead Recieved');
                                $email_cc->send();
                            endforeach;
                        }

                        if ($this->Auth->login($data['User'])) {
                            if (AuthComponent::user('role') == 11) {
                                $this->Session->setFlash(__('A mail has been sent to your mailbox . Please use Username And Password For Further Login.'), 'success');
                                return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                            } else {
                                $this->Session->setFlash(__('Your account has been successfully verified.'), 'success');
                                return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                            }
                        } else {
                            $this->Session->setFlash(__('Unauthorised user details, verification failed'), 'error');
                            return $this->redirect(array('action' => 'login'));
                        }
                    } else {
                        $this->Session->setFlash(__('Invalid information please try again'), 'success');
                        return $this->redirect(array('controller' => 'users', 'action' => 'login'));
                    }
                }
            } else {
                $enq_data['Enquiry']['product_id'] = $id;
                $enq_data['Enquiry']['category_id'] = $cat_id;
                $enq_data['Enquiry']['user_id'] = $this->Auth->user('id');
                $enq_data['Enquiry']['subject'] = $this->request->data['Enquiry']['subject'];
                $enq_data['Enquiry']['message'] = $this->request->data['Enquiry']['message'];
                $enq_data['Enquiry']['mobile'] = $this->request->data['Enquiry']['mobile'];
                $enq_data['Enquiry']['visited_ip'] = $this->request->clientIp();
                $enq_data['Enquiry']['ref_page'] = $ref_page;
                if ($this->Auth->user('mobile') != $this->request->data['Enquiry']['mobile']) {
                    $user_temp['User']['id'] = $this->Auth->user('id');
                    $user_temp['User']['mobile'] = $this->request->data['Enquiry']['mobile'];
                    $this->Enquiry->User->save($user_temp);
                }
                $data['User']['first_name'] = $this->Auth->user('first_name');
                $data['User']['last_name'] = $this->Auth->user('last_name');
                $data['User']['id'] = $this->Auth->user('id');
                $data['User']['email'] = $this->Auth->user('email');
                $data['User']['job_title'] = $this->Auth->user('job_title');
                $data['User']['visited_ip'] = $this->request->clientIp();
                $this->loadModel('Category');
                $category = $this->Category->find('all', array(
                    'conditions' => array('Category.parent_category_id' => 0, 'Category.is_active' => 1), 'recursive' => -1
                ));
                $this->set(compact('category'));



                $this->Enquiry->create();
                if ($this->Enquiry->save($enq_data)) {
                    $enq_id = $this->Enquiry->id;
                    $this->loadModel('Setting');
                    $mail_to = $this->Setting->find_setting(1);

                    $email = new CakeEmail();
                    $email->config('user_enquiery');
                    $email->to($data['User']['email']);
                    $email->viewVars(compact('data', 'pro_name', 'category'));
                    $email->subject('Regarding your inquiry for '.$pro_name['Product']['product_name']);
                    $email->send();

                    if (!empty($mail_to['Setting']['mail_to'])) {
                        $email_admin = new CakeEmail();
                        $email_admin->config('user_enquiery_received');
                        $email_admin->to($mail_to['Setting']['mail_to']);
                        $email_admin->viewVars(compact('data', 'pro_name', 'enq_id', 'category', 'enq_data'));
                        $email_admin->subject('New Lead Recieved');
                        $email_admin->send();
                    }

                    if ((!empty($mail_to['Setting']['emails'])) && ($mail_to['Setting']['is_email_active'] == 1)) {
                        $cc_ary = explode(',', $mail_to['Setting']['emails']);
                        $star_email = preg_replace('/(?=.).(?=.*@)/u', '*', $data['User']['email']);
                        $email_cc = new CakeEmail();
                        foreach ($cc_ary as $ekey => $email_id):
                            $email_cc->config('user_enquiery_received_cc');
                            $email_cc->to($email_id);
                            $email_cc->viewVars(compact('data', 'pro_name', 'star_email', 'enq_id', 'category', 'enq_data'));
                            $email_cc->subject('New Lead Recieved');
                            $email_cc->send();
                        endforeach;
                    }


                    $this->Session->setFlash(__('Enquiry Received Successfully'), 'success');
                    return $this->redirect(array('controller' => 'enquiries', 'action' => 'user_enquiry_index', $data['User']['id']));
                } else {
                    $this->Session->setFlash(__('The enquiry could not be saved. Please, try again.'), 'error');
                    return $this->redirect($this->referer());
                }
            }
        }
        // Configure::load('idata');
        $title = Configure::read('idata.title');
        $this->set(compact('title', 'ref_page'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Enquiry->id = $id;
        if (!$this->Enquiry->exists()) {
            throw new NotFoundException(__('Invalid enquiry'), 'error');
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Enquiry->delete()) {
            $this->Session->setFlash(__('The enquiry has been deleted.'), 'success');
        } else {
            $this->Session->setFlash(__('The enquiry could not be deleted. Please, try again.'), 'error');
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function active($id = NULL) {
        if ($this->Enquiry->updateAll(array('Enquiry.is_active' => 1, 'Enquiry.is_verified' => 1), array('Enquiry.id' => $id))) {
            $this->Session->setFlash(__('The enquiry has been Activated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The enquiry could not be Activated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deactive($id = NULL) {
        if ($this->Enquiry->updateAll(array('Enquiry.is_active' => 0), array('Enquiry.id' => $id))) {
            $this->Session->setFlash(__('The Enquiry has been DeActivated.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Enquiry could not be DeActivated. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function verified($id = NULL) {
        if ($this->Enquiry->updateAll(array('Enquiry.is_verified' => 1), array('Enquiry.id' => $id))) {
            $this->Session->setFlash(__('The Enquiry has been verified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Enquiry could not be verified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function deverified($id = NULL) {

        if ($this->Enquiry->updateAll(array('Enquiry.is_verified' => 0), array('Enquiry.id' => $id))) {
            $this->Session->setFlash(__('The Enquiry has been Deverified.'), 'success');
            return $this->redirect($this->referer());
        } else {
            $this->Session->setFlash(__('The Enquiry could not be Deverified. Please, try again.'), 'error');
            return $this->redirect($this->referer());
        }
    }

    public function change_status_selected() {
        $this->layout = false;
        if ($this->request->is('post')) {
            $data = $this->request->data;
            if ($data['action'] = "Delete Selected") {
                $enq_arr = explode(',', $data['Enquiry']['selected_enquiries']);
                foreach ($enq_arr as $key => $value) {
                    $this->Enquiry->delete($value);
                }
            }
        }
        $this->redirect($this->referer());
    }
    // CC Avenue Code Starts here VG-31-03-2017
    public function ccavrequesthandler($data)
    {
       //echo "<pre>"; print_r($data);exit;
      $merchant_data='';
      $working_key='';
      $access_code='';
      // commented code
      //$working_key='990D9FC9E730A8D330134CADAFD1782E';//Shared by CCAVENUES
      // $access_code='AVTR06CJ69AD34RTDA';//Shared by CCAVENUES

      if($_SERVER['HTTP_HOST'] == "www.decisiondatabases.com"){
        $working_key='990D9FC9E730A8D330134CADAFD1782E';//Shared by CCAVENUES
        $access_code='AVTR06CJ69AD34RTDA';//Shared by CCAVENUES
        $action = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
      }
      else{
          $working_key='0AE56FDF6EB88F81CB1C6EF171862373';//Shared by CCAVENUES
          $access_code='AVPB00EC93BS45BPSB';//Shared by CCAVENUES
          $action = 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
      }


    	foreach ($data as $key => $value){
    		$merchant_data.=$key.'='.urlencode($value).'&';
    	}
    	$encrypted_data= $this->encrypt($merchant_data,$working_key); // Method for encrypting the data.

      $this->set(compact('encrypted_data','access_code','action'));
      $this->render('/Ccavenue/ccavRequestHandler');
    }
   public function ccavresponsehandler()
   {
      $workingKey='990D9FC9E730A8D330134CADAFD1782E';		//Working Key should be provided here.
     	if (!empty($_POST['encResp']))
     	{
	        $encResponse=$_POST["encResp"];			//This is the response sent by the CCAvenue Server
	       	$rcvdString=$this->decrypt($encResponse,$workingKey);		//Crypto Decryption used as per the specified working key.
	       	$order_status="";
	       	$decryptValues=explode('&', $rcvdString);
	       	$dataSize=sizeof($decryptValues);
      	}
     	$this->set(compact('order_status','decryptValues','dataSize'));
    	$this->render('/Ccavenue/ccavResponseHandler');

   }
    public function encrypt($plainText,$key)
  	{
  		$secretKey = $this->hextobin(md5($key));
  		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
  	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
  	  	$blockSize = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, 'cbc');
  		$plainPad = $this->pkcs5_pad($plainText, $blockSize);
  	  	if (mcrypt_generic_init($openMode, $secretKey, $initVector) != -1)
  		{
  		      $encryptedText = mcrypt_generic($openMode, $plainPad);
  	      	      mcrypt_generic_deinit($openMode);

  		}
  		return bin2hex($encryptedText);
  	}

  	public function decrypt($encryptedText,$key)
  	{
  		$secretKey = $this->hextobin(md5($key));
  		$initVector = pack("C*", 0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f);
  		$encryptedText=$this->hextobin($encryptedText);
  	  	$openMode = mcrypt_module_open(MCRYPT_RIJNDAEL_128, '','cbc', '');
  		mcrypt_generic_init($openMode, $secretKey, $initVector);
  		$decryptedText = mdecrypt_generic($openMode, $encryptedText);
  		$decryptedText = rtrim($decryptedText, "\0");
  	 	mcrypt_generic_deinit($openMode);
  		return $decryptedText;
  	}
  	//*********** Padding Function *********************

  	public function pkcs5_pad ($plainText, $blockSize)
  	{
  	    $pad = $blockSize - (strlen($plainText) % $blockSize);
  	    return $plainText . str_repeat(chr($pad), $pad);
  	}

  	//********** Hexadecimal to Binary function for php 4.0 version ********

      public function hextobin($hexString)
     	 {
          	$length = strlen($hexString);
          	$binString="";
          	$count=0;
          	while($count<$length)
          	{
          	    $subString =substr($hexString,$count,2);
          	    $packedString = pack("H*",$subString);
          	    if ($count==0)
  		    {
  				$binString=$packedString;
  		    }

  		    else
  		    {
  				$binString.=$packedString;
  		    }

  		    $count+=2;
          	}
    	        return $binString;
     		}

    // CC Avenue Code ends here VG-31-03-2017

      // Chnge status and rating from grid code start VG-12-05-2017
    public function changestatus()
    {
      $this->loadModel('Enquiry');
      if (!empty($_GET))
      {
        $this->Enquiry->read(null,$_GET['id']);
        $this->Enquiry->set('status',$_GET['value']);
          if ($this->Enquiry->save())
          {
            return true;
          }

      }
      die;
    }

    public function changerating()
    {
      $this->loadModel('Enquiry');
      if (!empty($_GET))
      {
        $this->Enquiry->read(null,$_GET['id']);
        $this->Enquiry->set('rating',$_GET['value']);
          if ($this->Enquiry->save())
          {
            return true;
          }

      }
      die;
    }
    // Chnge status and rating from grid code End VG-12-05-2017

    public function beforeFilter() {
        $this->Auth->allow(array('get_lead_info_form', 'changestatus','changerating','send_enquiry', 'send_enquiry_speak_to_analyst', 'send_enquiry_table_of_content', 'send_enquiry_speak_to_custom', 'ccavrequesthandler','ccavresponsehandler','payresponsehandler','test'));
    }

    // Paypal payment gateway
    /* This Function supports paypal */
      public function process_order() {

        $this->Paypal = new Paypal(array(
           'sandboxMode' => true,
           'nvpUsername' => 'nishant.makam_api1.gmail.com',
           'nvpPassword' => '3DD7DHUZ7TRUXWJ7',
           'nvpSignature' => 'AyDoxrFPCkPRzyLteX7lxpFX8y0AAP5NBmt38ZFoWpxTKrcy5CgkPBB3'
         ));

        $base_url = Router::url('/', true);
        $cancel_url = $base_url . 'cart_items/add_to_cart';
        $return_url = $base_url . 'orders/my_orders';

        $order['description'] = 'Your purchase with Decision Databases';
        $order['currency'] = 'USD';
        $order['return'] = $return_url;
        $order['cancel'] = $cancel_url;
        $order['custom'] = 'test';
        $order['shipping'] = 0.00;
        $order['items'][0]['name'] = $data['User']['product_name'];
        $order['items'][0]['description'] = $data['User']['product_name'];
        $order['items'][0]['tax'] = 0.00;
        $order['items'][0]['subtotal'] = round($data['User']['price'], 2);
        $order['items'][0]['qty'] = 1;
        $order['items'][0]['product_id'] = $cart_item['CartItem']['product_id'];
        $order['items'][0]['licence_type'] = $cart_item['CartItem']['licence_type'];

        // $this->Session->write('order_session', $order);
        $res = $this->Paypal->setExpressCheckout($order);
        //debug($order);
        //debug($res);
        //die;
        $this->redirect($res);

    }
    public $HttpSocket = null;

    public function payresponsehandler()
    {
      if($_SERVER['HTTP_HOST'] == "www.decisiondatabases.com")
      {
        $this->Paypal = new Paypal(array(
           'sandboxMode' => false,
           'nvpUsername' => 'admin_api1.decisiondatabases.com',
           'nvpPassword' => '95NQFYL48UGNYUJV',
           'nvpSignature' => 'Ajwath3vWTxZhV-dji.0VNOXCfYMAPmQ-pzAeAM72MLSRQaFx4.3-jHm',
         ));
       }else{
        $this->Paypal = new Paypal(array(
           'sandboxMode' => true,
           'nvpUsername' => 'skshrikant510-business-us_api1.gmail.com',
           'nvpPassword' => '3PLDLETB2E7WJUW7',
           'nvpSignature' => 'AQJCtaN7Xv31h0261ixZd2P3S1EWAk42FICNBZwgEPYXFRvwY9gl0c3G',
         ));
       }

      $order_status="Failure";
      if(!empty($_REQUEST)){

        $token = $_REQUEST['token'];

        $res = $this->Paypal->getExpressCheckoutDetails($token);
        if ($res['ACK'] == 'Success' && !empty($_REQUEST['PayerID']) && ($res['ACK'] == 'Success' || $res['ACK'] == 'SuccessWithWarning' )) {
          $order_status="Success";
        }else{
          $order_status="Failure";
        }
      }
      $this->set(compact('order_status'));
      $this->render('/Paypal/paypalResponseHandler');
    }

}
