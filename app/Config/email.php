<?php

/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 2.0.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * This is email configuration file.
 *
 * Use it to configure email transports of CakePHP.
 *
 * Email configuration class.
 * You can specify multiple configurations for production, development and testing.
 *
 * transport => The name of a supported transport; valid options are as follows:
 *  Mail - Send using PHP mail function
 *  Smtp - Send using SMTP
 *  Debug - Do not send the email, just return the result
 *
 * You can add custom transports (or override existing transports) by adding the
 * appropriate file to app/Network/Email. Transports should be named 'YourTransport.php',
 * where 'Your' is the name of the transport.
 *
 * from =>
 * The origin email. See CakeEmail::from() about the valid values
 *
 */
/*
  use no-reply@m01.decisiondatabases.com
  in the from id
  SMTP Hostname: smtp.mailgun.org
  Default SMTP Login: postmaster@m01.decisiondatabases.com
  Default Password: e9f54bc44d4a943a3bfdc274d3f7cfb2
 */
class EmailConfig {

    public $default = array(
        'transport' => 'Mail',
        'from' => 'you@localhost',
            //'charset' => 'utf-8',
//'headerCharset' => 'utf-8',
    );
    public $smtp = array(
        'transport' => 'Smtp',
        'from' => array('site@localhost' => 'My Site'),
        'host' => 'localhost',
        'port' => 25,
        'timeout' => 30,
        'username' => 'user',
        'password' => 'secret',
        'client' => null,
        'log' => false,
            //'charset' => 'utf-8',
//'headerCharset' => 'utf-8',
    );
    public $fast = array(
        'from' => 'you@localhost',
        'sender' => null,
        'to' => null,
        'cc' => null,
        'bcc' => null,
        'replyTo' => null,
        'readReceipt' => null,
        'returnPath' => null,
        'messageId' => true,
        'subject' => null,
        'message' => null,
        'headers' => null,
        'viewRender' => null,
        'template' => false,
        'layout' => false,
        'viewVars' => null,
        'attachments' => null,
        'emailFormat' => null,
        'transport' => 'Smtp',
        'host' => 'localhost',
        'port' => 25,
        'timeout' => 30,
        'username' => 'user',
        'password' => 'secret',
        'client' => null,
        'log' => true,
            //'charset' => 'utf-8',
//'headerCharset' => 'utf-8',
    );
    public $forgot_password = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'forgot_password_email'
    );
    public $user_registration = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'user_registration',
        'bcc' => array('vaibhav@decisiondatabases.com')
    );
    public $reset_password = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'reset_password_email'
    );
    public $user_registration_enquiery = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'replyTo' => 'vaibhav@decisiondatabases.com',
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'user_registration_enquiery',
        // 'bcc' => array('vaibhav@decisiondatabases.com')
    );
    public $user_enquiery = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        'replyTo' => 'sales@decisiondatabases.com',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'user_enquiery',
        // 'bcc' => array("vaibhav@decisiondatabases.com")
    );
    /*Code for new sample enquiry mail Start VG-31/12/2016*/
     public $sample_request = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
      //  'replyTo' => 'vaibhav@decisiondatabases.com',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'new_sample_request',
      //  'bcc' => array("vaibhav@decisiondatabases.com")
    );
 /*Code for new sample enquiry mail Start VG-31/12/2016*/

    public $user_enquiery_toc = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'user_enquiery',
        // 'bcc' => array("vaibhav@decisiondatabases.com")
    );
    public $user_enquiery_received = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        'replyTo' => 'vaibhav@decisiondatabases.com',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'user_enquiery_received',
        'bcc' => array("vaibhav@decisiondatabases.com")
    );
    
    public $user_enquiery_txn_fail = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'user_enquiery_txn_fail',
        'bcc' => array("vaibhav@decisiondatabases.com")
    );
    
    public $user_enquiery_txn_success = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        'replyTo' => 'vaibhav@decisiondatabases.com',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'user_enquiery_txn_success',
        'bcc' => array("vaibhav@decisiondatabases.com")
    );
    
    public $user_enquiery_received_cc = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'replyTo' => 'vaibhav@decisiondatabases.com',
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'user_enquiery_received_cc',
        'bcc' => array("vaibhav@decisiondatabases.com")
    );
    public $thanking_contact_us = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'replyTo' => 'vaibhav@decisiondatabases.com',
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'thanking_contact_us',
        // 'bcc' => array("vaibhav@decisiondatabases.com")
    );
    public $new_contact_message = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'replyTo' => 'vaibhav@decisiondatabases.com',
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'new_contact_message',
        // 'bcc' => array("vaibhav@decisiondatabases.com")
    );
    public $order_placed = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        'replyTo' => 'vaibhav@decisiondatabases.com',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'order_placed',
        'bcc' => array("vaibhav@decisiondatabases.com")
    );
    
    public $order_placed_thankyou = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        'replyTo' => 'vaibhav@decisiondatabases.com',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'order_placed_thankyou',
        //'bcc' => array("vaibhav@decisiondatabases.com")
        'bcc' => array("vaibhav@decisiondatabases.com")
    );
    
    public $order_placed_hdfc = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'order_placed_hdfc',
        //'bcc' => array("vaibhav@decisiondatabases.com")
        'bcc' => array("vaibhav@decisiondatabases.com")
    );
    
    
    public $admin_order_notification = array(
        'transport' => 'Smtp',
        'from' => array('no-reply@m01.decisiondatabases.com' => 'Decision Databases'),
        'host' => 'smtp.mailgun.org',
        // 'port' => 25,
        'port' => 587,
        'username' => 'postmaster@m01.decisiondatabases.com',
        'password' => 'e9f54bc44d4a943a3bfdc274d3f7cfb2',
        'emailFormat' => 'html',
        'template' => 'admin_order_notification',
        //'bcc' => array("vaibhav@decisiondatabases.com")
        'to' => array("vaibhav@decisiondatabases.com")
        
    );

}
