<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */

Router::connect('/', array('controller' => 'categories', 'action' => 'home'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

Router::connect(
        '/reports/:id-:slug-:strPage-:page', array('controller' => 'products', 'action' => 'category_listing'), array('pass' => array('id', 'slug', 'page', 'strPage'), 'id' => '[0-9]+', 'strPage' => 'page', 'page' => '[0-9]+')
);


//latest eighty market reports
Router::connect(
        '/reports/latest-market-reports', array('controller' => 'products', 'action' => 'latest_market_reports')
);

// Router::connect(
//         '/feeds/feeds', array('controller' => 'products', 'action' => 'rss','ext'=>'xml')
// );

Router::connect(
        '/categories/:id-:slug', array('controller' => 'categories', 'action' => 'category_details_list'), array('pass' => array('id', 'slug'), 'id' => '[0-9]+')
);

Router::connect(
        '/reports/:id-:slug', array('controller' => 'products', 'action' => 'category_listing'), array('pass' => array('id', 'slug'), 'id' => '[0-9]+')
);



Router::connect(
        '/:main/:id-:slug', array('controller' => 'products', 'action' => 'category_details'), array('pass' => array('id', 'main', 'slug'), 'id' => '[0-9]+')
);

/* New route for search result page VG-21/10/2016 start */
Router::connect(
        '/search-results/*', array('controller' => 'categories', 'action' => 'home_search_form')
);
/* New route for search result page VG-21/10/2016 end */

//Router::connect('/admin/products', array('controller' => 'products', 'action' => 'index'));

Router::connect('/contact-us', array('controller' => 'users', 'action' => 'contactus'));
Router::connect('/custom404', array('controller' => 'users', 'action' => 'page_not_found'));
Router::connect('/about-us', array('controller' => 'users', 'action' => 'aboutus'));
Router::connect('/privacy-policy', array('controller' => 'users', 'action' => 'privacy_policy'));
Router::connect('/terms-and-conditions', array('controller' => 'users', 'action' => 'terms_conditions'));
Router::connect('/accept-terms-and-conditions', array('controller' => 'users', 'action' => 'accept_terms_conditions'));
Router::connect('/refund-policy', array('controller' => 'users', 'action' => 'refundpolicy'));
Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
Router::connect('/forgot_password', array('controller' => 'users', 'action' => 'forgot_password'));
Router::connect('/registration', array('controller' => 'users', 'action' => 'registration'));
Router::connect('/category-list', array('controller' => 'categories', 'action' => 'category_list'));
Router::connect('/cart', array('controller' => 'cart_items', 'action' => 'add_to_cart'));
Router::connect('/sitemap', array('controller' => 'sitemap', 'action' => 'generate','ext'=>'xml'));
Router::connect('/cat-sitemap', array('controller' => 'sitemap', 'action' => 'pagesitemap','ext'=>'xml'));
Router::connect('/sitemap1', array('controller' => 'sitemap', 'action' => 'generate1','ext'=>'xml'));
Router::connect('/sitemap2', array('controller' => 'sitemap', 'action' => 'generate2','ext'=>'xml'));
Router::connect('/sitemap3', array('controller' => 'sitemap', 'action' => 'generate3','ext'=>'xml'));
Router::connect('/careers', array('controller' => 'users', 'action' => 'careers'));
Router::connect('/thank-you', array('controller' => 'users', 'action' => 'thanks'));
Router::connect('/payment-success', array('controller' => 'users', 'action' => 'payment_success'));
Router::connect('/infophp', array('controller' => 'users', 'action' => 'infophp'));
Router::connect('/transaction-failed', array('controller' => 'users', 'action' => 'payment_failed'));
Router::connect('/ccavresponse', array('controller' => 'enquiries', 'action' => 'ccavresponsehandler'));
Router::connect('/payresponse', array('controller' => 'enquiries', 'action' => 'payresponsehandler'));

Router::connect('/contact/:ref_page-:id', array('controller' => 'enquiries', 'action' => 'get_lead_info_form'),
        array('pass'=>array('id','ref_page','main_cat'),'id' => '[0-9]+'));

// code starts here-VG-11/08/2016
Router::connect('/contact/:main_cat/:ref_page-:id', array('controller' => 'enquiries', 'action' => 'get_lead_info_form'),
        array('pass'=>array('id','ref_page','main_cat'),'id' => '[0-9]+'));
// code ends here-VG-11/08/2016

Router::connect('/insights/:section', array('controller' => 'articles', 'action' => 'article_listing'),
        array('pass'=>array('section')));

Router::connect('/insights/:folder/:url_slug/:article_id', array('controller' => 'articles', 'action' => 'article_details'),
        array('pass'=>array('folder','article_id','url_slug'),'article_id'=>'[0-9]+'));

/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
Router::parseExtensions('html','rss');
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
