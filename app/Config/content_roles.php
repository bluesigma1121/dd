<?php

$config = array(
    'articles' => array(
        1 => 'index',
        2 => 'add',
        3 => 'edit',
        4 => 'view'       
    ),
    'users' => array(
        6 => 'change_password',
    )
);
$config['roles'] = $config;
?>
