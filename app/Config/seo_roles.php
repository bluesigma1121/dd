<?php

$config = array(
    'products' => array(
        1 => 'seo_index',
        2 => 'view',
        3 => 'seo_edit',
        4 => 'add',
        5 => 'view',
        6 => 'add_category',
        8 => 'pending_status',
        9 => 'index',
        10 => 'edit',
        11 => 'edit_category',
        12 => 'delete'        
    ),
    'users' => array(
        6 => 'change_password',
    ),
    'categories' => array(
        0 => 'tree_view_categories',
        1 => 'edit_tree_category',
        2 => 'tree_view_sub_categories',
        3 => 'display_active'
    ),
    'specifications'=> array(
        0 => 'add_product_spc'
    )
);
$config['roles'] = $config;
?>
